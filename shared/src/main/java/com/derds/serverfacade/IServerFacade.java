package com.derds.serverfacade;

import com.derds.gamecommand.AbstractGameCommand;
import com.derds.gamecommand.AbstractSyncGameCommand;
import com.derds.models.DestinationCard;
import com.derds.models.Message;
import com.derds.models.Player;
import com.derds.models.PlayerColor;
import com.derds.models.River;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.result.*;

import java.util.List;

public interface IServerFacade {
    ////////////////
    // commands
    ////////////////
    LoginResult login(String username, String password, String firebaseToken);

    LoginResult register(String username, String password, String firstName, String lastName, String firebaseToken);

    GameResult startGame(String authToken, String gameID);

    GameResult joinGame(String authToken, String gameID, PlayerColor playerColor);

    GameResult createGame(String authToken, String gameName, PlayerColor playerColor);

    Result deleteGame(String authToken, String gameID);

    RefreshLobbyResult refreshLobby(String authToken);

    Result updateFirebaseToken(String authToken, String firebaseToken);

    Result getQueueHistory(String authToken, int lastHistoryId, String playerId, String gameId);

    ////////////////
    // game commands
    ////////////////
    public Result chooseDestination(String authToken, String playerId, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned);

    public Result claimRoute(String authToken, String playerId, Route route, List<TrainCard> cardsUsed, int trainPiecesUsed);

    public Result drawDestination(String authToken, String playerId, List<DestinationCard> cardsToPickFrom, boolean areCardsDrawn);

    public Result getChatHistory(String authToken, String playerId, int lastCommandId,  List<Message> messageHistory, int skip);

    public Result initGame(String authToken, String playerId, int turnOrder, Player player);

    public Result pickFaceupCard(String authToken, String playerId, TrainCard cardDrawn);

    public Result requestDrawDestination(String authToken, String playerId);

    public Result requestDrawTrain(String authToken, String playerId);

    public Result sendChat(String authToken, Message content);

    public Result updatePoints(String authToken, String playerId, int newPointsTotal, String playerIdToUpdate);

    public Result updateRiver(String authToken, String playerId, River newRiver);

    public Result syncGame(String authToken, String playerId, String gameId);

    public AbstractSyncGameCommand getSyncData(String playerId);

}
