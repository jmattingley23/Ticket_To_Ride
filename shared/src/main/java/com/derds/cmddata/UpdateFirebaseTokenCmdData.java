package com.derds.cmddata;

public class UpdateFirebaseTokenCmdData implements ICmdData {
    private String firebaseToken;

    public UpdateFirebaseTokenCmdData(String firebaseToken) {
        this.firebaseToken = firebaseToken;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }
}