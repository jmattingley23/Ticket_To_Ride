package com.derds.cmddata;

import com.derds.models.PlayerColor;

public class CreateGameCmdData implements ICmdData {
    private String gameName;
    private PlayerColor playerColor;

    public CreateGameCmdData(String gameName, PlayerColor playerColor) {
        this.gameName = gameName;
        this.playerColor = playerColor;
    }

    public String getGameName() {
        return gameName;
    }

    public PlayerColor getPlayerColor() {
        return playerColor;
    }
}
