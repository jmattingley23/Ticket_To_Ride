package com.derds.cmddata;

/**
 * Created by drc95 on 10/30/17.
 */

public class RequestQueueCmdData implements ICmdData {
    int lastHistoryId;
    String playerId;
    String gameId;

    public RequestQueueCmdData(int lastHistoryId, String playerId, String gameId) {
        this.lastHistoryId = lastHistoryId;
        this.playerId = playerId;
        this.gameId = gameId;
    }
}
