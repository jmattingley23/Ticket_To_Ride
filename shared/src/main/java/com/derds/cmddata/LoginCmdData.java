package com.derds.cmddata;

public class LoginCmdData implements ICmdData {
    private String username;
    private String password;
    private String firebaseToken;

    public LoginCmdData(String username, String password, String firebaseToken) {
        this.username = username;
        this.password = password;
        this.firebaseToken = firebaseToken;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirbaseToken() {
        return firebaseToken;
    }
}
