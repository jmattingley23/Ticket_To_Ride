package com.derds.cmddata;

public class StartGameCmdData implements ICmdData {
    private String gameID;

    public StartGameCmdData(String gameID) {
        this.gameID = gameID;
    }

    public String getGameID() {
        return gameID;
    }
}
