package com.derds.cmddata;

public class DeleteGameCmdData implements ICmdData {
    private String gameID;

    public DeleteGameCmdData(String gameID) {
        this.gameID = gameID;
    }

    public String getGameID() {
        return gameID;
    }
}
