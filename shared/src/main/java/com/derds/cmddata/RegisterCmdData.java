package com.derds.cmddata;

public class RegisterCmdData extends LoginCmdData {
    private String firstName;
    private String lastName;

    public RegisterCmdData(String username, String password, String firstName, String lastName, String firebaseToken) {
        super(username, password, firebaseToken);
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
}
