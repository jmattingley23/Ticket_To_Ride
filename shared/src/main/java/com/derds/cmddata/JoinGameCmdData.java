package com.derds.cmddata;

import com.derds.models.PlayerColor;

public class JoinGameCmdData implements ICmdData {
    private String gameID;
    private PlayerColor playerColor;

    public JoinGameCmdData(String gameID, PlayerColor playerColor) {
        this.gameID = gameID;
        this.playerColor = playerColor;
    }

    public String getGameID() {
        return gameID;
    }

    public PlayerColor getPlayerColor() {
        return playerColor;
    }
}
