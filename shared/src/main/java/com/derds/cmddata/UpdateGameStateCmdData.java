package com.derds.cmddata;

import com.derds.models.Player;

/**
 * Created by drc95 on 10/19/17.
 */

public class UpdateGameStateCmdData implements ICmdData {
    Player curPlayer;
    int curCommandSequenceId;   // this tells us to what point this information is up to date
}
