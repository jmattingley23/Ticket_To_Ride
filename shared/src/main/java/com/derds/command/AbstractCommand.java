package com.derds.command;

import com.derds.result.Result;

public abstract class AbstractCommand {
    private String authToken;
    public abstract Result execute();

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }
}
