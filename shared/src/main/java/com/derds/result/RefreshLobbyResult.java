package com.derds.result;

import com.derds.models.LobbyGameMetadata;

import java.util.List;

public class RefreshLobbyResult extends Result {
    private List<LobbyGameMetadata> openGames;
    private List<LobbyGameMetadata> currentGames;
    private List<LobbyGameMetadata> playerGames;

    public RefreshLobbyResult(String message) {
        super(message);
    }

    public RefreshLobbyResult(List<LobbyGameMetadata> openGames, List<LobbyGameMetadata> currentGames, List<LobbyGameMetadata> playerGames) {
        this.openGames = openGames;
        this.currentGames = currentGames;
        this.playerGames = playerGames;
    }

    public List<LobbyGameMetadata> getOpenGames() {
        return openGames;
    }

    public List<LobbyGameMetadata> getCurrentGames() {
        return currentGames;
    }

    public List<LobbyGameMetadata> getPlayerGames() {
        return playerGames;
    }
}
