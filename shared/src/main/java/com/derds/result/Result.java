package com.derds.result;

public class Result {
    private String message;

    public Result() {}

    public Result(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
