package com.derds.result;

public class LoginResult extends Result {

    private String authToken;
    private String username;

    public LoginResult(String authToken, String username) {
        super(null);
        this.authToken = authToken;
        this.username = username;
    }

    public LoginResult(String message) {
        super(message);
    }

    public String getAuthToken() {
        return authToken;
    }

    public String getUsername() {
        return username;
    }
}
