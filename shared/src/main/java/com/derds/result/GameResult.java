package com.derds.result;

public class GameResult extends Result {
    private String gameID;

    public GameResult(String message) {
        super(message);
    }

    public GameResult(String message, String gameID) {
        super(message);
        this.gameID = gameID;
    }

    public String getGameID() {
        return gameID;
    }
}
