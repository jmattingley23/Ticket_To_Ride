package com.derds.models;

public enum TrainColor {
    PINK (0xFFE91E63,"Pink"),
    WHITE (0xFFFFFFFF,"White"),
    BLUE (0xFF2196F3,"Blue"),
    YELLOW (0xFFFFFF00,"Yellow"),
    ORANGE (0xFFFF9800,"Orange"),
    PURPLE (0xFF9C27B0,"Purple"),
    RED (0xFFF44336,"Red"),
    GREEN (0xFF76FF03,"Green"),
    RAINBOW (0xFF000000,"Rainbow"),
    COLORLESS (0xFF9E9E9E,"Colorless");

    private final int hexVal;
    private String description;

    TrainColor(int hexVal,String description){
        this.hexVal = hexVal;
        this.description = description;
    }

    public int getHexVal(){
        return hexVal;
    }

    public String getDescription(){return description;}
}
