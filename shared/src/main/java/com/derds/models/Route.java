package com.derds.models;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class Route {
    private int length;
    private TrainColor color;
    private PlayerColor claimedColor;
    private boolean claimed;
    private String owner;
    private CityPair cities;
    private PointPair coordinates;

    public Route(CityPair cities, PointPair coordinates) {
        this.cities = cities;
        this.coordinates = coordinates;
    }

    public Route(int length, TrainColor color, CityPair cities, PointPair coordinates) {
        this.length = length;
        this.color = color;
        this.cities = cities;
        this.coordinates = coordinates;
        this.owner = null;
    }

    // Copy constructor
    public Route(Route otherRoute) {
        this.length = otherRoute.getLength();
        this.color = otherRoute.getColor();
        this.cities = otherRoute.getCities();
        this.coordinates = otherRoute.getCoordinates();
        this.claimed = otherRoute.isClaimed();
        this.owner = otherRoute.getOwner();
    }

    public void clearCities() {
        cities = null;
    }

    public boolean isClaimPossible(List<TrainCard> playerCards){
        int count = 0;
        HashMap<TrainColor,Integer> cardCount = new HashMap<>();

        for(TrainCard card : playerCards){
            if(color == TrainColor.COLORLESS){
                if(!cardCount.containsKey(card.getColor())){
                    cardCount.put(card.getColor(),0);
                }
                cardCount.put(card.getColor(),cardCount.get(card.getColor()) + 1);
            }
            else{
                if(card.getColor() == color || card.getColor() == TrainColor.RAINBOW) {
                    count++;
                }
            }
        }

        if(color == TrainColor.COLORLESS){
            Collection<Integer> values = cardCount.values();
            Integer largest = 0;
            Integer prevLargest = 0;
            for(Integer num : values){
                if(num >= largest){
                    prevLargest = largest;
                    largest = num;
                }
            }

            if(cardCount.containsKey(TrainColor.RAINBOW) && largest.equals(cardCount.get(TrainColor.RAINBOW))){
                return ((largest + prevLargest) >= length);
            }
            else if(cardCount.containsKey(TrainColor.RAINBOW)){
                return ((largest + cardCount.get(TrainColor.RAINBOW)) >= length);
            }
            else{
                return (largest >= length);
            }
        }
        else{
            return (count >= length);
        }
    }

    public boolean canClaim(List<TrainCard> selectedCards){
        if(selectedCards.size() != length){return false;}
        TrainColor color = null;

        for(TrainCard card : selectedCards){
            if(color == null && card.getColor() != TrainColor.RAINBOW){
                color = card.getColor();
            }
            else{
                if(card.getColor() != color && card.getColor() != TrainColor.RAINBOW){
                    return false;
                }
            }
        }

        int count = 0;

        for(TrainCard card : selectedCards){
            if(card.getColor() == color || card.getColor() == TrainColor.RAINBOW || color == TrainColor.COLORLESS){
                count++;
            }
            else{
                return false;
            }
        }
        return (count == length);
    }

    public void claim(String playerId, PlayerColor color){
        this.owner = playerId;
        claimed = true;
        claimedColor = color;
    }

    public int getDrawColor(){
        if(claimed){
            return claimedColor.getHexVal();
        }
        return color.getHexVal();
    }

    public PointPair getCoordinates() {
        return coordinates;
    }

    public MapPoint getMidPoint(){
        float x = (float) ((coordinates.p1.x + coordinates.p2.x)/2.0);
        float y = (float)((coordinates.p1.y + coordinates.p2.y)/2.0);
        return new MapPoint(x,y);
    }

    public double findDistance(MapPoint point){
        final double xDelta = coordinates.p2.x - coordinates.p1.x;
        final double yDelta = coordinates.p2.y - coordinates.p1.y;
        final double u = ((point.x - coordinates.p1.x) * xDelta + (point.y - coordinates.p1.y) * yDelta) / (xDelta * xDelta + yDelta * yDelta);

        final MapPoint closestPoint;
        if (u < 0) {
            closestPoint = coordinates.p1;
        } else if (u > 1) {
            closestPoint = coordinates.p2;
        } else {
            closestPoint = new MapPoint((float)(coordinates.p1.x + u * xDelta), (float)(coordinates.p1.y + u * yDelta));
        }

        return closestPoint.distance(point);
    }

    public String getDescription(){
        StringBuilder sb = new StringBuilder();
        sb.append(cities.getCity1().getName() + " to " + cities.getCity2().getName() + " "); //+ "\n");
        sb.append("Color: " + color.getDescription() + " " );//+ "\n");
        sb.append("Cost: " + Integer.toString(length) + " ");
        return sb.toString();
    }

    public int getPoints() {
        switch(length) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 3:
                return 4;
            case 4:
                return 7;
            case 5:
                return 10;
            case 6:
                return 15;
            default:
                return -200000;
            // TODO should we throw something here???
        }
    }


    public int getLength() {
        return length;
    }

    public TrainColor getColor() {
        return color;
    }

    public boolean isClaimed() {
        return claimed;
    }

    public String getOwner() {
        return owner;
    }

    public CityPair getCities() {
        return cities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Route route = (Route) o;

        if (getLength() != route.getLength()) return false;
        if (getColor() != route.getColor()) return false;
        return getCoordinates() != null ? getCoordinates().equals(route.getCoordinates()) : route.getCoordinates() == null;

    }

    @Override
    public int hashCode() {
        int result = getLength();
        result = 31 * result + getColor().hashCode();
        result = 31 * result + (getCoordinates() != null ? getCoordinates().hashCode() : 0);
        return result;
    }
}
