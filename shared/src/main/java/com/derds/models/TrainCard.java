package com.derds.models;

import java.awt.Color;

public class TrainCard implements ICard, Comparable<TrainCard>{
    private TrainColor color;

    public TrainCard(TrainColor color) {
        this.color = color;
    }

    public TrainColor getColor() {
        return color;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TrainCard trainCard = (TrainCard) o;

        return getColor() == trainCard.getColor();

    }

    @Override
    public int hashCode() {
        return getColor().hashCode();
    }

    @Override
    public int compareTo(TrainCard otherCard){
        if(this.getColor().getHexVal() > otherCard.getColor().getHexVal()){
            return 1;
        }else if(this.getColor().getHexVal() == otherCard.getColor().getHexVal()){
            return 0;
        }else {
            return -1;
        }
    }
}
