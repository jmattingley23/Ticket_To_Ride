package com.derds.models;

public class User {
    private String username;
    private String firstname;
    private String lastname;
    private String userId;

    // TODO pre and post conditions

    public User(String username, String firstname, String lastname, String userId) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        if (!getUsername().equals(user.getUsername())) return false;
        if (!getFirstname().equals(user.getFirstname())) return false;
        if (!getLastname().equals(user.getLastname())) return false;
        return getUserId().equals(user.getUserId());

    }

    @Override
    public int hashCode() {
        int result = getUsername().hashCode();
        result = 31 * result + getFirstname().hashCode();
        result = 31 * result + getLastname().hashCode();
        result = 31 * result + getUserId().hashCode();
        return result;
    }
}

