package com.derds.models;

/**
 * Created by mogard on 10/21/2017.
 */

public class MapPoint {
    public float x;
    public float y;

    public MapPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public MapPoint(){}

    public double distance(MapPoint point){
        return Math.hypot(x-point.x, y-point.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MapPoint mapPoint = (MapPoint) o;

        if (Float.compare(mapPoint.x, x) != 0) return false;
        return Float.compare(mapPoint.y, y) == 0;

    }

    @Override
    public int hashCode() {
        int result = (x != +0.0f ? Float.floatToIntBits(x) : 0);
        result = 31 * result + (y != +0.0f ? Float.floatToIntBits(y) : 0);
        return result;
    }
}
