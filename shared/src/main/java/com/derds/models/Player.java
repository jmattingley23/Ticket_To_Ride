package com.derds.models;

import com.derds.turnstate.TurnState;

import org.jgrapht.EdgeFactory;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.WeightedGraph;
import org.jgrapht.alg.interfaces.ShortestPathAlgorithm;
import org.jgrapht.alg.shortestpath.FloydWarshallShortestPaths;
import org.jgrapht.graph.ClassBasedEdgeFactory;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.WeightedPseudograph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class Player {
    private User myUser;
    private String playerId;
    private String gameId;
    private Hand hand;
    private PlayerColor playerColor;
    private int myTurn; // which turn is mine
    private TurnState.StateName turnState;
    private GameMetadata gameData;
    private List<Route> claimedRoutes;
    // TODO pre and post conditions


    public Player(User myUser, String gameId, PlayerColor playerColor, int myTurn, TurnState.StateName turnState) {
        this.myUser = myUser;
        this.playerId = UUID.randomUUID().toString().replace("-", "");
        this.gameId = gameId;
        this.playerColor = playerColor;
        this.myTurn = myTurn;
        this.turnState = turnState;
        gameData = null;
        hand = new Hand();
        claimedRoutes = new ArrayList<>();
    }

    public User getMyUser() {
        return myUser;
    }

    public String getPlayerId() {
        return playerId;
    }

    public TurnState.StateName getTurnState() {
        return turnState;
    }

    public void setTurnState(TurnState.StateName turnState) {
        this.turnState = turnState;
    }

    public List<TrainCard> getCards(TrainColor color, int number){
        return hand.getCards(color,number);
    }

    public void addClaimedRoute(Route r){
        claimedRoutes.add(r);
    }

    private Graph makeRouteGraph(){
        WeightedPseudograph<City,DefaultWeightedEdge> graph = new WeightedPseudograph<City,DefaultWeightedEdge>(DefaultWeightedEdge.class);
        Map<City,String> added = new HashMap<>();
        for(Route route : claimedRoutes){
            City city1 = route.getCities().getCity1();
            City city2 = route.getCities().getCity2();
            if(!added.containsKey(city1)){
                graph.addVertex(route.getCities().getCity1());
                added.put(city1,"");
            }
            if(!added.containsKey(city2)){
                graph.addVertex(route.getCities().getCity2());
                added.put(city2,"");
            }
            DefaultWeightedEdge defaultEdge = new DefaultWeightedEdge();

            defaultEdge = graph.addEdge(city1,city2);
            graph.setEdgeWeight(defaultEdge,-route.getLength());

        }

        return graph;
    }

    private Set<City> getClaimedRouteCities(){
        Map<City,String> added = new HashMap<>();
        for(Route route : claimedRoutes){
            City city1 = route.getCities().getCity1();
            City city2 = route.getCities().getCity2();
            if(!added.containsKey(city1)){
                added.put(city1,"");
            }
            if(!added.containsKey(city2)){
                added.put(city2,"");
            }
        }
        return added.keySet();
    }

    private int calculateRoutePoints(PlayerStats playerStats){
        FloydWarshallShortestPaths floydWarsh = new FloydWarshallShortestPaths(makeRouteGraph());

        double shortestLength = Double.POSITIVE_INFINITY;
        Map<City,String> executed = new HashMap<>();
        Set<City> cities = getClaimedRouteCities();
        List<DestinationCard> destCards = hand.getPlayerDestinationCards();
        int completedPoints = 0;
        int incompletedPoints = 0;
        double weight = 0;
//-29, -13 13
        //Compare every pair of cities
        for(City city1: cities) {
            //Get the shortest paths from city1 to every other city
            ShortestPathAlgorithm.SingleSourcePaths paths = floydWarsh.getPaths(city1);
            for(City city2 : cities) {

                //If the path from city1 to city2 is shorter, update the shortest length
                weight = paths.getWeight(city2);
                if(weight < shortestLength) {
                    shortestLength = weight;
                }

                for (DestinationCard card : destCards) {
                    //If the two cities represent a destination card the player has, see if a path exists between them
                    if (card.isDestination(city1, city2) && !card.isEvaluated()) {
                        //Update completed/uncompleted points accordingly
                        if (!Double.isInfinite(weight)) {
                            completedPoints += card.getPointValue();
                            card.setEvaluated(true);
                        }
                    }
                }

            }
        }

        for(DestinationCard card : destCards){
            if(!card.isEvaluated()){
                incompletedPoints -= card.getPointValue();
            }
        }

        playerStats.setCompleteDestScore(completedPoints);
        playerStats.setIncompleteDestScore(incompletedPoints);

        return Math.abs((int)shortestLength);
    }

    public PlayerStats getPlayerStats(){
        PlayerStats playerStats = new PlayerStats();
        playerStats.setPlayerId(playerId);
        playerStats.setPlayerName(gameData.getPlayerNameFromId(playerId));
        playerStats.setPlayerColor(gameData.getColorByPlayerId(playerId));
        playerStats.setNumDestCards(gameData.getNumDestCardsLeft(playerId));
        playerStats.setShipPiecesLeft(gameData.getNumPiecesLeft(playerId));
        playerStats.setRouteScore(gameData.getPlayerPoints(playerId));
        playerStats.setLongestRoute(calculateRoutePoints(playerStats));
        return playerStats;
    }


    public boolean canClaimRoute(TrainColor color, int cost){
        return hand.canClaimRoute(color,cost);
    }


    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public GameMetadata getMyGame() {
        return gameData;
    }

    public String getGameId() {
        return gameId;
    }

    public void setHand(Hand hand) {
        this.hand = hand;
    }

    public void addCardToHand(TrainCard card) {
        hand.addTrainCard(card);
    }

    public void removeCardsFromHand(List<TrainCard> cards) {
        hand.playTrainCards(cards);
    }

    public int getTrainCardCount() {
        return hand.getTrainCardCount();
    }

    public int getDestinationCardCount() {
        return hand.getDestCardCount();
    }

    public int getMyTurn() {
        return myTurn;
    }

    public void setPlayerColor(PlayerColor playerColor) {
        this.playerColor = playerColor;
    }

    public PlayerColor getPlayerColor() {
        return playerColor;
    }

    public void setGameData(GameMetadata gameData) {
        this.gameData = gameData;
    }

    public Hand getHand() {
        return hand;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Player player = (Player) o;

        if (!getMyUser().equals(player.getMyUser())) return false;
        if (!getPlayerId().equals(player.getPlayerId())) return false;
        return getMyGame() != null ? getMyGame().equals(player.getMyGame()) : player.getMyGame() == null;

    }

    @Override
    public int hashCode() {
        int result = getMyUser().hashCode();
        result = 31 * result + getPlayerId().hashCode();
        result = 31 * result + (getMyGame() != null ? getMyGame().hashCode() : 0);
        return result;
    }
}
