package com.derds.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


public class Hand {
    private List<TrainCard> playerTrainCards;
    private List<DestinationCard> unfinishedDestinationCards;
    private List<DestinationCard> finishedDestinationCards;

    public Hand() {
        playerTrainCards = new ArrayList<>();
        unfinishedDestinationCards = new ArrayList<>();
        finishedDestinationCards = new ArrayList<>();
    }

    public void clear() {
        playerTrainCards = new ArrayList<>();
        unfinishedDestinationCards = new ArrayList<>();
        finishedDestinationCards = new ArrayList<>();
    }

    public void playTrainCards(List<TrainCard> cards) {
        for(TrainCard card : cards) {
            playerTrainCards.remove(card);  // TODO just use removeall?
        }
    }

    public int countTrainColor(TrainColor color) {
        int retVal = 0;
        for (TrainCard card : playerTrainCards) {
            if(card.getColor() == color) {
                retVal++;
            }
        }
        return retVal;
    }

    public List<TrainCard> getCards(TrainColor color, int number){
        ArrayList<TrainCard> cards = new ArrayList<>();
        int count = 0;
        for(TrainCard card : playerTrainCards){
            if(card.getColor() == color){
                cards.add(card);
                count++;
                if(count == number){
                    return cards;
                }
            }
        }
        return null;
    }


    public boolean canClaimRoute(TrainColor color, int cost){
        return (countTrainColor(color) >= cost);
    }


    public Map<TrainColor, Integer> countTrains() {
        return null;
    }

    public boolean updateFinishedCards(List<Route> routes) {
        return false;
    }

    public void addDestinationCard(DestinationCard card) {
        unfinishedDestinationCards.add(card);
    }

    public int getPoints() {
        return 0;
    }

    public List<TrainCard> getPlayerTrainCards() {
        return playerTrainCards;
    }

    public List<DestinationCard> removeDestinationCards(List<DestinationCard> remove){
        List<DestinationCard> removed = new ArrayList<>();
        for(DestinationCard card : remove){
            for(DestinationCard inHand : unfinishedDestinationCards){
                if(card.equals(inHand)){
                    removed.add(inHand);

                }
            }
        }
        unfinishedDestinationCards.removeAll(removed);
        return removed;
    }

    public DestinationCard removeDestinationCard(DestinationCard card){
        if(unfinishedDestinationCards.contains(card)){
            unfinishedDestinationCards.remove(card);
        }
        if(finishedDestinationCards.contains(card)){
            finishedDestinationCards.remove(card);
        }
        return card;
    }

    public int getTrainCardCount() {
        return playerTrainCards.size();
    }

    public int getDestCardCount() {
        return finishedDestinationCards.size() + unfinishedDestinationCards.size();
    }

    public List<DestinationCard> getPlayerDestinationCards(){
        ArrayList<DestinationCard> all = new ArrayList<>();
        all.addAll(unfinishedDestinationCards);
        //all.addAll(finishedDestinationCards);
        return all;
    }


    public void addTrainCard(TrainCard card) {
        playerTrainCards.add(card);
    }
}
