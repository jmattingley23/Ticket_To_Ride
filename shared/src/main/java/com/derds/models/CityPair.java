package com.derds.models;

/**
 * Created by mogard on 10/21/2017.
 */

public class CityPair {
    private City city1;
    private City city2;

    public CityPair(City city1, City city2) {
        this.city1 = city1;
        this.city2 = city2;
    }

    public City getCity1() {
        return city1;
    }

    public void setCity1(City city1) {
        this.city1 = city1;
    }

    public City getCity2() {
        return city2;
    }

    public void setCity2(City city2) {
        this.city2 = city2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CityPair cityPair = (CityPair) o;

        if (city1 != null ? !city1.equals(cityPair.city1) : cityPair.city1 != null) return false;
        return city2 != null ? city2.equals(cityPair.city2) : cityPair.city2 == null;

    }

    @Override
    public int hashCode() {
        int result = city1 != null ? city1.hashCode() : 0;
        result = 31 * result + (city2 != null ? city2.hashCode() : 0);
        return result;
    }
}
