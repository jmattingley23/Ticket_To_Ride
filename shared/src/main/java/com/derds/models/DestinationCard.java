package com.derds.models;

import java.util.AbstractMap;
import java.util.Map;

public class DestinationCard implements ICard{
    private int pointValue;
    private CityPair cities;
    private boolean evaluated;

    public DestinationCard(int pointValue, City cityA, City cityB) {
        this.pointValue = pointValue;
        this.cities = new CityPair(cityA, cityB);
        this.evaluated = false;
    }

    public boolean isEvaluated() {
        return evaluated;
    }

    public void setEvaluated(boolean completed) {
        this.evaluated = completed;
    }

    public CityPair getCities(){
        return cities;
    }

    public int getPointValue(){
        return pointValue;
    }

    public boolean isDestination(City city1, City city2){
        return ((cities.getCity1().equals(city1) && cities.getCity2().equals(city2))
                || (cities.getCity2().equals(city1) && cities.getCity1().equals(city2)));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DestinationCard that = (DestinationCard) o;

        if (pointValue != that.pointValue) return false;
        return cities != null ? cities.equals(that.cities) : that.cities == null;

    }

    @Override
    public int hashCode() {
        int result = pointValue;
        result = 31 * result + (cities != null ? cities.hashCode() : 0);
        return result;
    }
}
