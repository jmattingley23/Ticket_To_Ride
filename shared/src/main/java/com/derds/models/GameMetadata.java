package com.derds.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by drc95 on 10/21/17.
 */

public class GameMetadata {

    private String gameName;
    private String gameId;
    private String ownerUserId;
    private Map<String, Integer> playerTurnOrders;

    private List<String> playerIds;
    private Map<String, String> playerNames; //playerId to name
    private Map<String, PlayerColor> playerColors; //playerId to color
    private Map<String, Integer> playerPiecesLeft; //playerId to pieces
    private Map<String, Integer> points; //playerId to points
    private Map<String, Integer> playerTrainCards; //playerId to train cards
    private Map<String, Integer> playerDestinationCards; //playerId to destination cards

    private int currentTurn;   // who's currentTurn is it
    private String longestRouteOwner;   // player id of longest route owner, null if not owned by anyone

    private int discardCount; // number of cards in discard pile
    private int trainDeckCount;   // number of cards in deck
    private int destinationDeckCount;
    private River river;

    public GameMetadata(String gameId, String ownerUserId, int discardCount, int trainDeckCount, int destinationDeckCount, String gameName) {
        this.gameId = gameId;
        this.ownerUserId = ownerUserId;
        this.discardCount = discardCount;
        this.trainDeckCount = trainDeckCount;
        this.destinationDeckCount = destinationDeckCount;
        this.gameName = gameName;

        playerIds = new ArrayList<>();
        playerNames = new HashMap<>();
        playerColors = new HashMap<>();
        playerPiecesLeft = new HashMap<>();
        points = new HashMap<>();
        playerTrainCards = new HashMap<>();
        playerDestinationCards = new HashMap<>();
        playerTurnOrders = new HashMap<>();

        this.currentTurn = 0;
        this.river = new River();
    }

    public void addPlayer(Player player) {
        player.setGameData(this);
        playerIds.add(player.getPlayerId());
        playerNames.put(player.getPlayerId(), player.getMyUser().getFirstname());
        playerColors.put(player.getPlayerId(), player.getPlayerColor());
        playerPiecesLeft.put(player.getPlayerId(), 45);
        points.put(player.getPlayerId(), 0);
        playerTrainCards.put(player.getPlayerId(), player.getTrainCardCount());
        playerDestinationCards.put(player.getPlayerId(), player.getDestinationCardCount());
        playerTurnOrders.put(player.getPlayerId(), player.getMyTurn());
    }

    public void addCardsToRiver(List<TrainCard> cards) {
        river.fillRiver(cards);
    }

    public void setDestinationDeckCount(int destinationDeckCount) {
        this.destinationDeckCount = destinationDeckCount;
    }

    public String getGameId() {
        return gameId;
    }

    public void decreaseDestinationCount(int amount) {
        destinationDeckCount -= amount;
    }

    public void decreaseTrainDeckCount(int amount) {
        trainDeckCount -= amount;
    }

    public Map<String, PlayerColor> getPlayerColors() {
        return playerColors;
    }

    public String getPlayerNameFromId(String playerId) {
        return playerNames.get(playerId);
    }

    public int getNumPiecesLeft(String playerId){
        return playerPiecesLeft.get(playerId);
    }

    public int getNumDestCardsLeft(String playerId){
        return playerDestinationCards.get(playerId);
    }

    public int getNumTrainCardsLeft(String playerId){
        return playerTrainCards.get(playerId);
    }

    public int getPlayerPoints(String playerId){
        return points.get(playerId);
    }

    public Boolean ownsLongestRoute(String playerId){
        return playerId.equals(longestRouteOwner);
    }

    public PlayerColor getColorByPlayerId(String playerId){
        return playerColors.get(playerId);
    }

    public int getTrainDeckCount(){
        return trainDeckCount;
    }

    public int getDestinationDeckCount(){
        return destinationDeckCount;
    }

    public void reduceTrainPieces(String playerId, int numPieces) {
        playerPiecesLeft.put(playerId, playerPiecesLeft.get(playerId) - numPieces);
    }

    public void setNumTrainPieces(String playerId, int numPieces){
        playerPiecesLeft.put(playerId, numPieces);
    }

    public void addToScore(String playerId, int numPieces) {
        points.put(playerId, points.get(playerId) + numPieces);
    }

    public void updateScore(String playerId, int newTotal) {
        points.put(playerId, newTotal);
    }

    public River getRiver() {
        return river;
    }

    public void setRiver(River river) {
        this.river = river;
    }

    public void removeTrainCards(String playerId, int numCards) {
        playerTrainCards.put(playerId, playerTrainCards.get(playerId) - numCards);
    }

    public void addTrainCards(String playerId, int numCards){
        playerTrainCards.put(playerId,playerTrainCards.get(playerId) + numCards);
    }

    public void setTrainCardsForPlayer(String playerId, int numCards){
        playerTrainCards.put(playerId, numCards);
    }

    public void removeDestinationCards(String playerId, int numCards) {
        playerDestinationCards.put(playerId, playerDestinationCards.get(playerId) - numCards);
    }

    public void addDestinationCards(String playerId, int numCards){
        playerDestinationCards.put(playerId,playerDestinationCards.get(playerId) + numCards);
    }

    public Map<String, Integer> getPlayerTurnOrders() {
        return playerTurnOrders;
    }

    public String getOwnerUserId() {
        return ownerUserId;
    }

    public String getGameName() {
        return gameName;
    }

    // TODO generate getters and setters as needed


    public boolean isPlayersTurn(String playerId) {
        return playerTurnOrders.get(playerId) == currentTurn;
    }

    public List<String> getPlayerIds() {
        return playerIds;
    }

    public int nextTurn() {
        currentTurn = (currentTurn + 1) % playerIds.size();
        return currentTurn;
    }

    public int getCurrentTurn() {
        return currentTurn;
    }

    public void setTurn(int whoseTurn) {
        currentTurn = whoseTurn % playerIds.size();
    }

    public Map<String, Integer> getPlayerPiecesLeft() {
        return playerPiecesLeft;
    }

    public void setTrainDeckCount(int deck){
        this.trainDeckCount = deck;
    }

    public void initializeNumPlayerTrainCards(){
        for(String playerId : playerIds){
            playerTrainCards.put(playerId, 4);
        }
    }

}
