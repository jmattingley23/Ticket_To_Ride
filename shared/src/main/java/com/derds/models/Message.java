package com.derds.models;

public class Message {
    private String playerId;
    private long timeSent;
    private String message;

    public Message() {}

    public Message(String playerId, String message) {
        this.playerId = playerId;
        this.message = message;
    }

    public Message(String playerId, String message, long timeSent) {
        this.playerId = playerId;
        this.message = message;
        this.timeSent = timeSent;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public long getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(long dateSent) {
        this.timeSent = dateSent;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
