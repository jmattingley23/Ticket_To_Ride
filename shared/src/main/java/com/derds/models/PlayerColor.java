package com.derds.models;

public enum PlayerColor {
    RED (0xFFB71C1C),
    GREEN (0xFF4CAF50),
    BLUE (0xFF0D47A1),
    YELLOW (0xFFFFC107),
    CYAN(0xFF00BCD4);

    private final int hexVal;

    PlayerColor(int hexVal){
        this.hexVal = hexVal;
    }

    public int getHexVal(){
        return hexVal;
    }
}
