package com.derds.models;

import java.io.Serializable;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LobbyGameMetadata implements Serializable{
    private Map<PlayerColor, String> playerColorsNames;
    private Map<String, String> playerIds; //userId to playerId
    private boolean started;
    private String ownerUserId;
    private String gameId;
    private String name;
    private String myPlayerId;

    // TODO pre and post conditions

    public LobbyGameMetadata(List<Player> players, String ownerUserId, String gameId, String gameName, boolean started) {
        this.playerColorsNames = new HashMap<>();
        this.playerIds = new HashMap<>();
        this.ownerUserId = ownerUserId;
        this.gameId = gameId;
        this.name = gameName;
        this.started = started;
        for(Player player : players) {
            addPlayer(player);
        }
    }

    public String getGameName() {
        return name;
    }

    private void addPlayer(Player player) {
        playerColorsNames.put(player.getPlayerColor(), player.getMyUser().getFirstname());
        playerIds.put(player.getMyUser().getUserId(), player.getPlayerId());
    }

    public Map<PlayerColor, String> getPlayerColorsNames() {
        return playerColorsNames;
    }

    public boolean isStarted() {
        return started;
    }

    public String getGameId() {
        return gameId;
    }

    public boolean isColorTaken(PlayerColor playerColor) {
        for(Map.Entry<PlayerColor, String> player : playerColorsNames.entrySet()) {
            if(playerColor == player.getKey()) {
                return true;
            }
        }
        return false;
    }

    public void setMyPlayerId(String userId) {
        this.myPlayerId = playerIds.get(userId);
    }

    public String getMyPlayerId() {
        return myPlayerId;
    }
}
