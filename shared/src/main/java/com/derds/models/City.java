package com.derds.models;

public class City {
    private String name;
    private MapPoint nameCoordinates;

    public City(String name, MapPoint nameCoordinates) {
        this.name = name;
        this.nameCoordinates = nameCoordinates;
    }

    public City() {

    }

    public float getX(){
        return nameCoordinates.x;
    }

    public float getY(){
        return nameCoordinates.y;
    }

    public String getName() {
        return name;
    }

    public MapPoint getNameCoordinates() {
        return nameCoordinates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        City city = (City) o;

        return getName().equals(city.getName());

    }

    @Override
    public int hashCode() {
        return getName().hashCode();
    }
}
