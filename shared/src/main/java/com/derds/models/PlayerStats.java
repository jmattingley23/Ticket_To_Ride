package com.derds.models;

/**
 * Created by justinbrunner on 11/20/17.
 */

public class PlayerStats {
    private String playerId;
    private String playerName; // FULL NAME
    private PlayerColor playerColor;
    private int finalScore;
    private int routeScore;
    private int completeDestScore;
    private int incompleteDestScore;
    private int numDestCards;
    private int shipPiecesLeft;
    private int longestRoute;
    private boolean hadLongestRoute;


    public PlayerStats(){}

    public PlayerStats(String playerId, String playerName, PlayerColor playerColor, int finalScore, int routeScore, int completeDestScore, int incompleteDestScore, int numDestCards, int shipPiecesLeft) {
        this.playerId = playerId;
        this.playerName = playerName;
        this.playerColor = playerColor;
        this.finalScore = finalScore;
        this.routeScore = routeScore;
        this.completeDestScore = completeDestScore;
        this.incompleteDestScore = incompleteDestScore;
        this.numDestCards = numDestCards;
        this.shipPiecesLeft = shipPiecesLeft;
        this.longestRoute = 0;
        this.hadLongestRoute = false;
    }

    public boolean isHadLongestRoute() {
        return hadLongestRoute;
    }

    public void setHadLongestRoute(boolean hadLongestRoute) {
        this.hadLongestRoute = hadLongestRoute;
    }

    public int getLongestRoute() {
        return longestRoute;
    }

    public void setLongestRoute(int longestRoute) {
        this.longestRoute = longestRoute;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public PlayerColor getPlayerColor() {
        return playerColor;
    }

    public void setPlayerColor(PlayerColor playerColor) {
        this.playerColor = playerColor;
    }

    public int getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(int finalScore) {
        this.finalScore = finalScore;
    }

    public int getRouteScore() {
        return routeScore;
    }

    public int calculateFinalScore(){
        return routeScore + completeDestScore + incompleteDestScore;
    }

    public void setRouteScore(int routeScore) {
        this.routeScore = routeScore;
    }

    public int getCompleteDestScore() {
        return completeDestScore;
    }

    public void setCompleteDestScore(int completeDestScore) {
        this.completeDestScore = completeDestScore;
    }

    public int getIncompleteDestScore() {
        return incompleteDestScore;
    }

    public void setIncompleteDestScore(int incompleteDestScore) {
        this.incompleteDestScore = incompleteDestScore;
    }

    public int getNumDestCards() {
        return numDestCards;
    }

    public void setNumDestCards(int numDestCards) {
        this.numDestCards = numDestCards;
    }

    public int getShipPiecesLeft() {
        return shipPiecesLeft;
    }

    public void setShipPiecesLeft(int shipPiecesLeft) {
        this.shipPiecesLeft = shipPiecesLeft;
    }
}
