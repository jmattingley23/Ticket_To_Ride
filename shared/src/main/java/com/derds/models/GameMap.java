package com.derds.models;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by mogard on 10/28/2017.
 */

public class GameMap {
    public static final String ENDOR = "Endor";
    public static final String TAKODANA = "Takodana";
    public static final String DEATH_STAR_II = "Death Star II";
    public static final String POLIS_MASSA = "Polis Massa";
    public static final String DEATH_STAR = "Death Star";
    public static final String MUSTAFAR = "Mustafar";
    public static final String UTAPAU = "Utapau";
    public static final String BESPIN = "Bespin";
    public static final String HOTH = "Hoth";
    public static final String DAGOBAH = "Dagobah";
    public static final String SCARIF = "Scarif";
    public static final String GEONOSIS = "Geonosis";
    public static final String TATOOINE = "Tatooine";
    public static final String NABOO = "Naboo";
    public static final String JEDHA = "Jedha";
    public static final String AHCH_TO = "Ahch-To";
    public static final String JAKKU = "Jakku";
    public static final String MALASTARE = "Malastare";
    public static final String CATO_NEIMOIDIA = "Cato Neimoidia";
    public static final String KAMINO = "Kamino";
    public static final String KESSEL = "Kessel";
    public static final String KASHYYYK = "Kashyyyk";
    public static final String AMBRIA = "Ambria";
    public static final String ALDERAAN = "Alderaan";
    public static final String CORELLIA = "Corellia";
    public static final String CORUSCANT = "Coruscant";
    public static final String ORD_MANTELL = "Ord Mantell";
    public static final String MANDALORE = "Mandalore";
    public static final String KORRIBAN = "Korriban";
    public static final String LOTHAL = "Lothal";
    public static final String MON_CALA = "Mon Cala";
    public static final String YAVIN_IV = "Yavin IV";
    public static final String FELUCIA = "Felucia";
    public static final String DATHOMIR = "Dathomir";
    public static final String DANTOOINE = "Dantooine";
    public static final String MYGEETO = "Mygeeto";

    private List<Route> routes;
    private Map<String,City> cities;
    private static final int CLICK_TOLERANCE = 20;



    public GameMap(){
        routes = new ArrayList<>();
        cities = new HashMap<>();
        addCities();
        addRoutes();
    }

    private void addNewCity(String name, float x, float y){
        cities.put(name, new City(name, new MapPoint(x,y)));
    }


    private void addNewRoute(TrainColor color, int length, String city1,float x1,float y1,String city2, float x2, float y2){
        routes.add(new Route(length,color,new CityPair(cities.get(city1),cities.get(city2)),new PointPair(new MapPoint(x1,y1),new MapPoint(x2,y2))));
    }

    public Route getRouteClicked(MapPoint pointClicked){
        Route closestRoute = null;
        double closestDistance = Double.MAX_VALUE;
        for(Route route : routes){
            double distance = route.findDistance(pointClicked);
            if ((distance != -1) && (distance < closestDistance)){
                closestRoute = route;
                closestDistance = distance;
            }
        }
        if(closestDistance > CLICK_TOLERANCE){
            return null;
        }
        else{
            return closestRoute;
        }
    }

    private List<Route> getDoubleRoute(Route firstRoute){
        ArrayList<Route> doubleRoute = new ArrayList<>();
        for(Route route : routes){
            if(firstRoute.getCities().equals(route.getCities())){
                doubleRoute.add(route);
            }
        }
        return doubleRoute;
    }

    public boolean isDoubleValid(Route wantedRoute, int numPlayers, String playerId){
        List<Route> doubleRoute = getDoubleRoute(wantedRoute);

        if(doubleRoute.size() == 1){
            return true;
        }

        int playerOwned = 0;
        int numClaimed = 0;
        for(Route route : doubleRoute){
            if(route.getOwner() != null && route.getOwner().equals(playerId)){
                playerOwned++;
            }
            if(route.isClaimed()){
                numClaimed++;
            }
        }

        if(playerOwned > 0){
            return false;
        }

        if(numPlayers < 4 && numClaimed > 0){
            return false;
        }

        return true;

    }

    public void claimRoute(Route claimedRoute, String playerId, PlayerColor playerColor) {
        // TODO why do we call claim twice???? I think just the one in the for loop is necessary
        claimedRoute.claim(playerId,playerColor);
        for(Route route : routes){
            // TODO replace with route equals
            if(route.getCoordinates().equals(claimedRoute.getCoordinates())){
                route.claim(playerId,playerColor);
            }
        }
    }


    public void addCities(){
        addNewCity(ENDOR,97f,81f);
        addNewCity(DEATH_STAR_II,108f,150f);
        addNewCity(POLIS_MASSA,36f,191f);
        addNewCity(MUSTAFAR,63f,300f);
        addNewCity(UTAPAU,123f,416f);
        addNewCity(DAGOBAH,179f,366f);
        addNewCity(SCARIF,252f,413f);
        addNewCity(HOTH,262f,265f);
        addNewCity(BESPIN,317f,208f);
        addNewCity(TAKODANA,216f,104f);
        addNewCity(GEONOSIS,349f,445f);
        addNewCity(TATOOINE,357f,378f);
        addNewCity(NABOO,380f,306f);
        addNewCity(JEDHA,442f,78f);
        addNewCity(AHCH_TO,463f,205f);
        addNewCity(JAKKU,510f,274f);
        addNewCity(MALASTARE,513f,360f);
        addNewCity(CATO_NEIMOIDIA,498f,433f);
        addNewCity(KAMINO,570f,501f);
        addNewCity(KESSEL,682f,460f);
        addNewCity(KASHYYYK,609f,360f);
        addNewCity(AMBRIA,633f,287f);
        addNewCity(CORELLIA,572f,141f);
        addNewCity(CORUSCANT,664f,119f);
        addNewCity(ALDERAAN,676f,226f);
        addNewCity(DEATH_STAR,716f,311f);
        addNewCity(KORRIBAN,778f,367f);
        addNewCity(LOTHAL,840f,483f);
        addNewCity(MON_CALA,866f,376f);
        addNewCity(YAVIN_IV,833f,301f);
        addNewCity(FELUCIA,908f,262f);
        addNewCity(MANDALORE,790f,213f);
        addNewCity(ORD_MANTELL,768f,126f);
        addNewCity(DATHOMIR,879f,167f);
        addNewCity(MYGEETO,857f,86f);
        addNewCity(DANTOOINE,928f,119f);
    }

    public void addRoutes(){
        //Bespin
        addNewRoute(TrainColor.YELLOW,6,DEATH_STAR_II,175f,155f,BESPIN,312f,195f);
        addNewRoute(TrainColor.PINK,3,HOTH,294f,242f,BESPIN,318f,220f);
        addNewRoute(TrainColor.COLORLESS,4,TAKODANA,262f,120f,BESPIN,321f,180f);
        addNewRoute(TrainColor.BLUE,4,BESPIN,351f,179f,JEDHA,441f,92f);
        addNewRoute(TrainColor.ORANGE,6,BESPIN,366f,192f,CORELLIA,568f,136f);
        addNewRoute(TrainColor.RED,5,BESPIN,365f,210f,AHCH_TO,462f,203f);
        addNewRoute(TrainColor.GREEN,4,BESPIN,348f,230f,NABOO,386f,282f);

        //Malastare
        addNewRoute(TrainColor.COLORLESS,2,MALASTARE,572f,357f,KASHYYYK,611f,357f);
        addNewRoute(TrainColor.COLORLESS,2,JAKKU,523f,298f,MALASTARE,527f,332f);
        addNewRoute(TrainColor.COLORLESS,2,JAKKU,543f,298f,MALASTARE,547f,332f);
        addNewRoute(TrainColor.RED,4,NABOO,425f,314f,MALASTARE,510f,345f);
        addNewRoute(TrainColor.BLUE,3,TATOOINE,416f,375f,MALASTARE,510f,360f);
        addNewRoute(TrainColor.YELLOW,5,GEONOSIS,405f,425f,MALASTARE,517f,370f);
        addNewRoute(TrainColor.COLORLESS,2,MALASTARE,537f,379f,CATO_NEIMOIDIA,537f,406f);
        addNewRoute(TrainColor.COLORLESS,2,MALASTARE,557f,379f,CATO_NEIMOIDIA,557f,406f);

        //Mandalore
        addNewRoute(TrainColor.ORANGE,3,ALDERAAN,733f,225f,MANDALORE,795f,217f);
        addNewRoute(TrainColor.PURPLE,3,ALDERAAN,733f,210f,MANDALORE,795f,202f);
        addNewRoute(TrainColor.COLORLESS,2,ORD_MANTELL,809f,150f,MANDALORE,814f,184f);
        addNewRoute(TrainColor.GREEN,5,AMBRIA,679f,272f,MANDALORE,800f,227f);
        addNewRoute(TrainColor.YELLOW,4,DEATH_STAR,764f,289f,MANDALORE,817f,235f);
        addNewRoute(TrainColor.PURPLE,3,MANDALORE,829f,236f,YAVIN_IV,848f,275f);
        addNewRoute(TrainColor.COLORLESS,2,MANDALORE,848f,221f,FELUCIA,904f,250f);
        addNewRoute(TrainColor.WHITE,2,MANDALORE,850f,207f,DATHOMIR,889f,185f);
        addNewRoute(TrainColor.GREEN,2,MANDALORE,844f,190f,DATHOMIR,883f,168f);

        //KORRIBAN
        addNewRoute(TrainColor.COLORLESS,1,DEATH_STAR,765f,330f,KORRIBAN,783f,348f);
        addNewRoute(TrainColor.COLORLESS,2,KORRIBAN,812f,335f,YAVIN_IV,829f,314f);
        addNewRoute(TrainColor.COLORLESS,2,KORRIBAN,829f,345f,YAVIN_IV,846f,324f);
        addNewRoute(TrainColor.BLUE,5,KORRIBAN,810f,389f,LOTHAL,849f,454f);
        addNewRoute(TrainColor.COLORLESS,2,KORRIBAN,831f,365f,MON_CALA,865f,365f);
        addNewRoute(TrainColor.YELLOW,4,KESSEL,712f,430f,KORRIBAN,774f,370f);
        addNewRoute(TrainColor.ORANGE,4,KESSEL,727f,445f,KORRIBAN,789f,385f);

        //Outer Track
        addNewRoute(TrainColor.RED,6,KESSEL,728f,462f,LOTHAL,831f,479f);
        addNewRoute(TrainColor.COLORLESS,2,KAMINO,618f,487f,KESSEL,680f,463f);
        addNewRoute(TrainColor.GREEN,6,GEONOSIS,398f,454f,KAMINO,561f,496f);
        addNewRoute(TrainColor.COLORLESS,3,SCARIF,296f,423f,GEONOSIS,345f,436f);
        addNewRoute(TrainColor.COLORLESS,3,UTAPAU,174f,414f,SCARIF,240f,411f);
        addNewRoute(TrainColor.PINK,3,MUSTAFAR,112f,320f,UTAPAU,144f,384f);
        addNewRoute(TrainColor.YELLOW,3,MUSTAFAR,92f,325f,UTAPAU,124f,389f);
        addNewRoute(TrainColor.PINK,5,POLIS_MASSA,67f,216f,MUSTAFAR,79f,274f);
        addNewRoute(TrainColor.GREEN,5,POLIS_MASSA,82f,211f,MUSTAFAR,94f,269f);
        addNewRoute(TrainColor.COLORLESS,1,POLIS_MASSA,101f,178f,DEATH_STAR_II,125f,165f);
        addNewRoute(TrainColor.COLORLESS,1,POLIS_MASSA,93f,165f,DEATH_STAR_II,117f,152f);
        addNewRoute(TrainColor.COLORLESS,1,ENDOR,118f,103f,DEATH_STAR_II,130f,128f);
        addNewRoute(TrainColor.COLORLESS,1,ENDOR,133f,96f,DEATH_STAR_II,145f,121f);
        addNewRoute(TrainColor.COLORLESS,3,ENDOR, 140f,80f,TAKODANA,219f,99f);
        addNewRoute(TrainColor.WHITE,6,TAKODANA,268f,101f,JEDHA,433f,75f);
        addNewRoute(TrainColor.COLORLESS,6,JEDHA,486f,78f,CORUSCANT,665f,113f);
        addNewRoute(TrainColor.PURPLE,5,CORUSCANT,719f,102f,MYGEETO,851f,81f);
        addNewRoute(TrainColor.COLORLESS,2,MYGEETO,906f,97f,DANTOOINE,934f,115f);
        addNewRoute(TrainColor.COLORLESS,2,MYGEETO,914f,82f,DANTOOINE,942f,100f);
        addNewRoute(TrainColor.YELLOW,2,DATHOMIR,919f,139f,DANTOOINE,939f,125f);
        addNewRoute(TrainColor.RED,2,DATHOMIR,934f,154f,DANTOOINE,954f,140f);
        addNewRoute(TrainColor.ORANGE,2,DATHOMIR,909f,190f,FELUCIA,918f,238f);
        addNewRoute(TrainColor.PURPLE,2,DATHOMIR,926f,185f,FELUCIA,935f,233f);
        addNewRoute(TrainColor.COLORLESS,2,YAVIN_IV,877f,278f,FELUCIA,906f,265f);
        addNewRoute(TrainColor.COLORLESS,2,YAVIN_IV,886f,294f,FELUCIA,915f,281f);
        addNewRoute(TrainColor.COLORLESS,2,YAVIN_IV,869f,319f,MON_CALA,887f,350f);
        addNewRoute(TrainColor.PINK,4,LOTHAL,872f,455f,MON_CALA,885f,399f);
        addNewRoute(TrainColor.PURPLE,6,UTAPAU,170f,426f,GEONOSIS,345f,449f);


        //Other Single Routes
        addNewRoute(TrainColor.GREEN,3,KASHYYYK,653f,376f,KESSEL,688f,432f);
        addNewRoute(TrainColor.COLORLESS,2,CATO_NEIMOIDIA,570f,410f,KASHYYYK,618f,372f);
        addNewRoute(TrainColor.RED,4,GEONOSIS,409f,438f,CATO_NEIMOIDIA,513f,432f);
        addNewRoute(TrainColor.COLORLESS,2,GEONOSIS,381f,416f,TATOOINE,384f,398f);
        addNewRoute(TrainColor.COLORLESS,3,SCARIF,298f,400f,TATOOINE,356f,383f);
        addNewRoute(TrainColor.WHITE,5,SCARIF,285f,384f,NABOO,380f,317f);
        addNewRoute(TrainColor.COLORLESS,2,UTAPAU,168f,394f,DAGOBAH,187f,380f);
        addNewRoute(TrainColor.ORANGE,3,DAGOBAH,221f,341f,HOTH,261f,286f);
        addNewRoute(TrainColor.BLUE,6,POLIS_MASSA,101f,195f,HOTH,250f,248f);
        addNewRoute(TrainColor.COLORLESS,4,DEATH_STAR_II,168f,133f,TAKODANA,222f,117f);
        addNewRoute(TrainColor.PURPLE,4,JEDHA,482f,92f,CORELLIA,574f,124f);
        addNewRoute(TrainColor.COLORLESS,3,CORELLIA,624f,133f,CORUSCANT,672f,126f);
        addNewRoute(TrainColor.COLORLESS,2,CORUSCANT,726f,116f,ORD_MANTELL,773f,121f);
        addNewRoute(TrainColor.PINK,6,CORELLIA,618f,152f,ORD_MANTELL,783f,140f);
        addNewRoute(TrainColor.RED,3,CORELLIA,608f,161f,ALDERAAN,677f,205f);
        addNewRoute(TrainColor.BLUE,4,AHCH_TO,517f,203f,ALDERAAN,666f,223f);
        addNewRoute(TrainColor.COLORLESS,2,AMBRIA,682f,293f,DEATH_STAR,717f,303f);
        addNewRoute(TrainColor.COLORLESS,2,KASHYYYK,645f,330f,AMBRIA,649f,308f);
        addNewRoute(TrainColor.WHITE,3,KASHYYYK,667f,348f,DEATH_STAR,725f,320f);
        addNewRoute(TrainColor.PINK,4,NABOO,415f,277f,AHCH_TO,470f,222f);
        addNewRoute(TrainColor.WHITE,4,ALDERAAN,725f,196f,ORD_MANTELL,789f,148f);
        addNewRoute(TrainColor.COLORLESS,3,ORD_MANTELL,829f,111f,MYGEETO,858f,96f);
        addNewRoute(TrainColor.PURPLE,3,DEATH_STAR,778f,307f,YAVIN_IV,827f,299f);
        addNewRoute(TrainColor.COLORLESS,2,TATOOINE,392f,349f,NABOO,396f,325f);
        addNewRoute(TrainColor.BLUE,3,MYGEETO,887f,107f,DATHOMIR,899f,139f);

        //Other Double Routes
        addNewRoute(TrainColor.ORANGE,5,MUSTAFAR,117f,280f,HOTH,243f,264f);
        addNewRoute(TrainColor.WHITE,5,MUSTAFAR,120f,296f,HOTH,246f,280f);
        addNewRoute(TrainColor.RED,3,HOTH,308f,262f,NABOO,371f,288f);
        addNewRoute(TrainColor.YELLOW,3,HOTH,304f,277f,NABOO,367f,303f);
        addNewRoute(TrainColor.COLORLESS,2,AHCH_TO,507f,178f,CORELLIA,573f,147f);
        addNewRoute(TrainColor.COLORLESS,2,AHCH_TO,517f,193f,CORELLIA,583f,162f);
        addNewRoute(TrainColor.COLORLESS,1,AHCH_TO,500f,226f,JAKKU,510f,250f);
        addNewRoute(TrainColor.COLORLESS,1,AHCH_TO,515f,221f,JAKKU,525f,245f);
        addNewRoute(TrainColor.PURPLE,4,NABOO,430f,301f,JAKKU,502f,284f);
        addNewRoute(TrainColor.ORANGE,4,NABOO,425f,286f,JAKKU,497f,269f);
        addNewRoute(TrainColor.BLUE,2,JAKKU,556f,264f,AMBRIA,631f,273f);
        addNewRoute(TrainColor.PINK,2,JAKKU,556f,279f,AMBRIA,631f,288f);
        addNewRoute(TrainColor.COLORLESS,1,CATO_NEIMOIDIA,553f,456f,KAMINO,570f,477f);
        addNewRoute(TrainColor.COLORLESS,1,CATO_NEIMOIDIA,574f,449f,KAMINO,591f,470f);
        addNewRoute(TrainColor.GREEN,2,AMBRIA,657f,255f,ALDERAAN,673f,236f);
        addNewRoute(TrainColor.WHITE,2,AMBRIA,672f,263f,ALDERAAN,687f,244f);
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public Map<String, City> getCities() {
        return cities;
    }


}
