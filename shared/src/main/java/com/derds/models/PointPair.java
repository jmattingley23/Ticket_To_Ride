package com.derds.models;

import com.derds.models.MapPoint;

/**
 * Created by mogard on 10/21/2017.
 */

public class PointPair {
    public MapPoint p1;
    public MapPoint p2;

    public PointPair(MapPoint p1, MapPoint p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PointPair pointPair = (PointPair) o;

        if (p1 != null ? !p1.equals(pointPair.p1) : pointPair.p1 != null) return false;
        return p2 != null ? p2.equals(pointPair.p2) : pointPair.p2 == null;

    }

    @Override
    public int hashCode() {
        int result = p1 != null ? p1.hashCode() : 0;
        result = 31 * result + (p2 != null ? p2.hashCode() : 0);
        return result;
    }
}
