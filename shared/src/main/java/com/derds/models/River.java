package com.derds.models;

import java.util.ArrayList;
import java.util.List;

public class River {
    private List<TrainCard> showingCards;
//    private int riverSize;

    public River() {
        showingCards = new ArrayList<>();
    }

    public int size() {
        return showingCards.size();
    }

    public boolean fillRiver(List<TrainCard> cardsToAdd) {
        showingCards.addAll(cardsToAdd);
        return showingCards.size() <= 5;
    }

    public void addCard(TrainCard card){showingCards.add(card);}

    public List<TrainCard> getShowingCards() {
        return showingCards;
    }

    public TrainCard drawCard(TrainCard card) {
        for(int i = 0; i < size(); i++) {
            if(showingCards.get(i).equals(card)) {
                return showingCards.remove(i);
            }
        }
        return null;
        // TODO show error?
    }

    public void clearRiver(){
        showingCards.clear();
    }

    public boolean containsThreeWilds(){
        int count = 0;
        for(TrainCard card : showingCards){
            if(card.getColor() == TrainColor.RAINBOW){
                count++;
            }
        }
        return (count > 2);
    }
}
