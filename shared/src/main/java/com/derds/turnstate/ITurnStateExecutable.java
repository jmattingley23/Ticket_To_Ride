package com.derds.turnstate;

import com.derds.models.DestinationCard;
import com.derds.models.Route;
import com.derds.models.TrainCard;

import java.util.List;

/**
 * Created by justinbrunner on 11/11/17.
 */

public interface ITurnStateExecutable {
    void sendMessage(String message);

    void claimRoute(String authToken, String playerId, Route route, List<TrainCard> cardsUsed, int trainPiecesUsed);

    void drawTrainCardFromDeck(String authToken, String playerId, boolean canDrawMoreCards);

    void drawFaceUpCard(String authToken, String playerId, TrainCard card, boolean canDrawMoreCards);

    void claimDestinationCards(String authToken, String playerId, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned);

    void failedClaimDestinationCards(String playerId, List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards);

    void drawDestinationCardsRequest();

    void drawDestinationCardsResponse(String playerId, List<DestinationCard> cards);
}
