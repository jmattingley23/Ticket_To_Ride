package com.derds.turnstate;

import com.derds.models.DestinationCard;
import com.derds.models.Player;
import com.derds.models.Route;
import com.derds.models.TrainCard;

import java.util.List;
import java.util.Map;

/**
 * Created by justinbrunner on 11/10/17.
 */

public abstract class TurnState {
    public enum StateName { END, WAIT, BEGIN_GAME, START_TURN, CHOOSE_DESTINATIONS, DRAW_TRAIN_CARD, SELECT_ROUTE };
    private ITurnStateExecutable turnStateExecutable;
    private final String INVALID_ACTION_MSG = "You cannot perform this action at the moment";

    public void setTurnStateExecutable(ITurnStateExecutable turnStateExecutable) {
        this.turnStateExecutable = turnStateExecutable;
    }

    public ITurnStateExecutable getTurnStateExecutable() {
        return this.turnStateExecutable;
    }

    public boolean claimDestinationCards(Player player, List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {
        turnStateExecutable.sendMessage(INVALID_ACTION_MSG);
        return false;
    }

    public boolean drawDestinationCards(Player player) {
        turnStateExecutable.sendMessage(INVALID_ACTION_MSG);
        return false;
    }

    public boolean startTurn(Player player) {
        turnStateExecutable.sendMessage(INVALID_ACTION_MSG);
        return false;
    }

    public boolean drawTrainCardFromDeck(Player player){
        turnStateExecutable.sendMessage(INVALID_ACTION_MSG);
        return false;
    }

    public boolean selectTrainCard(Player player, TrainCard card) {
        turnStateExecutable.sendMessage(INVALID_ACTION_MSG);
        return false;
    }

    public boolean selectRainbowTrainCard(Player player, TrainCard card) {
        turnStateExecutable.sendMessage(INVALID_ACTION_MSG);
        return false;
    }

    public boolean claimRoute(Player player, Route route, List<TrainCard> cardsUsed) {
        turnStateExecutable.sendMessage(INVALID_ACTION_MSG);
        return false;
    }

    public boolean selectRoute(Player player, Route route) {
        turnStateExecutable.sendMessage(INVALID_ACTION_MSG);
        return false;
    }

    public abstract boolean wait(Player player);

    public boolean endTurn(Player player) {
        player.setTurnState(StateName.END);
        return true;
    }
}
