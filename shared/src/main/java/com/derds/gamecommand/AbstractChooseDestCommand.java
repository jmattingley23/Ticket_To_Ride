package com.derds.gamecommand;

import com.derds.models.DestinationCard;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractChooseDestCommand extends AbstractGameCommand {
    protected List<DestinationCard> cardsChosen;
    protected List<DestinationCard> cardsReturned;

    public AbstractChooseDestCommand(String executor, String gameId, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned) {
        super(executor, gameId);
        this.cardsChosen = cardsChosen;
        this.cardsReturned = cardsReturned;
    }

    public List<DestinationCard> getCardsChosen() {
        return cardsChosen;
    }

    public void setCardsChosen(List<DestinationCard> cardsChosen) {
        this.cardsChosen = cardsChosen;
    }

    public List<DestinationCard> getCardsReturned() {
        return cardsReturned;
    }

    public void setCardsReturned(List<DestinationCard> cardsReturned) {
        this.cardsReturned = cardsReturned;
    }
}
