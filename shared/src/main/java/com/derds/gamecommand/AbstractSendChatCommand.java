package com.derds.gamecommand;

import com.derds.models.Message;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractSendChatCommand extends AbstractGameCommand {
    protected Message content;

    public AbstractSendChatCommand(String executor, String gameId, Message content) {
        super(executor, gameId);
        this.content = content;
    }

    public Message getContent() {
        return content;
    }

    public void setContent(Message content) {
        this.content = content;
    }
}
