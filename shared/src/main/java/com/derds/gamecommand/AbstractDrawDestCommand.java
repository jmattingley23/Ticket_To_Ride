package com.derds.gamecommand;

import com.derds.models.DestinationCard;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractDrawDestCommand extends AbstractGameCommand {
    protected List<DestinationCard> cardsToPickFrom;
    protected boolean areCardsDrawn;  // is set when server populates command with cards

    public AbstractDrawDestCommand(List<DestinationCard> cardsToPickFrom, boolean areCardsDrawn, String executor, String gameId) {
        super(executor, gameId);
        this.cardsToPickFrom = cardsToPickFrom;
        this.areCardsDrawn = areCardsDrawn;
    }
}
