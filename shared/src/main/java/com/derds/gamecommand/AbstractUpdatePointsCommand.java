package com.derds.gamecommand;

import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractUpdatePointsCommand extends AbstractGameCommand {
    protected int newPointsTotal;
    protected String playerId;

    public AbstractUpdatePointsCommand(String executor, String gameId, int newPointsTotal, String playerId) {
        super(executor, gameId);
        this.newPointsTotal = newPointsTotal;
        this.playerId = playerId;
    }
}
