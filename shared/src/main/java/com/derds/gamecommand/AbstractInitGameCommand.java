package com.derds.gamecommand;

import com.derds.models.Player;
import com.derds.models.TrainCard;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractInitGameCommand extends AbstractGameCommand {
    protected int turnOrder;
    protected Player player;

    public AbstractInitGameCommand(String executor, String gameId, int turnOrder, Player player) {
        super(executor, gameId);
        this.turnOrder = turnOrder;
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
