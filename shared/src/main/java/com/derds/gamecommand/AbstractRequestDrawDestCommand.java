package com.derds.gamecommand;

import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractRequestDrawDestCommand extends AbstractGameCommand {
    public AbstractRequestDrawDestCommand(String executor, String gameId) {
        super(executor, gameId);
    }
}
