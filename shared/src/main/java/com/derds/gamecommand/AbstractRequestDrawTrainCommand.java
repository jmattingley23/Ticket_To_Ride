package com.derds.gamecommand;

import com.derds.models.TrainCard;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractRequestDrawTrainCommand extends AbstractGameCommand {

    protected TrainCard drawnCard;
    protected int trainDeckSize;
    protected int playerTrainHandSize;

    public AbstractRequestDrawTrainCommand(String authToken, String executor, String gameId) {
        super(executor, gameId);
    }

    public void setDrawnCard(TrainCard drawnCard) {
        this.drawnCard = drawnCard;
    }

    public TrainCard getDrawnCard() {
        return drawnCard;
    }

    public int getTrainDeckSize() {
        return trainDeckSize;
    }

    public void setTrainDeckSize(int trainDeckSize) {
        this.trainDeckSize = trainDeckSize;
    }

    public int getPlayerTrainHandSize() {
        return playerTrainHandSize;
    }

    public void setPlayerTrainHandSize(int playerTrainHandSize) {
        this.playerTrainHandSize = playerTrainHandSize;
    }
}
