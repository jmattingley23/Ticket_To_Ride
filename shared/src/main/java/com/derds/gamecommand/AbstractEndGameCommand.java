package com.derds.gamecommand;

import com.derds.models.PlayerStats;
import com.derds.result.Result;

import java.util.List;

/**
 * Created by drc95 on 11/21/17.
 */

public abstract class AbstractEndGameCommand extends AbstractGameCommand {
    protected List<PlayerStats> playerStatses;

    public AbstractEndGameCommand(String executor, String gameId, List<PlayerStats> playerStatses) {
        super(executor, gameId);
        this.playerStatses = playerStatses;
    }

    public List<PlayerStats> getPlayerStatses() {
        return playerStatses;
    }

    public void setPlayerStatses(List<PlayerStats> playerStatses) {
        this.playerStatses = playerStatses;
    }
}
