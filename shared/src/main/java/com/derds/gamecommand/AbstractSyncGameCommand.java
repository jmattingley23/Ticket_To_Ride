package com.derds.gamecommand;

import com.derds.models.Player;
import com.derds.models.Route;

import java.util.List;

/**
 * Created by drc95 on 11/24/17.
 */

public abstract class AbstractSyncGameCommand extends AbstractGameCommand {
    protected Player player;
    protected int curTurn;
    protected List<Route> claimedRoutes;

    public AbstractSyncGameCommand(String executor, String gameId, Player player, int curTurn, List<Route> claimedRoutes) {
        super(executor, gameId);
        this.player = player;
        this.curTurn = curTurn;
        this.claimedRoutes = claimedRoutes;
    }

    // TODO
//    sync vs request queue, make them separate
//    add map to sync command
//    add command queue strings to sync command
}
