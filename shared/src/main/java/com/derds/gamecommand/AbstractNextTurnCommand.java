package com.derds.gamecommand;

public abstract class AbstractNextTurnCommand extends AbstractGameCommand {
    int newTurn;

    public AbstractNextTurnCommand(int newTurn, String gameId) {
        super(null, gameId);
        this.newTurn = newTurn;
    }

    public int getNewTurn() {
        return newTurn;
    }
}
