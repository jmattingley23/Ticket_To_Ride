package com.derds.gamecommand;

import com.derds.models.Message;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/21/17.
 */

public abstract class AbstractGetChatHistoryCommand extends AbstractGameCommand {
    protected int lastCommandSequenceId;  // commandSequenceId of last chat command in this history
    protected List<Message> messageHistory;
    protected int skip;

    public AbstractGetChatHistoryCommand(String executor, String gameId, int lastCommandSequenceId, List<Message> messageHistory, int skip) {
        super(executor, gameId);
        this.lastCommandSequenceId = lastCommandSequenceId;
        this.messageHistory = messageHistory;
        this.skip = skip;
    }
}
