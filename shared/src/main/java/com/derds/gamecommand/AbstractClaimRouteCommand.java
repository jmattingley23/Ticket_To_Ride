package com.derds.gamecommand;

import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractClaimRouteCommand extends AbstractGameCommand {
    protected Route claimedRoute;
    protected List<TrainCard> cardsUsed;
    protected int trainPiecesUsed;
    protected int numTrainPieces;
    protected int trainDeckSize;

    public AbstractClaimRouteCommand(Route claimedRoute, List<TrainCard> cardsUsed, int trainPiecesUsed, String executor, String gameId) {
        super(executor, gameId);
        this.claimedRoute = claimedRoute;
        this.cardsUsed = cardsUsed;
        this.trainPiecesUsed = trainPiecesUsed;
    }

    public Route getClaimedRoute() {
        return claimedRoute;
    }

    public List<TrainCard> getCardsUsed() {
        return cardsUsed;
    }

    public int getTrainPiecesUsed() {
        return trainPiecesUsed;
    }

    public int getTrainDeckSize() {
        return trainDeckSize;
    }

    public void setTrainDeckSize(int trainDeckSize) {
        this.trainDeckSize = trainDeckSize;
    }

    public int getNumTrainPieces() {
        return numTrainPieces;
    }

    public void setNumTrainPieces(int numTrainPieces) {
        this.numTrainPieces = numTrainPieces;
    }
}
