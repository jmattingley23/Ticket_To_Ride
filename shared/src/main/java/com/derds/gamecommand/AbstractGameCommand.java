package com.derds.gamecommand;

import com.derds.command.AbstractCommand;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractGameCommand extends AbstractCommand {
    protected int commandSequenceId;
    protected TurnState.StateName executorState;
    protected String executor;    //player id // TODO what happens if the server makes one of these of its own volition?
    protected String gameId;

    // TODO WRITE UNIT TESTS FOR THESE THINGS!!!!

    public AbstractGameCommand(String executor, String gameId) {
        this.executor = executor;
        this.gameId = gameId;
    }

    public void setExecutorState(TurnState.StateName executorState) {
        this.executorState = executorState;
    }

    public TurnState.StateName getExecutorState() {
        return executorState;
    }

    public String getExecutor() {
        return executor;
    }

    public abstract String toString();

    public abstract boolean isVisibleOnHistory();

    public void setCommandSequenceId(int commandSequenceId) {
        this.commandSequenceId = commandSequenceId;
    }

    public int getCommandSequenceId() {
        return commandSequenceId;
    }

    public String getGameId() {
        return gameId;
    }
}
