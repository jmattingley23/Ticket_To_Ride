package com.derds.gamecommand;

import com.derds.models.River;
import com.derds.models.TrainCard;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractPickFaceupCardCommand extends AbstractGameCommand{
    protected TrainCard cardDrawn;
    protected River newRiver;
    protected int trainDeckSize;
    protected int playerTrainHandSize;

    public AbstractPickFaceupCardCommand(String executor, String gameId, TrainCard cardDrawn) {
        super(executor, gameId);
        this.cardDrawn = cardDrawn;
    }

    public TrainCard getCardDrawn() {
        return cardDrawn;
    }

    public River getNewRiver() {
        return newRiver;
    }

    public void setNewRiver(River newRiver) {
        this.newRiver = newRiver;
    }

    public int getTrainDeckSize() {
        return trainDeckSize;
    }

    public void setTrainDeckSize(int trainDeckSize) {
        this.trainDeckSize = trainDeckSize;
    }

    public int getPlayerTrainHandSize() {
        return playerTrainHandSize;
    }

    public void setPlayerTrainHandSize(int playerTrainHandSize) {
        this.playerTrainHandSize = playerTrainHandSize;
    }
}
