package com.derds.gamecommand;

import com.derds.models.TrainCard;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractDrawTrainCommand extends AbstractGameCommand {
    protected TrainCard cardDrawn;

    public AbstractDrawTrainCommand(TrainCard cardDrawn, String executor, String gameId) {
        super(executor, gameId);
        this.cardDrawn = cardDrawn;
    }
}
