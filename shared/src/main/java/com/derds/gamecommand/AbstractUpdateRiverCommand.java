package com.derds.gamecommand;

import com.derds.models.River;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/19/17.
 */

public abstract class AbstractUpdateRiverCommand extends AbstractGameCommand {
    protected River newRiver;

    public AbstractUpdateRiverCommand(String executor, String gameId, River newRiver) {
        super(executor, gameId);
        this.newRiver = newRiver;
    }
}
