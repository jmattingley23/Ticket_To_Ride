package android.os;

/**
 * This is a shadow class for AsyncTask which forces it to run synchronously.
 * WARNING: this means that all asyncronous things will run syncronously in tests
 */
public abstract class AsyncTask<Params, Progress, Result> {

    protected abstract Result doInBackground(Params... params);

    protected void onPostExecute(Result result) {
    }

    protected void onProgressUpdate(Progress... values) {
    }

    public AsyncTask<Params, Progress, Result> execute(Params... params) {
//        System.out.println("Hey the replacement AsyncTask works!");
        Result result = doInBackground(params);
        onPostExecute(result);
        return this;
    }
}