package com.derds.tickettoride.stubs;

import android.os.Bundle;

import com.derds.tickettoride.login.ui.LoginActivity;

/**
 * Created by justinbrunner on 10/20/17.
 */

public class LoginActivityStub extends LoginActivity {
    @Override
    public void createToast(String message, int toastLength) {

    }

    @Override
    public void onBackPressed() {

    }

    @Override
    public void switchToLobbyActivity() {

    }

    @Override
    public void setLoginFragment(Bundle savedInstanceState) {

    }

    @Override
    public void setRegisterFragment(Bundle savedInstanceState) {

    }

    @Override
    public void showProgressWheel(boolean show) {

    }
}
