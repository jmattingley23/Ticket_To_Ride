package com.derds.tickettoride.stubs;

import com.derds.tickettoride.lobby.ILobbyController;
import com.derds.tickettoride.lobby.LobbyController;
import com.derds.tickettoride.lobby.ui.LobbyFragment;

/**
 * Created by drc95 on 10/7/17.
 */

public class LobbyFragmentStub extends LobbyFragment {
    public LobbyFragmentStub() {

    }

    @Override
    public void setController(ILobbyController newController) {

    }

    @Override
    public void refreshLobby() {

    }

    @Override
    public void createToast(String message) {

    }
}
