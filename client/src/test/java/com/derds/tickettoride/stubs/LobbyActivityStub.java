package com.derds.tickettoride.stubs;


import com.derds.tickettoride.lobby.ui.CreateGameFragment;
import com.derds.tickettoride.lobby.ui.LobbyActivity;
import com.derds.tickettoride.lobby.ui.LobbyFragment;
/**
 * Created by drc95 on 10/7/17.
 */

public class LobbyActivityStub extends LobbyActivity {

    @Override
    public void switchToLobby(LobbyFragment lobbyFragment) {

    }

    @Override
    public void switchToCreateGame(CreateGameFragment createGameFragment) {

    }


    @Override
    public void showProgressWheel(boolean show) {

    }

    @Override
    public void enableCreateGameBtn(boolean enable) {

    }
}
