package com.google.firebase.iid;

/**
 * Created by justinbrunner on 10/21/17.
 */

public class FirebaseInstanceId {
    private static FirebaseInstanceId instance;

    public static FirebaseInstanceId getInstance() {
        if (instance == null) {
            instance = new FirebaseInstanceId();
        }

        return instance;
    }

    private FirebaseInstanceId() {};

    public String getToken() {
        return "testingStringthangy";
    }
}
