package com.derds.tickettoride;

import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.login.ILoginController;
import com.derds.tickettoride.login.ui.LoginActivity;
import com.derds.tickettoride.login.LoginController;
import com.derds.tickettoride.stubs.LoginActivityStub;
import com.derds.tickettoride.testData.LoginControllerTestData;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by justinbrunner on 10/20/17.
 */

public class LoginControllerTest {
    private ILoginController loginController;
    private LoginActivity loginActivity;
    private boolean firstRun = false;

    @Before
    public void testSetup() {
        loginActivity = new LoginActivityStub();
        loginController =  new LoginController(loginActivity);

        if (!firstRun) {
            loginController.register(LoginControllerTestData.getInstance().getUsername("exists"),
                    LoginControllerTestData.getInstance().getPassword("exists"),
                    LoginControllerTestData.getInstance().getFirstName("new"),
                    LoginControllerTestData.getInstance().getLastName("new"));
            firstRun = true;
        }
    }

    @Test
    public void testValidateDataLoginValid() {
        boolean isValidData = loginController.validateData(false, LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("new"), null, null);

        assertTrue(isValidData);
    }

    @Test
    public void testValidateDataEmptyUsername() {
        boolean isValidData = loginController.validateData(false, LoginControllerTestData.getInstance().getUsername("empty"),
                LoginControllerTestData.getInstance().getPassword("new"), null, null);

        assertFalse(isValidData);
    }

    @Test
    public void testValidateDataEmptyPassword() {
        boolean isValidData = loginController.validateData(false, LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("empty"), null, null);

        assertFalse(isValidData);
    }

    @Test
    public void testValidateDataRegisterValid() {
        boolean isValidData = loginController.validateData(true, LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("new"),
                LoginControllerTestData.getInstance().getFirstName("new"),
                LoginControllerTestData.getInstance().getLastName("new"));

        assertTrue(isValidData);
    }

    @Test
    public void testValidateDataRegisterEmptyFirstname() {
        boolean isValidData = loginController.validateData(true, LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("new"),
                LoginControllerTestData.getInstance().getFirstName("empty"),
                LoginControllerTestData.getInstance().getLastName("new"));

        assertFalse(isValidData);
    }

    @Test
    public void testValidateDataRegisterEmptyLastName() {
        boolean isValidData = loginController.validateData(true, LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("new"),
                LoginControllerTestData.getInstance().getFirstName("new"),
                LoginControllerTestData.getInstance().getLastName("empty"));

        assertFalse(isValidData);
    }

    @Test
    public void testLoginExists() {
        AuthToken.getInstance().setAuthToken(null);

        loginController.login(LoginControllerTestData.getInstance().getUsername("exists"),
                LoginControllerTestData.getInstance().getPassword("exists"));

        assertNotEquals(null, AuthToken.getInstance().getAuthToken());
    }

    @Test
    public void testLoginInvalid() {
        loginController.login(LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("new"));

        assertEquals(null, AuthToken.getInstance().getAuthToken());
    }

    @Test
    public void testLoginEmpty() {
        loginController.login(LoginControllerTestData.getInstance().getUsername("empty"),
                LoginControllerTestData.getInstance().getPassword("empty"));

        assertEquals(null, AuthToken.getInstance().getAuthToken());
    }

    @Test
    public void testRegisterEmptyUsername() {
        loginController.register(LoginControllerTestData.getInstance().getUsername("empty"),
                LoginControllerTestData.getInstance().getPassword("new"),
                LoginControllerTestData.getInstance().getFirstName("new"),
                LoginControllerTestData.getInstance().getLastName("new"));

        assertEquals(null, AuthToken.getInstance().getAuthToken());
    }

    @Test
    public void testRegisterEmptyPassword() {
        loginController.register(LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("empty"),
                LoginControllerTestData.getInstance().getFirstName("new"),
                LoginControllerTestData.getInstance().getLastName("new"));

        assertEquals(null, AuthToken.getInstance().getAuthToken());
    }

    @Test
    public void testRegisterEmptyFirstName() {
        loginController.register(LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("new"),
                LoginControllerTestData.getInstance().getFirstName("empty"),
                LoginControllerTestData.getInstance().getLastName("new"));

        assertEquals(null, AuthToken.getInstance().getAuthToken());
    }

    @Test
    public void testRegisterEmptyLastName() {
        loginController.register(LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("new"),
                LoginControllerTestData.getInstance().getFirstName("new"),
                LoginControllerTestData.getInstance().getLastName("empty"));

        assertEquals(null, AuthToken.getInstance().getAuthToken());
    }

    @Test
    public void testRegisterNew() {
        AuthToken.getInstance().setAuthToken(null);

        loginController.register(LoginControllerTestData.getInstance().getUsername("new"),
                LoginControllerTestData.getInstance().getPassword("new"),
                LoginControllerTestData.getInstance().getFirstName("new"),
                LoginControllerTestData.getInstance().getLastName("new"));

        assertNotEquals(null, AuthToken.getInstance().getAuthToken());
    }

    @Test
    public void testRegisterExists() {
        AuthToken.getInstance().setAuthToken(null);

        loginController.register(LoginControllerTestData.getInstance().getUsername("exists"),
                LoginControllerTestData.getInstance().getPassword("exists"),
                LoginControllerTestData.getInstance().getFirstName("new"),
                LoginControllerTestData.getInstance().getLastName("new"));

        assertEquals(null, AuthToken.getInstance().getAuthToken());
    }
}
