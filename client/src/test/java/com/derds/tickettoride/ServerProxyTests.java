package com.derds.tickettoride;

import com.derds.result.LoginResult;
import com.derds.tickettoride.common.ServerProxy;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ServerProxyTests {

    @Test
    public void loginTest(){
        LoginResult result = ServerProxy.getInstance().login("test", "test","");
        assert(result == null);
    }
}