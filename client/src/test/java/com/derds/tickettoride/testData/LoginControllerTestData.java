package com.derds.tickettoride.testData;

import android.support.annotation.NonNull;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

/**
 * Created by justinbrunner on 10/20/17.
 */

public class LoginControllerTestData {
    private static LoginControllerTestData instance;

    private LoginControllerTestData() {
        usernames = new HashMap<>();
        passwords = new HashMap<>();
        firstnames = new HashMap<>();
        lastnames = new HashMap<>();

        usernames.put("exists", "username");
        passwords.put("exists", "usernamePassword");

        usernames.put("new", UUID.randomUUID().toString().replace("-", ""));
        passwords.put("new", "newUsernamePassword");
        firstnames.put("new", "fname");
        lastnames.put("new", "lname");

        usernames.put("empty", "");
        passwords.put("empty", "");
        firstnames.put("empty", "");
        lastnames.put("empty", "");

        usernames.put("invalid", "nonExistentUsername");
        passwords.put("invalid", "nonExistentUsername");
    }

    private Map<String, String> usernames;
    private Map<String, String> passwords;
    private Map<String, String> firstnames;
    private Map<String, String> lastnames;

    public static LoginControllerTestData getInstance() {
        if (instance == null) {
            instance = new LoginControllerTestData();
        }

        return instance;
    }

    public String getUsername(String key) {
        return usernames.get(key);
    }

    public String getPassword(String key) {
        return passwords.get(key);
    }

    public String getFirstName(String key) {
        return firstnames.get(key);
    }

    public String getLastName(String key) {
        return lastnames.get(key);
    }
}
