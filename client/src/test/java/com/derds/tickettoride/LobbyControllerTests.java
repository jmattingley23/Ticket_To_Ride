package com.derds.tickettoride;

import com.derds.models.PlayerColor;
import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.common.ServerProxy;
import com.derds.tickettoride.lobby.ILobbyController;
import com.derds.tickettoride.lobby.LobbyController;
import com.derds.tickettoride.lobby.ui.CreateGameFragment;
import com.derds.tickettoride.lobby.ui.LobbyActivity;
import com.derds.tickettoride.lobby.ui.LobbyFragment;
import com.derds.tickettoride.stubs.CreateGameFragmentStub;
import com.derds.tickettoride.stubs.LobbyActivityStub;
import com.derds.tickettoride.stubs.LobbyFragmentStub;

import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by drc95 on 10/7/17.
 */

public class LobbyControllerTests {

    private ILobbyController lobbyController;
    private LobbyActivity lobbyActivity;
    private LobbyFragment lobbyFragment;
    private CreateGameFragment createGameFragment;

    @BeforeClass
    public static void setupEverything() {
        // Need to login before every test as they run out of order
    }

    @Before
    public void testSetup() {
        // tests currently rely on a user with these credentials being in the database
        String testUser = "testtest";
        String password = "letmein";
        ServerProxy.getInstance().login(testUser,password,"IThoughtIFixedThis");
        // ensure logged in correctly
        assertTrue(AuthToken.getInstance().getAuthToken() != null);

        lobbyActivity = new LobbyActivityStub();
        lobbyFragment = new LobbyFragmentStub();
        createGameFragment = new CreateGameFragmentStub();
        lobbyController =  new LobbyController(lobbyActivity);
        lobbyController.setLobbyFragment(lobbyFragment);
        lobbyController.setCreateGameFragment(createGameFragment);
        lobbyController.updateGameLists();
    }

    @Test
    public void testCreateGameValid() {
        String gameName = "TEST_GAME";
        
        // Get validation data
        int openGameCount = ClientModelFacade.getInstance().getOpenGames().size();
        int currentGameCount = ClientModelFacade.getInstance().getJoinedGames().size();
        int playerGameCount = ClientModelFacade.getInstance().getOwnedGames().size();
        lobbyController.createGame(gameName, PlayerColor.BLUE);

        lobbyController.updateGameLists();

        int i = ClientModelFacade.getInstance().getOpenGames().size();

        // ensure game created
        assertTrue(openGameCount == ClientModelFacade.getInstance().getOpenGames().size());
        assertTrue(currentGameCount + 1 == ClientModelFacade.getInstance().getJoinedGames().size());
        assertTrue(playerGameCount + 1 == ClientModelFacade.getInstance().getOwnedGames().size());
    }

    @Test
    public void testCreateGameInvalidName() {
        String validName = "TEST_GAME_INVALID";
        String emptyName = "";
        String nullName = null;

        int openGameCount = ClientModelFacade.getInstance().getOpenGames().size();
        int currentGameCount = ClientModelFacade.getInstance().getJoinedGames().size();
        int playerGameCount = ClientModelFacade.getInstance().getOwnedGames().size();

        lobbyController.createGame(emptyName, PlayerColor.BLUE);

        lobbyController.updateGameLists();

        // ensure game was not created
        assertTrue(openGameCount == ClientModelFacade.getInstance().getOpenGames().size());
        assertTrue(currentGameCount == ClientModelFacade.getInstance().getJoinedGames().size());
        assertTrue(playerGameCount == ClientModelFacade.getInstance().getOwnedGames().size());

        lobbyController.createGame(nullName, PlayerColor.BLUE);

        lobbyController.updateGameLists();

        // ensure game was not created
        assertTrue(openGameCount == ClientModelFacade.getInstance().getOpenGames().size());
        assertTrue(currentGameCount == ClientModelFacade.getInstance().getJoinedGames().size());
        assertTrue(playerGameCount == ClientModelFacade.getInstance().getOwnedGames().size());

    }

    // TODO test other player creates a game and it shows up in your open games?

    @Test
    public void testCreateGameInvalidAuth() {
        String gameName = "TEST_GAME";

        // make auth token invalid
        AuthToken.getInstance().setAuthToken("blah blah blah");
        int openGameCount = ClientModelFacade.getInstance().getOpenGames().size();
        int currentGameCount = ClientModelFacade.getInstance().getJoinedGames().size();
        int playerGameCount = ClientModelFacade.getInstance().getOwnedGames().size();

        lobbyController.createGame(gameName, PlayerColor.BLUE);

        // ensure game was not created
        assertTrue(openGameCount == ClientModelFacade.getInstance().getOpenGames().size());
        assertTrue(currentGameCount == ClientModelFacade.getInstance().getJoinedGames().size());
        assertTrue(playerGameCount == ClientModelFacade.getInstance().getOwnedGames().size());

    }
}
