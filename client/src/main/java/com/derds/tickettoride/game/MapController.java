package com.derds.tickettoride.game;

import android.app.Activity;
import android.widget.Toast;

import com.derds.models.DestinationCard;
import com.derds.models.GameMap;
import com.derds.models.MapPoint;
import com.derds.models.Player;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.game.ui.MapFragment;
import com.derds.tickettoride.game.ui.MapView;
import com.derds.tickettoride.turnstate.StartTurnState;
import com.derds.turnstate.ITurnStateExecutable;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by justinbrunner on 10/23/17.
 */

public class MapController implements Observer, ITurnStateExecutable, IMapController{
    private MapFragment mapFragment;
    private MapView mapView;
    private Activity activity;

    public MapController(Activity activity) {
        this.activity = activity;
        ClientModelFacade.getInstance().addObserver(this);
    }


    public MapController(){
        ClientModelFacade.getInstance().addObserver(this);
    }

    @Override
    public void setMapView(MapView mapView) {
        this.mapView = mapView;
    }

    @Override
    public void selectRoute(Route route){
        StartTurnState.instance.setTurnStateExecutable(this);
        //ClientModelFacade.getInstance().getTurnState().claimRoute(ClientModelFacade.getInstance().getPlayer(),route,null);
        ClientModelFacade.getInstance().getTurnState().selectRoute(ClientModelFacade.getInstance().getPlayer(),route);
    }

    @Override
    public Route getRouteClicked(MapPoint pointClicked){
        return ClientModelFacade.getInstance().getGameMap().getRouteClicked(pointClicked);
    }

    @Override
    public void sendRouteToUI(Route route){
        ClientModelFacade.getInstance().claimRouteAttempt(route);
    }

    @Override
    public boolean isDoubleValid(Route route){
        Player player = ClientModelFacade.getInstance().getPlayer();
        return ClientModelFacade.getInstance().getGameMap().isDoubleValid(route, player.getMyGame().getPlayerIds().size(),player.getPlayerId());
    }

    @Override
    public void drawMap(GameMap map){
        mapView.drawMap(map);
    }

    @Override
    public void update(Observable observable, Object o) {
        if(o instanceof String) {
            String type = (String) o;
            if (type.equals("RouteClaimed") || type.equals("GameMapUpdate")) {
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        drawMap(ClientModelFacade.getInstance().getGameMap());
                    }
                });

            }
        }
    }

    @Override
    public void showErrorMessage(String message){
        Toast.makeText(activity,message,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sendMessage(String message) {
        showErrorMessage(message);
    }

    @Override
    public void claimRoute(String authToken, String playerId, Route route, List<TrainCard> cardsUsed, int trainPiecesUsed) {
        sendRouteToUI(route);
    }

    @Override
    public void drawTrainCardFromDeck(String authToken, String playerId, boolean canDrawMoreCards) {}

    @Override
    public void drawFaceUpCard(String authToken, String playerId, TrainCard card, boolean canDrawMoreCards) {}

    @Override
    public void claimDestinationCards(String authToken, String playerId, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned) {}

    @Override
    public void failedClaimDestinationCards(String playerId, List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {}

    @Override
    public void drawDestinationCardsRequest() {}

    @Override
    public void drawDestinationCardsResponse(String playerId, List<DestinationCard> cards) {
        
    }
}
