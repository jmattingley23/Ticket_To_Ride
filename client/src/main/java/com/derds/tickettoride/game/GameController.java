package com.derds.tickettoride.game;

import android.support.v4.app.Fragment;

import com.derds.tickettoride.R;
import com.derds.tickettoride.chat.ChatController;
import com.derds.tickettoride.chat.IChatController;
import com.derds.tickettoride.chat.ui.ChatFragment;
import com.derds.tickettoride.game.ui.GameActivity;
import com.derds.tickettoride.game.ui.GameFragment;
import com.derds.tickettoride.game.ui.MapFragment;
import com.derds.tickettoride.gamehistory.GameHistoryController;
import com.derds.tickettoride.gamehistory.IGameHistoryController;

/**
 * Created by justinbrunner on 10/21/17.
 */

public class GameController implements IGameController{
    private IChatController chatController;
    private IUIController uiController;
    private IMapController mapController;
    private IGameHistoryController gameHistoryController;
    private GameActivity gameActivity;

    public GameController(GameActivity gameActivity) {
        this.gameActivity = gameActivity;
    }

    public void setUiControllerFragments(GameFragment gameFragment) {
        getUIController().setGameFragment(gameFragment);
    }

    public void setChatControllerChatFragment(ChatFragment chatFragment) {
        getChatController().setChatFragment(chatFragment);
    }

    public IChatController getChatController() {
        if (chatController == null) {
            chatController = new ChatController();
        }

        return chatController;
    }

    public IUIController getUIController() {
        if (uiController == null) {
            uiController = new UIController();
        }

        return uiController;
    }

    public IMapController getMapController() {
        if (mapController == null) {
            mapController = new MapController();
        }

        return mapController;
    }


    public void switchToFragment(Fragment fragment) {
        gameActivity.switchToFragment(fragment);
    }

    public GameActivity getGameActivity() {
        return gameActivity;
    }

    public void setGameActivity(GameActivity gameActivity) {
        this.gameActivity = gameActivity;
    }

    public IGameHistoryController getGameHistoryController() {
        if (gameHistoryController == null) {
            gameHistoryController = new GameHistoryController();
        }

        return gameHistoryController;
    }

    public void setGameHistoryController(IGameHistoryController gameHistoryController) {
        this.gameHistoryController = gameHistoryController;
    }
}
