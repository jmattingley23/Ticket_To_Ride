package com.derds.tickettoride.game.ui;



import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.derds.models.TrainCard;
import com.derds.tickettoride.R;
import com.derds.tickettoride.game.IUIController;
import com.derds.tickettoride.game.UIController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aaron on 10/27/2017.
 */

public class TrainCardListAdapter extends RecyclerView.Adapter<TrainCardListAdapter.ViewHolder> {

    private List<Object> mCards;
    private List<TrainCard> mSelected;
    private static View mView;
    private static Context mContext;
    private static final int DECK = 0;
    private static final int TRAIN = 1;
    private IUIController uiController;
    private boolean isDeck = false;

    public TrainCardListAdapter(List<Object> cards, Context context, IUIController controller){
        mCards = cards;
        mContext = context;
        this.uiController = controller;
        mSelected = new ArrayList<>();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView mFalconImage;
        public Drawable mFalconDrawable;
        public CardView mCardView;
        public TextView mDeckCount;
        public ViewHolder(View v){
            super(v);
            mView = v;
            mFalconImage = (ImageView) v.findViewById(R.id.card_image_view);
            mCardView = (CardView) v.findViewById(R.id.train_card_view);
            mDeckCount = (TextView)v.findViewById(R.id.deck_count_text_view);
        }
    }

    public List<TrainCard> getSelected() {
        return new ArrayList<>(mSelected);
    }

    public void clearSelected(){
        mSelected.clear();
    }

    public void updateList(List<Object> cards){
        this.mCards = cards.subList(0,cards.size());
    }

    public void addCardToHand(TrainCard card){
        this.mCards.add(card);
    }

    public List<Object> getCards(){
        return mCards;
    }

    public void removeCard(TrainCard card){
        mCards.remove(card);
    }

    public void addCard(TrainCard card){
        mCards.add(card);
    }

    private boolean isSelected(TrainCard card){
        if(card != null) {
            for (TrainCard selectedCard : mSelected) {
                if (card == selectedCard) {
                    return true;
                }
            }
        }
        return false;
    }

    private void removeFromSelected(TrainCard card){
        for(int i = 0; i < mSelected.size(); i++){
            if(mSelected.get(i) == card){
                mSelected.remove(i);
                return;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mCards.get(position) instanceof ShowDeckButton) {
            return DECK;
        } else {
            return TRAIN;
        }
    }

    @Override
    public TrainCardListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){


        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.train_card, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final TrainCardListAdapter.ViewHolder holder, final int position) {

        if(mCards.get(position) instanceof TrainCard) {
            final TrainCard myCard = (TrainCard)mCards.get(position);
            if (isSelected(myCard)) {
                holder.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.card_highlight));
            } else {
                holder.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
            }
            holder.mFalconDrawable = ContextCompat.getDrawable(mContext, R.drawable.train_card_image);
            holder.mFalconDrawable.mutate().setColorFilter(myCard.getColor().getHexVal(), PorterDuff.Mode.SRC_ATOP);
            holder.mFalconImage.setImageDrawable(holder.mFalconDrawable);
            holder.mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(isDeck){
                        uiController.faceUpClickAttempt(myCard);
                    }else{
                        if(isSelected(myCard)){
                            removeFromSelected(myCard);
                            holder.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
                        }else{
                            mSelected.add(myCard);
                            holder.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.card_highlight));
                        }
                    }
                }
            });
        } else if (mCards.get(position) instanceof ShowDeckButton) {
            holder.mFalconDrawable = ContextCompat.getDrawable(mContext, R.drawable.add_card_to_hand);
            holder.mFalconImage.setImageDrawable(holder.mFalconDrawable);
            holder.mDeckCount.setText(String.valueOf(uiController.getTrainDeckSize()));
            holder.mDeckCount.setVisibility(View.VISIBLE);
            holder.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
            holder.mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uiController.showHideTrainDeck();
                    uiController.hideClaimRoute();
                }
            });
        } else if (mCards.get(position) instanceof TopOfDeckButton) {
            holder.mFalconDrawable = ContextCompat.getDrawable(mContext, R.drawable.deck_icon_white);
            holder.mFalconImage.setImageDrawable(holder.mFalconDrawable);
            holder.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
            isDeck = true;
            holder.mCardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    uiController.deckClickAttempt();
                }
            });
        }
    }

    @Override
    public int getItemCount(){
        return mCards.size();
    }
}
