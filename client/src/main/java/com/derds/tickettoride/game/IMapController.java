package com.derds.tickettoride.game;

import android.widget.Toast;

import com.derds.models.GameMap;
import com.derds.models.MapPoint;
import com.derds.models.Route;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.game.ui.MapView;
import com.derds.tickettoride.turnstate.StartTurnState;
import com.derds.turnstate.ITurnStateExecutable;

import java.util.Observer;

/**
 * Created by Aaron on 11/25/2017.
 */

public interface IMapController {

    public void setMapView(MapView mapView);

    public void selectRoute(Route route);

    public Route getRouteClicked(MapPoint pointClicked);

    public boolean isDoubleValid(Route route);

    public void sendRouteToUI(Route route);

    public void showErrorMessage(String message);

    public void drawMap(GameMap map);
}
