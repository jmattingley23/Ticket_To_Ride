package com.derds.tickettoride.game.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.derds.tickettoride.R;
import com.derds.tickettoride.chat.ui.ChatFragment;
import com.derds.tickettoride.game.GameController;
import com.derds.tickettoride.game.IGameController;

import static android.view.View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

public class GameActivity extends AppCompatActivity {

    private IGameController gameController;

    private GameFragment gameFragment;

    public static final int GO_FULLSCREEN = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_FULLSCREEN
            | SYSTEM_UI_FLAG_IMMERSIVE_STICKY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameController = new GameController(this);

        setContentView(R.layout.activity_game);
        
        FragmentManager fragmentManager = this.getSupportFragmentManager();

//        mapFragment = MapFragment.newInstance(this);
        Fragment curGameFragment =  fragmentManager.findFragmentById(R.id.game_fragment);
        if (curGameFragment == null) {
            gameFragment = GameFragment.newInstance();
            fragmentManager.beginTransaction().add(R.id.game_fragment, gameFragment).commit();
        } else {
            gameFragment = (GameFragment) curGameFragment;
        }
//        gameController.setUiControllerFragments(gameFragment,mapFragment);
//        gameController.setChatControllerChatFragment(chatFragment);
//        gameController.setMapControllerMapFragment(mapFragment);

//        fragmentManager.beginTransaction().add(R.id.game_fragment, chatFragment).commit();
    }

    public void switchToFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.game_fragment, fragment).commit();
    }

    @Override
    protected void onResume(){
        super.onResume();
        gameFragment.hideBars();
    }

//    @Override
//    public void onBackPressed() {
//        finish();
//    }

    public IGameController getGameController() {
        return gameController;
    }

    public void setGameController(IGameController gameController) {
        this.gameController = gameController;
    }
}
