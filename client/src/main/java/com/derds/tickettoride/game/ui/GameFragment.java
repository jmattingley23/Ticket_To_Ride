package com.derds.tickettoride.game.ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.derds.models.DestinationCard;
import com.derds.models.PlayerColor;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.tickettoride.R;
import com.derds.tickettoride.chat.ui.ChatFragment;
import com.derds.tickettoride.game.GameController;
import com.derds.tickettoride.game.IGameController;
import com.derds.tickettoride.game.IUIController;
import com.derds.tickettoride.game.UIController;
import com.derds.tickettoride.gamehistory.ui.GameHistoryFragment;
import com.derds.tickettoride.gameresults.ui.GameResultsActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.derds.tickettoride.game.ui.GameActivity.GO_FULLSCREEN;

/**
 * A placeholder fragment containing a simple view.
 */
public class GameFragment extends Fragment {

    final int REQUEST_CODE_FINISH_GAME = 3937;

    private ImageView mCardDrawerHandle;
    private LinearLayout mHandLayout;
    private MapFragment mMapFragment;
    private ImageView mFalconCard;
    private ImageView mDestinationCard;
    private IUIController mController;

    private RecyclerView mCardRecyclerView;
    private TrainCardListAdapter mCardAdapter;

    private RecyclerView mDestinationCardRecycler;
    private DestinationCardListAdapter mDestAdapter;

    private RecyclerView mPlayerInfoRecycler;
    private PlayerInfoAdapter mPlayerInfoAdapter;

    private RecyclerView mTrainDeckRecycler;
    private TrainCardListAdapter mTrainDeckAdapter;
    private RelativeLayout mTrainDeckRelativeLayout;
    private ProgressBar mTrainProgressBar;

    private RecyclerView mDestinationDeck;
    private DestinationCardListAdapter mDestinationDeckAdapter;
    private ImageView mChatBtn;
    private ImageView mHistoryBtn;
    private IGameController gameController;

    private LinearLayout mClaimRouteLayout;
    private TextView mClaimRouteTextView;
    private Button mClaimButton;
    private Button mClaimCancelButton;

    private Vibrator vibrator;

    public GameFragment() {
    }

    public static GameFragment newInstance(){
        GameFragment gameFragment = new GameFragment();
        return gameFragment;
    }

    public void setmMapFragment(MapFragment mMapFragment) {
        this.mMapFragment = mMapFragment;
    }

    public void resetDestCardList(List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {
        List<Object> realUnpickedCards = new ArrayList<>();
        for (DestinationCard card : unpickedCards) {
            realUnpickedCards.add(card);
        }
        List<Object> realPickedCards = new ArrayList<>();
        for (DestinationCard card : pickedCards) {
            realPickedCards.add(card);
        }
        // TODO will this reset everything correctly?
//        mDestinationDeckAdapter.setPickedCards(unpickedCards);
//        mDestinationDeckAdapter.setmCards(realUnpickedCards);
//        mDestAdapter.setmCards(realUnpickedCards);
        mDestAdapter.setmCards(realPickedCards);
        mDestAdapter.setPickedCards(pickedCards);
    }

    public void switchToGameActivity() {
        Intent intent = new Intent(getActivity(), GameResultsActivity.class);
        startActivityForResult(intent,REQUEST_CODE_FINISH_GAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_game, container, false);
        mFalconCard = (ImageView) v.findViewById(R.id.train_card_selector_image);
        mDestinationCard = (ImageView) v.findViewById(R.id.destination_card_selector_image);
        mCardDrawerHandle = (ImageView) v.findViewById(R.id.card_drawer_handle);
        mHandLayout = (LinearLayout) v.findViewById(R.id.card_hand_layout);
        mChatBtn = (ImageView) v.findViewById(R.id.chat_button);
        mHistoryBtn = (ImageView) v.findViewById(R.id.history_button);

        mClaimRouteLayout = (LinearLayout) v.findViewById(R.id.claim_route_layout);
        mClaimRouteTextView = (TextView) v.findViewById(R.id.claim_route_description_text);
        mClaimButton = (Button) v.findViewById(R.id.claim_route_button);
        mClaimCancelButton = (Button) v.findViewById(R.id.cancel_claim_route_button);

        mTrainDeckRelativeLayout = (RelativeLayout) v.findViewById(R.id.train_deck_layout);
        mTrainProgressBar = (ProgressBar) v.findViewById(R.id.card_chosen_spinner);

        gameController = ((GameActivity) getActivity()).getGameController();
        gameController.getUIController().setGameFragment(this);
        mController = gameController.getUIController();

        setTrainCards(v);
        setDestinationCard(v);
        setPlayerViews(v);
        setTrainDeckRecycler(v);
        setDestinationDeck(v);

        mFalconCard.setColorFilter(0xFF000000, PorterDuff.Mode.SRC_ATOP);

        mCardDrawerHandle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mHandLayout.getVisibility() == View.GONE){
                    mHandLayout.setVisibility(View.VISIBLE);
                }else{
                    mHandLayout.setVisibility(View.GONE);
                }
            }
        });

        mFalconCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFalconCard.setColorFilter(0xFF000000, PorterDuff.Mode.SRC_ATOP);
                mDestinationCard.setColorFilter(null);
                mDestinationCardRecycler.setVisibility(View.GONE);
                mDestinationDeck.setVisibility(View.GONE);
                mCardRecyclerView.setVisibility(View.VISIBLE);
            }
        });

        mDestinationCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDestinationCard.setColorFilter(0xFF000000, PorterDuff.Mode.SRC_ATOP);
                mFalconCard.setColorFilter(null);
                mCardRecyclerView.setVisibility(View.GONE);
                mTrainDeckRecycler.setVisibility(View.GONE);
                mTrainDeckRecycler.setVisibility(View.GONE);
                hideClaimRoute();

                mDestinationCardRecycler.setVisibility(View.VISIBLE);
            }
        });

        mClaimButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mController.attemptClaimRoute(mCardAdapter.getSelected());
                mCardAdapter.clearSelected();
                mCardAdapter.notifyDataSetChanged();
                hideClaimRoute();
            }
        });

        mClaimCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hideClaimRoute();
                mController.cancelClaimRoute();
            }
        });

        mChatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameController.switchToFragment(ChatFragment.newInstance(gameController));
                mChatBtn.setColorFilter(null);
            }
        });


        mHistoryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameController.switchToFragment(GameHistoryFragment.newInstance(gameController));
            }
        });

        vibrator = (Vibrator)getActivity().getSystemService(Context.VIBRATOR_SERVICE);

        return v;
    }

    public List<TrainCard> getSelectedCards(){
        return mCardAdapter.getSelected();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        FragmentManager fragmentManager = getChildFragmentManager();
        mMapFragment = (MapFragment) fragmentManager.findFragmentById(R.id.map_fragment);
        if (mMapFragment == null) {
            mMapFragment = MapFragment.newInstance(getActivity());
            fragmentManager.beginTransaction().add(R.id.map_fragment, mMapFragment).commit();
        }

        mController = gameController.getUIController();
    }

    private void setTrainCards(View v){
        mCardRecyclerView = (RecyclerView) v.findViewById(R.id.card_lists_recycler_view);
        mCardRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        List<TrainCard> testCards = mController.getPlayerTrainHand();
        Collections.sort(testCards);
        List<Object> testICards = new ArrayList<>();

        //Add deck icon card, onclick listener set in adapter to show train deck
        testICards.add(new ShowDeckButton());

        testICards.addAll(testCards);
        mCardAdapter = new TrainCardListAdapter(testICards, getContext(), mController);
        mCardRecyclerView.setAdapter(mCardAdapter);
    }

    private void setDestinationCard(View v){
        mDestinationCardRecycler = (RecyclerView) v.findViewById(R.id.destination_card_recycler_view);
        mDestinationCardRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        List<Object> destCards = new ArrayList<>();
        destCards.add(new ShowDeckButton());
        destCards.addAll(mController.getPlayerDestinationCardHand());
        mDestAdapter = new DestinationCardListAdapter(destCards, getContext(), mController);
        mDestinationCardRecycler.setAdapter(mDestAdapter);
        mDestinationCardRecycler.setVisibility(View.GONE);
    }

    private void setPlayerViews(View v){
        mPlayerInfoRecycler = (RecyclerView) v.findViewById(R.id.player_info_recycler);
        mPlayerInfoRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        List<PlayerDisplayInfo> playerIds = new ArrayList<>();
        playerIds.addAll(mController.getPlayerInfo());
        mPlayerInfoAdapter = new PlayerInfoAdapter(playerIds, getContext());
        mPlayerInfoRecycler.setAdapter(mPlayerInfoAdapter);
    }

    private void setTrainDeckRecycler(View v){
        mTrainDeckRecycler = (RecyclerView) v.findViewById(R.id.train_deck_recycler_view);
        mTrainDeckRecycler.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        List<Object> deckCards = new ArrayList<>();
        deckCards.add(new TopOfDeckButton());
        deckCards.addAll(mController.getRiver());
        mTrainDeckAdapter = new TrainCardListAdapter(deckCards, getContext(), mController);
        mTrainDeckRecycler.setAdapter(mTrainDeckAdapter);
    }

    private void setDestinationDeck(View v){
        mDestinationDeck = (RecyclerView) v.findViewById(R.id.destination_deck_recycler_view);
        mDestinationDeck.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        List<Object> destRiver = new ArrayList<>();
        destRiver.add(new FinishButton());
        mDestinationDeckAdapter = new DestinationCardListAdapter(destRiver, getContext(), mController);
        mDestinationDeck.setAdapter(mDestinationDeckAdapter);
    }

    public void toggleTrainDeck(){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Check to see if TrainDeck is showing and hide it or show it
                    if(mTrainDeckRelativeLayout.getVisibility() == View.GONE){
                        mTrainDeckRelativeLayout.setVisibility(View.VISIBLE);
                        mTrainDeckRecycler.setVisibility(View.VISIBLE);
                    }else{
                        mTrainDeckRecycler.setVisibility(View.GONE);
                        mTrainDeckRelativeLayout.setVisibility(View.GONE);
                    }
                }
            });
        }

    }

    public void showTrainHand(){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCardRecyclerView.setVisibility(View.VISIBLE);
                    mTrainDeckRecycler.setVisibility(View.GONE);
                }
            });
        }
    }

    public void toggleDestinationDeck(){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //Check to see if DestinationDeck is showing and hide it or show it
                    if(mDestinationDeck.getVisibility() == View.GONE){
                        mDestinationDeck.setVisibility(View.VISIBLE);
                    }else{
                        mDestinationDeck.setVisibility(View.GONE);
                    }
                }
            });
        }

    }

    public void showDestinationDeck(){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDestinationDeck.setVisibility(View.VISIBLE);
                }
            });
        }

    }

    public void hideDestinationDeck(){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDestinationDeck.setVisibility(View.GONE);
                }
            });
        }

    }

    public void addCardToTrainHand(TrainCard card){
        mCardAdapter.addCardToHand(card);
        mCardAdapter.notifyDataSetChanged();
    }

    public void updateRiver(ArrayList<TrainCard> cards) {
        final List<Object> objectList = new ArrayList<>();
        objectList.addAll(cards);

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTrainDeckAdapter.updateList(objectList);
                    mTrainDeckAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void updateDestinationDeck(final List<DestinationCard> cards){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDestinationDeckAdapter.clearPickedCards();
                    List<Object> o = new ArrayList<>();
                    o.add(new FinishButton());
                    o.addAll(cards);
                    mDestinationDeckAdapter.updateList(o);
                    mDestinationDeckAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void updateHand(){
        final List<Object> objects = new ArrayList<>();
        objects.add(new ShowDeckButton());
        List<TrainCard> playerTrainCards = mController.getPlayerTrainHand();
        Collections.sort(playerTrainCards);
        objects.addAll(playerTrainCards);

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mCardAdapter.updateList(objects);
                    mCardAdapter.notifyDataSetChanged();
                    hideTrainProgressBar();
//                gameHistoryRecycler.smoothScrollToPosition(0);
                }
            });

        }
//        mCardRecyclerView.swapAdapter(mCardAdapter, true);
//        mCardRecyclerView.scrollBy(0, 0);

        final List<Object> objectList = new ArrayList<>();
        objectList.add(new ShowDeckButton());
        objectList.addAll(mController.getPlayerDestinationCardHand());

        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDestAdapter.updateList(objectList);
                    mDestAdapter.notifyDataSetChanged();
//                gameHistoryRecycler.smoothScrollToPosition(0);
                }
            });
        }
    }

    public void updateTrainDeck(){
        final ArrayList<Object> list = new ArrayList<>();
        list.add(new TopOfDeckButton());
        list.addAll(mController.getRiver());
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTrainDeckAdapter.updateList(list);
                    mTrainDeckAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void updatePeopleInfo(){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mPlayerInfoAdapter.updateList(mController.getPlayerInfo());
                    mPlayerInfoAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void swapDestinationCards(final DestinationCard card){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mDestinationDeckAdapter.getCards().contains(card)) {
                        mDestinationDeckAdapter.removeCard(card);
                        mDestinationDeckAdapter.notifyDataSetChanged();
                        mDestAdapter.addCard(card);
                        mDestAdapter.notifyDataSetChanged();
                    } else if (mDestAdapter.getCards().contains(card)) {
                        mDestAdapter.removeCard(card);
                        mDestAdapter.removePickedCard(card);
                        mDestAdapter.notifyDataSetChanged();
                        mDestinationDeckAdapter.addCard(card);
                        mDestinationDeckAdapter.notifyDataSetChanged();
                    }
                }
            });
        }

    }

    public void swapTrainCards(final TrainCard card){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (mTrainDeckAdapter.getCards().contains(card)) {
                        mTrainDeckAdapter.removeCard(card);
                        mTrainDeckAdapter.notifyDataSetChanged();
                        mCardAdapter.addCard(card);
                        mCardAdapter.notifyDataSetChanged();
                    } else if (mCardAdapter.getCards().contains(card)) {
                        //Do nothing
                    }
                }
            });
        }
    }

    public void clearDestinationCardDeck(){
        mDestinationDeckAdapter.updateList(new ArrayList<Object>());
        if(getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDestinationDeck.setVisibility(View.GONE);
                    mDestinationDeckAdapter.setPickedCards(new ArrayList<DestinationCard>());
                    mDestinationDeckAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    public void notifyPlayerOfTurn() {
        vibrator.vibrate(new long[]{0, 250, 250, 250}, -1);
    }

    public void hideBars(){
        mMapFragment.hideBars();
    }


    public IGameController getGameController() {
        return gameController;
    }

    public void setGameController(IGameController gameController) {
        this.gameController = gameController;
    }

    public void dismiss() {
        getFragmentManager().beginTransaction().remove(this).commit();
    }

    public void showTrainProgressBar(){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTrainProgressBar.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public void hideTrainProgressBar() {
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mTrainProgressBar.setVisibility(View.GONE);
                }
            });
        }
    }

    public void showClaimRoute(final Route selectedRoute){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mDestinationCardRecycler.setVisibility(View.GONE);
                    mDestinationDeck.setVisibility(View.GONE);
                    mTrainDeckRecycler.setVisibility(View.GONE);
                    mCardRecyclerView.setVisibility(View.VISIBLE);
                    mClaimRouteLayout.setVisibility(View.VISIBLE);
                    mClaimRouteTextView.setText(selectedRoute.getDescription());
                    mHandLayout.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public void hideClaimRoute(){
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mClaimRouteLayout.setVisibility(View.GONE);
                    mClaimRouteTextView.setText("");
                }
            });
        }
    }

    public void showIncorrectCardsSelected(){
        final Context fragmentContext = this.getContext();
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(fragmentContext, getString(R.string.incorrect_cards_selected), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void showNoMoreCardsMsg(){
        final Context fragmentContext = this.getContext();
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(fragmentContext, getString(R.string.no_more_cards), Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void setChatNotification(){
        final Context fragmentContext = this.getContext();
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mChatBtn.setColorFilter(PlayerColor.RED.getHexVal(), PorterDuff.Mode.SRC_ATOP);
                }
            });
        }
    }

    public void clearChatNotification(){
        final Context fragmentContext = this.getContext();
        if (getActivity() != null) {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mChatBtn.setColorFilter(null);
                }
            });
        }
    }

}
