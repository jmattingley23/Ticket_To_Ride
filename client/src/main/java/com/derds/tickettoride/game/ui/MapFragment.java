package com.derds.tickettoride.game.ui;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.derds.models.GameMap;
import com.derds.tickettoride.R;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.game.GameController;
import com.derds.tickettoride.game.IMapController;
import com.derds.tickettoride.game.MapController;

import static com.derds.tickettoride.game.ui.GameActivity.GO_FULLSCREEN;


/**
 * Created by mogard on 10/28/2017.
 */

public class MapFragment extends Fragment {
    private MapView mapView;
    private Activity activity;
    private IMapController mapController;

    public static MapFragment newInstance(Activity activity){
        MapFragment mapFragment = new MapFragment();
        mapFragment.setActivity(activity);
        mapFragment.initializeMap();
        return mapFragment;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_map, container, false);
//        LinearLayout content = (LinearLayout) v.findViewById(R.id.mapFragmentLayout);
//        content.addView(mapView);
        this.setActivity(getActivity());


        return v;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mapController = new MapController(getActivity());

        LinearLayout content = (LinearLayout) view.findViewById(R.id.mapFragmentLayout);
        this.initializeMap();
        mapController.setMapView(mapView);
        content.addView(mapView);
    }

    public void initializeMap(){
        mapView = new MapView(activity,null, mapController);
        mapView.drawMap(ClientModelFacade.getInstance().getGameMap());
    }

    public void hideBars(){
        mapView.setSystemUiVisibility(GO_FULLSCREEN);
    }




}
