package com.derds.tickettoride.game;

import android.util.Log;
import android.widget.Toast;

import com.derds.models.DestinationCard;
import com.derds.models.GameMetadata;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.tickettoride.async.AsyncMapTask;
import com.derds.tickettoride.async.AsyncUITask;
import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.game.ui.GameFragment;
import com.derds.tickettoride.game.ui.PlayerDisplayInfo;
import com.derds.tickettoride.gamecommands.ChooseDestCommand;
import com.derds.tickettoride.gamecommands.ClaimRouteCommand;
import com.derds.tickettoride.gamecommands.PickFaceupCardCommand;
import com.derds.tickettoride.gamecommands.RequestDrawDestCommand;
import com.derds.tickettoride.gamecommands.RequestDrawTrainCommand;
import com.derds.tickettoride.turnstate.BeginGameState;
import com.derds.tickettoride.turnstate.ChooseDestinationCardsState;
import com.derds.tickettoride.turnstate.DrawTrainCardState;
import com.derds.tickettoride.turnstate.StartTurnState;
import com.derds.tickettoride.turnstate.EndTurnState;
import com.derds.tickettoride.turnstate.WaitState;
import com.derds.turnstate.ITurnStateExecutable;
import com.derds.turnstate.TurnState;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by Aaron on 10/30/2017.
 */

public class UIController implements Observer, ITurnStateExecutable, IUIController {
    private static final String TAG = "UIController";
    private GameFragment gameFragment;
    private Route selectedRoute;

    public UIController(){
        ClientModelFacade.getInstance().addObserver(this);

        // Set executable for turn states\
        BeginGameState.instance.setTurnStateExecutable(this);
        ChooseDestinationCardsState.instance.setTurnStateExecutable(this);
        DrawTrainCardState.instance.setTurnStateExecutable(this);
        StartTurnState.instance.setTurnStateExecutable(this);
        EndTurnState.instance.setTurnStateExecutable(this);
        WaitState.instance.setTurnStateExecutable(this);
    }

    @Override
    public void hideBars(){
        gameFragment.hideBars();
    }

    @Override
    public Route getSelectedRoute() {
        return selectedRoute;
    }

    @Override
    public void showHideTrainDeck(){
        gameFragment.toggleTrainDeck();
    }

    @Override
    public void showHideDestinationDeck(){
        gameFragment.toggleDestinationDeck();
    }

    @Override
    public void showDestinationDeck(){
        gameFragment.showDestinationDeck();
    }

    @Override
    public void hideDestinationDeck(){
        gameFragment.hideDestinationDeck();
    }

    @Override
    public void showTrainProgressBar(){
        gameFragment.showTrainProgressBar();
    }

    @Override
    public void hideTrainProgressBar(){
        gameFragment.hideTrainProgressBar();
    }

    @Override
    public void hideClaimRoute(){
        gameFragment.hideClaimRoute();
    }

    @Override
    public List<TrainCard> getPlayerTrainHand(){
        return ClientModelFacade.getInstance().getPlayer().getHand().getPlayerTrainCards();
    }

    @Override
    public List<DestinationCard> getPlayerDestinationCardHand(){
        return ClientModelFacade.getInstance().getPlayer().getHand().getPlayerDestinationCards();
    }

    @Override
    public void getDestinationDeck(){
        ClientModelFacade.getInstance().getTurnState().drawDestinationCards(ClientModelFacade.getInstance().getPlayer());
    }

    @Override
    public void updatePlayerDestinationHand(List<DestinationCard> cards){
        for(DestinationCard card : cards) {
            if (!ClientModelFacade.getInstance().getPlayer().getHand().getPlayerDestinationCards().contains(card)) {
                ClientModelFacade.getInstance().getPlayer().getHand().addDestinationCard(card);
            }
        }
        gameFragment.updateHand();
    }

    @Override
    //Returns PlayerDisplayInfo objects for displaying info in UI
    public List<PlayerDisplayInfo> getPlayerInfo(){
        List<PlayerDisplayInfo> playerInfo = new ArrayList<>();
        GameMetadata data = ClientModelFacade.getInstance().getPlayer().getMyGame();
        List<String> playerIds = data.getPlayerIds();
        for(String playerId : playerIds){
            playerInfo.add(new PlayerDisplayInfo(
                    data.getPlayerNameFromId(playerId),
                    data.getNumTrainCardsLeft(playerId),
                    data.getNumDestCardsLeft(playerId),
                    data.getNumPiecesLeft(playerId),
                    data.ownsLongestRoute(playerId),
                    data.getPlayerPoints(playerId),
                    data.getColorByPlayerId(playerId),
                    data.isPlayersTurn(playerId)));
        }

        return playerInfo;
    }

    @Override
    public void moveDestinationToHand(DestinationCard card){
        gameFragment.swapDestinationCards(card);
    }

    @Override
    public void faceUpClickAttempt(TrainCard card){
        ClientModelFacade.getInstance().getTurnState().selectTrainCard(ClientModelFacade.getInstance().getPlayer(),card);
    }

    @Override
    public void deckClickAttempt(){
        if(ClientModelFacade.getInstance().getPlayer().getMyGame().getTrainDeckCount() > 0) {
            ClientModelFacade.getInstance().getTurnState().drawTrainCardFromDeck(ClientModelFacade.getInstance().getPlayer());
        }else{
            gameFragment.showNoMoreCardsMsg();
        }
    }

    @Override
    public void attemptClaimRoute(List<TrainCard> cardsUsed) {
        ClientModelFacade.getInstance().getTurnState().claimRoute(ClientModelFacade.getInstance().getPlayer(),selectedRoute,cardsUsed);
    }

    @Override
    public GameFragment getGameFragment() {
        return gameFragment;
    }

    @Override
    public void setGameFragment(GameFragment gameFragment) {
        this.gameFragment = gameFragment;
    }

    @Override
    public int getTrainDeckSize(){
        return ClientModelFacade.getInstance().getPlayer().getMyGame().getTrainDeckCount();
    }

    @Override
    public int getDestinationDeckSize(){
        return ClientModelFacade.getInstance().getPlayer().getMyGame().getDestinationDeckCount();
    }

    @Override
    public void clearDestinationCardDeck(){
        gameFragment.clearDestinationCardDeck();
    }

    //------------------------------------------------
    // Implementation of turnstate interface
    //------------------------------------------------

    @Override
    public void sendMessage(String message) {
        Toast.makeText(gameFragment.getActivity(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void claimRoute(String authToken, String playerId, Route route, List<TrainCard> cardsUsed, int trainPiecesUsed) {
        ClaimRouteCommand command = new ClaimRouteCommand(selectedRoute, cardsUsed, selectedRoute.getLength(), playerId);
        AsyncMapTask task = new AsyncMapTask();
        task.execute(command);
    }

    @Override
    public void drawTrainCardFromDeck(String authToken, String playerId, boolean canDrawMoreCards) {
        showTrainProgressBar();
        RequestDrawTrainCommand command = new RequestDrawTrainCommand(AuthToken.getInstance().getAuthToken(), ClientModelFacade.getInstance().getPlayerId());
        AsyncUITask asyncUITask = new AsyncUITask(this);
        asyncUITask.execute(command);
    }

    @Override
    public void drawFaceUpCard(String authToken, String playerId, TrainCard card, boolean canDrawMoreCards) {
        showTrainProgressBar();
        PickFaceupCardCommand command = new PickFaceupCardCommand(ClientModelFacade.getInstance().getPlayerId(), card);
        AsyncUITask asyncUITask = new AsyncUITask(this);
        asyncUITask.execute(command);
    }
    @Override
    public void claimDestinationCards(String authToken, String playerId, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned) {
        returnDestinationCards(cardsReturned, cardsChosen);
    }

    @Override
    public void failedClaimDestinationCards(String playerId, List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {
        gameFragment.resetDestCardList(pickedCards,unpickedCards);
    }

    @Override
    public void drawDestinationCardsRequest() {
        // TODO move this maybe
        RequestDrawDestCommand command = new RequestDrawDestCommand(ClientModelFacade.getInstance().getPlayerId());
        AsyncUITask myTask = new AsyncUITask(this);
        myTask.execute(command);
    }

    @Override
    public void drawDestinationCardsResponse(String playerId, List<DestinationCard> cards) {

    }

    @Override
    public void clearChatNotification(){
        gameFragment.clearChatNotification();
    }

    //------------------------------------------------
    // Client commands to ClientModelFacade
    //------------------------------------------------

    public void attemptClaimDestinationCards(List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {
        ClientModelFacade.getInstance().getTurnState().claimDestinationCards(ClientModelFacade.getInstance().getPlayer(),pickedCards, unpickedCards);
    }

    public List<TrainCard> getRiver(){
        List<TrainCard> river = ClientModelFacade.getInstance().getPlayer().getMyGame().getRiver().getShowingCards();
        return river;
    }

    public boolean returnDestinationCards(List<DestinationCard> unpickedCards, List<DestinationCard> pickedCards){
        //TODO: Make this return the cards the player doesn't want back to bottom of deck
        ChooseDestCommand command = new ChooseDestCommand(ClientModelFacade.getInstance().getPlayerId(),
                pickedCards, unpickedCards);
        AsyncUITask task = new AsyncUITask(this);
        task.execute(command);
        gameFragment.updateHand();
        return true;
    }

    public void sendToDiscard(List<TrainCard> discardedCards){
        //TODO: Make this return the played train cards to discard pile
    }

    public void cancelClaimRoute(){
        //ClientModelFacade.getInstance().getTurnState().startTurn(ClientModelFacade.getInstance().getPlayer());
        gameFragment.hideClaimRoute();
    }

    //------------------------------------------------
    // ClientModelFacade updates for client
    //------------------------------------------------

    @Override
    public void update(Observable o, Object arg) {
        if(arg instanceof String) {
            Log.v(TAG, "update from ClientModelFacade");
            String type = (String) arg;
            if (type.equals("DestinationCardsDrawn")) {
                gameFragment.updateHand();
            }
            if (type.equals("Player")) {
                gameFragment.updateHand();
                gameFragment.updatePeopleInfo();
            }
            if (type.equals("CardToHand")) {
                gameFragment.updateHand();
                gameFragment.updatePeopleInfo();
            }
            if (type.equals("TrainCardDrawn")) {
                gameFragment.updateHand();
                gameFragment.updateTrainDeck();
            }
            if (type.equals("RiverCardDrawn")) {
                gameFragment.updateHand();
                gameFragment.updateTrainDeck();
                //gameFragment.updateDestinationDeck();
            }
            if (type.equals("ScoreUpdated")) {
                gameFragment.updatePeopleInfo();
            }
            if (type.equals("CardsUsed")) {
                gameFragment.updateHand();
                gameFragment.updatePeopleInfo();
            }
            if (type.equals("RiverUpdated")) {
                gameFragment.updateHand();
                gameFragment.updateTrainDeck();
                hideTrainProgressBar();
            }
            if (type.equals("PiecesRemoved")) {
                gameFragment.updatePeopleInfo();
            }
            if (type.equals("DestCardAdd")) {
                clearDestinationCardDeck();
                gameFragment.updateHand();
            }
            //Another player has had their train card number is updated
            if (type.equals("AddTrainCardNumber")) {
                gameFragment.updatePeopleInfo();
            }
            //Another player has had their destination card number is updated
            if (type.equals("AddDestCardNumber")) {
                gameFragment.updatePeopleInfo();
            }
            if (type.equals("DestDeckNumber")) {
                gameFragment.updateHand();
            }
            if (type.equals("TrainDeckNumber")) {
                gameFragment.updateHand();
            }
            if (type.equals("TurnChanged")) {
                gameFragment.updatePeopleInfo();
//                if(ClientModelFacade.getInstance().getPlayer().getMyGame().isPlayersTurn(ClientModelFacade.getInstance().getPlayerId())) {
//                    Toast.makeText(getGameFragment().getActivity(),"It's your turn!", Toast.LENGTH_LONG).show();
//                } else {
//                    Toast.makeText(getGameFragment().getActivity(), "Please wait for your turn", Toast.LENGTH_SHORT).show();
//                }
            }
            if (type.equals("RouteClaimed")) {
                gameFragment.updatePeopleInfo();
            }
            if(type.equals("OtherPlayerHandUpdated")){
                gameFragment.updatePeopleInfo();
                gameFragment.updateHand();
            }
            // TODO remove this after testing is done
            if (type.equals("NewMessage")) {
                Log.d(TAG, "We got a new message!");
                gameFragment.setChatNotification();
            }
            if(type.equals("isPlayersTurn")) {
                gameFragment.notifyPlayerOfTurn();
            }
            if(type.equals("EndOfGame")) {
                gameFragment.switchToGameActivity();
            }
        }else if( arg instanceof List) {
            List<DestinationCard> cards = (List)arg;
            gameFragment.updateDestinationDeck(cards);
        }else if ( arg instanceof Route){
            StartTurnState.instance.setTurnStateExecutable(this);
            gameFragment.showClaimRoute((Route)arg);
            selectedRoute = (Route)arg;
        }
    }
}
