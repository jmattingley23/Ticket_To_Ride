package com.derds.tickettoride.game;

import android.support.v4.app.Fragment;

import com.derds.tickettoride.chat.ChatController;
import com.derds.tickettoride.chat.IChatController;
import com.derds.tickettoride.chat.ui.ChatFragment;
import com.derds.tickettoride.game.ui.GameActivity;
import com.derds.tickettoride.game.ui.GameFragment;
import com.derds.tickettoride.gamehistory.GameHistoryController;
import com.derds.tickettoride.gamehistory.IGameHistoryController;

/**
 * Created by Aaron on 11/25/2017.
 */

public interface IGameController {

    public void setUiControllerFragments(GameFragment gameFragment);

    public void setChatControllerChatFragment(ChatFragment chatFragment);

    public IChatController getChatController();

    public IUIController getUIController();

    public IMapController getMapController();

    public void switchToFragment(Fragment fragment);

    public GameActivity getGameActivity();

    public void setGameActivity(GameActivity gameActivity);

    public IGameHistoryController getGameHistoryController();

    public void setGameHistoryController(IGameHistoryController gameHistoryController);
}
