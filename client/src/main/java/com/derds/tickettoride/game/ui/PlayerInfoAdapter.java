package com.derds.tickettoride.game.ui;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.derds.tickettoride.R;

import java.util.List;

/**
 * Created by Aaron on 10/30/2017.
 */

public class PlayerInfoAdapter extends RecyclerView.Adapter<PlayerInfoAdapter.ViewHolder> {

    private List<PlayerDisplayInfo> mPlayerIds;
    private static View mView;
    private static Context mContext;

    public PlayerInfoAdapter(List<PlayerDisplayInfo> playerIds, Context context){
        mPlayerIds = playerIds;
        mContext = context;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mNumTrainCards;
        public TextView mNumTrainPieces;
        public TextView mNumDestinationCards;
        public ImageView mIsTurn;
        public TextView mNumPoints;
        public LinearLayout mPlayerPointsLayout;
        //public FrameLayout mPlayerPointsLayout;
        public LinearLayout mPlayerInfoLayout;
        public TextView mPlayerName;

        public ViewHolder(View v){
            super(v);
            mView = v;
            mNumTrainCards = (TextView) v.findViewById(R.id.num_train_cards);
            mNumTrainPieces = (TextView) v.findViewById(R.id.num_train_pieces);
            mNumDestinationCards = (TextView) v.findViewById(R.id.num_destination_cards);
            mIsTurn = (ImageView) v.findViewById(R.id.player_longest_path);
            mNumPoints = (TextView) v.findViewById(R.id.num_points);
            mPlayerPointsLayout = (LinearLayout) v.findViewById(R.id.player_points_layout);//.player_points_layout);
            mPlayerInfoLayout = (LinearLayout) v.findViewById(R.id.player_info_slider);
            mPlayerName = (TextView) v.findViewById(R.id.player_name);
        }
    }

    public void updateList(List<PlayerDisplayInfo> playerIds){
        this.mPlayerIds = playerIds;
    }

    @Override
    public PlayerInfoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.player_info, parent, false);

        return new PlayerInfoAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final PlayerInfoAdapter.ViewHolder holder, int position) {
        holder.mNumTrainCards.setText(String.valueOf(mPlayerIds.get(position).getNumTrainCards()));
        holder.mNumPoints.setText(String.valueOf(mPlayerIds.get(position).getNumPoints()));
        holder.mNumDestinationCards.setText(String.valueOf(mPlayerIds.get(position).getNumDestinationCards()));
        holder.mNumTrainPieces.setText(String.valueOf(mPlayerIds.get(position).getNumTrainPieces()));
        holder.mPlayerInfoLayout.setVisibility(View.GONE);
        Drawable roundedTabs = ContextCompat.getDrawable(mContext, R.drawable.rounded_taps_background);
        roundedTabs.setColorFilter(mPlayerIds.get(position).getColor().getHexVal(), PorterDuff.Mode.SRC_ATOP);
        holder.mPlayerPointsLayout.setBackground(roundedTabs);
        holder.mPlayerName.setText(mPlayerIds.get(position).getName());
        if(mPlayerIds.get(position).getTurn()){
            holder.mIsTurn.setVisibility(View.VISIBLE);
        }else{
            holder.mIsTurn.setVisibility(View.INVISIBLE);
        }
        holder.mPlayerPointsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(holder.mPlayerInfoLayout.getVisibility() == View.GONE){
                    holder.mPlayerInfoLayout.setVisibility(View.VISIBLE);
                }else{
                    holder.mPlayerInfoLayout.setVisibility(View.GONE);
                }
            }
        });
    }

    @Override
    public int getItemCount(){
        return mPlayerIds.size();
    }

}
