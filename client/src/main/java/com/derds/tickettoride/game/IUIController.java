package com.derds.tickettoride.game;

import com.derds.models.DestinationCard;
import com.derds.models.GameMetadata;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.game.ui.GameFragment;
import com.derds.tickettoride.game.ui.PlayerDisplayInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aaron on 11/25/2017.
 */

public interface IUIController {

    public void hideBars();

    public Route getSelectedRoute();

    public void showHideTrainDeck();

    public void showHideDestinationDeck();

    public void showDestinationDeck();

    public void hideDestinationDeck();

    public void showTrainProgressBar();

    public void hideTrainProgressBar();

    public void hideClaimRoute();

    public List<TrainCard> getPlayerTrainHand();

    public List<DestinationCard> getPlayerDestinationCardHand();

    public void getDestinationDeck();

    public void updatePlayerDestinationHand(List<DestinationCard> cards);

    public void cancelClaimRoute();

    public List<TrainCard> getRiver();

    public void attemptClaimDestinationCards(List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards);

    //Returns PlayerDisplayInfo objects for displaying info in UI
    public List<PlayerDisplayInfo> getPlayerInfo();

    public void moveDestinationToHand(DestinationCard card);

    public void faceUpClickAttempt(TrainCard card);

    public void deckClickAttempt();

    public void attemptClaimRoute(List<TrainCard> cardsUsed);

    public GameFragment getGameFragment();

    public void setGameFragment(GameFragment gameFragment);

    public int getTrainDeckSize();

    public int getDestinationDeckSize();

    public void clearDestinationCardDeck();

    public void clearChatNotification();
}
