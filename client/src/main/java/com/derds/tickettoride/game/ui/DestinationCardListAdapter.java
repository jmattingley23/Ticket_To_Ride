package com.derds.tickettoride.game.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.derds.models.City;
import com.derds.models.CityPair;
import com.derds.models.DestinationCard;
import com.derds.tickettoride.R;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.game.IUIController;
import com.derds.tickettoride.game.UIController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by Aaron on 10/30/2017.
 */

public class DestinationCardListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> mCards;
    private static View mView;
    private static Context mContext;
    private IUIController controller;
    private List<DestinationCard> pickedCards;
    private static final int DRAW = 0;
    private static final int DESTINATION = 1;
    private static final int FINISH = 2;
    private boolean isDeck = false;

    public DestinationCardListAdapter(List<Object> cards, Context context, IUIController controller){
        mCards = cards;
        mContext = context;
        this.controller = controller;
        this.pickedCards = new ArrayList<>();
    }

    public void setmCards(List<Object> mCards) {
        this.mCards = mCards;
    }

    public void setPickedCards(List<DestinationCard> pickedCards) {
        this.pickedCards = pickedCards;
    }

    public void clearPickedCards() {
        pickedCards = new ArrayList<>();
    }

    public static class ViewHolderDestination extends RecyclerView.ViewHolder {
        public TextView mCardString;
        public TextView mCardPoints;
        public CardView mCardView;
        public ViewHolderDestination(View v){
            super(v);
            mView = v;
            mCardString = (TextView) v.findViewById(R.id.destination_card_text_view);
            mCardPoints = (TextView) v.findViewById(R.id.destination_card_points);
            mCardView = (CardView) v.findViewById(R.id.destination_card_view);
        }
    }

    public static class ViewHolderAdd extends RecyclerView.ViewHolder {
        public ImageView mAddImage;
        public TextView mDeckCount;
        public ViewHolderAdd (View v){
            super(v);
            mAddImage = (ImageView) v.findViewById(R.id.card_image_view);
            mDeckCount = (TextView) v.findViewById(R.id.deck_count_text_view);
        }
    }

    public static class ViewHolderFinish extends RecyclerView.ViewHolder {
        public ImageView mFinishImage;
        public CardView mCardView;
        public ViewHolderFinish (View v){
            super(v);
            mFinishImage = (ImageView) v.findViewById(R.id.card_image_view);
            mCardView = (CardView) v.findViewById(R.id.train_card_view);
        }
    }

    // TODO this does the same thing as setmCards, refactor to only do one
    public void updateList(List<Object> cards){
        this.mCards = cards;
    }

    public List<Object> getCards(){
        return mCards;
    }

    public void removeCard(DestinationCard card){
        mCards.remove(card);
    }

    public void removePickedCard(DestinationCard card) {
        pickedCards.remove(card);
    }

    public void addCard(DestinationCard card){
        mCards.add(card);
    }

    @Override
    public int getItemViewType(int position) {
        if (mCards.get(position) instanceof ShowDeckButton) {
            return DRAW;
        } else if(mCards.get(position) instanceof FinishButton) {
            isDeck = true;
            return FINISH;
        } else {
            return DESTINATION;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case DRAW:
                //Draw card icon
                View v = inflater.inflate(R.layout.train_card, parent, false);
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        controller.getDestinationDeck();
                        controller.showDestinationDeck();
                    }
                });
                viewHolder = new ViewHolderAdd(v);
                break;
            case FINISH:
                View v1 = inflater.inflate(R.layout.train_card, parent, false);
                viewHolder = new ViewHolderFinish(v1);
                break;
            default:
                //Destination card
                View v2 = inflater.inflate(R.layout.destination_card, parent, false);
                viewHolder = new ViewHolderDestination(v2);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        switch(holder.getItemViewType()){
            case(DRAW):
                //Draw card item
                ViewHolderAdd vha = (ViewHolderAdd) holder;
                Drawable addSymbol = ContextCompat.getDrawable(mContext, R.drawable.add_card_to_hand);
                vha.mAddImage.setImageDrawable(addSymbol);
                vha.mDeckCount.setText(String.valueOf(ClientModelFacade.getInstance().getPlayer().getMyGame().getDestinationDeckCount()));
                vha.mDeckCount.setVisibility(View.VISIBLE);
                break;
            case(FINISH):
                //Finish choosing item
                ViewHolderFinish vhf = (ViewHolderFinish) holder;
                vhf.mFinishImage.setImageDrawable(ContextCompat.getDrawable(mContext, R.drawable.finish_icon));
                vhf.mCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        List<DestinationCard> unpickedCards = new ArrayList<>();
                        for (Object card : mCards) {
                            if (card instanceof DestinationCard && !pickedCards.contains(card)) {
                                unpickedCards.add((DestinationCard) card);
                            }
                        }
                        controller.attemptClaimDestinationCards(pickedCards, unpickedCards);
                        clearPickedCards();

                    }
                });
                break;
            default:
                //Destination card item
                final ViewHolderDestination vhd = (ViewHolderDestination) holder;
                CityPair cities = ((DestinationCard)mCards.get(position)).getCities();
                int points = ((DestinationCard)mCards.get(position)).getPointValue();
                vhd.mCardString.setText(cities.getCity1().getName() + " to " + cities.getCity2().getName());
                vhd.mCardPoints.setText(String.valueOf(points));
                vhd.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext,R.color.black));
                vhd.mCardView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(isDeck) {
                            if (pickedCards.contains((DestinationCard) mCards.get(position))) {
                                pickedCards.remove((DestinationCard) mCards.get(position));
                                vhd.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.black));
                            } else {
                                pickedCards.add((DestinationCard) mCards.get(position));
                                vhd.mCardView.setCardBackgroundColor(ContextCompat.getColor(mContext, R.color.card_highlight));
                            }
                        }
                    }
                });
                break;
        }
    }

    @Override
    public int getItemCount(){
        return mCards.size();
    }
}
