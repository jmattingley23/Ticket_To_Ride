package com.derds.tickettoride.game.ui;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;

import com.derds.models.City;
import com.derds.models.GameMap;
import com.derds.models.MapPoint;
import com.derds.models.PointPair;
import com.derds.models.Route;
import com.derds.tickettoride.R;
import com.derds.models.TrainColor;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.game.IMapController;
import com.derds.tickettoride.game.MapController;

import java.util.List;
import java.util.Map;

import static com.derds.tickettoride.game.ui.GameActivity.GO_FULLSCREEN;


/**
 * Created by mogard on 10/20/2017.
 */

public class MapView extends SurfaceView {

    private Paint paint;
    private static Bitmap original;
    private IMapController mapController;
    private Activity activity;

    public static final double BASE_WIDTH = 1024;
    public static final double BASE_HEIGHT = 552;


    public MapView(Context context, @Nullable AttributeSet attrs, final IMapController controller) {
        super(context, attrs);
        this.mapController = controller;
        activity = (Activity) context;

        this.setSystemUiVisibility(GO_FULLSCREEN);

        if(original == null) {
            original = BitmapFactory.decodeResource(context.getResources(), R.drawable.background);
        }
        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        this.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:

                        //if is player turn
                        double x =  event.getX();
                        double y =  event.getY();
                        double z = x + y;

                        Point point = getRealSize();
                        double width = point.x;
                        double height = point.y;

                        x = x * (BASE_WIDTH/width);
                        y = y * (BASE_HEIGHT/height);

                        Route route = controller.getRouteClicked(new MapPoint((float)x,(float)y));

                        if(route != null) {
                            if (mapController.isDoubleValid(route)) {
                                if (!(route.isClaimed())) {
                                    controller.selectRoute(route);
                                }
                            } else {
                                mapController.showErrorMessage("Can't claim that route");
                            }
                        }

                        Log.d("X:",Double.toString(x) + '\n');
                        Log.d("Y: ",Double.toString(y) + '\n');

                        break;
                }

                return true;
            }
        });
    }

    public Point getRealSize(){
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point point = new Point();
        display.getRealSize(point);
        return point;
    }

    public void drawMap(GameMap gameMap){
        setBackgroundResource(0);
        if(gameMap == null) {gameMap = new GameMap();}
        Bitmap b = Bitmap.createScaledBitmap(original,(int)BASE_WIDTH,(int)BASE_HEIGHT,true);
        Canvas bitCanvas = new Canvas(b);

        drawRoutes(bitCanvas,gameMap);
        drawNames(bitCanvas,gameMap);
        setBackground(new BitmapDrawable(getResources(),b));
    }


    private void drawRoutes(Canvas canvas, GameMap gameMap){
        Paint lengthPaint = new Paint();
        lengthPaint.setColor(Color.WHITE);
        lengthPaint.setTextSize(12f);
        lengthPaint.setTextAlign(Paint.Align.CENTER);
        paint.setStrokeWidth(10f);
        List<Route> routes = gameMap.getRoutes();

        for(Route route : routes){
            int color = route.getDrawColor();
            PointPair points = route.getCoordinates();
            MapPoint mid = route.getMidPoint();
            paint.setColor(color);
            canvas.drawLine(points.p1.x,points.p1.y,points.p2.x,points.p2.y,paint);

            if(!route.isClaimed()) {
                if (color == TrainColor.WHITE.getHexVal() || color == TrainColor.YELLOW.getHexVal() || color == TrainColor.GREEN.getHexVal()) {
                    lengthPaint.setColor(Color.BLACK);
                }
                Rect r = new Rect();
                lengthPaint.getTextBounds(Integer.toString(route.getLength()),0,1,r);
                float offset = (float) (r.height()/2.0);
                canvas.drawText(Integer.toString(route.getLength()), mid.x, mid.y+offset, lengthPaint);
                lengthPaint.setColor(Color.WHITE);
            }
        }
    }

    private void drawNames(Canvas canvas, GameMap gameMap){
        Paint textPaint = new Paint();
        textPaint.setTextSize(12f);
        textPaint.setColor(Color.WHITE);
        textPaint.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.BOLD));

        Map<String,City> cities = gameMap.getCities();

        for(String name : cities.keySet()){
            City city = cities.get(name);
            canvas.drawText(name,city.getX(),city.getY(),textPaint);
        }
    }

    public void setMapController(IMapController mapController) {
        this.mapController = mapController;
    }

}
