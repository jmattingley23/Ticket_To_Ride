package com.derds.tickettoride.game.ui;

import com.derds.models.PlayerColor;

/**
 * Created by Aaron on 10/31/2017.
 */

public class PlayerDisplayInfo {

    private String name;
    private int numTrainCards;
    private int numDestinationCards;
    private int numTrainPieces;
    private int numPoints;
    private Boolean longestRoute;
    private PlayerColor color;
    private Boolean isTurn;

    public PlayerDisplayInfo(String name, int numTrainCards,
                             int numDestinationCards, int numTrainPieces, Boolean longestRoute,
                             int numPoints, PlayerColor color, Boolean isTurn){
        this.name = name;
        this.numTrainCards = numTrainCards;
        this.numDestinationCards = numDestinationCards;
        this.numTrainPieces = numTrainPieces;
        this.longestRoute = longestRoute;
        this.numPoints = numPoints;
        this.color = color;
        this.isTurn = isTurn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumTrainCards() {
        return numTrainCards;
    }

    public void setNumTrainCards(int numTrainCards) {
        this.numTrainCards = numTrainCards;
    }

    public int getNumDestinationCards() {
        return numDestinationCards;
    }

    public void setNumDestinationCards(int numDestinationCards) {
        this.numDestinationCards = numDestinationCards;
    }

    public int getNumTrainPieces() {
        return numTrainPieces;
    }

    public void setNumTrainPieces(int numTrainPieces) {
        this.numTrainPieces = numTrainPieces;
    }

    public Boolean getLongestRoute() {
        return longestRoute;
    }

    public void setLongestRoute(Boolean longestRoute) {
        this.longestRoute = longestRoute;
    }


    public int getNumPoints() {
        return numPoints;
    }

    public void setNumPoints(int numPoints) {
        this.numPoints = numPoints;
    }

    public PlayerColor getColor() {
        return color;
    }

    public void setColor(PlayerColor color) {
        this.color = color;
    }

    public Boolean getTurn() {
        return isTurn;
    }

    public void setTurn(Boolean turn) {
        isTurn = turn;
    }
}
