package com.derds.tickettoride.common;

import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.DeleteGameCmdData;
import com.derds.cmddata.JoinGameCmdData;
import com.derds.cmddata.LoginCmdData;
import com.derds.cmddata.RefreshLobbyCmdData;
import com.derds.cmddata.RegisterCmdData;
import com.derds.cmddata.RequestQueueCmdData;
import com.derds.cmddata.StartGameCmdData;
import com.derds.cmddata.UpdateFirebaseTokenCmdData;
import com.derds.gamecommand.AbstractGameCommand;
import com.derds.gamecommand.AbstractSyncGameCommand;
import com.derds.models.DestinationCard;
import com.derds.models.Message;
import com.derds.models.Player;
import com.derds.models.PlayerColor;
import com.derds.models.River;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.result.GameResult;
import com.derds.result.LoginResult;
import com.derds.result.RefreshLobbyResult;
import com.derds.result.Result;
import com.derds.serverfacade.IServerFacade;
import com.derds.tickettoride.gamecommands.ChooseDestCommand;
import com.derds.tickettoride.gamecommands.ClaimRouteCommand;
import com.derds.tickettoride.gamecommands.GetChatHistoryCommand;
import com.derds.tickettoride.gamecommands.InitGameCommand;
import com.derds.tickettoride.gamecommands.PickFaceupCardCommand;
import com.derds.tickettoride.gamecommands.RequestDrawDestCommand;
import com.derds.tickettoride.gamecommands.RequestDrawTrainCommand;
import com.derds.tickettoride.gamecommands.SendChatCommand;
import com.derds.tickettoride.gamecommands.SyncGameCommand;
import com.derds.tickettoride.gamecommands.UpdatePointsCommand;
import com.derds.tickettoride.gamecommands.UpdateRiverCommand;

import java.util.List;

/**
 * Created by justinbrunner on 9/29/17.
 */

public class ServerProxy implements IServerFacade {
    private static ServerProxy serverProxy;

    private ServerProxy() {}

    public static ServerProxy getInstance() {
        if (serverProxy == null) {
            serverProxy = new ServerProxy();
        }

        return serverProxy;
    }

    @Override
    public LoginResult login(String username, String password, String firebaseToken) {
        LoginCmdData request = new LoginCmdData(username, password, firebaseToken);
        LoginResult myResult = (LoginResult)ClientCommunicator.getInstance().sendRequest(AuthToken.getInstance().getAuthToken(), request, LoginResult.class, "/execCommand");
        if(myResult == null) {
            myResult = new LoginResult("Unable to connect to server");
        }
        AuthToken.getInstance().setAuthToken(myResult.getAuthToken());
        return myResult;
    }

    @Override
    public LoginResult register(String username, String password, String firstName, String lastName, String firebaseToken) {
        RegisterCmdData request = new RegisterCmdData(username, password, firstName, lastName, firebaseToken);
        LoginResult myResult = (LoginResult)ClientCommunicator.getInstance().sendRequest(AuthToken.getInstance().getAuthToken(), request, LoginResult.class, "/execCommand");
        if(myResult == null) {
            myResult = new LoginResult("Unable to connect to server");
        }
        AuthToken.getInstance().setAuthToken(myResult.getAuthToken());
        return myResult;
    }

    @Override
    public GameResult startGame(String authToken, String gameID) {
        StartGameCmdData request = new StartGameCmdData(gameID);
        GameResult myResult = (GameResult)ClientCommunicator.getInstance().sendRequest(authToken, request, GameResult.class, "/execCommand");
        if(myResult == null) {
            myResult = new GameResult("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public GameResult joinGame(String authToken, String gameID, PlayerColor color) {
        JoinGameCmdData request = new JoinGameCmdData(gameID, color);

        GameResult myResult = (GameResult)ClientCommunicator.getInstance().sendRequest(authToken, request, GameResult.class, "/execCommand");
        if(myResult == null) {
            myResult = new GameResult("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public GameResult createGame(String authToken, String gameName, PlayerColor color) {
        CreateGameCmdData request = new CreateGameCmdData(gameName, color);

        GameResult myResult = (GameResult)ClientCommunicator.getInstance().sendRequest(authToken, request, GameResult.class, "/execCommand");
        if(myResult == null) {
            myResult = new GameResult("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result deleteGame(String authToken, String gameID) {
        DeleteGameCmdData request = new DeleteGameCmdData(gameID);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public RefreshLobbyResult refreshLobby(String authToken) {
        RefreshLobbyCmdData request = new RefreshLobbyCmdData();
        RefreshLobbyResult myResult = (RefreshLobbyResult)ClientCommunicator.getInstance().sendRequest(authToken, request, RefreshLobbyResult.class, "/execCommand");
        if(myResult == null) {
            myResult = new RefreshLobbyResult("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result updateFirebaseToken(String authToken, String firebaseToken) {
        UpdateFirebaseTokenCmdData request = new UpdateFirebaseTokenCmdData(firebaseToken);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result getQueueHistory(String authToken, int lastHistoryId, String playerId, String gameId) {
        RequestQueueCmdData request = new RequestQueueCmdData(lastHistoryId, playerId, gameId);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    // game commands

    @Override
    public Result chooseDestination(String authToken, String playerId, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned) {
        ChooseDestCommand request = new ChooseDestCommand(playerId,cardsChosen,cardsReturned);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result claimRoute(String authToken, String playerId, Route route, List<TrainCard> cardsUsed, int trainPiecesUsed) {
        ClaimRouteCommand request = new ClaimRouteCommand(route, cardsUsed, trainPiecesUsed, playerId);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    //Don't use on client!!
    public Result drawDestination(String authToken, String playerId, List<DestinationCard> cardsToPickFrom, boolean areCardsDrawn) {
        return null;
    }

    @Override
    public Result initGame(String authToken, String playerId, int turnOrder, Player player) {
        InitGameCommand request = new InitGameCommand(playerId, turnOrder, player);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result pickFaceupCard(String authToken, String playerId, TrainCard cardDrawn) {
        PickFaceupCardCommand request = new PickFaceupCardCommand(playerId, cardDrawn);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result requestDrawDestination(String authToken, String playerId) {
        RequestDrawDestCommand request = new RequestDrawDestCommand(playerId);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result requestDrawTrain(String authToken, String playerId) {
        RequestDrawTrainCommand request = new RequestDrawTrainCommand(authToken, playerId);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result sendChat(String authToken, Message content) {
        SendChatCommand request = new SendChatCommand(content.getPlayerId(), content);
        Result myResult = (Result) ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result updatePoints(String authToken, String playerId, int newPointsTotal, String playerIdToUpdate) {
        UpdatePointsCommand request = new UpdatePointsCommand(playerId, newPointsTotal, playerIdToUpdate);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result updateRiver(String authToken, String playerId, River newRiver) {
        UpdateRiverCommand request = new UpdateRiverCommand(playerId, newRiver);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    // TODO REFACTOR ALL THIS REPEATED CODE
    @Override
    public Result getChatHistory(String authToken, String playerId, int lastCommandId, List<Message> messageHistory, int skip) {
        GetChatHistoryCommand request = new GetChatHistoryCommand(playerId,lastCommandId,null,skip);    // we don't need the messageHistory here that's what we are getting
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public Result syncGame(String authToken, String playerId, String gameId) {
        // set most fields to null because we don't set them on the client side
        SyncGameCommand request = new SyncGameCommand(playerId, gameId, null, -1, null);
        Result myResult = (Result)ClientCommunicator.getInstance().sendRequest(authToken, request, Result.class, "/execCommand");
        if(myResult == null) {
            myResult = new Result("Unable to connect to server");
        }
        return myResult;
    }

    @Override
    public SyncGameCommand getSyncData(String playerId) {
        SyncGameCommand command = (SyncGameCommand)ClientCommunicator.getInstance().sendRequest(AuthToken.getInstance().getAuthToken(), playerId, SyncGameCommand.class, "/getSyncData");
        return command;
    }
}
