package com.derds.tickettoride.common;

import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.TreeMap;

import com.derds.gamecommand.AbstractGameCommand;
import com.derds.models.DestinationCard;
import com.derds.models.GameMap;
import com.derds.models.LobbyGameMetadata;
import com.derds.models.Message;
import com.derds.models.Player;
import com.derds.models.PlayerStats;
import com.derds.models.River;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.result.Result;
import com.derds.tickettoride.turnstate.BeginGameState;
import com.derds.tickettoride.turnstate.ChooseDestinationCardsState;
import com.derds.tickettoride.turnstate.DrawTrainCardState;
import com.derds.tickettoride.turnstate.StartTurnState;
import com.derds.tickettoride.turnstate.EndTurnState;
import com.derds.tickettoride.turnstate.WaitState;
import com.derds.turnstate.TurnState;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by drc95 on 9/27/17.
 */

public class ClientModelFacade extends Observable {
    private static ClientModelFacade ourInstance;
    private static final String TAG = "ClientModelFacade";

    public static ClientModelFacade getInstance() {
        Log.v(TAG,"getInstance() called");
        if(ourInstance == null) {
            ourInstance = new ClientModelFacade();
        }
        return ourInstance;
    }

    private Player player;
    // TODO fix the naming on these to reflect the gui naming
    private List<LobbyGameMetadata> openGames = new ArrayList<>();
    private List<LobbyGameMetadata> joinedGames = new ArrayList<>();
    private List<LobbyGameMetadata> ownedGames = new ArrayList<>();
    private FirebaseMessageHandler fbManager;
    private int responseCode;
    private List<Message> messageHistory = new ArrayList<>();
    private List<String> gameHistory = new ArrayList<>();
    private List<AbstractGameCommand> commandsRecieved = new ArrayList<>();
    private int lastCommandSequenceId = -1; // this should be invalid
    private int myTurnOrder = -1;
    private String gameId;
    private String playerId;
    private GameMap gameMap;
    private static Map<TurnState.StateName, TurnState> turnStates = new HashMap<>();
    private List<PlayerStats> playerStatsList = new ArrayList<>();

    static {
        turnStates = new TreeMap<>();
        turnStates.put(EndTurnState.key, EndTurnState.instance);
        turnStates.put(BeginGameState.key, BeginGameState.instance);
        turnStates.put(StartTurnState.key, StartTurnState.instance);
        turnStates.put(ChooseDestinationCardsState.key, ChooseDestinationCardsState.instance);
        turnStates.put(DrawTrainCardState.key, DrawTrainCardState.instance);
        turnStates.put(WaitState.key, WaitState.instance);
    }

    public GameMap getGameMap() {
        return gameMap;
    }

    public void setGameMap(GameMap gameMap) {
        if(gameMap != null) {
            this.gameMap = gameMap;
            setChanged();
            notifyObservers("GameMapUpdate");
        } else {
            Log.e(TAG,"Attempt to update game map with a null reference.");
        }
    }

    private ClientModelFacade() {
        Log.v(TAG, "Constructor");
        fbManager = new FirebaseMessageHandler();
        gameMap = new GameMap();


    }

    public void endGame(List<PlayerStats> playerStatsList) {
        this.playerStatsList = playerStatsList;
        setChanged();
        notifyObservers("EndOfGame");
    }

    public List<PlayerStats> getPlayerStatsList() {
        return playerStatsList;
    }

    public void notifyTurn() {
        setChanged();
        notifyObservers("isPlayersTurn");
    }

    public TurnState getTurnState() {
        return turnStates.get(player.getTurnState());
    }

    public void setTurnState(TurnState.StateName turnState) {
        if(player != null) {
            player.setTurnState(turnState);
        }
    }

    public String getStateName() {
        return turnStates.get(player.getTurnState()).getClass().getSimpleName();
    }

    public void reset() {
//        ourInstance = new ClientModelFacade();
        openGames = new ArrayList<>();
        joinedGames = new ArrayList<>();
        ownedGames = new ArrayList<>();
        messageHistory = new ArrayList<>();
        gameHistory = new ArrayList<>();
        commandsRecieved = new ArrayList<>();
        lastCommandSequenceId = -1; // this should be invalid
        myTurnOrder = -1;
        player = null;
        playerStatsList = new ArrayList<>();
    }

    public int getLastCommandSequenceId() {
        return lastCommandSequenceId;
    }

    public void setLastCommandSequenceId(int lastCommandSequenceId) {
        this.lastCommandSequenceId = lastCommandSequenceId;
    }

    public void incrementLastCommandSequenceId() {
        lastCommandSequenceId++;
    }

    public List<Message> getMessageHistory() {
        return messageHistory;
    }

    /**
     * @pre new_player is a valid player
     * @post player object is updated with new information and observers have been notified
     * @param new_player
     */
    public void updatePlayer(Player new_player) {
        Log.v(TAG, "Entering updatePlayer");
        if(player == null || !player.equals(new_player)) {
            player = new_player;
            //int numTrainCards = player.getHand().getTrainCardCount();
            //player.getMyGame().addTrainCards(player.getPlayerId(),numTrainCards);
            player.setHand(new_player.getHand());
            setTurnState(player.getTurnState());
            setChanged();
            notifyObservers("Player");
        }
        Log.v(TAG, "Exiting updatePlayer");
    }

    public List<TrainCard> getCardsUsed(Route route){
        if(player.canClaimRoute(route.getColor(),route.getLength())){
            return player.getCards(route.getColor(),route.getLength());
        }
        else{
            return null;
        }
    }


    public void notifyError(Result result) {
        setChanged();
        notifyObservers(result);
    }

    /**
     * @pre newList is full of valid games
     * @post list of available games is updated and observers have been notified
     * @param newOpenGames
     * @param newCurrentGames
     * @param newPlayerGames
     */
    public void updateGameLists(List<LobbyGameMetadata> newOpenGames, List<LobbyGameMetadata> newCurrentGames, List<LobbyGameMetadata> newPlayerGames) {
        Log.v(TAG, "Entering updateGamesList");
        openGames = newOpenGames;
        joinedGames = newCurrentGames;
        ownedGames = newPlayerGames;
        // Tell controllers that we got new games list
        setChanged();
        notifyObservers("GameList");
        Log.v(TAG, "Exiting updatePlayer");
    }

    public void addChatMessage(Message message) {
        messageHistory.add(message);
        setChanged();
        notifyObservers("NewMessage");
    }

    public void updateChatMessageHistory(List<Message> messages) {
        messageHistory = messages;

    }

    public void drawDestinationCards(List<DestinationCard> cards) {
        setChanged();
        notifyObservers(cards); // the observers now have to handle this
    }

    public void subtractDestinationCards(int numCardsDrawn) {
        player.getMyGame().decreaseDestinationCount(numCardsDrawn);
        setChanged();
        notifyObservers("DestinationCardsDrawn");
    }

    public void removeDestinationCardFromHand(DestinationCard card) {
        player.getHand().removeDestinationCard(card);
        setChanged();
        notifyObservers("RemoveDestination");
    }

    public void mapUpdated(){
        setChanged();
        notifyObservers();
    }

    public void setGameReady() {
        setChanged();
        notifyObservers("GameReady");
    }



    public void addTrainCardToHand(TrainCard card) {
        player.addCardToHand(card);
        player.getMyGame().addTrainCards(getPlayerId(),1);
        setChanged();
        notifyObservers("CardToHand");
    }

    public void decreaseTrainDeckCount(int amount){
        player.getMyGame().decreaseTrainDeckCount(amount);
        setChanged();
        notifyObservers("TrainCardDrawn");
    }

//    public void addDestCardsToPlayer(int numCards, String playerId) {
//        player.getMyGame().addDestinationCards(playerId,numCards);
//        notifyObservers("CardToHand");
//    }

    public void trainCardDrawn() {
        player.getMyGame().decreaseTrainDeckCount(1);   // drawing a card only takes one card from the deck
        setChanged();
        notifyObservers("TrainCardDrawn");
    }

    public String getNameFromId(String id) {
        return player.getMyGame().getPlayerNameFromId(id);
    }

    public void riverCardDrawn(TrainCard drawnCard, TrainCard newRiverCard) {
        player.getMyGame().getRiver().drawCard(drawnCard);
        if(newRiverCard != null) {
            player.getMyGame().getRiver().addCard(newRiverCard);
        }
        setChanged();
        notifyObservers("RiverCardDrawn");
    }

    public void claimRouteAttempt(Route route){
        setChanged();
        notifyObservers(route);
    }

    public void updateNumberOfTrainCards(String playerId, int numCardDifference){
        getPlayer().getMyGame().addTrainCards(playerId, numCardDifference);
        setChanged();
        notifyObservers("OtherPlayerHandUpdated");
    }

    public void setNumberOfTrainCardForPlayer(String playerId, int numCards){
        getPlayer().getMyGame().setTrainCardsForPlayer(playerId, numCards);
        setChanged();
        notifyObservers("OtherPlayerHandUpdated");
    }

    public void updatePoints(String playerId, int numPoints) {
        player.getMyGame().addToScore(playerId, numPoints);
        setChanged();
        notifyObservers("ScoreUpdated");
    }

    public void showClaimedRoute(Route claimedRoute, String ownerPlayerId) {
        gameMap.claimRoute(claimedRoute,ownerPlayerId,player.getMyGame().getColorByPlayerId(ownerPlayerId));
        setChanged();
        notifyObservers("RouteClaimed");
    }

    public void claimRoute(Route claimedRoute, String ownerPlayerId, int piecesUsed, int numPieces) {
        gameMap.claimRoute(claimedRoute,ownerPlayerId,player.getMyGame().getColorByPlayerId(ownerPlayerId));
        //player.getMyGame().reduceTrainPieces(ownerPlayerId, piecesUsed);
        player.getMyGame().setNumTrainPieces(ownerPlayerId, numPieces);
        player.getMyGame().addToScore(ownerPlayerId, claimedRoute.getPoints());
        // TODO is there anything else we need to do here like update train pieces counts?
        setChanged();
        notifyObservers("RouteClaimed");
    }

    public void removeTrainPieces(String playerId, int numPieces){
        player.getMyGame().reduceTrainPieces(playerId,numPieces);
        setChanged();
        notifyObservers("PiecesRemoved");
    }

    public void addTrainCardNumber(String playerId, int number){
        player.getMyGame().addTrainCards(playerId,number);
        setChanged();
        notifyObservers("AddTrainCardNumber");
    }

    public void addDestinationCard(DestinationCard card){
        player.getHand().addDestinationCard(card);
        setChanged();
        notifyObservers("DestCardAdd");
    }

    public void addDestinationCardNumber(String playerId, int number){
        player.getMyGame().addDestinationCards(playerId,number);
        setChanged();
        notifyObservers("AddDestCardNumber");
    }

    public void decreaseDestinationDeckNumber(int amount){
        player.getMyGame().decreaseDestinationCount(amount);
        setChanged();
        notifyObservers("DestDeckNumber");
    }

    public void decreaseTrainDeckNumber(int amount){
        player.getMyGame().decreaseTrainDeckCount(amount);
        setChanged();
        notifyObservers("TrainDeckNumber");
    }

    public void updateTurn(int newTurn){
        player.getMyGame().setTurn(newTurn);
        setChanged();
        notifyObservers("TurnChanged");
    }

    public void cardsUsed(List<TrainCard> cards, String owner) {
        // TODO should we just put this in claimRoute?
        if(owner.equals(player.getPlayerId())) {
            // we used these cards
            player.removeCardsFromHand(cards);
        }
        player.getMyGame().removeTrainCards(owner, cards.size());
        setChanged();
        notifyObservers("CardsUsed");
    }

    public void updateRiver(River newRiver) {
        player.getMyGame().setRiver(newRiver);
        setChanged();
        notifyObservers("RiverUpdated");
    }

    public void addCardToRiver(TrainCard card){
        player.getMyGame().getRiver().addCard(card);
        setChanged();
        notifyObservers("AddToRiver");
    }

    private class FirebaseMessageHandler extends FirebaseMessagingService {
        private static final String TAG = "FirebaseMessageHandler";

        @Override
        public void onMessageReceived(RemoteMessage remoteMessage) {
            Log.d(TAG, "Firebase Message From: " + remoteMessage.getFrom());

            // Check if message contains a data payload.
            if (remoteMessage.getData().size() > 0) {
                // TODO figure out nature of message and call singleton instance of Observable to broadcast
            }

            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {

            }
        }
    }

    public Player getPlayer() {
        return player;
    }

    public int getMyTurnOrder() {
        return myTurnOrder;
    }

    public void setMyTurnOrder(int myTurnOrder) {
        this.myTurnOrder = myTurnOrder;
    }

    public List<LobbyGameMetadata> getOpenGames() {
        return openGames;
    }

    public List<LobbyGameMetadata> getJoinedGames() {
        return joinedGames;
    }

    public List<LobbyGameMetadata> getOwnedGames() {
        return ownedGames;
    }

    public int getResponseCode(){
        return responseCode;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public String getPlayerId() {
        return playerId;
    }

    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    public List<String> getGameHistory() {
        return gameHistory;
    }

    public void setGameHistory(List<String> gameHistory) {
        this.gameHistory = gameHistory;
    }

    public void addToGameHistory(String item) {
        gameHistory.add(item);
        setChanged();
        notifyObservers("GameHistoryUpdated");
    }

    public void setTrainDeckCount(int trainDeckCount){
        this.getPlayer().getMyGame().setTrainDeckCount(trainDeckCount);
        setChanged();
        notifyObservers("TrainDeckNumber");
    }
}
