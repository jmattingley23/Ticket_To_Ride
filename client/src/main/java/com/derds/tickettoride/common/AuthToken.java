package com.derds.tickettoride.common;

/**
 * Created by Aaron on 10/7/2017.
 */

public class AuthToken {

    private static AuthToken instance;

    private String authToken;


    public static AuthToken getInstance(){
        if(instance == null){
            instance = new AuthToken();
        }
        return instance;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    private AuthToken(){
    }

}
