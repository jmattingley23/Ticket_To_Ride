package com.derds.tickettoride.common;

import android.util.Log;

import com.derds.cmddata.ICmdData;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;

//Singleton
public class ClientCommunicator {

    private static String TAG = "ClientCommunicator";
    private String address = "192.168.252.153";
    private String port = "8080";
    private Gson gson;

    private static ClientCommunicator communicator;

    private ClientCommunicator() {}

    public static ClientCommunicator getInstance(){
        if(communicator == null){
            communicator = new ClientCommunicator();
        }
        return communicator;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Object sendRequest(String authToken, Object command, Class returnType, String route) {
        try {
            gson = new Gson();

            //build URL
            URL url = new URL("http://" + address + ":" + port + route);
            HttpURLConnection http = (HttpURLConnection)url.openConnection();
            http.setConnectTimeout(5000); //timeout after 5 seconds

            //set up connection
            http.setRequestMethod("POST");
            http.setDoOutput(true);
            http.addRequestProperty("Content-Type", "application/json");

            //add authToken if necessary
            if(authToken != null) {
                http.addRequestProperty("AuthToken", authToken);
            }
            //add the type of command to the header
            http.addRequestProperty("CommandType", command.getClass().getSimpleName());
            http.connect();

            //serialize the CmdData object to a JSON string
            String jsonStr = gson.toJson(command);

            //write the JSON string to the outputStream
            OutputStream outputStream = http.getOutputStream();
            outputStream.write(jsonStr.getBytes());
            outputStream.close();

            Log.d(TAG, "Server returned a " + String.valueOf(http.getResponseCode()));
            Reader inputStreamReader = new InputStreamReader(http.getInputStream());

            return gson.fromJson(inputStreamReader, returnType);
        } catch (Exception e) {
            e.printStackTrace();
            Log.d(TAG, "Unable to connect to server");
            e.printStackTrace();
        }
        return null;
    }

}
