package com.derds.tickettoride.services;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.derds.tickettoride.R;
import com.derds.tickettoride.async.AsyncGetSyncDataTask;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    public FirebaseMessagingService() {}

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        boolean receivedCommand = false;
        String className = null;
        String serializedGameCommand = null;

        //go through all the key-value pairs in the message payload
        for(Map.Entry<String, String> entry :  remoteMessage.getData().entrySet()) {
            if(entry.getKey().equals("UPDATE")) { //lobby ping
                if(entry.getValue().equals("LOBBY")) {
                    Intent intent = new Intent(getString(R.string.firebase_refreshlobby));
                    LocalBroadcastManager.getInstance(getBaseContext()).sendBroadcast(intent);
                } else if(entry.getValue().equals("SYNC")) {
                    GameCommandExecutor.getInstance().getSyncData();
                }
            } else if(entry.getKey().equals("COMMAND_DATA")) { //serialized game command data
                receivedCommand = true;
                serializedGameCommand = entry.getValue();
            } else if(entry.getKey().equals("GAME_COMMAND")) { //game command type
                receivedCommand = true;
                className = entry.getValue();
            }
        }
        if(receivedCommand) {
            GameCommandExecutor.getInstance().executeGameCommand(className, serializedGameCommand);
        }
    }
}
