package com.derds.tickettoride.services;

import android.util.Log;

import com.derds.gamecommand.AbstractGameCommand;
import com.derds.gamecommand.AbstractSyncGameCommand;
import com.derds.tickettoride.async.AsyncGetSyncDataTask;
import com.derds.tickettoride.async.AsyncRequestQueueTask;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.gamecommands.ChooseDestCommand;
import com.derds.tickettoride.gamecommands.ClaimRouteCommand;
import com.derds.tickettoride.gamecommands.DrawDestCommand;
import com.derds.tickettoride.gamecommands.EndGameCommand;
import com.derds.tickettoride.gamecommands.GetChatHistoryCommand;
import com.derds.tickettoride.gamecommands.InitGameCommand;
import com.derds.tickettoride.gamecommands.NextTurnCommand;
import com.derds.tickettoride.gamecommands.PickFaceupCardCommand;
import com.derds.tickettoride.gamecommands.RequestDrawDestCommand;
import com.derds.tickettoride.gamecommands.RequestDrawTrainCommand;
import com.derds.tickettoride.gamecommands.SendChatCommand;
import com.derds.tickettoride.gamecommands.SyncGameCommand;
import com.derds.tickettoride.gamecommands.UpdatePointsCommand;
import com.derds.tickettoride.gamecommands.UpdateRiverCommand;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class GameCommandExecutor {
    private static GameCommandExecutor instance;
    private Gson gson;

    private GameCommandExecutor() {
        gson = new GsonBuilder().serializeNulls().create();
    }

    public static GameCommandExecutor getInstance() {
        if(instance == null) {
            instance = new GameCommandExecutor();
        }
        return instance;
    }

    public void executeGameCommand(String commandType, String serializedCommand) {
        if(commandType.equals(ChooseDestCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(ChooseDestCommand.class, serializedCommand);

        } else if(commandType.equals(ClaimRouteCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(ClaimRouteCommand.class, serializedCommand);

        } else if(commandType.equals(RequestDrawDestCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(RequestDrawDestCommand.class, serializedCommand);
            
        } else if(commandType.equals(DrawDestCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(DrawDestCommand.class, serializedCommand);

        } else if(commandType.equals(GetChatHistoryCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(GetChatHistoryCommand.class, serializedCommand);

        } else if(commandType.equals(InitGameCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(InitGameCommand.class, serializedCommand);

        } else if(commandType.equals(PickFaceupCardCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(PickFaceupCardCommand.class, serializedCommand);

        } else if(commandType.equals(RequestDrawTrainCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(RequestDrawTrainCommand.class, serializedCommand);

        } else if(commandType.equals(SendChatCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(SendChatCommand.class, serializedCommand);

        } else if(commandType.equals(UpdatePointsCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(UpdatePointsCommand.class, serializedCommand);

        } else if(commandType.equals(UpdateRiverCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(UpdateRiverCommand.class, serializedCommand);

        } else if(commandType.equals(NextTurnCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(NextTurnCommand.class, serializedCommand);

        } else if(commandType.equals(EndGameCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(EndGameCommand.class, serializedCommand);

        } else if(commandType.equals(SyncGameCommand.class.getSimpleName())) {
            verifyAndExecuteGameCommand(SyncGameCommand.class, serializedCommand);

        }
    }

    public void getSyncData() {
        new AsyncGetSyncDataTask().execute();
    }

    public void processSyncData(SyncGameCommand command) {

        //execute the command
        if(command != null) {
            command.execute();
            if (command.getExecutorState() != null && ClientModelFacade.getInstance().getPlayerId().equals(command.getExecutor())) {
                ClientModelFacade.getInstance().setTurnState(command.getExecutorState());
            }

            //            if (command.isVisibleOnHistory()) {
            ClientModelFacade.getInstance().addToGameHistory(command.toString());
        }
    }

    private void verifyAndExecuteGameCommand(Class commandType, String serializedCommand) {
        Object commandObj = null;
        try {
            commandObj = gson.fromJson(serializedCommand, commandType);
        } catch (Exception e) {
            Log.e("GameCommandExecutor", "could not deserialize json string");
            Log.e("GameCommandExecutor", e.getMessage());
            e.printStackTrace();
            return; // we don't want to continue...
        }

        AbstractGameCommand command = (AbstractGameCommand) commandObj;

        if(!command.getGameId().equals(ClientModelFacade.getInstance().getGameId())) {
            return;
        }

        // Syncing games needs to bypass the commandSequenceId
        if(command instanceof SyncGameCommand) {
            ClientModelFacade.getInstance().setLastCommandSequenceId(command.getCommandSequenceId() - 1);
        }

        //make sure the command is the next one
        int receivedId = command.getCommandSequenceId();
        int lastCommandSequenceId = ClientModelFacade.getInstance().getLastCommandSequenceId();
        //if the current command is anything other than 1 command after the last executed command, fetch the needed commands
        if(receivedId - lastCommandSequenceId > 1) {
            new AsyncRequestQueueTask().execute();
            return;
        } else if(receivedId - lastCommandSequenceId < 1) {
            return; // We have already executed this command, we don't need to do that anymore
        } else {

            //execute the command
            command.execute();
            if (command.getExecutorState() != null && ClientModelFacade.getInstance().getPlayerId().equals(command.getExecutor())) {
                ClientModelFacade.getInstance().setTurnState(command.getExecutorState());
            }

//            if (command.isVisibleOnHistory()) {
                ClientModelFacade.getInstance().addToGameHistory(command.toString());
//            }

            ClientModelFacade.getInstance().setLastCommandSequenceId(command.getCommandSequenceId());
        }
    }
}