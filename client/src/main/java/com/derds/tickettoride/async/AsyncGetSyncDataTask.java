package com.derds.tickettoride.async;

import android.os.AsyncTask;

import com.derds.gamecommand.AbstractSyncGameCommand;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.common.ServerProxy;
import com.derds.tickettoride.gamecommands.SyncGameCommand;
import com.derds.tickettoride.services.GameCommandExecutor;

/**
 * Created by jmatt on 11/28/2017.
 */

public class AsyncGetSyncDataTask extends AsyncTask<Void, Void, SyncGameCommand> {
    @Override
    protected SyncGameCommand doInBackground(Void... params) {
        return ServerProxy.getInstance().getSyncData(ClientModelFacade.getInstance().getPlayerId());
    }

    @Override
    protected void onPostExecute(SyncGameCommand command) {
        super.onPostExecute(command);
        GameCommandExecutor.getInstance().processSyncData(command);
    }
}
