package com.derds.tickettoride.async;


import android.os.AsyncTask;

import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.common.ServerProxy;
import com.derds.tickettoride.game.MapController;
import com.derds.tickettoride.gamecommands.ClaimRouteCommand;

/**
 * Created by mogard on 10/31/2017.
 */

public class AsyncMapTask extends AsyncTask<ClaimRouteCommand,Void,Void>{

    public AsyncMapTask() {
        super();
    }

    @Override
    protected Void doInBackground(ClaimRouteCommand... params) {

        ClaimRouteCommand claimRouteCommand = params[0];
        String authToken = AuthToken.getInstance().getAuthToken();
        String playerId = ClientModelFacade.getInstance().getPlayerId();
        ServerProxy.getInstance().claimRoute(authToken,playerId,claimRouteCommand.getClaimedRoute(),claimRouteCommand.getCardsUsed(),claimRouteCommand.getTrainPiecesUsed());

        return null;
    }

    @Override
    protected void onProgressUpdate(Void... result) {}

    @Override
    protected void onPostExecute(Void o) {

    }
}
