package com.derds.tickettoride.async;

import android.os.AsyncTask;

import com.derds.result.Result;
import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.common.ServerProxy;

public class AsyncRequestQueueTask extends AsyncTask<Void, Void, Result> {
    @Override
    protected Result doInBackground(Void... params) {
        return ServerProxy.getInstance().getQueueHistory(
                AuthToken.getInstance().getAuthToken(),
                ClientModelFacade.getInstance().getLastCommandSequenceId(),
                ClientModelFacade.getInstance().getPlayerId(),
                ClientModelFacade.getInstance().getGameId());
    }
}
