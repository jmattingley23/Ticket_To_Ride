package com.derds.tickettoride.async;

import android.os.AsyncTask;

import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.ICmdData;
import com.derds.cmddata.JoinGameCmdData;
import com.derds.cmddata.RefreshLobbyCmdData;
import com.derds.cmddata.StartGameCmdData;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.lobby.ILobbyController;
import com.derds.tickettoride.lobby.LobbyController;
import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.common.ServerProxy;

public class AsyncLobbyTask extends AsyncTask<ICmdData, Void,Result> {

    ICmdData dataInput;
    ILobbyController controller;

    public AsyncLobbyTask(ILobbyController newController) {
        super();
        controller = newController;
    }

    @Override
    protected Result doInBackground(ICmdData... params) {
        Result result = null;

        Class classType = params[0].getClass();
        dataInput = params[0];
        if(classType.equals(RefreshLobbyCmdData.class)){
            RefreshLobbyCmdData data = (RefreshLobbyCmdData) params[0];
            result = ServerProxy.getInstance().refreshLobby(AuthToken.getInstance().getAuthToken());
        }
        else if(classType.equals(StartGameCmdData.class)){
            StartGameCmdData data = (StartGameCmdData) params[0];
            result = ServerProxy.getInstance().startGame(AuthToken.getInstance().getAuthToken(),data.getGameID());
        }
        else if(classType.equals(JoinGameCmdData.class)){
            JoinGameCmdData data = (JoinGameCmdData) params[0];
            result = ServerProxy.getInstance().joinGame(AuthToken.getInstance().getAuthToken(),data.getGameID(), data.getPlayerColor());
        }
        else if(classType.equals(CreateGameCmdData.class)){
            CreateGameCmdData data = (CreateGameCmdData) params[0];
            result = ServerProxy.getInstance().createGame(AuthToken.getInstance().getAuthToken(),data.getGameName(), data.getPlayerColor());
        }

        return result;
    }

    @Override
    protected void onPostExecute(Result result) {
        if (result.getMessage() == null) {
            controller.onSuccess(result,dataInput);
        } else {
            controller.onError(result);
        }
    }
}
