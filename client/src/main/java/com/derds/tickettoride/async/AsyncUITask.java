package com.derds.tickettoride.async;

import android.os.AsyncTask;
import android.widget.Toast;

import com.derds.gamecommand.AbstractGameCommand;
import com.derds.result.Result;
import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.common.ServerProxy;
import com.derds.tickettoride.game.IUIController;
import com.derds.tickettoride.game.UIController;
import com.derds.tickettoride.gamecommands.ChooseDestCommand;
import com.derds.tickettoride.gamecommands.DrawDestCommand;
import com.derds.tickettoride.gamecommands.PickFaceupCardCommand;
import com.derds.tickettoride.gamecommands.RequestDrawDestCommand;
import com.derds.tickettoride.gamecommands.RequestDrawTrainCommand;
import com.derds.tickettoride.gamecommands.SendChatCommand;
import com.derds.tickettoride.turnstate.EndTurnState;
import com.derds.tickettoride.turnstate.WaitState;
import com.derds.turnstate.TurnState;

public class AsyncUITask extends AsyncTask<AbstractGameCommand, Void, Result> {
    private IUIController controller;

    public AsyncUITask(IUIController controller) {
        super();
        this.controller = controller;
    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);

        // If there was an error
        if(result.getMessage() != null) {
            Toast.makeText(controller.getGameFragment().getActivity(),result.getMessage(),Toast.LENGTH_SHORT).show();
            TurnState state = ClientModelFacade.getInstance().getTurnState();
            if(state instanceof WaitState) {
                ((WaitState)state).revertToPreviousState(); // if we are waiting for input or something, revert to previous state
            }
        }
    }

    @Override
    protected Result doInBackground(AbstractGameCommand... params) {
        Result result = null;

        Class classType = params[0].getClass();
        if(classType.equals(PickFaceupCardCommand.class)) {
            PickFaceupCardCommand data = (PickFaceupCardCommand)params[0];
            result = ServerProxy.getInstance().pickFaceupCard(AuthToken.getInstance().getAuthToken(), ClientModelFacade.getInstance().getPlayerId(), data.getCardDrawn());
        } else if(classType.equals(RequestDrawTrainCommand.class)) {
            RequestDrawTrainCommand data = (RequestDrawTrainCommand)params[0];
            result = ServerProxy.getInstance().requestDrawTrain(AuthToken.getInstance().getAuthToken(), ClientModelFacade.getInstance().getPlayerId());
        } else if(classType.equals(SendChatCommand.class)) {
            SendChatCommand data = (SendChatCommand) params[0];
            result = ServerProxy.getInstance().sendChat(AuthToken.getInstance().getAuthToken(), data.getMessage());
        } else if(classType.equals(DrawDestCommand.class)){
            DrawDestCommand data = (DrawDestCommand) params[0];
            result = ServerProxy.getInstance().requestDrawDestination(AuthToken.getInstance().getAuthToken(),
                    ClientModelFacade.getInstance().getPlayerId());
        } else if(classType.equals(RequestDrawDestCommand.class)){
            RequestDrawDestCommand data = (RequestDrawDestCommand) params[0];
            result = ServerProxy.getInstance().requestDrawDestination(AuthToken.getInstance().getAuthToken(),
                    ClientModelFacade.getInstance().getPlayerId());
        } else if(classType.equals(ChooseDestCommand.class)){
            ChooseDestCommand data = (ChooseDestCommand) params[0];
            result = ServerProxy.getInstance().chooseDestination(AuthToken.getInstance().getAuthToken(),
                    ClientModelFacade.getInstance().getPlayerId(), data.getCardsChosen(), data.getCardsReturned());
        }
        return result;
    }


}
