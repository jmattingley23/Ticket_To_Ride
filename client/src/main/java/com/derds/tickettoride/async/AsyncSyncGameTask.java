package com.derds.tickettoride.async;

import android.os.AsyncTask;

import com.derds.result.Result;
import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.common.ServerProxy;

public class AsyncSyncGameTask extends AsyncTask<Void, Void, Result> {
    @Override
    protected Result doInBackground(Void... params) {
        return ServerProxy.getInstance().syncGame(AuthToken.getInstance().getAuthToken(),
                                                    ClientModelFacade.getInstance().getPlayerId(),
                                                    ClientModelFacade.getInstance().getGameId());
    }
}