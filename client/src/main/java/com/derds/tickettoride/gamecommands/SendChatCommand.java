package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractSendChatCommand;
import com.derds.models.Message;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/20/17.
 */

public class SendChatCommand extends AbstractSendChatCommand {

    public SendChatCommand(String executor, Message content) {
        super(executor, ClientModelFacade.getInstance().getGameId(), content);
    }

    @Override
    public Result execute() {
        ClientModelFacade.getInstance().addChatMessage(content);
        return new Result(null);
    }

    @Override
    public String toString() {
        return ClientModelFacade.getInstance().getPlayer().getMyGame().getPlayerNameFromId(executor) + " sent a chat message";
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }

    public Message getMessage() {
        return content;
    }

    public void setMessage(Message message) {
        this.content = message;
    }
}
