package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractPickFaceupCardCommand;
import com.derds.models.TrainCard;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;

/**
 * Created by drc95 on 10/26/17.
 */

public class PickFaceupCardCommand extends AbstractPickFaceupCardCommand {

    public PickFaceupCardCommand(String executor, TrainCard cardDrawn) {
        super(executor, ClientModelFacade.getInstance().getGameId(), cardDrawn);
    }

    @Override
    public Result execute() {
        if(executor.equals(ClientModelFacade.getInstance().getPlayerId())) {
            ClientModelFacade.getInstance().addTrainCardToHand(cardDrawn);
        }else{
            //ClientModelFacade.getInstance().updateNumberOfTrainCards(executor, 1);
            ClientModelFacade.getInstance().setNumberOfTrainCardForPlayer(executor, getPlayerTrainHandSize());
        }

        //ClientModelFacade.getInstance().riverCardDrawn(cardDrawn, newRiverCard);
        ClientModelFacade.getInstance().updateRiver(getNewRiver());
        ClientModelFacade.getInstance().getPlayer().getMyGame().setTrainDeckCount(this.getTrainDeckSize());
        return new Result(null);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ClientModelFacade.getInstance().getNameFromId(executor));
        sb.append(" drew a train card from the river.");
        return sb.toString();
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
