package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractSyncGameCommand;
import com.derds.models.GameMap;
import com.derds.models.Player;
import com.derds.models.Route;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;

import java.util.List;

/**
 * Created by drc95 on 11/24/17.
 */

public class SyncGameCommand extends AbstractSyncGameCommand {
    @Override
    public Result execute() {
        String savedPlayerId = ClientModelFacade.getInstance().getPlayerId();
        if(executor.equals(savedPlayerId)) {
            ClientModelFacade.getInstance().reset();

            // updating the player should update the turnstate and the hand and other things eventually.
            ClientModelFacade.getInstance().updatePlayer(player);

            // Reset game map???
            GameMap gameMap = new GameMap();
            for(Route route : claimedRoutes) {
                //ClientModelFacade.getInstance().showClaimedRoute(route, route.getOwner());
                gameMap.claimRoute(route,route.getOwner(),ClientModelFacade.getInstance().getPlayer().getMyGame().getColorByPlayerId(route.getOwner()));
            }

            ClientModelFacade.getInstance().setGameMap(gameMap);

            // TODO do we need to trigger the ClientModelFacade's updates somehow????

            ClientModelFacade.getInstance().setLastCommandSequenceId(getCommandSequenceId());

            ClientModelFacade.getInstance().setGameReady();
        }

        return new Result(null);
    }

    // TODO we never really need to set non-null things in the constructor on the client side, lets try getting rid of that
    public SyncGameCommand(String executor, String gameId, Player player, int curTurn, List<Route> claimedRoutes) {
//    public SyncGameCommand(String executor, String gameId) {
        super(executor, gameId, player, curTurn, claimedRoutes);
    }

    @Override
    // TODO FIX THIS TO BE PRETTIER
    public String toString() {
        return "Syncing game";
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
