package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractUpdatePointsCommand;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/28/17.
 */

public class UpdatePointsCommand extends AbstractUpdatePointsCommand {
    public UpdatePointsCommand(String executor, int newPointsTotal, String playerId) {
        super(executor, ClientModelFacade.getInstance().getGameId(), newPointsTotal, playerId);
    }

    @Override
    public Result execute() {
        ClientModelFacade.getInstance().updatePoints(playerId, newPointsTotal);
        return new Result(null);
    }

    @Override
    public String toString() {
        return ClientModelFacade.getInstance().getPlayer().getMyUser().getFirstname() + "updated the points for " + ClientModelFacade.getInstance().getPlayer().getMyGame().getPlayerNameFromId(playerId);
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
