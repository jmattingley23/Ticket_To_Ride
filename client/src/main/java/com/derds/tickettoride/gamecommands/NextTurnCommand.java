package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractNextTurnCommand;
import com.derds.models.Player;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

public class NextTurnCommand extends AbstractNextTurnCommand {
    public NextTurnCommand(int newTurn) {
        super(newTurn, ClientModelFacade.getInstance().getGameId());
    }

    @Override
    public Result execute() {
        //set the game's turn index to the one received from the server
        ClientModelFacade.getInstance().updateTurn(getNewTurn());

        //if the new turn is the same as your player's turn
        if(ClientModelFacade.getInstance().getPlayer().getMyTurn() == getNewTurn()) {

            //if the first turn is done already go to begin game, otherwise just go to start turn
            if(ClientModelFacade.getInstance().getPlayer().getHand().getDestCardCount() > 0) {
                ClientModelFacade.getInstance().setTurnState(TurnState.StateName.START_TURN);
            } else {
                ClientModelFacade.getInstance().setTurnState(TurnState.StateName.BEGIN_GAME);
            }

            //notify the player it is their turn by vibrating the phone
            ClientModelFacade.getInstance().notifyTurn();
        } else {
            //if it isn't your turn just go into the wait state
            ClientModelFacade.getInstance().setTurnState(TurnState.StateName.END);
        }

        return new Result();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Game advanced to turn ");
        sb.append(getNewTurn());
        sb.append(" and ");
        sb.append(ClientModelFacade.getInstance().getPlayer().getMyUser().getFirstname());
        sb.append(" entered the ");
        sb.append(ClientModelFacade.getInstance().getStateName());
        return sb.toString();
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
