package com.derds.tickettoride.gamecommands;

import android.util.Log;

import com.derds.gamecommand.AbstractDrawDestCommand;
import com.derds.models.DestinationCard;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/25/17.
 */

public class DrawDestCommand extends AbstractDrawDestCommand {
    public static final String TAG = "DrawDestCommand";
    public DrawDestCommand(List<DestinationCard> cardsToPickFrom, boolean areCardsDrawn, String executor) {
        super(cardsToPickFrom, areCardsDrawn, executor, ClientModelFacade.getInstance().getGameId());
        // TODO should areCardsDrawn be set to false in the constructor?
    }

    @Override
    public Result execute() {
        if(areCardsDrawn && ClientModelFacade.getInstance().getPlayerId().equals(executor)) {
            ClientModelFacade.getInstance().drawDestinationCards(cardsToPickFrom);
        } else {
            Log.d(TAG,"Tried to execute a drawDestCommand but there were no cards to pick from");
        }
        return new Result(null);    // nothing to see here
    }

    @Override
    public String toString() {
        return ClientModelFacade.getInstance().getPlayer().getMyGame().getPlayerNameFromId(executor) + " is drawing destination cards.";
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
