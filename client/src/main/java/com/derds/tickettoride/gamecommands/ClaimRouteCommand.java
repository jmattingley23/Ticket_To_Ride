package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractClaimRouteCommand;
import com.derds.models.CityPair;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/26/17.
 */

public class ClaimRouteCommand extends AbstractClaimRouteCommand {
    public ClaimRouteCommand(Route claimedRoute, List<TrainCard> cardsUsed, int trainPiecesUsed, String executor) {
        super(claimedRoute, cardsUsed, trainPiecesUsed, executor, ClientModelFacade.getInstance().getGameId());
    }

    @Override
    public Result execute() {
//        if(ClientModelFacade.getInstance().getPlayerId().equals(executor)){
//            //TODO: Do we need this?
//        }
        ClientModelFacade.getInstance().claimRoute(claimedRoute, executor, trainPiecesUsed, getNumTrainPieces());
        ClientModelFacade.getInstance().cardsUsed(cardsUsed,executor);
        ClientModelFacade.getInstance().getPlayer().getMyGame().setTrainDeckCount(getTrainDeckSize());
        return new Result(null);
    }

    @Override
    public String toString() {
        CityPair cities = claimedRoute.getCities();
        StringBuilder sb = new StringBuilder();
        sb.append(ClientModelFacade.getInstance().getNameFromId(executor));
        sb.append(" claimed a route from ");
        sb.append(cities.getCity1().getName());
        sb.append(" to " );
        sb.append(cities.getCity2().getName());
        sb.append(".");
        return sb.toString();
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
