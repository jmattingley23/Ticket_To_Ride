package com.derds.tickettoride.gamecommands;

import android.util.Log;

import com.derds.gamecommand.AbstractRequestDrawTrainCommand;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/25/17.
 */

public class RequestDrawTrainCommand extends AbstractRequestDrawTrainCommand {
    public static final String TAG = "RequestDrawTrainCommand";
    public RequestDrawTrainCommand(String authToken, String executor) {
        super(authToken, executor, ClientModelFacade.getInstance().getGameId());
    }

    @Override
    public Result execute() {

        if(executor.equals(ClientModelFacade.getInstance().getPlayerId())) {
            ClientModelFacade.getInstance().addTrainCardToHand(drawnCard);
        }else{
            ClientModelFacade.getInstance().setNumberOfTrainCardForPlayer(executor, getPlayerTrainHandSize());
        }

        ClientModelFacade.getInstance().setTrainDeckCount(this.getTrainDeckSize());

        return new Result(null);    // nothing to see here
    }

    @Override
    public String toString() {
        return ClientModelFacade.getInstance().getPlayer().getMyGame().getPlayerNameFromId(executor) + " requested to draw train cards";
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
