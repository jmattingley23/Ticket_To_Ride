package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractUpdateRiverCommand;
import com.derds.models.River;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/28/17.
 */

public class UpdateRiverCommand extends AbstractUpdateRiverCommand {
    public UpdateRiverCommand(String executor, River newRiver) {
        super(executor, ClientModelFacade.getInstance().getGameId(), newRiver);
    }

    @Override
    public Result execute() {
        ClientModelFacade.getInstance().updateRiver(newRiver);
        return new Result(null);
    }

    @Override
    public String toString() {
        return ClientModelFacade.getInstance().getPlayer().getMyUser().getFirstname() + " updated their river";
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
