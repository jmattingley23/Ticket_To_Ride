package com.derds.tickettoride.gamecommands;

import android.util.Log;

import com.derds.gamecommand.AbstractInitGameCommand;
import com.derds.models.Player;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/27/17.
 */

public class InitGameCommand extends AbstractInitGameCommand {
    public InitGameCommand(String executor, int turnOrder, Player player) {
        super(executor, ClientModelFacade.getInstance().getGameId(), turnOrder, player);
    }

    @Override
    public Result execute() {
        String savedPlayerId = ClientModelFacade.getInstance().getPlayerId();
        if(executor.equals(savedPlayerId)) {
            // we want to reset the ClientModelFacade
            ClientModelFacade.getInstance().setMyTurnOrder(turnOrder);
            // clear hand so when we run subsequent commands the hand is not duplicated
            //player.getHand().clear();
            ClientModelFacade.getInstance().updatePlayer(player);
            ClientModelFacade.getInstance().getPlayer().getMyGame().setTurn(0);

            ClientModelFacade.getInstance().setPlayerId(player.getPlayerId());
            ClientModelFacade.getInstance().setGameReady();
        }

        return new Result(null);
    }

    @Override
    public String toString() {
        return player.getMyUser().getFirstname() + " initialized their game.";
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
