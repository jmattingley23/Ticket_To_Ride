package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractGetChatHistoryCommand;
import com.derds.models.Message;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/21/17.
 */

public class GetChatHistoryCommand extends AbstractGetChatHistoryCommand {
    public GetChatHistoryCommand(String executor, int lastCommandSequenceId, List<Message> messageHistory, int skip) {
        super(executor, ClientModelFacade.getInstance().getGameId(), lastCommandSequenceId, messageHistory, skip);
    }

    @Override
    public Result execute() {
        // When we get this on the client, it means we have already sent this to the server
        // and it has been populated with the chat history
        Result result = new Result(null);
        ClientModelFacade.getInstance().updateChatMessageHistory(messageHistory);
        return result;
    }

    @Override
    public String toString() {
        return ClientModelFacade.getInstance().getPlayer().getMyUser().getFirstname() + " received a chat message";
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
