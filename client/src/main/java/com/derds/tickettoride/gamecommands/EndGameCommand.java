package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractEndGameCommand;
import com.derds.models.PlayerStats;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;

import java.util.List;

/**
 * Created by drc95 on 11/21/17.
 */

public class EndGameCommand extends AbstractEndGameCommand {
    public EndGameCommand(String executor, String gameId, List<PlayerStats> playerStatses) {
        super(executor, gameId, playerStatses);
        setExecutorState(null);
    }

    @Override
    public Result execute() {
        ClientModelFacade.getInstance().endGame(getPlayerStatses());
        return new Result(null);
    }

    @Override
    public String toString() {
        return "The game is done! Showing player stats.";
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
