package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractChooseDestCommand;
import com.derds.models.DestinationCard;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/25/17.
 */

public class ChooseDestCommand extends AbstractChooseDestCommand {
    public static final String TAG = "ChooseDestCommand";

    public ChooseDestCommand(String executor, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned) {
        super(executor, ClientModelFacade.getInstance().getGameId(), cardsChosen, cardsReturned);
    }

    @Override
    public Result execute() {
        ClientModelFacade.getInstance().addDestinationCardNumber(executor, cardsChosen.size());
        ClientModelFacade.getInstance().subtractDestinationCards(cardsChosen.size());

        if(executor.equals(ClientModelFacade.getInstance().getPlayer().getPlayerId())) {
            for(DestinationCard card : cardsChosen) {
                ClientModelFacade.getInstance().addDestinationCard(card);
            }
            // ensure this player is in the wait state because their turn is over
            ClientModelFacade.getInstance().getTurnState().endTurn(ClientModelFacade.getInstance().getPlayer());
        }
        return new Result(null);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(ClientModelFacade.getInstance().getNameFromId(executor));
        sb.append(" chose ");
        sb.append(cardsChosen.size());
        sb.append(" destination cards.");
        return sb.toString();
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
