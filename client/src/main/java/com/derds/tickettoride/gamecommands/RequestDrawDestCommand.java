package com.derds.tickettoride.gamecommands;

import com.derds.gamecommand.AbstractRequestDrawDestCommand;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/25/17.
 */

public class RequestDrawDestCommand extends AbstractRequestDrawDestCommand {
    public RequestDrawDestCommand(String executor) {
        super(executor, ClientModelFacade.getInstance().getGameId());
    }

    @Override
    public Result execute() {
        return new Result(null);    // don't neeed to do anything on the client side
    }

    @Override
    public String toString() {
        return ClientModelFacade.getInstance().getPlayer().getMyGame().getPlayerNameFromId(executor)+ " requested to draw destination cards";
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
