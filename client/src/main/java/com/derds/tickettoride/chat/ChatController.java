package com.derds.tickettoride.chat;

import android.os.AsyncTask;
import android.util.Log;

import com.derds.models.Message;
import com.derds.result.GameResult;
import com.derds.result.Result;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.chat.ui.ChatFragment;
import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.common.ServerProxy;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by justinbrunner on 10/23/17.
 */

public class ChatController implements Observer, IChatController {
    private ChatFragment chatFragment;
    private static final String TAG = "ChatController";

    @Override
    public void update(Observable o, Object arg) {
        Log.v(TAG,"update from ClientModelFacade");
        if(arg.getClass().getSimpleName().equals(String.class.getSimpleName())) {
            String type = (String) arg;
            if (type.equals("NewMessage")) {
                Log.d(TAG, "We got a new message!");
                List<Message> messages = ClientModelFacade.getInstance().getMessageHistory();
//            chatFragment.addMessage(messages.get(messages.size() - 1));
                chatFragment.updateMessages(messages);
            }
        }
    }

    @Override
    public List<Message> getMessageHistory() {
        return ClientModelFacade.getInstance().getMessageHistory();
    }

    public ChatController() {
        ClientModelFacade.getInstance().addObserver(this);
    }

    @Override
    public ChatFragment getChatFragment() {
        return chatFragment;
    }

    @Override
    public void setChatFragment(ChatFragment chatFragment) {
        this.chatFragment = chatFragment;
    }

    @Override
    public void sendMessage(String message) {
        if (!message.equals("")) {
            Message newMessage = new Message(ClientModelFacade.getInstance().getPlayerId(), message);
            AsyncChatTask asyncChatTask = new AsyncChatTask();

            asyncChatTask.execute(newMessage);
        } else {
            onError(new Result(null));
        }
    }

    public class AsyncChatTask extends AsyncTask<Message, Void, Result> {
        public AsyncChatTask() {
            super();
        }

        @Override
        protected Result doInBackground(Message... params) {
            return ServerProxy.getInstance().sendChat(AuthToken.getInstance().getAuthToken(), params[0]);
        }

        @Override
        protected void onProgressUpdate(Void... result) {}

        @Override
        protected void onPostExecute(Result result) {
            if (result.getMessage() == null) {
                onSuccess(result);
            } else {
                onError(result);
            }
        }
    }

    public void onSuccess(Result result) {
        //chatFragment.addMessage();
    }

    public void onError(Result result) {
        if (result.getMessage() != null) {
            //  Why????
//            GameResult gameResult = (GameResult) result;
        }
    }
}
