package com.derds.tickettoride.chat;

import com.derds.models.Message;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by justinbrunner on 10/28/17.
 */

public class FakeChatData {
    public static FakeChatData instance = new FakeChatData();

    private FakeChatData() {
        messages = new ArrayList<Message>();

        try {
            String[] texts = {
                    "Hey guys, what's up!",
                    "Dude's, I'm totes readies to beat y'all at this games",
                    "I'm da Weiner!",
                    "Can't touch dis!, Naa na na na  na na  na na",
                    "This is going to be a super long test text message with the word porg in it\nAnd random spacing\n\nwtj"
            };
            Date[] dates = {
                    new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.US).parse("10/28/2017 09:10:25"),
                    new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.US).parse("10/28/2017 09:11:15"),
                    new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.US).parse("10/28/2017 09:11:35"),
                    new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.US).parse("10/28/2017 09:12:16"),
                    new SimpleDateFormat("MM/dd/yyyy hh:mm:ss", Locale.US).parse("10/29/2017 01:10:05"),
            };
            String[] personIds = {
                    "Taco Joe",
                    "BOGO Timbo",
                    "DOGGO Shean",
                    "YOLO Toots",
                    "Taco Joe"
            };

            for (int i = 0; i < texts.length; i++) {
                Message message = new Message(personIds[i], texts[i], dates[i].getTime());

                messages.add(message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<Message> messages;

    public List<Message> getMessages() {
        return messages;
    }
}
