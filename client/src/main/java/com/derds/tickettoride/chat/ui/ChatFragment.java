package com.derds.tickettoride.chat.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.derds.models.Message;
import com.derds.tickettoride.R;
import com.derds.tickettoride.chat.FakeChatData;
import com.derds.tickettoride.chat.ui.ChatAdapter;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.game.IGameController;

import java.util.List;

import static com.derds.tickettoride.game.ui.GameActivity.GO_FULLSCREEN;

/**
 * Created by justinbrunner on 10/21/17.
 */

public class ChatFragment extends Fragment {
    private static final String TAG = "ChatFragment";
    private IGameController gameController;
    private RecyclerView messagesRecyler;
    private ChatAdapter messagesAdapter;
    private ImageView sendBtn;
    private ImageButton closeBtn;
    private EditText message;
    private int maxRootViewHeight = 0;
    private int currentRootViewHeight = 0;
    private View mView;

    public ChatFragment() {}

    public static ChatFragment newInstance(IGameController gameController) {
        ChatFragment chatFragment = new ChatFragment();
        chatFragment.gameController = gameController;

        return chatFragment;
    }

    public void setGameController(IGameController gameController) {
        this.gameController = gameController;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Fragment self = this;
        final View view = inflater.inflate(R.layout.fragment_chat, container, false);
        setHasOptionsMenu(true);


        view.setBackgroundColor(getResources().getColor(R.color.chat_background));

        gameController.getChatController().setChatFragment(this);

        messagesRecyler = (RecyclerView) view.findViewById(R.id.recyclerview_message_list);
        messagesAdapter = new ChatAdapter(gameController.getChatController().getMessageHistory(), this);
        sendBtn = (ImageView) view.findViewById(R.id.button_chatbox_send);
        closeBtn = (ImageButton) view.findViewById(R.id.close_chat_button);
        message = (EditText) view.findViewById(R.id.edittext_chatbox);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String messageText = message.getText().toString();
                gameController.getChatController().sendMessage(messageText);
                hideKeyboard();
                message.setText("");
                message.clearFocus();
            }
        });

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (message.hasFocus()) {
                    hideKeyboard();
                    message.clearFocus();
                } else {
                    gameController.getUIController().clearChatNotification();
                    gameController.getUIController().hideBars();
                    getFragmentManager().beginTransaction().remove(self).commit();
                }
            }
        });

        messagesRecyler.setLayoutManager(new LinearLayoutManager(getContext()));
        messagesRecyler.setAdapter(messagesAdapter);

        mView = view;

        return view;
    }

    public void addMessage(Message message) {
        messagesAdapter.addMessage(message);
    }

    public void updateMessages(List<Message> messages) {
        messagesAdapter.updateList(messages);
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager =
                (InputMethodManager) getActivity().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                getActivity().getCurrentFocus().getWindowToken(), 0);
        mView.setSystemUiVisibility(GO_FULLSCREEN);
    }

}
