package com.derds.tickettoride.chat;

import android.os.AsyncTask;
import android.util.Log;

import com.derds.models.Message;
import com.derds.result.Result;
import com.derds.tickettoride.chat.ui.ChatFragment;
import com.derds.tickettoride.common.AuthToken;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.common.ServerProxy;

import java.util.List;
import java.util.Observable;

/**
 * Created by Aaron on 11/25/2017.
 */

public interface IChatController {

    public void update(Observable o, Object arg);

    public List<Message> getMessageHistory();

    public ChatFragment getChatFragment();

    public void setChatFragment(ChatFragment chatFragment);

    public void sendMessage(String message);

    public void onSuccess(Result result);

    public void onError(Result result);
}
