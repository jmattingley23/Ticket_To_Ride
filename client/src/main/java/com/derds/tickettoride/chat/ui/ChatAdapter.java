package com.derds.tickettoride.chat.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.derds.models.Message;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.R;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Aaron on 10/9/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<Message> messages;
    private static final int PLAYER = 0;
    private static final int OTHER_PLAYER = 1;
    private RecyclerView chatRecycler;
    private ChatFragment parent;

    public ChatAdapter(List<Message> messages, ChatFragment myParent) {
        this.messages = messages;
        parent = myParent;
    }

//    public ChatAdapter(List<Message> messages){
//        this.messages = messages;
//    }

    public interface  ChatViewHolder {
        public void setIcon(String playerId);
        public void setPlayerNameById(String playerId);
        public void setMessage(String message);
        public void setTimeSent(long timeSent);
    }

    public class ViewHolderPlayerMessage extends RecyclerView.ViewHolder implements ChatViewHolder {
        public TextView timeSent;
        public TextView message;
        public ViewHolderPlayerMessage(View v){
            super(v);
            timeSent = (TextView) v.findViewById(R.id.chat_time_sent_view);
            message = (TextView) v.findViewById(R.id.text_message);
        }

        @Override
        public void setIcon(String playerId) {}

        @Override
        public void setPlayerNameById(String playerId) {}

        @Override
        public void setMessage(String message) {
            this.message.setText(message);
        }

        @Override
        public void setTimeSent(long timeSent) {
            Date dateTimeSent = new Date(timeSent);
            this.timeSent.setText(DateFormat.getDateTimeInstance().format(dateTimeSent));
        }
    }

    public class ViewHolderOtherPlayerMessage extends RecyclerView.ViewHolder  implements ChatViewHolder {
        public TextView playerName;
        public TextView timeSent;
        public TextView message;
        public ImageView icon;
        public ViewHolderOtherPlayerMessage(View v){
            super(v);
            chatRecycler = (RecyclerView) parent.getActivity().findViewById(R.id.recyclerview_message_list);
            icon = (ImageView) v.findViewById(R.id.player_name_image_view);
            playerName = (TextView) v.findViewById(R.id.player_name_text_view);
            timeSent = (TextView) v.findViewById(R.id.chat_time_sent_view);
            message = (TextView) v.findViewById(R.id.text_message);
        }

        @Override
        public void setIcon(String playerId) {
            System.out.println("in setIcon");
            switch (ClientModelFacade.getInstance().getPlayer().getMyGame().getPlayerColors().get(playerId)) {
                case BLUE:
                    System.out.println("Blue");
                    icon.setImageResource(R.drawable.player_blue);
                    break;
                case RED:
                    System.out.println("Red");
                    icon.setImageResource(R.drawable.player_red);
                    break;
                case GREEN:
                    System.out.println("Green");
                    icon.setImageResource(R.drawable.player_green);
                    break;
                case CYAN:
                    System.out.println("White");
                    icon.setImageResource(R.drawable.player_white);
                    break;
                case YELLOW:
                    System.out.println("Yellow");
                    icon.setImageResource(R.drawable.player_yellow);
                    break;
            }
        }

        @Override
        public void setPlayerNameById(String playerId) {
            System.out.println(ClientModelFacade.getInstance().getNameFromId(playerId));
            this.playerName.setText(ClientModelFacade.getInstance().getNameFromId(playerId));
        }

        @Override
        public void setMessage(String message) {
            this.message.setText(message);
        }

        @Override
        public void setTimeSent(long timeSent) {
            Date dateTimeSent = new Date(timeSent);
            this.timeSent.setText(DateFormat.getDateTimeInstance().format(dateTimeSent));
        }
    }

    public void updateList(final List<Message> messages){
        this.messages = messages;

        if (parent.getActivity() != null) {
            parent.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                    if (messages.size() > 0 && chatRecycler != null) {
                        chatRecycler.smoothScrollToPosition(messages.size() - 1);
                    }
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        String chatPlayerId = messages.get(position).getPlayerId();
        String myPlayerId = ClientModelFacade.getInstance().getPlayerId();
        return chatPlayerId.equals(myPlayerId) ? PLAYER : OTHER_PLAYER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case PLAYER:
                View v1 = inflater.inflate(R.layout.player_chat_item, parent, false);
                viewHolder = new ViewHolderPlayerMessage(v1);
                break;
            case OTHER_PLAYER:
                View v2 = inflater.inflate(R.layout.other_player_chat_item, parent, false);
                viewHolder = new ViewHolderOtherPlayerMessage(v2);
                break;
            default:
                View v3 = inflater.inflate(R.layout.other_player_chat_item, parent, false);
                viewHolder = new ViewHolderOtherPlayerMessage(v3);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position){
        ChatViewHolder chatView = (ChatViewHolder) holder;

        chatView.setIcon(messages.get(position).getPlayerId());
        chatView.setPlayerNameById(messages.get(position).getPlayerId());
        chatView.setMessage(messages.get(position).getMessage());
        chatView.setTimeSent(messages.get(position).getTimeSent());
    }

    @Override
    public int getItemCount(){
        return messages.size();
    }

    public void addMessage(Message message){
        this.messages.add(message);
        /*parent.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyItemInserted(0);
                chatRecycler.smoothScrollToPosition(0);
                messages.sort(new Comparator<Message>() {
                    @Override
                    public int compare(Message o1, Message o2) {
                        Date d1 = new Date(o1.getTimeSent()),
                                d2 = new Date(o2.getTimeSent());
                        if (d1.before(d2)) {
                            return 1;
                        } else if (d1.after(d2)) {
                            return -1;
                        } else {
                            return 0;
                        }
                    }
                });
            }
        });*/

    }


}
