package com.derds.tickettoride.lobby.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.derds.models.LobbyGameMetadata;
import com.derds.tickettoride.R;
import com.derds.tickettoride.async.AsyncRequestQueueTask;
import com.derds.tickettoride.async.AsyncSyncGameTask;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.lobby.ILobbyController;
import com.derds.tickettoride.lobby.LobbyController;
import com.joanzapata.iconify.IconDrawable;
import com.joanzapata.iconify.fonts.FontAwesomeIcons;

import java.util.ArrayList;
import java.util.List;

/**
 * A placeholder fragment containing a simple view.
 */
public class LobbyFragment extends Fragment{

    ILobbyController controller;
    private GameExpListAdapter adapter;
    ExpandableListView expListView;
    private PlayerListAdapter mPlayerAdapter;

    private BroadcastReceiver firebaseMessageReceiver;

    public LobbyFragment() {
    }

    public void setController(ILobbyController newController) {
        controller = newController;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_lobby, container, false);
        // TODO do all the other stuff

        setHasOptionsMenu(true);
        controller.updateGameLists();

        adapter = new GameExpListAdapter(getContext());

        expListView = (ExpandableListView) v.findViewById(R.id.games_exp_list_view);
        expListView.setAdapter(adapter);
        expListView.expandGroup(0);
        expListView.expandGroup(1);
        expListView.expandGroup(2);

        FloatingActionButton fab = (FloatingActionButton) v.findViewById(R.id.create_game_button);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.switchToCreateGameFragment();
            }
        });

        return v;

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_lobby, menu);

        MenuItem refresh = menu.findItem(R.id.action_refresh);

        refresh.setIcon(new IconDrawable(getActivity(), FontAwesomeIcons.fa_refresh)
                .colorRes(R.color.menu_options)
                .actionBarSize());
    }

    public void refreshLobby() {
        adapter.notifyDataSetChanged();
    }

    public void createToast(String message) {
        Toast t = Toast.makeText(controller.getLobbyActivity(), message, Toast.LENGTH_SHORT);
        t.show();
    }

    private void setButtonOnClick(Button button, int groupIndex, final String gameId, final String playerId) {
        switch (groupIndex){
            case 0: // game is owned by player but not started yet
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ClientModelFacade.getInstance().setGameId(gameId);
                        ClientModelFacade.getInstance().setPlayerId(playerId);
                        ClientModelFacade.getInstance().reset();
                        controller.startGame(gameId);
                    }
                });
                break;
            case 1: // you are already in this game and it is started
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ClientModelFacade.getInstance().setGameId(gameId);
                        ClientModelFacade.getInstance().setPlayerId(playerId);
                        ClientModelFacade.getInstance().reset();
                        new AsyncSyncGameTask().execute();
                    }
                });
                break;
            default:
                // do nothing
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        firebaseMessageReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                controller.updateGameLists();
            }
        };
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(firebaseMessageReceiver, new IntentFilter(getString(R.string.firebase_refreshlobby)));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(firebaseMessageReceiver);
    }

    public class GameExpListAdapter extends BaseExpandableListAdapter {
        private String[] groupNames = getResources().getStringArray(R.array.games_list_names);
        String[] actionButtonText = getResources().getStringArray(R.array.games_list_action_button_text);
        private Context context;
        private List<LobbyGameMetadata> getListForGroup(int group) {
            switch(group) {
                case 0:
                    return ClientModelFacade.getInstance().getOwnedGames();
                case 1:
                    return ClientModelFacade.getInstance().getJoinedGames();
                case 2:
                    return ClientModelFacade.getInstance().getOpenGames();
                default:
                    return ClientModelFacade.getInstance().getOwnedGames();
                    // What should we send if there is an invalid request? just sending 0
            }
        }

        public GameExpListAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getGroupCount() {
            return 3;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return getListForGroup(groupPosition).size();
        }

        @Override
        public Object getGroup(int groupPosition) {
            // TODO check valid index?
            return groupNames[groupPosition];
        }

        @Override
        public Object getChild(int groupPosition, int childPosition) {
            return getListForGroup(groupPosition).get(childPosition);
        }

        @Override
        public long getGroupId(int groupPosition) {
            return groupPosition;
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
            String title = groupNames[groupPosition];
            if(convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.fragment_lobby_list, null);
            }

            TextView titleTextView = (TextView) convertView.findViewById(R.id.fragment_lobby_list_title);
            titleTextView.setTypeface(null, Typeface.BOLD_ITALIC);
            titleTextView.setText(title);

            return convertView;
        }

        @Override
        public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
            LobbyGameMetadata game = getListForGroup(groupPosition).get(childPosition);
            String gameName = game.getGameName() + ":  ";

            if(convertView == null) {
                LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.fragment_lobby_list_item, null);
            }

            TextView gameNameTextView = (TextView) convertView.findViewById(R.id.list_item_game_name);
            gameNameTextView.setText(gameName);
            RecyclerView playerListRecyclerView = (RecyclerView) convertView.findViewById(R.id.list_player_recyclerview);
            playerListRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            ArrayList<Object> recyclerViewElements = new ArrayList<>();
            recyclerViewElements.addAll(game.getPlayerColorsNames().values());
            Button actionButton = (Button) convertView.findViewById(R.id.list_item_action_button);
            if(groupPosition == 0 && !game.isStarted()) {//Set start button
                actionButton.setText(actionButtonText[0]);
                setButtonOnClick(actionButton,0,game.getGameId(),game.getMyPlayerId());
                actionButton.setVisibility(View.VISIBLE);
            } else if (groupPosition == 1 && !game.isStarted()) {
                // Game joined but not started, don't show button
                actionButton.setVisibility(View.GONE);
            } else if (game.isStarted()) {//Set Play button
                actionButton.setVisibility(View.VISIBLE);
                actionButton.setText(actionButtonText[1]);
                setButtonOnClick(actionButton, 1, game.getGameId(),game.getMyPlayerId());
            } else if (groupPosition == 2){//Add join button to recycler view
                //OnClickListener implemented in PlayerListAdapter
                recyclerViewElements.add(new JoinButton());
                actionButton.setVisibility(View.GONE);
            }
            mPlayerAdapter = new PlayerListAdapter(recyclerViewElements, controller, game, getFragmentManager());
            playerListRecyclerView.setAdapter(mPlayerAdapter);

            return convertView;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }


    }
}
