package com.derds.tickettoride.lobby.ui;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.derds.models.PlayerColor;

import java.util.ArrayList;
import java.util.List;

import com.derds.tickettoride.lobby.ILobbyController;
import com.derds.tickettoride.lobby.LobbyController;
import com.derds.tickettoride.R;

/**
 * A placeholder fragment containing a simple view.
 */
public class CreateGameFragment extends Fragment {

    ILobbyController controller;
    EditText gameNameEditor;
    ProgressBar progressBar;
    Button createGameCreateButton;
    private Spinner playerColor;

    public CreateGameFragment() {
    }

    public void setController(ILobbyController controller) {
        this.controller = controller;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_create_game, container, false);
        gameNameEditor = (EditText) v.findViewById(R.id.create_game_name_entry);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        createGameCreateButton = (Button) v.findViewById(R.id.create_game_create_button);
        playerColor = (Spinner) v.findViewById(R.id.color_spinner_create_game);
        final PlayerColor[] unpickedColors = PlayerColor.values();
        List<String> unpickedColorsString = new ArrayList<>();
        for(PlayerColor color : unpickedColors){
            unpickedColorsString.add(color.toString());
        }

        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, unpickedColorsString);

        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        playerColor.setAdapter(spinnerAdapter);

        createGameCreateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getActivity().getClass().getSimpleName().equals("LobbyActivity")) {
                    ((LobbyActivity) getActivity()).enableCreateGameBtn(false);
                }
                PlayerColor color = unpickedColors[playerColor.getSelectedItemPosition()];

                progressBar.setVisibility(View.VISIBLE);
                controller.createGame(gameNameEditor.getText().toString(), unpickedColors[playerColor.getSelectedItemPosition()]);
            }
        });
        return v;
    }
}