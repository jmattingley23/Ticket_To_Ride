package com.derds.tickettoride.lobby;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.ICmdData;
import com.derds.cmddata.JoinGameCmdData;
import com.derds.cmddata.RefreshLobbyCmdData;
import com.derds.cmddata.StartGameCmdData;
import com.derds.models.PlayerColor;
import com.derds.result.GameResult;
import com.derds.result.RefreshLobbyResult;
import com.derds.result.Result;
import com.derds.tickettoride.lobby.ui.LobbyActivity;
import com.derds.tickettoride.lobby.ui.LobbyFragment;
import com.derds.tickettoride.async.AsyncLobbyTask;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.lobby.ui.CreateGameFragment;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by drc95 on 10/6/17.
 */

/**
 * @invariant The user must be logged in for this class to work properly
 * @invariant A server must be running for this class to function properly
 */
public class LobbyController implements Observer, ILobbyController{
    private static final String TAG = "LobbyController";
    private LobbyActivity lobbyActivity;
    private LobbyFragment lobbyFragment;
    private CreateGameFragment createGameFragment;
    private Fragment currentFragment;

    /**
     * @pre none
     * @param activity
     * @post LobbyController is instantiated correctly, is observing the ClientModelFacade, and the gui has been updated to show the game lobby
     */
    public LobbyController(LobbyActivity activity) {
        Log.v(TAG, "LobbyController Constructor");
        lobbyActivity = activity;

        // register with the observable
        ClientModelFacade.getInstance().addObserver(this);

        // set up display and fragments
        if(lobbyFragment == null) {
            lobbyFragment = new LobbyFragment();
            lobbyFragment.setController(this);
        }

        if(createGameFragment == null) {
            createGameFragment = new CreateGameFragment();
            createGameFragment.setController(this);
        }

        switchToLobbyFragment();

    }

    @Override
    public void setLobbyActivity(LobbyActivity lobbyActivity) {
        this.lobbyActivity = lobbyActivity;
    }

    @Override
    public LobbyActivity getLobbyActivity() {
        return lobbyActivity;
    }

    @Override
    public void setLobbyFragment(LobbyFragment lobbyFragment) {
        this.lobbyFragment = lobbyFragment;
    }

    @Override
    public void setCreateGameFragment(CreateGameFragment createGameFragment) {
        this.createGameFragment = createGameFragment;
    }

    /**
     * @pre ClientModelFacade is setup correctly
     * @param o The observable that notified this LobbyController
     * @param arg The update message object
     * @post Updates to the ClientModelFacade are now reflected on the lobby
     */
    @Override
    public void update(Observable o, Object arg) {
        Log.v(TAG,"update from ClientModelFacade");
        if(arg.getClass().getSimpleName().equals(String.class.getSimpleName())) {
            String type = (String) arg;
            if (type.equals("GameList")) {
                refreshLobby(); // refresh the lobby if the games list has been updated
            } else if(type.equals("GameReady")) {
                lobbyActivity.switchToGameActivity();
            }
        }
    }

    /**
     * @pre User is logged in correctly
     * @post List of games is up to date with cached data and lobby display is refreshed
     */
    @Override
    public void refreshLobby() {
        Log.v(TAG,"Entering refreshLobby");
        lobbyFragment.refreshLobby();
        Log.v(TAG,"Exiting RefreshLobby");
    }

    /**
     * @pre an async task has completed execution and succeded
     * @param result A result from an asynchronous call to the server which has no error message
     * @param dataInput The command that was sent to the server associated with this call
     * @post The gui has been updated to the correct view to reflect the action taken on the server
     */
    @Override
    public void onSuccess(Result result, ICmdData dataInput){
        lobbyActivity.showProgressWheel(false);
        lobbyActivity.enableCreateGameBtn(true);


        if(dataInput instanceof RefreshLobbyCmdData){
            RefreshLobbyResult newResult = (RefreshLobbyResult) result;
            ClientModelFacade.getInstance().updateGameLists(newResult.getOpenGames(), newResult.getCurrentGames(), newResult.getPlayerGames());
        }
        else if(dataInput instanceof StartGameCmdData){
            GameResult gameResult = (GameResult) result;
        }
        else if(dataInput instanceof CreateGameCmdData){
            GameResult gameResult = (GameResult) result;
            switchToLobbyFragment();
        }
        else if(dataInput instanceof JoinGameCmdData){
            GameResult gameResult = (GameResult) result;
        }
    }

    /**
     * @pre an async task has failed or the server has sent an error response.
     * @param result A result from an asynchronous call to the server which has an error message
     * @post relevant gui elements have been modified from their disabled/loading state and a message with the error has been shown to the user
     */
    @Override
    public void onError(Result result){
        lobbyActivity.showProgressWheel(false);
        lobbyActivity.enableCreateGameBtn(true);

        Log.e(TAG, "LobbyController got an error result:\n" + result.getMessage());
        lobbyFragment.createToast(result.getMessage());
    }

    /**
     * @pre user is logged in correctly
     * @post cached list of games has been updated (albeit asynchronously)
     */
    @Override
    public void updateGameLists() {
        Log.v(TAG,"Entering updateGameLists");
        AsyncLobbyTask asyncLobbyTask = new AsyncLobbyTask(this);
        asyncLobbyTask.execute(new RefreshLobbyCmdData());
        Log.v(TAG,"Exiting updateGameLists");
    }

    /**
     * @pre user owns the game associated with the game id and it hasn't been started yet
     * @param gameId is the id of the selected game
     * @post The game has been started and game view is brought up
     */
    @Override
    public void startGame(String gameId) {
        Log.v(TAG,"Entering startGame");
        if(gameId == null) {
            Log.e(TAG, "gameId in startGame was null");
            lobbyFragment.createToast("An error occured");
        }
        AsyncLobbyTask asyncLobbyTask = new AsyncLobbyTask(this);
        asyncLobbyTask.execute(new StartGameCmdData(gameId));
        Log.v(TAG,"Exiting startGame");
    }

    /**
     * @pre user has selected a valid game to join
     * @param gameId is the id of the selected game
     * @post user has joined the game
     */
    @Override
    public void joinGame(String gameId, PlayerColor color) {
        Log.v(TAG,"Entering joinGame");
        if(gameId == null) {
            Log.e(TAG, "gameId in joinGame was null");
            lobbyFragment.createToast("An error occured");
        }
        AsyncLobbyTask asyncLobbyTask = new AsyncLobbyTask(this);
        asyncLobbyTask.execute(new JoinGameCmdData(gameId, color));
        Log.v(TAG,"Exiting joinGame");
    }

    @Override
    public void switchToCreateGameFragment() {
        currentFragment = createGameFragment;
        lobbyActivity.switchToCreateGame(createGameFragment);
    }

    /**
     * @pre Lobby activity is active and in a valid state
     * @post The Lobby Activity has the lobby fragment (containing the lists of games) as its active fragment
     */
    @Override
    public void switchToLobbyFragment() {
        currentFragment = lobbyFragment;
        lobbyActivity.switchToLobby(lobbyFragment);
    }

    /**
     * @pre user has entered valid game information (Valid name, valid player color picked)
     * @post a new game owned by the user has been created on the server (an async update is also on its way, but may have not arrived by the time this returns)
     */
    @Override
    public void createGame(String gameName, PlayerColor color) {
        Log.v(TAG,"Entering createGame");
        AsyncLobbyTask asyncLobbyTask = new AsyncLobbyTask(this);
        asyncLobbyTask.execute(new CreateGameCmdData(gameName, color));
        Log.v(TAG,"Exiting createGame");
    }

    /**
     * @pre back was pressed
     * @post correct fragments were moved
     * @return true if should use default onBackPressed()
     */
    @Override
    public boolean backPressed() {
        if(currentFragment.equals(createGameFragment)) {
            switchToLobbyFragment();
            return false;
        } else {
            return true;
        }
    }
}
