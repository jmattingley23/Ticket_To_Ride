package com.derds.tickettoride.lobby.ui;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.derds.models.LobbyGameMetadata;
import com.derds.models.PlayerColor;
import com.derds.tickettoride.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aaron on 10/21/2017.
 */

public class ChooseColorDialog extends DialogFragment {

    private LobbyGameMetadata mGame;
    private Spinner mColorSpinner;

    /* The activity that creates an instance of this dialog fragment must
    * implement this interface in order to receive event callbacks.
    * Each method passes the DialogFragment in case the host needs to query it.
    */
    public interface ChooseColorDialogListener {
        public void onDialogPositiveClick(DialogFragment dialog, String gameId, PlayerColor color);
        public void onDialogNegativeClick(DialogFragment dialog);
    }

    // Use this instance of the interface to deliver action events
    ChooseColorDialogListener mListener;

    // Override the Fragment.onAttach() method to instantiate the ChooseColorDialogListener
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        // Verify that the host activity implements the callback interface
        try {
            // Instantiate the ChooseColorDialogListener so we can send events to the host
            mListener = (ChooseColorDialogListener) context;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(context.toString()
                    + " must implement NoticeDialogListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();
        mGame = (LobbyGameMetadata) args.getSerializable("gameObject");
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View v = inflater.inflate(R.layout.color_picker, null);
        mColorSpinner = (Spinner)v.findViewById(R.id.color_spinner);

        PlayerColor[] unpickedColors = PlayerColor.values();
        final List<PlayerColor> unpickedColorsList = new ArrayList<>();
        for(PlayerColor color : unpickedColors){
            if(!mGame.isColorTaken(color)){
                unpickedColorsList.add(color);
            }
        }

        List<String> unpickedColorsString = new ArrayList<>();
        for(PlayerColor color : unpickedColorsList){
            unpickedColorsString.add(color.toString());
        }

        ArrayAdapter<String> spinnerAdapter =
                new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, unpickedColorsString);

        mColorSpinner.setAdapter(spinnerAdapter);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(v)
                // Add action buttons
                .setPositiveButton(R.string.join_game_label, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the positive button event back to the host activity
                        mListener.onDialogPositiveClick(ChooseColorDialog.this, mGame.getGameId(),
                                unpickedColorsList.get(mColorSpinner.getSelectedItemPosition()));
                    }
                })
                .setNegativeButton(R.string.cancel_join, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Send the negative button event back to the host activity
                        mListener.onDialogNegativeClick(ChooseColorDialog.this);
                    }
                }).setTitle(R.string.choose_color_dialog_title);

        return builder.create();
    }
}
