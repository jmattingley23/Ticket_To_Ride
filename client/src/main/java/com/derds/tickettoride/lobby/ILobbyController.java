package com.derds.tickettoride.lobby;

import android.util.Log;

import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.ICmdData;
import com.derds.cmddata.JoinGameCmdData;
import com.derds.cmddata.RefreshLobbyCmdData;
import com.derds.cmddata.StartGameCmdData;
import com.derds.models.PlayerColor;
import com.derds.result.GameResult;
import com.derds.result.RefreshLobbyResult;
import com.derds.result.Result;
import com.derds.tickettoride.async.AsyncLobbyTask;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.lobby.ui.CreateGameFragment;
import com.derds.tickettoride.lobby.ui.LobbyActivity;
import com.derds.tickettoride.lobby.ui.LobbyFragment;

import java.util.Observable;

/**
 * Created by Aaron on 11/25/2017.
 */

public interface ILobbyController {

    public void setLobbyActivity(LobbyActivity lobbyActivity);

    public LobbyActivity getLobbyActivity();

    public void setLobbyFragment(LobbyFragment lobbyFragment);

    public void setCreateGameFragment(CreateGameFragment createGameFragment);

    public void update(Observable o, Object arg);

    public void refreshLobby();

    public void onSuccess(Result result, ICmdData dataInput);

    public void onError(Result result);

    public void updateGameLists();

    public void startGame(String gameId);

    public void joinGame(String gameId, PlayerColor color);

    public void switchToCreateGameFragment();

    public void switchToLobbyFragment();

    public void createGame(String gameName, PlayerColor color);

    public boolean backPressed();
}
