package com.derds.tickettoride.lobby.ui;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.derds.models.LobbyGameMetadata;
import com.derds.tickettoride.R;
import com.derds.tickettoride.lobby.ILobbyController;
import com.derds.tickettoride.lobby.LobbyController;
import com.derds.tickettoride.lobby.ui.ChooseColorDialog;
import com.derds.tickettoride.lobby.ui.JoinButton;

import java.util.List;

/**
 * Created by Aaron on 10/9/2017.
 */

public class PlayerListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> mPlayers;
    private static final int PLAYER = 0;
    private static final int JOIN = 1;
    private ILobbyController mLobbyController;
    private LobbyGameMetadata mGame;
    private FragmentManager mFragManager;

    public PlayerListAdapter(List<Object> players, ILobbyController lc, LobbyGameMetadata game, FragmentManager fm){
        this.mPlayers = players;
        this.mLobbyController = lc;
        this.mGame = game;
        this.mFragManager = fm;
    }

    public static class ViewHolderPlayer extends RecyclerView.ViewHolder {
        public TextView mPersonName;
        public View mView;
        public ViewHolderPlayer(View v){
            super(v);
            mView = v;
            mPersonName = (TextView)v.findViewById(R.id.player_name_text_view);
        }
    }

    public static class ViewHolderJoin extends RecyclerView.ViewHolder {
        public ViewHolderJoin(View v){
            super(v);
        }
    }

    public void updateList(List<Object> players){
        this.mPlayers = players;
    }

    @Override
    public int getItemViewType(int position) {
        if (mPlayers.get(position) instanceof JoinButton) {
            return JOIN;
        } else {
            return PLAYER;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case PLAYER:
                View v1 = inflater.inflate(R.layout.player_list_item, parent, false);
                viewHolder = new ViewHolderPlayer(v1);
                break;
            default:
                View v2 = inflater.inflate(R.layout.join_game_layout, parent, false);
                viewHolder = new ViewHolderJoin(v2);
                v2.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogFragment chooseColor = new ChooseColorDialog();
                        Bundle args = new Bundle();
                        //args.putString("gameId", mGame.getGameId());
                        args.putSerializable("gameObject", mGame);
                        chooseColor.setArguments(args);
                        chooseColor.show(mFragManager, "joinGameDialog");
                    }
                });
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position){
        switch (holder.getItemViewType()) {
            case PLAYER:
                ViewHolderPlayer vh1 = (ViewHolderPlayer) holder;
                vh1.mPersonName.setText(((String)mPlayers.get(position)));
                break;
            default:
                ViewHolderJoin vh2 = (ViewHolderJoin) holder;
                break;
        }
    }

    @Override
    public int getItemCount(){
        return mPlayers.size();
    }

}
