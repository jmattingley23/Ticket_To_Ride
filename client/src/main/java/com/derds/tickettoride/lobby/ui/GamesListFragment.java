package com.derds.tickettoride.lobby.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.derds.tickettoride.R;
import com.derds.tickettoride.lobby.ILobbyController;
import com.derds.tickettoride.lobby.LobbyController;

/**
 * Created by drc95 on 10/7/17.
 */

public class GamesListFragment extends Fragment {
    ILobbyController controller;

    public void setController(ILobbyController newController) {
        controller = newController;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lobby_list, container, false);
    }
}
