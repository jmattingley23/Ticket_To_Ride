package com.derds.tickettoride.lobby.ui;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.derds.models.PlayerColor;
import com.derds.tickettoride.R;
import com.derds.tickettoride.game.ui.GameActivity;
import com.derds.tickettoride.lobby.ILobbyController;
import com.derds.tickettoride.lobby.LobbyController;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;

public class LobbyActivity extends AppCompatActivity implements ChooseColorDialog.ChooseColorDialogListener{

    // TODO make proper request codes
    final int REQUEST_CODE_PLAY_GAME = 3749;

    private ILobbyController controller;
    private FragmentManager manager;
    private static boolean isActiveActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // TODO add bundle loading stuff here?

        Iconify.with(new FontAwesomeModule());

        setContentView(R.layout.activity_lobby);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        manager = getSupportFragmentManager();

        controller = new LobbyController(this);
    }

    public void switchToLobby(LobbyFragment lobbyFragment) {
        Fragment prev_fragment = manager.findFragmentById(R.id.lobby_activity_fragment_container);
        if(prev_fragment == null) {
            manager.beginTransaction()
                    .add(R.id.lobby_activity_fragment_container, lobbyFragment)
                    .commit();
        } else {
            manager.beginTransaction()
                    .replace(R.id.lobby_activity_fragment_container, lobbyFragment)
                    .commit();
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case android.R.id.home:
                // this takes the user 'back', as if they pressed the left-facing triangle icon on the main android toolbar.
                // if this doesn't work as desired, another possibility is to call `finish()` here.
                onBackPressed();
                return true;
            case R.id.action_refresh:
                controller.updateGameLists();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void switchToCreateGame(CreateGameFragment createGameFragment) {
        Fragment prev_fragment = manager.findFragmentById(R.id.lobby_activity_fragment_container);
        if(prev_fragment == null) {
            manager.beginTransaction()
                    .add(R.id.lobby_activity_fragment_container, createGameFragment)
                    .commit();
        } else {
            manager.beginTransaction()
                    .replace(R.id.lobby_activity_fragment_container, createGameFragment)
                    .commit();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        isActiveActivity = true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActiveActivity = false;
    }

    public void switchToGameActivity() {
        if(isActiveActivity) {
            Intent intent = new Intent(this, GameActivity.class);
            startActivityForResult(intent, REQUEST_CODE_PLAY_GAME);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // TODO pass results into LobbyController

    }

    @Override
    public void onBackPressed()
    {
        if(controller.backPressed()) super.onBackPressed();
    }

    public void showProgressWheel(boolean show) {
        if (findViewById(R.id.progressBar) != null) {
            if (show) {
                findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            } else {
                findViewById(R.id.progressBar).setVisibility(View.GONE);
            }
        }
    }

    public void enableCreateGameBtn(boolean enable) {
        Button createGameCreateBtn = (Button) findViewById(R.id.create_game_create_button);

        if (createGameCreateBtn != null) {
            if (!enable) {
                createGameCreateBtn.getBackground().setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.primaryButtonDisabled), PorterDuff.Mode.MULTIPLY);
                createGameCreateBtn.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.grayedText));
            } else {
                createGameCreateBtn.getBackground().setColorFilter(ContextCompat.getColor(getBaseContext(), R.color.primaryButton), PorterDuff.Mode.MULTIPLY);
                createGameCreateBtn.setTextColor(ContextCompat.getColor(getBaseContext(), R.color.whiteText));
            }
            createGameCreateBtn.setEnabled(enable);
        }
    }

    // The dialog fragment receives a reference to this Activity through the
    // Fragment.onAttach() callback, which it uses to call the following methods
    // defined by the NoticeDialogFragment.NoticeDialogListener interface
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, String gameId, PlayerColor color) {
        controller.joinGame(gameId, color);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {
        // User touched the dialog's negative button

    }
}
