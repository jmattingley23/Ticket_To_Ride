package com.derds.tickettoride.turnstate;

import com.derds.models.Player;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class EndTurnState extends TurnState {
    public static EndTurnState instance = new EndTurnState();
    public static TurnState.StateName key = StateName.END;

    @Override
    public boolean wait(Player player) {
        // do nothing
        return false;
    }
}
