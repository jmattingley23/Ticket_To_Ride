package com.derds.tickettoride.turnstate;

import com.derds.models.Player;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class WaitState extends TurnState {
    public static WaitState instance = new WaitState();
    public static StateName key = StateName.WAIT;

    private WaitState() {
        prevState = StateName.WAIT;
    }

    private StateName prevState;

    @Override
    public boolean wait(Player player) {
        // do nothing
        return true;
    }

    public StateName getPrevState() {
        return prevState;
    }

    public void setPrevState(StateName prevState) {
        this.prevState = prevState;
    }

    public void revertToPreviousState() {
        ClientModelFacade.getInstance().getPlayer().setTurnState(prevState);
        getTurnStateExecutable().sendMessage("An error occurred (reverted to previous state)");
        ClientModelFacade.getInstance().addToGameHistory(
                ClientModelFacade.getInstance().getPlayer().getMyUser().getFirstname() + " reverted to the " + prevState);
    }
}
