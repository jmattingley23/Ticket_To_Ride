package com.derds.tickettoride.turnstate;

import com.derds.models.DestinationCard;
import com.derds.models.Player;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class BeginGameState extends TurnState {
    public static BeginGameState instance = new BeginGameState();
    public static TurnState.StateName key = StateName.BEGIN_GAME;
    private static String notEnoughCards = "You must select at least 2 destination cards to begin the game";

    private BeginGameState() {}

    @Override
    public boolean claimDestinationCards(Player player, List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {
        
        return false;
    }

    @Override
    public boolean drawDestinationCards(Player player) {
        // not allowed - eventually
        wait(player);

        if(player.getMyGame().isPlayersTurn(player.getPlayerId())) {
            getTurnStateExecutable().drawDestinationCardsRequest();
        } else {
            getTurnStateExecutable().sendMessage("The possibility of successfully drawing destination cards when it's not your turn is approximately 3720 to 1!");
        }

        return true;
    }

    @Override
    public boolean wait(Player player) {
        WaitState.instance.setPrevState(key);
        player.setTurnState(WaitState.key);
        return true;
    }
}
