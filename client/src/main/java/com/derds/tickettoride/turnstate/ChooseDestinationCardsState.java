package com.derds.tickettoride.turnstate;

import com.derds.models.DestinationCard;
import com.derds.models.Player;
import com.derds.tickettoride.common.AuthToken;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class ChooseDestinationCardsState extends TurnState {
    public static ChooseDestinationCardsState instance = new ChooseDestinationCardsState();
    public static TurnState.StateName key = StateName.CHOOSE_DESTINATIONS;

    private ChooseDestinationCardsState() {}

    @Override
    public boolean claimDestinationCards(Player player, List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {

        int minNumCards;
        if(player.getHand().getDestCardCount() == 0) {
            minNumCards = 2;
        } else minNumCards = 1;

        if (pickedCards.size() < minNumCards) {
            getTurnStateExecutable().sendMessage("You didn't pick enough cards");
            getTurnStateExecutable().failedClaimDestinationCards(player.getPlayerId(), pickedCards, unpickedCards);
            return false; // they did something invalid don't continue
        }

        wait(player);

        getTurnStateExecutable().claimDestinationCards(AuthToken.getInstance().getAuthToken(),
                player.getPlayerId(),
                pickedCards,
                unpickedCards);

        return true;
    }

    @Override
    public boolean drawDestinationCards(Player player) {
        // Dont show a message, they are probably just trying to make the destination card deck show up again
        return false;
    }

    @Override
    public boolean wait(Player player) {
        WaitState.instance.setPrevState(key);
        player.setTurnState(WaitState.key);
        return true;
    }
}
