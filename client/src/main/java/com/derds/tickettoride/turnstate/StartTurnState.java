package com.derds.tickettoride.turnstate;

import com.derds.models.Player;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class StartTurnState extends TurnState {
    public static StartTurnState instance = new StartTurnState();
    public static TurnState.StateName key = StateName.START_TURN;

    private StartTurnState() {}

    @Override
    public boolean drawDestinationCards(Player player) {

        wait(player);

        if(player.getMyGame().getDestinationDeckCount() > 0) {
            getTurnStateExecutable().drawDestinationCardsRequest();
            return true;
        } else {
            getTurnStateExecutable().sendMessage("There are no more destination cards to draw!");
            return false;
        }

    }

    @Override
    public boolean selectTrainCard(Player player, TrainCard card) {
        wait(player);
        getTurnStateExecutable().drawFaceUpCard(null,player.getPlayerId(),card, false);
        return true;
    }

    @Override
    public boolean drawTrainCardFromDeck(Player player){
        wait(player);
        getTurnStateExecutable().drawTrainCardFromDeck(null,player.getPlayerId(), false);
        return true;
    }

    @Override
    public boolean claimRoute(Player player, Route route, List<TrainCard> cardsUsed) {
        if(route.canClaim(cardsUsed)) {
            wait(player);
            getTurnStateExecutable().claimRoute(null,player.getPlayerId(),route,cardsUsed,route.getLength());
        } else {
            // TODO get rid of this jank
            player.setTurnState(StateName.START_TURN);
            getTurnStateExecutable().sendMessage("Can't claim route with selected cards");
        }
        return true;
    }

    @Override
    public boolean wait(Player player) {
        WaitState.instance.setPrevState(key);
        player.setTurnState(WaitState.key);
        return true;
    }

    @Override
    public boolean selectRoute(Player player, Route route) {
        if(!route.isClaimPossible(player.getHand().getPlayerTrainCards())){
            getTurnStateExecutable().sendMessage("Not enough resources");
            return false;
        }

        if(player.getMyGame().getNumPiecesLeft(player.getPlayerId()) < route.getLength()){
            getTurnStateExecutable().sendMessage("Not enough ship pieces");
            return false;
        }

        getTurnStateExecutable().claimRoute(null,player.getPlayerId(),route,null,route.getLength());
        return true;
    }
}
