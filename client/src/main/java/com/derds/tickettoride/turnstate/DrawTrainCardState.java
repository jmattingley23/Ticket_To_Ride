package com.derds.tickettoride.turnstate;

import com.derds.models.Player;
import com.derds.models.TrainCard;
import com.derds.models.TrainColor;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.turnstate.TurnState;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class DrawTrainCardState extends TurnState {
    public static DrawTrainCardState instance = new DrawTrainCardState();
    public static TurnState.StateName key = StateName.DRAW_TRAIN_CARD;

    private DrawTrainCardState() {}

    @Override
    public boolean selectTrainCard(Player player, TrainCard card) {
        if(card.getColor() == TrainColor.RAINBOW) {
            getTurnStateExecutable().sendMessage("You can't select a rainbow card now");
            return false;
        }
        wait(player);
        getTurnStateExecutable().drawFaceUpCard(null, player.getPlayerId(), card, false);
        return true;
    }

    @Override
    public boolean drawTrainCardFromDeck(Player player){
        wait(player);
        getTurnStateExecutable().drawTrainCardFromDeck(null,player.getPlayerId(), false);
        return true;
    }

    @Override
    public boolean wait(Player player) {
        WaitState.instance.setPrevState(key);
        player.setTurnState(WaitState.key);
        return true;
    }
}
