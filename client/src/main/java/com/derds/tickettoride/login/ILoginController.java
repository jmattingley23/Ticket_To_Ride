package com.derds.tickettoride.login;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.derds.cmddata.LoginCmdData;
import com.derds.cmddata.RegisterCmdData;
import com.derds.result.LoginResult;
import com.derds.tickettoride.common.ServerProxy;
import com.derds.tickettoride.login.ui.LoginActivity;
import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by Aaron on 11/25/2017.
 */

public interface ILoginController {

    public void setLoginActivity(LoginActivity loginActivity);

    public void login(String username, String password);

    public void register(String username, String password, String firstname, String lastname);

    public boolean validateData(boolean register, String username, String password, String firstname, String lastname);

    public void showLoadSpinnerDialog(boolean show);

}
