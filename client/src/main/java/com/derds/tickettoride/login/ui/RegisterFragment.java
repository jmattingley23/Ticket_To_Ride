package com.derds.tickettoride.login.ui;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.derds.tickettoride.R;
import com.derds.tickettoride.common.ClientCommunicator;
import com.derds.tickettoride.login.ILoginController;
import com.derds.tickettoride.login.LoginController;
import com.derds.tickettoride.login.ui.LoginActivity;

/**
 * A placeholder fragment containing a simple view.
 */
public class RegisterFragment extends Fragment {

    private EditText ipAddressText;
    private EditText usernameET;
    private EditText passwordET;
    private EditText firstnameET;
    private EditText lastnameET;
    private Button registerBtn;
    private ILoginController loginController;
    private ProgressBar mSpinner;

    public RegisterFragment() {}

    public void setLoginController(ILoginController loginController) {
        this.loginController = loginController;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        setHasOptionsMenu(true);

        ipAddressText = (EditText) view.findViewById(R.id.ip_address);
        usernameET = (EditText) view.findViewById(R.id.user_name);
        passwordET = (EditText) view.findViewById(R.id.password);
        firstnameET = (EditText) view.findViewById(R.id.first_name);
        lastnameET = (EditText) view.findViewById(R.id.last_name);
        registerBtn = (Button) view.findViewById(R.id.register_btn);
        mSpinner = (ProgressBar) view.findViewById(R.id.progressBar);

        ipAddressText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ClientCommunicator.getInstance().setAddress(ipAddressText.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSpinner.setVisibility(View.VISIBLE);
                loginController.register(usernameET.getText().toString(), passwordET.getText().toString(),
                        firstnameET.getText().toString(), lastnameET.getText().toString());
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        String firstname = "",
                lastname = "",
                username = "",
                password = "";

        Bundle args = getArguments();
        if (args != null) {
            firstname = args.getString(getString(R.string.EXTRA_FIRSTNAME));
            lastname = args.getString(getString(R.string.EXTRA_LASTNAME));
            username = args.getString(getString(R.string.EXTRA_USERNAME));
            password = args.getString(getString(R.string.EXTRA_PASSWORD));

            firstnameET.setText(firstname);
            lastnameET.setText(lastname);
            usernameET.setText(username);
            passwordET.setText(password);
        }

        loginController = new LoginController((LoginActivity) getActivity());

        loginController.validateData(true, firstname, lastname, username, password);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();

        inflater.inflate(R.menu.menu_login, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home :
                getActivity().finish();
                return true;
            default :
                System.out.println("this should never happen!");
                return false;
        }
    }
}