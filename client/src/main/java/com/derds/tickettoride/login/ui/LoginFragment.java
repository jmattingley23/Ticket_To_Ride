package com.derds.tickettoride.login.ui;

import android.graphics.Typeface;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.derds.tickettoride.R;
import com.derds.tickettoride.common.ClientCommunicator;
import com.derds.tickettoride.login.ILoginController;
import com.derds.tickettoride.login.LoginController;

/**
 * A placeholder fragment containing a simple view.
 */
public class LoginFragment extends Fragment {
    private static final String TAG = "LoginFragment";
    private EditText ipAddressText;
    private EditText usernameText;
    private EditText passwordText;
    private Button loginBtn;
    private TextView registerFragmentBtn;
    private ILoginController loginController;
    private ProgressBar mSpinner;

    public LoginFragment() {}

    public void setLoginController(ILoginController loginController) {
        this.loginController = loginController;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        setHasOptionsMenu(true);

        ipAddressText = (EditText) view.findViewById(R.id.ip_address);
        usernameText = (EditText) view.findViewById(R.id.user_name);
        passwordText = (EditText) view.findViewById(R.id.password);
        loginBtn = (Button) view.findViewById(R.id.login_btn);
        registerFragmentBtn = (TextView) view.findViewById(R.id.register_fragment_button);
        mSpinner = (ProgressBar) view.findViewById(R.id.progressBar);

        ipAddressText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ClientCommunicator.getInstance().setAddress(ipAddressText.getText().toString());
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });

        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mSpinner.setVisibility(View.VISIBLE);
                loginController.showLoadSpinnerDialog(true);
                loginController.login(usernameText.getText().toString(), passwordText.getText().toString());
            }
        });

        registerFragmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loginController.showLoadSpinnerDialog(true);
                RegisterFragment registerFragment = new RegisterFragment();
                registerFragment.setLoginController(loginController);

                FragmentManager fm = getFragmentManager();
                fm.beginTransaction().replace(R.id.login_fragment_container, registerFragment)
                        .addToBackStack("register").commit();
            }
        });

        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
       super.onViewCreated(view, savedInstanceState);

        String username = "",
                password = "";

        Bundle args = getArguments();

        if (args != null) { //data was previously in fields, probably from a previous screen orientation
            /*username = args.getString(getString(R.string.EXTRA_USERNAME));
            password = args.getString(getString(R.string.EXTRA_PASSWORD));

            //set fields and check button based on what was passed in
            usernameText.setText(username);
            passwordText.setText(password);*/

        }

        loginController = new LoginController((LoginActivity) getActivity());

        loginController.validateData(false, username, password, null, null);
    }
}
