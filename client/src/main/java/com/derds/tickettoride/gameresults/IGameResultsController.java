package com.derds.tickettoride.gameresults;

import android.support.v4.app.Fragment;

import com.derds.tickettoride.game.GameController;
import com.derds.tickettoride.game.IGameController;
import com.derds.tickettoride.gameresults.ui.GameResultsActivity;

/**
 * Created by Aaron on 11/25/2017.
 */

public interface IGameResultsController {

    public IGameController getGameController();

    public void setGameController(IGameController gameController);

    public GameResultsActivity getActivity();

    public void setActivity(GameResultsActivity activity);

    public void switchToFragment(Fragment fragment);
}
