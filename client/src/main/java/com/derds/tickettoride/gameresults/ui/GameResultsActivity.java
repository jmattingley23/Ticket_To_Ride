package com.derds.tickettoride.gameresults.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.derds.tickettoride.R;
import com.derds.tickettoride.gameresults.GameResultsController;
import com.derds.tickettoride.gameresults.IGameResultsController;
import com.derds.tickettoride.gameresults.ui.GameResultFragment;

/**
 * Created by justinbrunner on 11/20/17.
 */

public class GameResultsActivity extends AppCompatActivity {
    private IGameResultsController gameResultController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        gameResultController = new GameResultsController();
        gameResultController.setActivity(this);

        setContentView(R.layout.activity_game_result);

        FragmentManager fragmentManager = this.getSupportFragmentManager();

        GameResultFragment gameResultFragment = new GameResultFragment();
        gameResultFragment.setController(gameResultController);

        fragmentManager.beginTransaction().add(R.id.game_result_fragment_container, gameResultFragment).commit();
    }

    public void switchToFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().add(R.id.game_result_fragment_container, fragment).commit();
    }
}
