package com.derds.tickettoride.gameresults;

import com.derds.models.Message;
import com.derds.models.PlayerColor;
import com.derds.models.PlayerStats;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by justinbrunner on 10/28/17.
 */

public class FakeGameResultData {
    public static FakeGameResultData instance = new FakeGameResultData();

    private FakeGameResultData() {
        stats = new ArrayList<PlayerStats>();

        try {
            PlayerColor[] playerIcons = { PlayerColor.CYAN, PlayerColor.RED, PlayerColor.GREEN, PlayerColor.BLUE, PlayerColor.YELLOW };
            String[] playerNames = { "Justin", "Dallin", "Michael", "Jordan", "A-Aron" };
            Integer[] finalScore = { 150, 15, 120, 145, 135 };
            Integer[] routeScore = { 60, 40, 70, 65, 50 };
            // Integer[] destTotal = { 90, -25, 50, 80, 85 };
            Integer[] destComplete = { 90, 10, 60, 85, 100 };
            Integer[] destIncomplete = { 0, -35, -10, -5, -15 };
            Integer[] destCards = { 12, 5, 6, 5, 13 };
            Integer[] trainCardsLeft = { 0, 13, 4, 3, 2 };


//            for (int i = 0; i < playerIcons.length; i++) {
//                PlayerStats stats = new PlayerStats(playerNames[i], playerIcons[i], finalScore[i], routeScore[i],
//                        destComplete[i], destIncomplete[i], destCards[i], trainCardsLeft[i]);
//
//                this.stats.add(stats);
//            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private List<PlayerStats> stats;

    public List<PlayerStats> getStats() {
        return stats;
    }
}
