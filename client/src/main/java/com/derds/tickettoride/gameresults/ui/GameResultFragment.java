package com.derds.tickettoride.gameresults.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.derds.models.PlayerStats;
import com.derds.tickettoride.R;
import com.derds.tickettoride.chat.ui.ChatFragment;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.game.GameController;
import com.derds.tickettoride.gamehistory.ui.GameHistoryFragment;
import com.derds.tickettoride.gameresults.FakeGameResultData;
import com.derds.tickettoride.gameresults.GameResultsController;
import com.derds.tickettoride.gameresults.IGameResultsController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by justinbrunner on 11/20/17.
 */

public class GameResultFragment extends Fragment {
    private IGameResultsController controller;
    private TextView titleText;
    public ImageView backBtn;
    public ImageView chatBtn;
    public ImageView historyBtn;
    public RecyclerView resultsView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_game_result, container, false);

        titleText = (TextView) v.findViewById(R.id.game_result_title);
        backBtn = (ImageView) v.findViewById(R.id.back_button);
        chatBtn = (ImageView) v.findViewById(R.id.chat_button);
        historyBtn = (ImageView) v.findViewById(R.id.history_button);
        resultsView = (RecyclerView) v.findViewById(R.id.states_view);


        String title = String.format(getResources().getString(R.string.title_activity_game_results), "Game Title");
        titleText.setText(title);

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });

        chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.switchToFragment(ChatFragment.newInstance(controller.getGameController()));
            }
        });


        historyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                controller.switchToFragment(GameHistoryFragment.newInstance(controller.getGameController()));
            }
        });

        resultsView.setLayoutManager(new LinearLayoutManager(getContext()));
        ResultListAdapter adapter = new ResultListAdapter(ClientModelFacade.getInstance().getPlayerStatsList(), this);
        resultsView.setAdapter(adapter);

        return v;
    }

    public IGameResultsController getController() {
        return controller;
    }

    public void setController(IGameResultsController controller) {
        this.controller = controller;
    }
}
