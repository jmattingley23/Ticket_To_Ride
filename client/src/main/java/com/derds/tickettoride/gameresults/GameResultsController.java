package com.derds.tickettoride.gameresults;

import android.support.v4.app.Fragment;

import com.derds.tickettoride.game.GameController;
import com.derds.tickettoride.game.IGameController;
import com.derds.tickettoride.gameresults.ui.GameResultsActivity;

/**
 * Created by Aaron on 10/30/2017.
 */

public class GameResultsController implements IGameResultsController{
    private static final String TAG = "GameResultsController";
    private GameResultsActivity activity;
    private IGameController gameController;

    public GameResultsController(){
        gameController = new GameController(null);
    }

    public IGameController getGameController() {
        return gameController;
    }

    public void setGameController(IGameController gameController) {
        this.gameController = gameController;
    }

    public GameResultsActivity getActivity() {
        return activity;
    }

    public void setActivity(GameResultsActivity activity) {
        this.activity = activity;
    }

    public void switchToFragment(Fragment fragment) {
        activity.switchToFragment(fragment);
    }
}
