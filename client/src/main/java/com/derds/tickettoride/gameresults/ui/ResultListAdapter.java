package com.derds.tickettoride.gameresults.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.derds.models.Message;
import com.derds.models.PlayerColor;
import com.derds.models.PlayerStats;
import com.derds.models.TrainColor;
import com.derds.tickettoride.R;
import com.derds.tickettoride.chat.ui.ChatFragment;
import com.derds.tickettoride.common.ClientModelFacade;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.derds.models.TrainColor.WHITE;

/**
 * Created by Aaron on 10/9/2017.
 */

public class ResultListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<PlayerStats> stats;
    private static final int HEADER = 0;
    private static final int STATS = 1;
    private RecyclerView resultsRecycler;
    private GameResultFragment parent;

    public ResultListAdapter(List<PlayerStats> stats, GameResultFragment myParent) {
        parent = myParent;
        updateList(stats);
    }

    public class ViewHolderHeader extends RecyclerView.ViewHolder {
        public ViewHolderHeader(View v) {
            super(v);
        }
    }

    public class ViewHolderStats extends RecyclerView.ViewHolder {
        public ImageView playerIcon;
        public TextView playerName;
        public TextView finalScore;
        public TextView routeScore;
        public TextView longestPathBonus;
        public TextView destTotalScore;
        public TextView destCompleteScore;
        public TextView destIncompleteScore;
        public TextView destinationCards;
        public TextView shipPiecesLeft;

        public ViewHolderStats(View v){
            super(v);
            playerIcon = (ImageView) v.findViewById(R.id.player_icon);
            playerName = (TextView) v.findViewById(R.id.player_name);
            finalScore = (TextView) v.findViewById(R.id.final_score);
            routeScore = (TextView) v.findViewById(R.id.route_score);
            longestPathBonus = (TextView) v.findViewById(R.id.longest_path_bonus);
            destTotalScore = (TextView) v.findViewById(R.id.destination_total_score);
            destCompleteScore = (TextView) v.findViewById(R.id.destination_complete_score);
            destIncompleteScore = (TextView) v.findViewById(R.id.destination_incomplete_score);
            destinationCards = (TextView) v.findViewById(R.id.destination_cards);
            shipPiecesLeft = (TextView) v.findViewById(R.id.ship_pieces_left);
        }

        public ImageView getPlayerIcon() {
            return playerIcon;
        }

        public void setPlayerIcon(PlayerColor color) {
            switch (color) {
                case BLUE:
                    System.out.println("Blue");
                    playerIcon.setImageResource(R.drawable.player_blue);
                    break;
                case RED:
                    System.out.println("Red");
                    playerIcon.setImageResource(R.drawable.player_red);
                    break;
                case GREEN:
                    System.out.println("Green");
                    playerIcon.setImageResource(R.drawable.player_green);
                    break;
                case CYAN:
                    System.out.println("White");
                    playerIcon.setImageResource(R.drawable.player_white);
                    break;
                case YELLOW:
                    System.out.println("Yellow");
                    playerIcon.setImageResource(R.drawable.player_yellow);
                    break;
            }
        }

        public TextView getPlayerName() {
            return playerName;
        }

        public void setPlayerName(String playerName) {
            this.playerName.setText(playerName);
        }

        public TextView getFinalScore() {
            return finalScore;
        }

        public void setFinalScore(int finalScore) {
            this.finalScore.setText(Integer.toString(finalScore));
        }

        public TextView getRouteScore() {
            return routeScore;
        }

        public void setRouteScore(int routeScore) {
            this.routeScore.setText(Integer.toString(routeScore));
        }

        public TextView getLongestPathBonus() {
            return longestPathBonus;
        }

        public void setLongestPathBonus(boolean longestPathBonus) {
            this.longestPathBonus.setText(longestPathBonus ? "10" : "0");
        }

        public TextView getDestTotalScore() {
            return destTotalScore;
        }

        public void setDestTotalScore(int destTotalScore) {
            this.destTotalScore.setText(Integer.toString(destTotalScore));
        }

        public TextView getDestCompleteScore() {
            return destCompleteScore;
        }

        public void setDestCompleteScore(int destCompleteScore) {
            this.destCompleteScore.setText(Integer.toString(destCompleteScore));
        }

        public TextView getDestIncompleteScore() {
            return destIncompleteScore;
        }

        public void setDestIncompleteScore(int destIncompleteScore) {
            this.destIncompleteScore.setText(Integer.toString(destIncompleteScore));
        }

        public TextView getDestinationCards() {
            return destinationCards;
        }

        public void setDestinationCards(int destinationCards) {
            this.destinationCards.setText(Integer.toString(destinationCards));
        }

        public TextView getShipPiecesLeft() {
            return shipPiecesLeft;
        }

        public void setShipPiecesLeft(int shipPiecesLeft) {
            this.shipPiecesLeft.setText(Integer.toString(shipPiecesLeft));
        }
    }

    public void updateList(final List<PlayerStats> stats){
        stats.add(0, null); // this will add the header
        this.stats = stats;

        if (parent.getActivity() != null) {
            parent.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyDataSetChanged();
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        PlayerStats playerStats = stats.get(position);
        return (playerStats != null) ? STATS : HEADER;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch(viewType){
            case HEADER:
                View v1 = inflater.inflate(R.layout.game_results_header, parent, false);
                viewHolder = new ViewHolderHeader(v1);
                break;
            case STATS:
                View v2 = inflater.inflate(R.layout.game_results_item, parent, false);
                viewHolder = new ViewHolderStats(v2);
                break;
            default:
                View v3 = inflater.inflate(R.layout.game_results_item, parent, false);
                viewHolder = new ViewHolderStats(v3);
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position){
        if (holder.getClass().getSimpleName().equals("ViewHolderStats")) {
            ViewHolderStats statsView = (ViewHolderStats) holder;

            statsView.setPlayerIcon(stats.get(position).getPlayerColor());
            statsView.setPlayerName(stats.get(position).getPlayerName());
            statsView.setFinalScore(stats.get(position).getFinalScore());
            statsView.setRouteScore(stats.get(position).getRouteScore());
            statsView.setLongestPathBonus(stats.get(position).isHadLongestRoute());
            statsView.setDestTotalScore(stats.get(position).getCompleteDestScore() + stats.get(position).getIncompleteDestScore());
            statsView.setDestCompleteScore(stats.get(position).getCompleteDestScore());
            statsView.setDestIncompleteScore(stats.get(position).getIncompleteDestScore());
            statsView.setDestinationCards(stats.get(position).getNumDestCards());
            statsView.setShipPiecesLeft(stats.get(position).getShipPiecesLeft());
        }
    }

    @Override
    public int getItemCount(){
        return stats.size();
    }
}
