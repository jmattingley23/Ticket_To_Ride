package com.derds.tickettoride.gamehistory;

import android.util.Log;

import com.derds.models.DestinationCard;
import com.derds.models.Message;
import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.gamehistory.ui.GameHistoryFragment;

import java.util.List;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by drc95 on 10/31/17.
 */

public class GameHistoryController implements Observer, IGameHistoryController {
    private GameHistoryFragment gameHistoryFragment;
    private static final String TAG = "GameHistoryConroller";

    @Override
    public void update(Observable o, Object arg) {
        Log.v(TAG,"update from ClientModelFacade");
        if(arg.getClass().equals(List.class)) {
            Log.e(TAG,"got in");
        }
        if(arg.getClass().getSimpleName().equals(String.class.getSimpleName())) {
            String type = (String) arg;
            if (type.equals("GameHistoryUpdated")) {
                Log.e(TAG, "also got in");
                List<String> history = ClientModelFacade.getInstance().getGameHistory();
                gameHistoryFragment.addHistory(history.get(history.size() - 1));
            }
        }
    }

    public GameHistoryFragment getGameHistoryFragment() {
        return gameHistoryFragment;
    }

    public void setGameHistoryFragment(GameHistoryFragment gameHistoryFragment) {
        this.gameHistoryFragment = gameHistoryFragment;
    }

    public List<String> getGameHistory() {
        return ClientModelFacade.getInstance().getGameHistory();
    }
}
