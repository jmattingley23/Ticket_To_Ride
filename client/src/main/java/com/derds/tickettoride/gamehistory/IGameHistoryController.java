package com.derds.tickettoride.gamehistory;

import android.util.Log;

import com.derds.tickettoride.common.ClientModelFacade;
import com.derds.tickettoride.gamehistory.ui.GameHistoryFragment;

import java.util.List;
import java.util.Observable;

/**
 * Created by Aaron on 11/25/2017.
 */

public interface IGameHistoryController {

    public void update(Observable o, Object arg);

    public GameHistoryFragment getGameHistoryFragment();

    public void setGameHistoryFragment(GameHistoryFragment gameHistoryFragment);

    public List<String> getGameHistory();
}
