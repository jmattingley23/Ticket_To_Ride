package com.derds.tickettoride.gamehistory.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.derds.tickettoride.R;
import com.derds.tickettoride.game.GameController;
import com.derds.tickettoride.game.IGameController;
import com.derds.tickettoride.game.ui.GameActivity;
import com.derds.tickettoride.gamehistory.FakeHistoryData;

/**
 * Created by justinbrunner on 10/21/17.
 */

public class GameHistoryFragment extends Fragment {
    private static final String TAG = "HistoryFragment";
    private IGameController gameController;
    private RecyclerView gameHistoryRecycler;
    private GameHistoryAdapter gameHistoryAdapter;
    private ImageButton closeBtn;

    public GameHistoryFragment() {}

    public void setGameController(IGameController gameController) {
        this.gameController = gameController;
    }

    public static GameHistoryFragment newInstance(IGameController gameController) {
        GameHistoryFragment gameHistoryFragment = new GameHistoryFragment();
        gameHistoryFragment.setGameController(gameController);
        return gameHistoryFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Fragment self = this;
        View view = inflater.inflate(R.layout.fragment_game_history, container, false);
        setHasOptionsMenu(true);

        view.setBackgroundColor(getResources().getColor(R.color.transparent_black_opaque));

        gameController = ((GameActivity) getActivity()).getGameController();
        gameController.getGameHistoryController().setGameHistoryFragment(this);

        gameHistoryRecycler = (RecyclerView) view.findViewById(R.id.game_history_recycler);
        gameHistoryAdapter = new GameHistoryAdapter(gameController.getGameHistoryController().getGameHistory(), gameController, this);
        closeBtn = (ImageButton) view.findViewById(R.id.close_history_button);

        gameHistoryRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        gameHistoryRecycler.setAdapter(gameHistoryAdapter);

        closeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().beginTransaction().remove(self).commit();
            }
        });

        return view;
    }

    public void addHistory(String historyText) {
        gameHistoryAdapter.addHistoryText(historyText);
    }
}
