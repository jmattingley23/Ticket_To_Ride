package com.derds.tickettoride.gamehistory;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by justinbrunner on 10/31/17.
 */

public class FakeHistoryData {
    public static FakeHistoryData instance = new FakeHistoryData();

    private FakeHistoryData() {
        stuff = new ArrayList<>();
        String[] junk = {
                "This little piggy went to the market",
                "This little piggy ate a bone",
                "This little piggy wanted pizza",
                "this little piggy wanted scones",
                "And this little piggy farted wee wee weee all the way home"
        };

        for (int i = 0; i < junk.length; i++) {
            stuff.add(junk[i]);
        }
    }

    private List<String> stuff;

    public List<String> getStuff() {
        return stuff;
    }

    public void setStuff(List<String> stuff) {
        this.stuff = stuff;
    }
}
