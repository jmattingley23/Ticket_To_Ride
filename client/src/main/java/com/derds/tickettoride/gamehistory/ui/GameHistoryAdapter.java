package com.derds.tickettoride.gamehistory.ui;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.derds.tickettoride.R;
import com.derds.tickettoride.game.GameController;
import com.derds.tickettoride.game.IGameController;
import com.derds.tickettoride.gamehistory.GameHistoryController;

import java.util.Comparator;
import java.util.Date;
import java.util.List;

/**
 * Created by Aaron on 10/9/2017.
 */

public class GameHistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private List<String> gameHistory;
    private IGameController gameController;
    private RecyclerView gameHistoryRecycler;
    private GameHistoryFragment parent;

    public GameHistoryAdapter(List<String> history, IGameController gc, GameHistoryFragment parent){
        this.gameHistory = history;
        this.gameController = gc;
        this.parent = parent;
    }

    public class ViewHolderHistoryText extends RecyclerView.ViewHolder {
        public TextView historyTextView;
        public ViewHolderHistoryText(View v){
            super(v);
            historyTextView = (TextView) v.findViewById(R.id.history_action_message);
        }

        public void setHistoryText(String historyText) {
            this.historyTextView.setText(historyText);
        }
    }

    public void updateList(List<String> history){
        this.gameHistory = history;

        parent.getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType){

        RecyclerView.ViewHolder viewHolder;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        View v1 = inflater.inflate(R.layout.game_history_item, parent, false);
        viewHolder = new ViewHolderHistoryText(v1);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, int position){
        ViewHolderHistoryText historyView = (ViewHolderHistoryText) holder;

        historyView.setHistoryText(gameHistory.get(position));
    }

    @Override
    public int getItemCount(){
        return gameHistory.size();
    }

    public void addHistoryText(String historyText) {

        if (parent.getActivity() != null) {
            parent.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    notifyItemInserted(0);
                    if (gameHistory.size() > 0 && gameHistoryRecycler != null) {
                        gameHistoryRecycler.smoothScrollToPosition(gameHistory.size() - 1);
                    }
                }
            });
        }
    }


}
