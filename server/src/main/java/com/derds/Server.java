package com.derds;

import com.derds.handlers.CommandHandler;
import com.derds.handlers.SyncDataHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;

import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;

/**
 * Main server class
 */
public class Server {

    private static Logger logger;
    private int portNumber = 8080;

    static {
        try {
            initLog();
        }
        catch (IOException e) {
            System.out.println("Could not initialize log: " + e.getMessage());
        }
    }

    private static void initLog() throws IOException {
        Level logLevel = Level.FINEST;
        String fileAppend = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
        // TODO should we add a special folder for these?
        FileHandler file = new FileHandler("logs/TTRLog_" + fileAppend + ".swag");
        SimpleFormatter formatter = new SimpleFormatter();
        file.setFormatter(formatter);
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
        logger.addHandler(file);
        logger.setLevel(logLevel);
    }

    private void run() {
        HttpServer httpServer;
        logger.info("Initializing HTTP Server on port " + portNumber + "...");
        try {
            httpServer = HttpServer.create(new InetSocketAddress(portNumber), 0);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Creation of server failed.");
            logger.log(Level.SEVERE, e.getMessage(), e);
            return;
        }

        logger.info("Done creating server.");
        httpServer.setExecutor(null);
        logger.info("Creating contexts...");

        logger.info("\t/execCommand/...");
        httpServer.createContext("/execCommand", new CommandHandler());
        logger.info("Done.");

        logger.info("\t/getSyncData/...");
        httpServer.createContext("/getSyncData", new SyncDataHandler());
        logger.info("Done.");

        logger.info("Contexts created.");
        logger.info("Starting...");
        httpServer.start();
    }

    public static void main(String args[]) {
        logger.info("Starting up...");
        new Server().run();
    }
}
