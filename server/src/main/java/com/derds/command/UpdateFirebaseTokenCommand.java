package com.derds.command;

import com.derds.ServerFacade;
import com.derds.result.Result;

import java.util.logging.Logger;

public class UpdateFirebaseTokenCommand extends AbstractCommand {
    private static Logger logger;

    private String firebaseToken;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    public String getFirebaseToken(){return firebaseToken;}


    @Override
    public Result execute() {
        logger.entering("UpdateFirebaseTokenCommand","execute");
        Result result = ServerFacade.getInstance().updateFirebaseToken(getAuthToken(), firebaseToken);
        logger.exiting("UpdateFirebaseTokenCommand","execute");
        return result;
    }
}
