package com.derds.command;

import com.derds.ServerFacade;
import com.derds.result.GameResult;

import java.util.logging.Logger;

public class StartGameCommand extends AbstractCommand {
    private static Logger logger;

    private String gameID;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    public String getGameID() {
        return gameID;
    }

    @Override
    public GameResult execute() {
        logger.entering("StartGameCommand","execute");
        GameResult result = ServerFacade.getInstance().startGame(getAuthToken(), gameID);
        logger.exiting("StartGameCommand","execute");
        return result;
    }
}
