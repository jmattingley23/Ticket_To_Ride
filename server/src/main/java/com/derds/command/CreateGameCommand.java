package com.derds.command;

import com.derds.ServerFacade;
import com.derds.models.PlayerColor;
import com.derds.result.GameResult;

import java.util.logging.Logger;

public class CreateGameCommand extends AbstractCommand {
    private static Logger logger;

    private String gameName;
    private PlayerColor playerColor;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    public String getGameName() {
        return gameName;
    }

    public PlayerColor getPlayerColor() {
        return playerColor;
    }

    @Override
    public GameResult execute() {
        logger.entering("CreateGameCommand","execute");
        GameResult result = ServerFacade.getInstance().createGame(getAuthToken(), gameName, playerColor);
        logger.exiting("CreateGameCommand","execute");
        return result;
    }
}
