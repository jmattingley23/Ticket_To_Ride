package com.derds.command;

import com.derds.ServerFacade;
import com.derds.result.LoginResult;

import java.util.logging.Logger;

public class RegisterCommand extends AbstractCommand {
    private static Logger logger;

    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String firebaseToken;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    @Override
    public LoginResult execute() {
        logger.entering("RegisterCommand","execute");
        LoginResult result = ServerFacade.getInstance().register(username, password, firstName, lastName, firebaseToken);
        logger.exiting("RegisterCommand","execute");
        return result;
    }
}
