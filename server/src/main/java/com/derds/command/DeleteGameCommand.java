package com.derds.command;

import com.derds.ServerFacade;
import com.derds.result.Result;

import java.util.logging.Logger;

public class DeleteGameCommand extends AbstractCommand {
    private static Logger logger;

    private String gameID;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    public String getGameID() {
        return gameID;
    }

    @Override
    public Result execute() {
        logger.entering("DeleteGameCommand","execute");
        Result result = ServerFacade.getInstance().deleteGame(getAuthToken(), gameID);
        logger.exiting("DeleteGameCommand","execute");
        return result;
    }
}
