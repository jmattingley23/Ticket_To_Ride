package com.derds.command;

import com.derds.ServerFacade;
import com.derds.models.PlayerColor;
import com.derds.result.Result;

import java.util.logging.Logger;

/**
 * Created by drc95 on 10/30/17.
 */

public class RequestQueueCommand extends AbstractCommand {
    private static Logger logger;

    int lastHistoryId;
    String playerId;
    String gameId;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    @Override
    public Result execute() {
        logger.entering("RequestQueueCommand","execute");
        Result result = ServerFacade.getInstance().getQueueHistory(getAuthToken(),lastHistoryId,playerId, gameId);
        logger.exiting("RequestQueueCommand","execute");
        return result;
    }
}
