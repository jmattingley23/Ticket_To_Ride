package com.derds.command;

import com.derds.ServerFacade;
import com.derds.models.PlayerColor;
import com.derds.result.GameResult;

import java.util.logging.Logger;

public class JoinGameCommand extends AbstractCommand {
    private static Logger logger;

    private String gameID;
    private PlayerColor playerColor;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    public String getGameID() {
        return gameID;
    }

    public PlayerColor getPlayerColor() {
        return playerColor;
    }

    @Override
    public GameResult execute() {
        logger.entering("JoinGameCommand","execute");
        GameResult result = ServerFacade.getInstance().joinGame(getAuthToken(), gameID, playerColor);
        logger.exiting("JoinGameCommand","execute");
        return result;
    }
}
