package com.derds.command.game;

import com.derds.Strings;
import com.derds.gamecommand.AbstractInitGameCommand;
import com.derds.models.Player;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/27/17.
 */

public class InitGameCommand extends AbstractInitGameCommand {
    public InitGameCommand(String executor, String gameId, TurnState.StateName executorState, int turnOrder, Player player) {
        super(executor, gameId, turnOrder, player);
        setExecutorState(executorState);
    }

    @Override
    public Result execute() {
        return new Result(Strings.INVALID_GAME_COMMAND);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
