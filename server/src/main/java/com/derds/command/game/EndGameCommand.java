package com.derds.command.game;

import com.derds.gamecommand.AbstractEndGameCommand;
import com.derds.models.PlayerStats;
import com.derds.result.Result;

import java.util.List;

/**
 * Created by drc95 on 11/21/17.
 */

public class EndGameCommand extends AbstractEndGameCommand {
    public EndGameCommand(String executor, String gameId, List<PlayerStats> playerStatses) {
        super(executor, gameId, playerStatses);
        setExecutorState(null);
    }

    @Override
    public Result execute() {
        // Don't do anything on the server
        return null;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
