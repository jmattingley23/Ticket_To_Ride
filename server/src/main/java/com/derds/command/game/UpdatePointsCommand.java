package com.derds.command.game;

import com.derds.gamecommand.AbstractUpdatePointsCommand;
import com.derds.result.Result;

/**
 * Created by drc95 on 10/28/17.
 */

public class UpdatePointsCommand extends AbstractUpdatePointsCommand {
    public UpdatePointsCommand(String executor, String gameId, int newPointsTotal, String playerId) {
        super(executor, gameId, newPointsTotal, playerId);
        setExecutorState(null);
    }

    @Override
    public Result execute() {
        return null;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
