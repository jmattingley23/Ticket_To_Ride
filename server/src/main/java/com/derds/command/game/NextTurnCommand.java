package com.derds.command.game;

import com.derds.gamecommand.AbstractNextTurnCommand;
import com.derds.result.Result;

public class NextTurnCommand extends AbstractNextTurnCommand {
    public NextTurnCommand(int newTurn, String gameId) {
        super(newTurn, gameId);
        setExecutorState(null);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }

    @Override
    public Result execute() {
        return null;
    }
}
