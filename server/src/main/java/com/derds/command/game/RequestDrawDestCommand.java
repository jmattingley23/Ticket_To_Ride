package com.derds.command.game;

import com.derds.ServerFacade;
import com.derds.gamecommand.AbstractRequestDrawDestCommand;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/25/17.
 */

public class RequestDrawDestCommand extends AbstractRequestDrawDestCommand {
    public RequestDrawDestCommand(String executor, String gameId, TurnState.StateName executorState) {
        super(executor, gameId);
        setExecutorState(executorState);
    }

    @Override
    public Result execute() {
         return ServerFacade.getInstance().requestDrawDestination(getAuthToken(),executor);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
