package com.derds.command.game;

import com.derds.ServerFacade;
import com.derds.gamecommand.AbstractGetChatHistoryCommand;
import com.derds.models.Message;
import com.derds.result.Result;

import java.util.List;

/**
 * Created by drc95 on 10/21/17.
 */

public class GetChatHistoryCommand extends AbstractGetChatHistoryCommand {
    public GetChatHistoryCommand(String executor, String gameId, int lastCommandSequenceId, List<Message> messages, int skip) {
        super(executor, gameId, lastCommandSequenceId, messages, skip);
        setExecutorState(null);
    }

    @Override
    public Result execute() {
        return ServerFacade.getInstance().getChatHistory(getAuthToken(),executor,lastCommandSequenceId,messageHistory, skip);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
