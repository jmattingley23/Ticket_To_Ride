package com.derds.command.game;

import com.derds.ServerFacade;
import com.derds.gamecommand.AbstractPickFaceupCardCommand;
import com.derds.models.TrainCard;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/26/17.
 */

public class PickFaceupCardCommand extends AbstractPickFaceupCardCommand {
    public PickFaceupCardCommand(String executor, String gameId, TurnState.StateName executorState, TrainCard cardDrawn) {
        super(executor, gameId, cardDrawn);
        setExecutorState(executorState);
    }

    @Override
    public Result execute() {
        return ServerFacade.getInstance().pickFaceupCard(getAuthToken(), executor, cardDrawn);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
