package com.derds.command.game;

import com.derds.gamecommand.AbstractUpdateRiverCommand;
import com.derds.models.River;
import com.derds.result.Result;

/**
 * Created by drc95 on 10/28/17.
 */

public class UpdateRiverCommand extends AbstractUpdateRiverCommand {
    public UpdateRiverCommand(String executor, String gameId, River newRiver) {
        super(executor, gameId, newRiver);
        setExecutorState(null);
    }

    @Override
    public Result execute() {
        return null;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
