package com.derds.command.game;

import com.derds.ServerFacade;
import com.derds.gamecommand.AbstractRequestDrawTrainCommand;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/25/17.
 */

public class RequestDrawTrainCommand extends AbstractRequestDrawTrainCommand {
    public static final String TAG = "RequestDrawTrainCommand";
    public RequestDrawTrainCommand(String authToken, String executor, String gameId, TurnState.StateName executorState) {
        super(authToken, executor, gameId);
        setExecutorState(executorState);
    }

    @Override
    public Result execute() {
        return ServerFacade.getInstance().requestDrawTrain(getAuthToken(), executor);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
