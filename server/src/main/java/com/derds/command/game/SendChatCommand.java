package com.derds.command.game;

import com.derds.ServerFacade;
import com.derds.gamecommand.AbstractSendChatCommand;
import com.derds.models.Message;
import com.derds.result.Result;

public class SendChatCommand extends AbstractSendChatCommand {
    public SendChatCommand(String authToken, String gameId, Message content) {
        super(content.getPlayerId(), gameId, content);
        setExecutorState(null);
    }

    @Override
    public Result execute() {
        return ServerFacade.getInstance().sendChat(getAuthToken(), content);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
