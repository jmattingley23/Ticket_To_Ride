package com.derds.command.game;

import com.derds.ServerFacade;
import com.derds.gamecommand.AbstractChooseDestCommand;
import com.derds.models.DestinationCard;
import com.derds.models.Game;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/25/17.
 */

public class ChooseDestCommand extends AbstractChooseDestCommand {
    public ChooseDestCommand(String executor, String gameId, TurnState.StateName executorState, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned) {
        super(executor, gameId, cardsChosen, cardsReturned);
        setExecutorState(executorState);
    }

    @Override
    public Result execute() {
        return ServerFacade.getInstance().chooseDestination(getAuthToken(),executor,cardsChosen,cardsReturned);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
