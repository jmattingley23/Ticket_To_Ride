package com.derds.command.game;

import com.derds.gamecommand.AbstractDrawTrainCommand;
import com.derds.models.TrainCard;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;

/**
 * Created by drc95 on 10/25/17.
 */

public class DrawTrainCommand extends AbstractDrawTrainCommand {
    public DrawTrainCommand(TrainCard cardDrawn, String executor, String gameId, TurnState.StateName executorState) {
        super(cardDrawn, executor, gameId);
        setExecutorState(executorState);
    }

    @Override
    public Result execute() {
        return null;
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
