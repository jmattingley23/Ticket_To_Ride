package com.derds.command.game;

import com.derds.ServerFacade;
import com.derds.gamecommand.AbstractClaimRouteCommand;
import com.derds.models.CityPair;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/26/17.
 */

public class ClaimRouteCommand extends AbstractClaimRouteCommand {
    public ClaimRouteCommand(Route claimedRoute, List<TrainCard> cardsUsed, int trainPiecesUsed, String executor, String gameId, TurnState.StateName executorState) {
        super(claimedRoute, cardsUsed, trainPiecesUsed, executor, gameId);
        setExecutorState(executorState);
    }

    @Override
    public Result execute() {
        return ServerFacade.getInstance().claimRoute(getAuthToken(),executor,claimedRoute,cardsUsed,trainPiecesUsed);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return true;
    }
}
