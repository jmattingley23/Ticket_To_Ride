package com.derds.command.game;


import com.derds.Strings;
import com.derds.models.DestinationCard;
import com.derds.result.Result;
import com.derds.gamecommand.AbstractDrawDestCommand;
import com.derds.models.DestinationCard;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 10/25/17.
 */

public class DrawDestCommand extends AbstractDrawDestCommand {
    public static final String TAG = "DrawDestCommand";
    public DrawDestCommand(List<DestinationCard> cardsToPickFrom, boolean areCardsDrawn, String executor, String gameId, TurnState.StateName executorState) {
        super(cardsToPickFrom, areCardsDrawn, executor, gameId);
        // TODO should areCardsDrawn be set to false in the constructor?
        setExecutorState(executorState);
    }

    @Override
    public Result execute() {
        return new Result(Strings.INVALID_GAME_COMMAND);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
