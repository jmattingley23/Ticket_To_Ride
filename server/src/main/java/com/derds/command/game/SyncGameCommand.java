package com.derds.command.game;

import com.derds.ServerFacade;
import com.derds.gamecommand.AbstractSyncGameCommand;
import com.derds.models.Player;
import com.derds.models.Route;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;

import java.util.List;

/**
 * Created by drc95 on 11/24/17.
 */

public class SyncGameCommand extends AbstractSyncGameCommand {

    public SyncGameCommand(String executor, String gameId, TurnState.StateName executorState, int turnOrder, Player player, List<Route> claimedRoutes) {
        super(executor, gameId, player, turnOrder, claimedRoutes);
        setExecutorState(executorState);
    }

    @Override
    public Result execute() {
        return ServerFacade.getInstance().syncGame(getAuthToken(),executor,gameId);
    }

    @Override
    public String toString() {
        return null;
    }

    @Override
    public boolean isVisibleOnHistory() {
        return false;
    }
}
