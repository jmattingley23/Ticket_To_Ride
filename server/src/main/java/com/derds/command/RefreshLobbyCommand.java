package com.derds.command;

import com.derds.ServerFacade;
import com.derds.result.RefreshLobbyResult;

import java.util.logging.Logger;

public class RefreshLobbyCommand extends AbstractCommand {
    private static Logger logger;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    @Override
    public RefreshLobbyResult execute() {
        logger.entering("RefreshLobbyCommand","execute");
        RefreshLobbyResult result = ServerFacade.getInstance().refreshLobby(getAuthToken());
        logger.exiting("RefreshLobbyCommand","execute");
        return result;
    }
}
