package com.derds.command;

import com.derds.ServerFacade;
import com.derds.result.LoginResult;

import java.util.logging.Logger;

public class LoginCommand extends AbstractCommand {
    private static Logger logger;

    private String username;
    private String password;
    private String firebaseToken;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirebaseToken() {
        return firebaseToken;
    }

    @Override
    public LoginResult execute() {
        logger.entering("LoginCommand","execute");
        LoginResult result = ServerFacade.getInstance().login(username, password, firebaseToken);
        logger.exiting("LoginCommand","execute");
        return result;
    }
}
