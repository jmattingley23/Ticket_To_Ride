package com.derds;

import com.derds.gamecommand.AbstractSyncGameCommand;
import com.derds.models.DestinationCard;
import com.derds.models.Message;
import com.derds.models.Player;
import com.derds.models.PlayerColor;
import com.derds.models.River;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.result.GameResult;
import com.derds.result.LoginResult;
import com.derds.result.RefreshLobbyResult;
import com.derds.result.Result;
import com.derds.serverfacade.IServerFacade;
import com.derds.services.ChatService;
import com.derds.services.CommandQueueService;
import com.derds.services.FirebaseService;
import com.derds.services.GameService;
import com.derds.services.LobbyService;
import com.derds.services.UserService;

import java.util.List;

public class ServerFacade implements IServerFacade {

    private static ServerFacade instance;

    private ServerFacade() {}

    public static ServerFacade getInstance() {
        if(instance == null) {
            instance = new ServerFacade();
        }
        return instance;
    }


    @Override
    public LoginResult login(String username, String password, String firebaseToken) {
        if(password == null) {
            return UserService.getInstance().login(username, null, firebaseToken);
        }
        return UserService.getInstance().login(username, password.toCharArray(), firebaseToken);
    }

    @Override
    public LoginResult register(String username, String password, String firstName, String lastName, String firebaseToken) {
        if(password == null) {
            return UserService.getInstance().register(username, null, firstName, lastName, firebaseToken);
        }
        return UserService.getInstance().register(username, password.toCharArray(), firstName, lastName, firebaseToken);
    }

    @Override
    public GameResult startGame(String authToken, String gameID) {
        return LobbyService.getInstance().startGame(authToken, gameID);
    }

    @Override
    public GameResult joinGame(String authToken, String gameID, PlayerColor playerColor) {
        return LobbyService.getInstance().joinGame(authToken, gameID, playerColor);
    }

    @Override
    public GameResult createGame(String authToken, String gameName, PlayerColor playerColor) {
        return LobbyService.getInstance().createGame(authToken, gameName, playerColor);
    }

    @Override
    public Result deleteGame(String authToken, String gameID) {
        return LobbyService.getInstance().deleteGame(authToken, gameID);
    }

    @Override
    public RefreshLobbyResult refreshLobby(String authToken) {
        return LobbyService.getInstance().refreshLobby(authToken);
    }

    @Override
    public Result updateFirebaseToken(String authToken, String firebaseToken) {
        return FirebaseService.getInstance().addFirebaseToken(authToken, firebaseToken);
    }

    @Override
    public Result getQueueHistory(String authToken, int lastHistoryId, String playerId, String gameId) {
        return CommandQueueService.getInstance().getCommandsFromQueue(authToken,lastHistoryId,playerId,gameId);
    }
    // Game specific commands
    @Override
    public Result chooseDestination(String authToken, String playerId, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned) {
        return GameService.getInstance().chooseDestination(authToken,playerId,cardsChosen,cardsReturned);
    }

    @Override
    public Result claimRoute(String authToken, String playerId, Route route, List<TrainCard> cardsUsed, int trainPiecesUsed) {
        return GameService.getInstance().claimRouteAttempt(authToken,playerId,route,cardsUsed,trainPiecesUsed);
    }

    @Override
    public Result drawDestination(String authToken, String playerId, List<DestinationCard> cardsToPickFrom, boolean areCardsDrawn) {
        return null;
    }

    @Override
    public Result getChatHistory(String authToken, String playerId, int lastCommandId,  List<Message> messageHistory, int skip) {
        return GameService.getInstance().getChatHistory(authToken,playerId,lastCommandId,messageHistory,skip);
    }

    @Override
    public Result initGame(String authToken, String playerId, int turnOrder, Player player) {
        return null;
    }

    @Override
    public Result pickFaceupCard(String authToken, String playerId, TrainCard cardDrawn) {
        return GameService.getInstance().pickFaceupCardAttempt(authToken, playerId, cardDrawn);
    }

    @Override
    public Result requestDrawDestination(String authToken, String playerId) {
        return GameService.getInstance().requestDrawDestination(authToken, playerId);
    }

    @Override
    public Result requestDrawTrain(String authToken, String playerId) {
        return GameService.getInstance().drawTrainCardFromDeckAttempt(authToken, playerId);
    }

    @Override
    public Result sendChat(String authToken, Message content) {
        return ChatService.getInstance().sendChat(authToken, content);
    }

    @Override
    public Result updatePoints(String authToken, String playerId, int newPointsTotal, String playerIdToUpdate) {
        return null;
    }

    @Override
    public Result updateRiver(String authToken, String playerId, River newRiver) {
        return null;
    }

    @Override
    public Result syncGame(String authToken, String playerId, String gameId) {
        return GameService.getInstance().sync(authToken, playerId);
    }

    @Override
    public AbstractSyncGameCommand getSyncData(String playerId) {
        return GameService.getInstance().getSyncData(playerId);
    }
}
