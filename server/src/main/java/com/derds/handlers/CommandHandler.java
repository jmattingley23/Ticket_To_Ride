package com.derds.handlers;

import com.derds.Strings;
import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.DeleteGameCmdData;
import com.derds.cmddata.JoinGameCmdData;
import com.derds.cmddata.LoginCmdData;
import com.derds.cmddata.RefreshLobbyCmdData;
import com.derds.cmddata.RegisterCmdData;
import com.derds.cmddata.RequestQueueCmdData;
import com.derds.cmddata.StartGameCmdData;
import com.derds.cmddata.UpdateFirebaseTokenCmdData;
import com.derds.command.AbstractCommand;
import com.derds.command.CreateGameCommand;
import com.derds.command.DeleteGameCommand;
import com.derds.command.JoinGameCommand;
import com.derds.command.LoginCommand;
import com.derds.command.RefreshLobbyCommand;
import com.derds.command.RegisterCommand;
import com.derds.command.RequestQueueCommand;
import com.derds.command.StartGameCommand;
import com.derds.command.UpdateFirebaseTokenCommand;
import com.derds.command.game.ChooseDestCommand;
import com.derds.command.game.ClaimRouteCommand;
import com.derds.command.game.DrawDestCommand;
import com.derds.command.game.GetChatHistoryCommand;
import com.derds.command.game.InitGameCommand;
import com.derds.command.game.PickFaceupCardCommand;
import com.derds.command.game.RequestDrawDestCommand;
import com.derds.command.game.RequestDrawTrainCommand;
import com.derds.command.game.SendChatCommand;
import com.derds.command.game.SyncGameCommand;
import com.derds.result.Result;
import com.google.gson.Gson;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.util.logging.Level;
import java.util.logging.Logger;

public class CommandHandler implements HttpHandler {

    private static Logger logger;

    private final String COMMAND_KEY = "CommandType";
    private final String AUTH_KEY = "AuthToken";

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        logger.entering("CommandHandler","handle");

        try {
            if(httpExchange.getRequestMethod().toLowerCase().equals("post")) {
                Headers requestHeaders = httpExchange.getRequestHeaders();
                //make sure the request has a command type
                if(!requestHeaders.containsKey(COMMAND_KEY)) {
                    //build a new result with the missing command error
                    String resultString = new Gson().toJson(new Result(Strings.CMD_MISSING));
                    //write to response body
                    sendResponse(httpExchange, resultString);
                }

                //get data from exchange
                String commandType = requestHeaders.getFirst(COMMAND_KEY); //get commandType
                String authToken = requestHeaders.getFirst(AUTH_KEY); //get the authToken
                //get the requestBody JSON string
                Reader requestBody = new InputStreamReader(httpExchange.getRequestBody());


                //******************//
                // Process Commands //
                //******************//
                if(commandType.equals(LoginCmdData.class.getSimpleName())) {
                    logger.info("Handling a Login command");

                    //de-serialize & execute a LoginCommand, and get the serialized result
                    String resultString = createAndExecuteCommand(LoginCommand.class, requestBody);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(RegisterCmdData.class.getSimpleName())) {
                    logger.info("Handling a Register command");

                    //de-serialize & execute a RegisterCommand, and get the serialized result
                    String resultString = createAndExecuteCommand(RegisterCommand.class, requestBody);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(CreateGameCmdData.class.getSimpleName())) {
                    logger.info("Handling a CreateGame command");

                    //de-serialize & execute a CreateGameCommand, and get the serialized result
                    String resultString = createAndExecuteAuthCommand(CreateGameCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(DeleteGameCmdData.class.getSimpleName())) {
                    logger.info("Handling a DeleteGame command");

                    //de-serialize & execute a RegisterCommand, and get the serialized result
                    String resultString = createAndExecuteAuthCommand(DeleteGameCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(JoinGameCmdData.class.getSimpleName())) {
                    logger.info("Handling a JoinGame command");

                    //de-serialize & execute a RegisterCommand, and get the serialized result
                    String resultString = createAndExecuteAuthCommand(JoinGameCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(StartGameCmdData.class.getSimpleName())) {
                    logger.info("Handling a StartGame command");

                    //de-serialize & execute a RegisterCommand, and get the serialized result
                    String resultString = createAndExecuteAuthCommand(StartGameCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(RefreshLobbyCmdData.class.getSimpleName())) {
                    logger.info("Handling a RefreshLobby command");

                    //de-serialize & execute a RegisterCommand, and get the serialized result
                    String resultString = createAndExecuteAuthCommand(RefreshLobbyCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(UpdateFirebaseTokenCmdData.class.getSimpleName())) {
                    logger.info("Handling an UpdateFirebaseToken command");

                    //de-serialize & execute a RegisterCommand, and get the serialized result
                    String resultString = createAndExecuteAuthCommand(UpdateFirebaseTokenCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(RequestQueueCmdData.class.getSimpleName())) {
                    logger.info("Handling a RequestQueue command");

                    String resultString = createAndExecuteAuthCommand(RequestQueueCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(ChooseDestCommand.class.getSimpleName())) {
                    logger.info("Handling a ChooseDest command");

                    String resultString = createAndExecuteAuthCommand(ChooseDestCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(ClaimRouteCommand.class.getSimpleName())) {
                    logger.info("Handling a ClaimRoute command");

                    String resultString = createAndExecuteAuthCommand(ClaimRouteCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(DrawDestCommand.class.getSimpleName())) {
                    logger.info("Handling a DrawDest command");

                    String resultString = createAndExecuteAuthCommand(DrawDestCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(GetChatHistoryCommand.class.getSimpleName())) {
                    logger.info("Handling a GetChatHistory command");

                    String resultString = createAndExecuteAuthCommand(GetChatHistoryCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(InitGameCommand.class.getSimpleName())) {
                    logger.info("Handling an InitGame command");

                    String resultString = createAndExecuteAuthCommand(InitGameCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(PickFaceupCardCommand.class.getSimpleName())) {
                    logger.info("Handling a PickFaceupCard comand");

                    String resultString = createAndExecuteAuthCommand(PickFaceupCardCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(RequestDrawDestCommand.class.getSimpleName())) {
                    logger.info("Handling a RequestDrawDest command");

                    String resultString = createAndExecuteAuthCommand(RequestDrawDestCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(RequestDrawTrainCommand.class.getSimpleName())) {
                    logger.info("Handling a RequestDrawTrain command");

                    String resultString = createAndExecuteAuthCommand(RequestDrawTrainCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(SendChatCommand.class.getSimpleName())) {
                    logger.info("Handling a SendChatCommand command");

                    String resultString = createAndExecuteAuthCommand(SendChatCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else if(commandType.equals(SyncGameCommand.class.getSimpleName())) {
                    logger.info("Handling a SyncGameCommand command");

                    String resultString = createAndExecuteAuthCommand(SyncGameCommand.class, requestBody, authToken);
                    sendResponse(httpExchange, resultString);

                } else {
                    logger.log(Level.SEVERE, "CommandType " + commandType + " was not recognized as a valid command");
                    String resultString = new Gson().toJson(new Result(Strings.BAD_CMD));
                    sendResponse(httpExchange, resultString);
                }
            } else {
                logger.log(Level.SEVERE, "HttpRequestMethod was not POST.");
                String resultString = new Gson().toJson(new Result(Strings.BAD_REQUEST_METHOD));
                sendResponse(httpExchange, resultString);
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "IOException while processing HttpExchange");
            logger.log(Level.SEVERE, e.getMessage(), e);
            //build a new generic result with the server error
            String resultString = new Gson().toJson(new Result(Strings.SERVER_ERROR));
            //write to response body
            sendResponse(httpExchange, resultString);
        }
        logger.exiting("CommandHandler","handle");
    }

    /**
     * Sends the given string as a response to the given HttpExchange
     * @pre the HttpExchange is valid; the string is not null
     * @param httpExchange the httpExchange to write the response to
     * @param data the data to write
     * @post the string is written to the httpExchange's responseBody
     */
    private void sendResponse(HttpExchange httpExchange, String data) throws IOException {
        logger.entering("CommandHandler","sendResponse");

        httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
        OutputStream responseBody = httpExchange.getResponseBody();
        responseBody.write(data.getBytes());
        responseBody.close();

        logger.exiting("CommandHandler","sendResponse");
    }

    /**
     * Creates a AbstractCommand object of the given classType using the given JSON data, executes it, and serializes the result
     * @pre the classType is a valid AbstractCommand class; the jsonString is a valid representation of the corresponding CmdData class
     * @param classType the type of command to execute
     * @param jsonString a JSON representation of the CmdData
     * @post the result of the command execution as a JSON string
     */
    private String createAndExecuteCommand(Class classType, Reader jsonString) {
        logger.entering("CommandHandler","createAndExecuteCommand");

        //create an object with the given CmdData & class type
        Object command = null;
        try {
            command = new Gson().fromJson(jsonString, classType);
        } catch (Exception e) { //phantom menace
            logger.log(Level.SEVERE, "could not deserialize json string");
            logger.log(Level.SEVERE, e.getMessage());
            e.printStackTrace();
        }
        //execute the result
        logger.info("executing command");
        Result result = ((AbstractCommand)command).execute();

        //serialize the result
        String resultString = new Gson().toJson(result);

        logger.exiting("CommandHandler","createAndExecuteCommand");
        return resultString;
    }

    /**
     * Creates a AbstractCommand object of the given classType using the given JSON data, gives it the
     *  authToken, executes it, & serializes the result
     * @pre the classType is a valid AbstractCommand class; the jsonString is a valid representation of the corresponding CmdData class; the authToken is not null
     * @param classType the type of command to execute
     * @param jsonString a JSON representation of the CmdData
     * @param authToken the authToken for the command
     * @post the result of the command execution as a JSON string
     */
    private String createAndExecuteAuthCommand(Class classType, Reader jsonString, String authToken) {
        logger.entering("CommandHandler","createAndExecuteAuthCommand");

        //create an object with the given CmdData & class type
        Object command = null;
        try {
            command = new Gson().fromJson(jsonString, classType);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "could not deserialize json string");
            logger.log(Level.SEVERE, e.getMessage());
            e.printStackTrace();
        }
        //give the authToken to the AbstractCommand
        ((AbstractCommand)command).setAuthToken(authToken);
        //execute the result
        logger.info("executing command");
        Result result = ((AbstractCommand) command).execute();

        //serialize the result
        String resultString = new Gson().toJson(result);

        logger.exiting("CommandHandler","createAndExecuteAuthCommand");
        return resultString;
    }
}
