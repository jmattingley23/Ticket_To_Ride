package com.derds.handlers;

import com.derds.ServerFacade;
import com.derds.gamecommand.AbstractSyncGameCommand;
import com.google.gson.Gson;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.HttpURLConnection;

public class SyncDataHandler implements HttpHandler {
    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        try {
            if(httpExchange.getRequestMethod().toLowerCase().equals("post")) {
                Reader requestBody = new InputStreamReader(httpExchange.getRequestBody());
                String playerId = new Gson().fromJson(requestBody, String.class);

                AbstractSyncGameCommand command = ServerFacade.getInstance().getSyncData(playerId);

                String resultString = new Gson().toJson(command);

                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
                OutputStream responseBody = httpExchange.getResponseBody();
                responseBody.write(resultString.getBytes());
                responseBody.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
