package com.derds;

public class Strings {
    public static final String USERNAME_EXISTS = "ERROR: Username already exists.";
    public static final String BAD_LOGIN = "ERROR: Username or password was incorrect.";
    public static final String SERVER_ERROR = "ERROR: Internal server error.";
    public static final String BAD_PASSWORD = "ERROR: Password is invalid.";
    public static final String BAD_USERNAME = "ERROR: Username is invalid.";
    public static final String BAD_FIRSTNAME = "ERROR: First name cannot be left blank.";
    public static final String BAD_LASTNAME = "ERROR: Last name cannot be left blank.";
    public static final String BAD_FIREBASE_TOKEN = "ERROR: Firebase token was null or invalid.";
    public static final String CMD_MISSING = "ERROR: CommandType header missing from HttpRequest";
    public static final String BAD_CMD = "ERROR: CommandType was invalid";
    public static final String BAD_REQUEST_METHOD = "ERROR: Request method was wrong";
    public static final String BAD_GAME_NAME = "ERROR: Invalid game name.";
    public static final String AUTH_NOT_FOUND = "ERROR: AuthToken not found or was invalid.";
    public static final String GAME_NOT_FOUND = "ERROR: Game was not found.";
    public static final String GAME_NOT_OWNED = "ERROR: Game not owned.";
    public static final String NOT_ENOUGH_PLAYERS = "ERROR: Not enough players.";
    public static final String TOO_MANY_PLAYERS = "ERROR: Too many players.";
    public static final String ALREADY_IN_GAME = "ERROR: User is already in game.";
    public static final String BAD_PLAYER_COLOR = "ERROR: Player color was invalid.";
    public static final String PLAYER_COLOR_TAKEN = "ERROR: Player color was already in use.";
    public static final String NOT_PLAYER_TURN = "ERROR: It is not the player's turn.";
    public static final String INVALID_GAME_COMMAND = "ERROR: this command shouldn't be sent to the server";
    public static final String INVALID_CARDS = "ERROR: Incorrect cards to claim route";
    public static final String NOT_ENOUGH_PIECES = "ERROR: Not enough ship pieces to claim route";
    public static final String NO_MORE_TRAIN_CARDS = "ERROR: Train card deck is empty";
}
