package com.derds;

import com.derds.models.Game;
import com.derds.models.LobbyGameMetadata;
import com.derds.models.Player;
import com.derds.models.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Logger;

public class Database {
    private static Database instance;
    private static Logger logger;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    private Map<User, String> users; //map of user objects to passwords
    private Map<String,User> authUser; //map from authtoken to user
    private Map<String, String> userAuth;   // map from user id to auth token
    private Map<String, Game> games;
    private Map<String, String> playerGame; // map of playerIds to gameIds

    private Database () {
        users = new HashMap<>();
        authUser = new HashMap<>();
        userAuth = new HashMap<>();
        games = new HashMap<>();
        playerGame = new HashMap<>();
    }

    public static Database getInstance() {
        logger.finest("Database getInstance.");
        if(instance == null) {
            instance = new Database();
        }
        return instance;
    }

    //*********************************
    //  Database Methods - eventually we'll split these up into nice DAOs...but we have no database yet
    //                      so for now enjoy a big list of methods
    //*********************************

    /**
     * Checks to see if the given username already exists in the database
     * @pre username is not null
     * @param username the username to look for
     * @post whether or not the username exists
     */
    public boolean doesUsernameExist(String username) {
        logger.entering("Database","doesUsernameExist");
        for(Map.Entry<User, String> user : users.entrySet()) {
            if(user.getKey().getUsername().equals(username)) {
                logger.exiting("Database","doesUsernameExist");
                return true;
            }
        }
        logger.exiting("Database","doesUsernameExist");
        return false;
    }

    /**
     * Gets the hashed & salted password stored in the database for the given username
     * @pre the username exists
     * @param username the username to find the password for
     * @post the password for the username, null if username did not exist
     */
    public String getStoredPasswordForUsername(String username) {
        logger.entering("Database","getStoredPasswordForUsername");
        for(Map.Entry<User, String> user : users.entrySet()) {
            if(user.getKey().getUsername().equals(username)) {
                logger.exiting("Database","getStoredPasswordForUsername");
                return user.getValue();
            }
        }
        logger.exiting("Database","getStoredPasswordForUsername");
        return null;
    }

    public void deleteAuthToken(String authToken) {
        String userId = authUser.get(authToken).getUserId();
        authUser.remove(authToken);
        userAuth.remove(userId);
    }

    /**
     * Adds a new user to the database
     * @pre username does not already exist
     * @pre password has been hashed & salted by the passwordservice
     * @param username the username of the new user
     * @param securePassword the hashed & salted password for the new user
     * @param firstname the firstname of the new user
     * @param lastname the lastname of the new user
     */
    public void pushNewUser(String username, String securePassword, String firstname, String lastname) {
        logger.entering("Database","pushNewUser");

        String userId = UUID.randomUUID().toString().replace("-" , "");
        User user = new User(username, firstname, lastname, userId);
        users.put(user, securePassword);

        logger.exiting("Database","pushNewUser");
    }

    public String getUserIdForUsername(String username) {
        logger.entering("Database","getUserIdForUsername");

        for(Map.Entry<User, String> user : users.entrySet()) {
            if(user.getKey().getUsername().equals(username)) {
                logger.exiting("Database","getUserIdForUsername");
                return user.getKey().getUserId();
            }
        }
        logger.exiting("Database","getUserIdForUsername");
        return null;
    }

    private User getUserForUserId(String userId) {
        logger.entering("Database","getUserForUserId");

        for(Map.Entry<User, String> user : users.entrySet()) {
            if(user.getKey().getUserId().equals(userId)) {
                logger.exiting("Database","getUserForUserId");
                return user.getKey();
            }
        }

        logger.exiting("Database","getUserForUserId");
        return null;
    }

    public boolean addAuthTokenForUserId(String userId, String authToken) {
        logger.entering("Database","addAuthTokenForUserId");
        User user = getUserForUserId(userId);

        if(user == null) {
            logger.exiting("Database","addAuthTokenForUserId");
            return false;
        }

        authUser.put(authToken, user);
        userAuth.put(user.getUserId(), authToken);

        logger.exiting("Database","addAuthTokenForUserId");
        return true;
    }

    /**
     * @pre gameID is not null and exists
     * @post return value contains the LobbyGameMetadata object with the Id of gameID
     * @param gameID The ID of the game
     * @return
     */
    public LobbyGameMetadata getLobbyGameMetadata(String gameID){
        if(doesGameExist(gameID)){
            Game game = games.get(gameID);
            return new LobbyGameMetadata(game.getPlayers(), game.getGameMetadata().getOwnerUserId(), game.getGameId(), game.getGameMetadata().getGameName(), game.isStarted());
        } else {
            return null;
        }
    }

    public User getUser(String authToken){
        if(authUser.containsKey(authToken)){
            return authUser.get(authToken);
        }
        else{
            return null;
        }
    }

    public String getAuthToken(String userId) {
        if(userAuth.containsKey(userId)) {
            return userAuth.get(userId);
        } else {
            return null;
        }
    }

    /**
     * Gets all the lobbyGames that the user owns
     * @param authToken the authToken of the current user
     * @return the list of lobbyGames that the player owns
     */
    public List<LobbyGameMetadata> getOwnedGames(String authToken) {
        if(!doesUserExist(authToken)) {
            return null;
        }

        ArrayList<LobbyGameMetadata> ownedGames = new ArrayList<>();
        User user = authUser.get(authToken);

        //go through all the lobbyGames on the server
        for(Map.Entry<String, Game> gameMapEntry : games.entrySet()) {
            Game game = gameMapEntry.getValue();

            if(game.getGameMetadata().getOwnerUserId().equals(user.getUserId())) {
                //add it to the list
                LobbyGameMetadata lobbyGame = new LobbyGameMetadata(game.getPlayers(), game.getGameMetadata().getOwnerUserId(), game.getGameId(), game.getGameMetadata().getGameName(), game.isStarted());
                lobbyGame.setMyPlayerId(user.getUserId());
                ownedGames.add(lobbyGame);
            }
        }

        return ownedGames;
    }

    /**
     * Gets the list of lobbyGames that the user is in
     * @param authToken the authToken of the current user
     * @return the list of lobbyGames the player is in
     */
    public List<LobbyGameMetadata> getJoinedGames(String authToken){
        if(!doesUserExist(authToken)) {
            return null;
        }

        ArrayList<LobbyGameMetadata> joinedGames = new ArrayList<>();
        User user = authUser.get(authToken);

        //go through all the lobbyGames on the server
        for(Map.Entry<String, Game> gameMapEntry : games.entrySet()) {
            Game game = gameMapEntry.getValue();
            //go through the players in the game
            for(Player player : game.getPlayers()) {
                //if the user owns the current player
                if(player.getMyUser().equals(user)) {
                    //add it to the list of joined lobbyGames
                    LobbyGameMetadata lobbyGame = new LobbyGameMetadata(game.getPlayers(), game.getGameMetadata().getOwnerUserId(), game.getGameId(), game.getGameMetadata().getGameName(), game.isStarted());
                    lobbyGame.setMyPlayerId(user.getUserId());
                    joinedGames.add(lobbyGame);
                }
            }
        }
        return joinedGames;
    }

    /**
     * Gets the list of lobbyGames that are open & that the user isn't already in
     * @param authToken the authToken of the current user
     * @return the list of lobbyGames that open & that the user isn't already in
     */
    public List<LobbyGameMetadata> getOpenGames(String authToken){
        if(!doesUserExist(authToken)) {
            return null;
        }

        ArrayList<LobbyGameMetadata> openGames = new ArrayList<>();
        User user = authUser.get(authToken);

        //go through all lobbyGames in the databases
        for(Map.Entry<String, Game> gameMapEntry : games.entrySet()){
            Game game = gameMapEntry.getValue();

            boolean playerInGame = false;

            //we only care about join-able, open lobbyGames
            if(!game.isStarted()) {
                //go through the players in the game
                for (Player player : game.getPlayers()) {
                    //if the current user owns the player
                    if (player.getMyUser() == user) {
                        playerInGame = true;
                    }
                }
                //only show the game on the open list if the user isn't already in it
                if (!playerInGame) {
                    //add to the list to return
                    LobbyGameMetadata lobbyGame = new LobbyGameMetadata(game.getPlayers(), game.getGameMetadata().getOwnerUserId(), game.getGameId(), game.getGameMetadata().getGameName(), game.isStarted());
                    lobbyGame.setMyPlayerId(user.getUserId());
                    openGames.add(lobbyGame);
                }
            }
        }
        return openGames;
    }

    public void addPlayerGame(String playerId, String gameId) {
        playerGame.put(playerId,gameId);
    }

    public void addGame(Game game){
        games.put(game.getGameId(), game);
        for(Player player : game.getPlayers()) {
            playerGame.put(player.getPlayerId(),game.getGameId());
        }
    }

    public void removeGame(String gameID){
        games.remove(gameID);
    }

    public boolean doesGameExist(String gameID){
        return games.containsKey(gameID);
    }

    public boolean doesUserExist(String authToken){
        return authUser.containsKey(authToken);
    }

    /**
     * Removes the information associated with the given username - pretty much only used for unit tests right now
     * @param username the username to remove
     */
    public void removeUser(String username) {
        for(Iterator<Map.Entry<User, String>> iterator = users.entrySet().iterator(); iterator.hasNext();) {
            User currentUser = iterator.next().getKey();
            if(currentUser.getUsername().equals(username)) {
                iterator.remove();
            }
        }
    }

    public Game getGame(String gameId) {
        return games.get(gameId);
    }

    public Game getGameForPlayer(String playerId) {
        return getGame(playerGame.get(playerId));
    }
}
