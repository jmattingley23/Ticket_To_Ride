package com.derds.turnstate;

import com.derds.Database;
import com.derds.Strings;
import com.derds.command.game.ChooseDestCommand;
import com.derds.models.DestinationCard;
import com.derds.models.Game;
import com.derds.models.Player;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.result.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class BeginGameState extends TurnState {
    public static BeginGameState instance = new BeginGameState();
    public static StateName key = StateName.BEGIN_GAME;

    public BeginGameState() {}

    @Override
    public boolean claimDestinationCards(Player player, List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {

        return false;
    }

    @Override
    public boolean drawDestinationCards(Player player) {
        Game game = Database.getInstance().getGameForPlayer(player.getPlayerId());

        if(!game.getGameMetadata().isPlayersTurn(player.getPlayerId())) {
            return false;
        }


        List<DestinationCard> cards = new ArrayList<>();

        // draw 3 cards
        for(int i = 0; i < 3; i++) {
            cards.add(game.drawDestinationCard());
        }

        // Set player to choose destinations state
        player.setTurnState(TurnState.StateName.CHOOSE_DESTINATIONS);

        getTurnStateExecutable().drawDestinationCardsResponse(player.getPlayerId(), cards);

        return true;
    }

    @Override
    public boolean wait(Player player) {
        return false;
    }
}
