package com.derds.turnstate;

import com.derds.models.Player;
import com.derds.models.TrainCard;
import com.derds.models.TrainColor;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class DrawTrainCardState extends TurnState {
    public static DrawTrainCardState instance = new DrawTrainCardState();
    public static StateName key = StateName.DRAW_TRAIN_CARD;

    private DrawTrainCardState() {}

    @Override
    public boolean drawTrainCardFromDeck(Player player) {
        endTurn(player);
        getTurnStateExecutable().drawTrainCardFromDeck(null,player.getPlayerId(), false);
        return true;
    }


    @Override
    public boolean selectTrainCard(Player player, TrainCard card) {
        if(card.getColor() == TrainColor.RAINBOW){
            return false;
        }
        endTurn(player);
        getTurnStateExecutable().drawFaceUpCard(null,player.getPlayerId(),card, false);

        return true;
    }

    @Override
    public boolean wait(Player player) {
        return false;
    }

    @Override
    public boolean endTurn(Player player) {
        player.setTurnState(StateName.END);
        return true;
    }
}
