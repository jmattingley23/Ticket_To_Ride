package com.derds.turnstate;

import com.derds.models.Player;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class EndTurnState extends TurnState {
    public static EndTurnState instance = new EndTurnState();
    public static StateName key = StateName.END;

    private EndTurnState() {
    }

    @Override
    public boolean wait(Player player) {
        // do nothing
        return true;
    }
}
