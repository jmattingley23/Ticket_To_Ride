package com.derds.turnstate;

import com.derds.Database;
import com.derds.models.DestinationCard;
import com.derds.models.Game;
import com.derds.models.Player;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.models.TrainColor;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class StartTurnState extends TurnState {
    public static StartTurnState instance = new StartTurnState();
    public static StateName key = StateName.START_TURN;

    private StartTurnState() {}

    @Override
    public boolean drawDestinationCards(Player player) {
        Game game = Database.getInstance().getGameForPlayer(player.getPlayerId());

        if(!game.getGameMetadata().isPlayersTurn(player.getPlayerId())) {
            return false;
        }

        List<DestinationCard> cards = new ArrayList<>();

        try {
            // draw 3 cards
            for (int i = 0; i < 3; i++) {
                if(game.getDeckManager().getDestinationDeckCount() > 0) {
                    cards.add(game.drawDestinationCard());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        // Set player to choose destinations state
        player.setTurnState(TurnState.StateName.CHOOSE_DESTINATIONS);

        getTurnStateExecutable().drawDestinationCardsResponse(player.getPlayerId(), cards);

        return true;
    }

    @Override
    public boolean drawTrainCardFromDeck(Player player){
        boolean canDrawMoreTrainCards = true;
        player.setTurnState(StateName.DRAW_TRAIN_CARD);
        getTurnStateExecutable().drawTrainCardFromDeck(null,player.getPlayerId(), canDrawMoreTrainCards);
        return true;
    }

    @Override
    public boolean selectTrainCard(Player player, TrainCard card) {
        boolean canDrawMoreCards = false;
        if(card.getColor() == TrainColor.RAINBOW){
            endTurn(player);
        } else{
            player.setTurnState(StateName.DRAW_TRAIN_CARD);
            canDrawMoreCards = true;
        }
        getTurnStateExecutable().drawFaceUpCard(null,player.getPlayerId(),card, canDrawMoreCards);
        return true;
    }

    @Override
    public boolean selectRainbowTrainCard(Player player, TrainCard card) {
        return false;
    }

    @Override
    public boolean claimRoute(Player player, Route route, List<TrainCard> cardsUsed) {
        endTurn(player);
        getTurnStateExecutable().claimRoute(null,player.getPlayerId(),route,cardsUsed,route.getLength());
        return true;
    }

    @Override
    public boolean wait(Player player) {
        return false;
    }

    @Override
    public boolean endTurn(Player player) {
        player.setTurnState(StateName.END);
        return true;
    }
}
