package com.derds.turnstate;

import com.derds.Database;
import com.derds.models.DestinationCard;
import com.derds.models.Game;
import com.derds.models.Player;
import com.derds.models.Route;
import com.derds.models.TrainCard;

import java.util.List;
import java.util.Map;

/**
 * Created by justinbrunner on 11/10/17.
 */

public class ChooseDestinationCardsState extends TurnState {
    public static ChooseDestinationCardsState instance = new ChooseDestinationCardsState();
    public static StateName key = StateName.CHOOSE_DESTINATIONS;

    private ChooseDestinationCardsState() {}

    @Override
    public boolean claimDestinationCards(Player player, List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {
        int minNumCards;
        if(player.getHand().getDestCardCount() == 0) {
            minNumCards = 2;
        } else minNumCards = 1;

        // TODO fix the check once unpickedCards is being sent correctly
        if(/*pickedCards.size() + unpickedCards.size() != 3
                ||*/ pickedCards.size() == 0
                || !Database.getInstance().getGameForPlayer(player.getPlayerId()).getGameMetadata().isPlayersTurn(player.getPlayerId())
                || pickedCards.size() < minNumCards
                || pickedCards.size() > 3) {
            getTurnStateExecutable().failedClaimDestinationCards(player.getPlayerId(), pickedCards, unpickedCards);
            return false;
        }

        Game game = Database.getInstance().getGameForPlayer(player.getPlayerId());

        game.returnDestinationCards(unpickedCards);

        for(DestinationCard card : pickedCards) {
            player.getHand().addDestinationCard(card);
        }
        player.getMyGame().addDestinationCards(player.getPlayerId(),pickedCards.size());

        getTurnStateExecutable().claimDestinationCards(Database.getInstance().getAuthToken(player.getMyUser().getUserId()), player.getPlayerId(), pickedCards, unpickedCards);

        return true;
    }

    @Override
    public boolean wait(Player player) {
        return false;
    }

}
