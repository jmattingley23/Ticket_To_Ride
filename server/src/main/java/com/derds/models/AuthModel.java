package com.derds.models;

import com.derds.cmddata.LoginCmdData;

import java.util.UUID;
import java.util.logging.Logger;

/**
 * Basic auth token model for storing authtokens in our in-memory database
 */
public class AuthModel {
    private String userId;
    private String authToken;
    private long created;

    private final long AUTH_EXPIRATION = 3600000; // 1 hour in milliseconds

    private static Logger logger;

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    /**
     * Creates a new AuthToken for the given userid
     * @param userId the userid to make an authtoken for
     */
    public AuthModel(String userId) {
        logger.entering("AuthModel","constructor");
        this.userId = userId;
        authToken = UUID.randomUUID().toString().replace("-", ""); //random token
        created = System.currentTimeMillis(); //create now
        logger.exiting("AuthModel","constructor");
    }

    /**
     * Determines if this authToken is valid
     * @return whether the token is valid or not
     */
    private boolean isAuthTokenValid() {
        logger.entering("Database","isAuthTokenValid");
        long elapsedTime = System.currentTimeMillis() - created;  //calculate different
        logger.exiting("Database","isAuthTokenValid");
        return (elapsedTime < AUTH_EXPIRATION); //valid if less than 1 hour old
    }
}
