package com.derds.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
import java.util.Map;
import java.util.Random;

public class DeckManager {

    private List<TrainCard> trainCards;
    private List<TrainCard> trainCardsDiscard;
    private List<DestinationCard> destinationCards;
    private River river;

    public DeckManager(GameMap gameMap) {
        trainCards = new ArrayList<>();
        trainCardsDiscard = new ArrayList<>();
        destinationCards = new ArrayList<>();
        river = new River();

        // create deck
        for(int i = 0; i < 12; i++) {
            trainCards.add(new TrainCard(TrainColor.PINK));
            trainCards.add(new TrainCard(TrainColor.WHITE));
            trainCards.add(new TrainCard(TrainColor.BLUE));
            trainCards.add(new TrainCard(TrainColor.YELLOW));
            trainCards.add(new TrainCard(TrainColor.ORANGE));
            trainCards.add(new TrainCard(TrainColor.PURPLE));
            trainCards.add(new TrainCard(TrainColor.RED));
            trainCards.add(new TrainCard(TrainColor.GREEN));
            trainCards.add(new TrainCard(TrainColor.RAINBOW));
        }
        // add two extra rainbow train cards
        trainCards.add(new TrainCard(TrainColor.RAINBOW));
        trainCards.add(new TrainCard(TrainColor.RAINBOW));

        Map<String, City> list = gameMap.getCities();
        destinationCards.add(new DestinationCard(21,list.get(GameMap.UTAPAU),list.get(GameMap.DATHOMIR)));
        destinationCards.add(new DestinationCard(8,list.get(GameMap.CORELLIA),list.get(GameMap.KAMINO)));
        destinationCards.add(new DestinationCard(8,list.get(GameMap.CORUSCANT),list.get(GameMap.DEATH_STAR)));
        destinationCards.add(new DestinationCard(6,list.get(GameMap.DATHOMIR),list.get(GameMap.KORRIBAN)));
        destinationCards.add(new DestinationCard(17,list.get(GameMap.POLIS_MASSA),list.get(GameMap.DEATH_STAR)));
        destinationCards.add(new DestinationCard(20,list.get(GameMap.ENDOR),list.get(GameMap.MYGEETO)));
        destinationCards.add(new DestinationCard(10,list.get(GameMap.CORELLIA),list.get(GameMap.GEONOSIS)));
        destinationCards.add(new DestinationCard(10,list.get(GameMap.ORD_MANTELL),list.get(GameMap.LOTHAL)));
        destinationCards.add(new DestinationCard(11,list.get(GameMap.POLIS_MASSA),list.get(GameMap.SCARIF)));
        destinationCards.add(new DestinationCard(11,list.get(GameMap.CATO_NEIMOIDIA),list.get(GameMap.DATHOMIR)));
        destinationCards.add(new DestinationCard(7,list.get(GameMap.TAKODANA),list.get(GameMap.HOTH)));
        destinationCards.add(new DestinationCard(13,list.get(GameMap.TAKODANA),list.get(GameMap.SCARIF)));
        destinationCards.add(new DestinationCard(20,list.get(GameMap.UTAPAU),list.get(GameMap.LOTHAL)));
        destinationCards.add(new DestinationCard(11,list.get(GameMap.JEDHA),list.get(GameMap.KASHYYYK)));
        destinationCards.add(new DestinationCard(17,list.get(GameMap.MUSTAFAR),list.get(GameMap.KORRIBAN)));
        destinationCards.add(new DestinationCard(5,list.get(GameMap.JAKKU),list.get(GameMap.KAMINO)));
        destinationCards.add(new DestinationCard(16,list.get(GameMap.UTAPAU),list.get(GameMap.ALDERAAN)));
        destinationCards.add(new DestinationCard(11,list.get(GameMap.NABOO),list.get(GameMap.MANDALORE)));
        destinationCards.add(new DestinationCard(9,list.get(GameMap.ALDERAAN),list.get(GameMap.TATOOINE)));
        destinationCards.add(new DestinationCard(13,list.get(GameMap.ENDOR),list.get(GameMap.TATOOINE)));
        destinationCards.add(new DestinationCard(12,list.get(GameMap.DANTOOINE),list.get(GameMap.LOTHAL)));
        destinationCards.add(new DestinationCard(7,list.get(GameMap.ALDERAAN),list.get(GameMap.KESSEL)));
        destinationCards.add(new DestinationCard(9,list.get(GameMap.MYGEETO),list.get(GameMap.KORRIBAN)));
        destinationCards.add(new DestinationCard(22,list.get(GameMap.DEATH_STAR_II),list.get(GameMap.DATHOMIR)));
        destinationCards.add(new DestinationCard(4,list.get(GameMap.NABOO),list.get(GameMap.GEONOSIS)));
        destinationCards.add(new DestinationCard(8,list.get(GameMap.BESPIN),list.get(GameMap.UTAPAU)));
        destinationCards.add(new DestinationCard(12,list.get(GameMap.JEDHA),list.get(GameMap.KAMINO)));
        destinationCards.add(new DestinationCard(13,list.get(GameMap.MYGEETO),list.get(GameMap.KESSEL)));
        destinationCards.add(new DestinationCard(9,list.get(GameMap.CORUSCANT),list.get(GameMap.MALASTARE)));
        destinationCards.add(new DestinationCard(9,list.get(GameMap.DEATH_STAR_II),list.get(GameMap.UTAPAU)));

        // TODO create destination card deck
        Collections.shuffle(destinationCards, new Random(System.currentTimeMillis()));

        // fill river
        // TODO check river
        river.fillRiver(drawMultipleTrainCards(5 - river.size()));
    }

    public TrainCard drawTrainCard() {
        if(trainCards.size() != 0) {
            TrainCard card = trainCards.remove(0);  // get top card
            if (trainCards.size() == 0) {//shuffle if deck is empty
                shuffle();
            }
            return card;
        }
        return null;
    }

    public List<TrainCard> drawMultipleTrainCards(int howMany) {
        List<TrainCard> cards = new ArrayList<>();
        for(int i = 0; i < howMany; i++) {
            cards.add(drawTrainCard());
        }
        return cards;
    }

    public DestinationCard drawDestCard() {
        if(destinationCards.size() > 0) {
            DestinationCard card = destinationCards.remove(0);  // get top card
            return card;
        } else return null;
    }

    public void shuffle() {
        trainCards.addAll(trainCardsDiscard);
        trainCardsDiscard.clear();
        Collections.shuffle(trainCards, new Random(System.currentTimeMillis()));
    }

    public void addDiscardTrainCard(TrainCard card) {
        trainCardsDiscard.add(card);
    }

    public void addDiscardTrainCards(List<TrainCard> cards) {
        trainCardsDiscard.addAll(cards);
    }

    public void returnDestinationCard(DestinationCard card) {
        destinationCards.add(card); // add to end
    }

    public int getDiscardCount() {
        return trainCardsDiscard.size();
    }

    public int getTrainDeckCount() {
        return trainCards.size();
    }

    public int getDestinationDeckCount() {
        return destinationCards.size();
    }

    private int getNumberOfNonWild(){
        int count = 0;
        for(TrainCard card : trainCards){
            if(card.getColor() != TrainColor.RAINBOW){
                count++;
            }
        }
        return count;
    }

    //Returns the card added to the river
    public TrainCard drawCardFromRiver(TrainCard card){
        river.drawCard(card);
        if(getTrainDeckCount() > 0) {
            TrainCard cardAdded = drawTrainCard();
            river.addCard(cardAdded);
            while(river.containsThreeWilds() && (getNumberOfNonWild() > 2 && getTrainDeckCount() > 0)){
                addDiscardTrainCards(river.getShowingCards());
                river.clearRiver();
                //shuffle();
                for(int i = 0; (i < 5) && (getTrainDeckCount() > 0); i++){
                    //fill the river back up
                    river.addCard(drawTrainCard());
                }
            }
            return cardAdded;
        }
        return null;
    }

    public River getRiver() {
        return river;
    }
}
