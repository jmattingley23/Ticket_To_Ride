package com.derds.models.firebase;

import com.derds.gamecommand.AbstractGameCommand;

public class FirebaseDataCommand  implements AbstractFirebaseData {
    private String GAME_COMMAND;
    private AbstractGameCommand COMMAND_DATA;

    public FirebaseDataCommand(String className, AbstractGameCommand command) {
        this.GAME_COMMAND = className;
        this.COMMAND_DATA = command;
    }
}
