package com.derds.models.firebase;

public class FirebaseMessage {
    private String to;
    private AbstractFirebaseData data;

    public FirebaseMessage(String to, AbstractFirebaseData data) {
        this.to = to;
        this.data = data;
    }
}
