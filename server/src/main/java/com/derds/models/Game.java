package com.derds.models;

import com.derds.Database;
import com.derds.gamecommand.AbstractGameCommand;
import com.derds.turnstate.BeginGameState;
import com.derds.turnstate.StartTurnState;
import com.derds.turnstate.ChooseDestinationCardsState;
import com.derds.turnstate.DrawTrainCardState;
import com.derds.turnstate.TurnState;
import com.derds.turnstate.EndTurnState;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.UUID;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;


public class Game {
    private GameMetadata gameMetadata;
    private DeckManager deckManager;
    private List<Player> players;
    private String gameId;
    private List<AbstractGameCommand> commands;
    private boolean isStarted;
    private List<Message> chatMessages;
    private GameMap gameMap;
    private int endingTurn;
    private boolean lastRound;
    private boolean pastEndingPlayer;

    private static Map<TurnState.StateName, TurnState> turnStates = new HashMap<>();

    static {
        turnStates = new TreeMap<>();
        turnStates.put(EndTurnState.key, EndTurnState.instance);
        turnStates.put(BeginGameState.key, BeginGameState.instance);
        turnStates.put(StartTurnState.key, StartTurnState.instance);
        turnStates.put(ChooseDestinationCardsState.key, ChooseDestinationCardsState.instance);
        turnStates.put(DrawTrainCardState.key, DrawTrainCardState.instance);
    }

    private static Logger logger;

    static {
        try {
            initLog();
        } catch (IOException e) {
            System.out.println("Could not initialize log: " + e.getMessage());
        }
    }

    private static void initLog() throws IOException {
        Level logLevel = Level.FINEST;
        String fileAppend = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
        // TODO should we add a special folder for these?
        FileHandler file = new FileHandler("logs/TTRLog_" + fileAppend + ".swag");
        SimpleFormatter formatter = new SimpleFormatter();
        file.setFormatter(formatter);
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
        logger.addHandler(file);
        logger.setLevel(logLevel);
    }

    public GameMap getGameMap() {
        return gameMap;
    }

    public Game(User owner, String gameName, PlayerColor ownerColor) {
        gameMap = new GameMap();
        deckManager = new DeckManager(gameMap);
        gameId = UUID.randomUUID().toString().replace("-", "");
        players = new ArrayList<>();
        gameMetadata = new GameMetadata(gameId, owner.getUserId(),
                deckManager.getDiscardCount(), deckManager.getTrainDeckCount(),
                deckManager.getDestinationDeckCount(), gameName);
        isStarted = false;
        chatMessages = new ArrayList<>();
        commands = new ArrayList<>();
        endingTurn = -1;
        lastRound = false;
        pastEndingPlayer = false;

        gameMetadata.addCardsToRiver(deckManager.getRiver().getShowingCards());
        addPlayer(owner, ownerColor);
    }


    public GameMetadata getGameMetadata() {
        return gameMetadata;
    }



    public void addPlayer(User user, PlayerColor playerColor) {
        Player player = new Player(user, gameId, playerColor, players.size(), TurnState.StateName.BEGIN_GAME);
        gameMetadata.addPlayer(player);
        //gameMetadata.setTrainDeckCount(deckManager.getTrainDeckCount());
        Database.getInstance().addPlayerGame(player.getPlayerId(), gameId);
        players.add(player);
    }

    public boolean isPastEndingPlayer() {
        return pastEndingPlayer;
    }

    public void setPastEndingPlayer(boolean pastEndingPlayer) {
        this.pastEndingPlayer = pastEndingPlayer;
    }

    public boolean isLastRound() {
        return lastRound;
    }

    public void setLastRound(boolean lastRound) {
        this.lastRound = lastRound;
    }

    public int getEndingTurn() {
        return endingTurn;
    }

    public void setEndingTurn(int endingTurn) {
        this.endingTurn = endingTurn;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public Player getOwnerPlayer() {
        for(Player player : players) {
            if(player.getMyUser().getUserId().equals(gameMetadata.getOwnerUserId())) {
                return player;
            }
        }
        return null;
    }

    public String getGameId() {
        return gameId;
    }

    public Player getPlayerForUserId(String userId) {
        for(Player player : players) {
            if(player.getMyUser().getUserId().equals(userId)) {
                return player;
            }
        }
        return null;
    }

    public Player getPlayerForPlayerId(String playerId) {
        for(Player player : players) {
            if(player.getPlayerId().equals(playerId)) {
                return player;
            }
        }
        return null;
    }

    public List<AbstractGameCommand> getCommandsFromQueue(int index, String playerId) {
        if(index < 0) {
            index = 0;
        }
        return commands.subList(index, commands.size());
        // TODO null out fields not visible to the player passed in
    }

    public boolean isColorTaken(PlayerColor color) {
        for(Player player : players) {
            if(player.getPlayerColor() == color) {
                return true;
            }
        }
        return false;
    }

    public void addChat(Message message) {
        chatMessages.add(message);
    }

    public List<Message> getChatMessages() {
        return chatMessages;
    }

    public boolean isStarted() {
        return isStarted;
    }

    public void setStarted(boolean started) {
        this.isStarted = started;
    }

    public PlayerColor getPlayerColorForPlayerId(String playerId) {
        return null;
    }

    public DestinationCard drawDestinationCard() {
        return deckManager.drawDestCard();
    }

    public void returnDestinationCards(List<DestinationCard> cardsReturned) {
        for(DestinationCard card : cardsReturned) {
            deckManager.returnDestinationCard(card);
        }
    }

    public void updateNumberTrainPieces(String playerId, int amount){
        //for(Player player : players){
            //player.getMyGame().reduceTrainPieces(playerId,amount);
        //}
        getPlayerForPlayerId(playerId).getMyGame().reduceTrainPieces(playerId,amount);
    }

    public void updateNumberTrainCards(String playerId, int amount){
            getPlayerForPlayerId(playerId).getMyGame().addTrainCards(playerId,amount);
    }

    public void updatePlayerPoints(String playerId, int amount){
            getPlayerForPlayerId(playerId).getMyGame().addToScore(playerId,amount);
    }

    public int getCommandQueueSize() {
        return commands.size();
    }

    public void addCommand(AbstractGameCommand command) {
        if(command.getCommandSequenceId() == commands.size()) {
            commands.add(command);
        } else {
            logger.severe("Command Sequence Id for command added to game " + gameId + " was not in the correct order...");
        }
    }

    public TrainCard drawTrainCard() {
        return deckManager.drawTrainCard();
    }

    public void addToDiscard(List<TrainCard> cards){
        deckManager.addDiscardTrainCards(cards);
        if(deckManager.getTrainDeckCount() == 0){
            deckManager.shuffle();
            List<TrainCard> newRiver = new ArrayList<>();
            int numEmptyRiver = 5 - deckManager.getRiver().size();
            for(int i = 0; i < numEmptyRiver && deckManager.getTrainDeckCount() > 0; i++){
                newRiver.add(drawTrainCard());
            }
            deckManager.getRiver().fillRiver(newRiver);
            gameMetadata.addCardsToRiver(deckManager.getRiver().getShowingCards());
        }
    }

    public TrainCard drawCardFromRiver(TrainCard card){
        return deckManager.drawCardFromRiver(card);
    }

    public TurnState getPlayerTurnState(String playerId) {
        for (Player player : players) {
            if(player.getPlayerId().equals(playerId)) {
                return turnStates.get(player.getTurnState());
            }
        }
        logger.log(Level.SEVERE, "Turn state not found.");
        return null;
    }

    public DeckManager getDeckManager() {
        return deckManager;
    }

}