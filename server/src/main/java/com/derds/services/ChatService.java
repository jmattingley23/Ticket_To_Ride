package com.derds.services;

import com.derds.Database;
import com.derds.Strings;
import com.derds.command.game.SendChatCommand;
import com.derds.gamecommand.AbstractGameCommand;
import com.derds.models.Game;
import com.derds.models.Message;
import com.derds.models.Player;
import com.derds.result.Result;

import java.util.Date;

public class ChatService {
    private static ChatService instance;

    private ChatService() {}

    public static ChatService getInstance() {
        if(instance == null) {
            instance = new ChatService();
        }
        return instance;
    }

    private void addToGame(Game game, AbstractGameCommand command) {
        command.setCommandSequenceId(game.getCommandQueueSize());
        game.addCommand(command);
    }

    private void sendViaFirebase(Game game, AbstractGameCommand command) {
        for(Player player : game.getPlayers()) {
            FirebaseService.getInstance().sendFirebaseCommand(Database.getInstance().getAuthToken(player.getMyUser().getUserId()),
                    command.getClass().getSimpleName(), command);
        }
    }

    public Result sendChat(String authToken, Message content) {
        //validate authToken
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }

        //validate game
        Game game = Database.getInstance().getGameForPlayer(content.getPlayerId());
        if(game == null) {
            return new Result(Strings.GAME_NOT_FOUND);
        }

        //set the time to now
        content.setTimeSent(new Date().getTime());
        game.addChat(content);

        //build command
        SendChatCommand command = new SendChatCommand(null, game.getGameId(), content);

        addToGame(game, command);
        sendViaFirebase(game, command);

        return new Result();
    }
}
