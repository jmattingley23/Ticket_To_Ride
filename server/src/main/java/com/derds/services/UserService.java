package com.derds.services;

import com.derds.Database;
import com.derds.Strings;
import com.derds.result.LoginResult;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.UUID;

public class UserService {
    private static UserService instance;

    private UserService() {}

    public static UserService getInstance() {
        if(instance == null) {
            instance = new UserService();
        }
        return instance;
    }

    /**
     * Attempts to login a user with the given information
     * @param username the username entered by the user
     * @param password the password entered by the user
     * @param firebaseToken the firebase token for the device the user is using
     * @post a LoginResult with details on if the login was successful
     */
    public LoginResult login(String username, char[] password, String firebaseToken) {
        //server-side validation
        if(!isUsernameValid(username)) {
            return new LoginResult(Strings.BAD_USERNAME);
        } else if(!isPasswordValid(password)) {
            return new LoginResult(Strings.BAD_PASSWORD);
        } else if(!isStringValid(firebaseToken)) {
            return new LoginResult(Strings.BAD_FIREBASE_TOKEN);
        }

        //proceed
        try {
            //make sure the username exists
            if (!Database.getInstance().doesUsernameExist(username)) {
                return new LoginResult(Strings.BAD_LOGIN);
            }

            //get the stored password hash & salt for the user
            String storedPassword = Database.getInstance().getStoredPasswordForUsername(username); //verify password
            if (!PasswordService.getInstance().validatePassword(password, storedPassword)) {
                return new LoginResult(Strings.BAD_LOGIN);
            }
            password = new char[256]; //overwrite the original password in memory

            String userId = Database.getInstance().getUserIdForUsername(username);
            if(userId == null) {
                return new LoginResult(Strings.SERVER_ERROR);
            }

            String authToken = UUID.randomUUID().toString().replace("-", "");
            if(!Database.getInstance().addAuthTokenForUserId(userId, authToken)) {
                return new LoginResult(Strings.SERVER_ERROR);
            }

            FirebaseService.getInstance().addFirebaseToken(authToken, firebaseToken);

            //everything worked
            return new LoginResult(authToken, username);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) { //something went wrong with password service
            e.printStackTrace();
            return new LoginResult(Strings.SERVER_ERROR);
        }
    }

    /**
     * Attempts to register a user with the given information
     * @param username the username entered by the user
     * @param password the password entered by the user
     * @param firstname the firstname entered by the user
     * @param lastname the lastname entered by the user
     * @param firebaseToken the firebase token for the device the user is using
     * @post a LoginResult with details on if the login was successful
     */
    public LoginResult register(String username, char[] password, String firstname, String lastname, String firebaseToken) {
        //server-side validation
        if(!isUsernameValid(username)) {
            return new LoginResult(Strings.BAD_USERNAME);
        } else if(!isPasswordValid(password)) {
            return new LoginResult(Strings.BAD_PASSWORD);
        } else if(!isStringValid(firstname)) {
            return new LoginResult(Strings.BAD_FIRSTNAME);
        } else if(!isStringValid(lastname)) {
            return new LoginResult(Strings.BAD_LASTNAME);
        } else if(!isStringValid(firebaseToken)) {
            return new LoginResult(Strings.BAD_FIREBASE_TOKEN);
        }

        //proceed
        try {
            //make sure the username isn't already taken
            if(Database.getInstance().doesUsernameExist(username)) {
                return new LoginResult(Strings.USERNAME_EXISTS);
            }

            //salt & hash the password
            String hashedSaltedPassword = PasswordService.getInstance().getSaltedHashedPassword(password);

            //add new user object to database
            Database.getInstance().pushNewUser(username, hashedSaltedPassword, firstname, lastname);

            //everything worked
            return login(username, password, firebaseToken); //login after registering
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) { //something went wrong with password service
            e.printStackTrace();
            return new LoginResult(Strings.SERVER_ERROR);
        }
    }

    /**
     * Checks to see if the given username is valid
     * @pre none
     * @param username the username to check
     * @post whether or not the username was valid
     */
    private boolean isUsernameValid(String username) {
        return (username != null && username.matches("^([a-zA-Z]|\\d)+$")); //check if null or contains non alphanumeric characters
    }

    /**
     * Checks to see if the given password is valid
     * @pre none
     * @param password the password to check
     * @post whether or not the password was valid
     */
    private boolean isPasswordValid(char[] password) {
        //check if null or empty array
        return (password != null && password.length != 0);
    }

    /**
     * Checks to see if the given string is valid
     * @pre none
     * @param string the string to check
     * @post whether or not the name was valid
     */
    private boolean isStringValid(String string) {
        return (string != null && string.trim().length() != 0);
    }
}
