package com.derds.services;

import com.derds.Database;
import com.derds.Strings;
import com.derds.command.game.InitGameCommand;
import com.derds.models.Game;
import com.derds.models.Hand;
import com.derds.models.LobbyGameMetadata;
import com.derds.models.Player;
import com.derds.models.PlayerColor;
import com.derds.models.User;
import com.derds.result.GameResult;
import com.derds.result.RefreshLobbyResult;
import com.derds.result.Result;
import com.derds.turnstate.TurnState;
import com.google.gson.Gson;

import java.util.List;
import java.util.UUID;

import javax.xml.crypto.Data;

import static com.derds.Strings.AUTH_NOT_FOUND;
import static com.derds.Strings.GAME_NOT_FOUND;

/**
 * Handles the logic for manipulating lobby information
 */
public class LobbyService {
    public static final int MIN_PLAYERS = 2;
    public static final int MAX_PLAYERS = 5;
    private Gson gson;
    private static LobbyService instance;

    private LobbyService() {
        gson = new Gson();
    }

    public static LobbyService getInstance() {
        if(instance == null) {
            instance = new LobbyService();
        }
        return instance;
    }

    /**
     * Starts the game indicated by the gameID
     * @pre gameID is not null, exists in the database, the user owns the game, and the minimum player count is met
     * @post The game associated with the gameId will be started
     * @param authToken The authToken of the user
     * @param gameID The id for the game to be started
     * @return
     */
    public GameResult startGame(String authToken, String gameID) {
        //validate authToken
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)){
            return new GameResult(AUTH_NOT_FOUND);
        }
        //validate gameId
        if(gameID == null || gameID.trim().length() == 0) {
            return new GameResult(Strings.GAME_NOT_FOUND);
        }

        //get the game associated with the given gameID
        Game game = Database.getInstance().getGame(gameID);
        if(game == null) {
            return new GameResult(Strings.GAME_NOT_FOUND);
        }
        //get the player that owns the game
        Player ownerPlayer = game.getOwnerPlayer();
        //get the user for the owner player
        User owner = ownerPlayer.getMyUser();

        //check if the user associated with the given authToken is the same one that owns the game
        if(Database.getInstance().getUser(authToken) != owner) {
            return new GameResult(Strings.GAME_NOT_OWNED);
        }

        //check if the game has enough players to start
        if(game.getPlayers().size() < MIN_PLAYERS) {
           return new GameResult(Strings.NOT_ENOUGH_PLAYERS);
        }

        //start the game & return
        game.setStarted(true);

        for(Player player : game.getPlayers()) {
            player.getHand().addTrainCard(game.drawTrainCard());
            player.getHand().addTrainCard(game.drawTrainCard());
            player.getHand().addTrainCard(game.drawTrainCard());
            player.getHand().addTrainCard(game.drawTrainCard());
        }

        for(Player player : game.getPlayers()) {
//            game.getGameMetadata().addTrainCards(player.getPlayerId(),4);
            player.getMyGame().setTurn(0);  // Initially we start with turn of 0
            player.getMyGame().setTrainDeckCount(game.getDeckManager().getTrainDeckCount());
            player.setTurnState(TurnState.StateName.BEGIN_GAME);    // make sure everyone is in the begin game state
            player.getMyGame().initializeNumPlayerTrainCards();
            InitGameCommand initGameCommand = new InitGameCommand(player.getPlayerId(), player.getMyGame().getGameId(), player.getTurnState(), player.getMyTurn(), player);
            initGameCommand.setCommandSequenceId(game.getCommandQueueSize());
            game.addCommand(initGameCommand);
            FirebaseService.getInstance().sendFirebaseCommand(Database.getInstance().getAuthToken(player.getMyUser().getUserId()), initGameCommand.getClass().getSimpleName(), initGameCommand);
        }

        //games changed, update the clients
        FirebaseService.getInstance().notifyLobbyClients();

        return new GameResult(null, gameID);
    }

    /**
     * @pre authToken and gameID are not null, gameID exists, and the game doesn't have the max number of players
     * @param authToken the authToken for the user
     * @param gameID the id of the game to join
     * @return A GameResult object with the gameID
     * @post The user will be added to the game
     */
    public GameResult joinGame(String authToken, String gameID, PlayerColor playerColor){
        //validate authToken
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)){
            return new GameResult(AUTH_NOT_FOUND);
        }
        //validate gameId
        if(gameID == null || !Database.getInstance().doesGameExist(gameID)){
            return new GameResult(GAME_NOT_FOUND);
        }

        //get game associated with the gameId
        Game game = Database.getInstance().getGame(gameID);
        if(game == null) {
            return new GameResult(Strings.GAME_NOT_FOUND);
        }

        //make sure there is space
        if(game.getPlayers().size() >= MAX_PLAYERS) {
            return new GameResult(Strings.TOO_MANY_PLAYERS);
        }

        if(playerColor == null) {
            return new GameResult(Strings.BAD_PLAYER_COLOR);
        }

        //get the user associated with the given authToken
        User user = Database.getInstance().getUser(authToken);
        if(user == null) {
            return new GameResult(Strings.AUTH_NOT_FOUND);
        }

        //make sure the user isn't already in the game
        if(game.getPlayerForUserId(user.getUserId()) != null) {
            return new GameResult(Strings.ALREADY_IN_GAME);
        }

        //make sure the player's selected color
        if(game.isColorTaken(playerColor)) {
            return new GameResult(Strings.PLAYER_COLOR_TAKEN);
        }

        //join game
        game.addPlayer(user, playerColor);

        //games changed, update the clients
        FirebaseService.getInstance().notifyLobbyClients();

        return new GameResult(null, gameID);
    }

    /**
     * @pre authToken and gameName are not null; user exists
     * @param authToken the authToken for the current user
     * @param gameName the name of the game they're creating
     * @post new game with gameName will be created
     */
    public GameResult createGame(String authToken, String gameName, PlayerColor playerColor) {
        //validate authToken
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new GameResult(AUTH_NOT_FOUND);
        }
        //validate gameName
        if(gameName == null || gameName.trim().length() == 0) {
            return new GameResult(Strings.BAD_GAME_NAME);
        }
        //validate playerColor
        if(playerColor == null) {
            return new GameResult(Strings.BAD_PLAYER_COLOR);
        }

        //get the user for the authToken
        User user = Database.getInstance().getUser(authToken);
        //make a new game
        Game game = new Game(user, gameName, playerColor);

        //save new player & game to the database
        Database.getInstance().addGame(game);

        //games changed, update the clients
        FirebaseService.getInstance().notifyLobbyClients();

        return new GameResult(null, game.getGameId());
    }

    /**
     * @pre authToken is not null and user exists, gameId is not null and game exists
     * @post The game will be deleted
     * @param authToken the authToken for the user
     * @param gameID the gameId of the game to delete
     * @return The Result object
     */
    public Result deleteGame(String authToken, String gameID){
        //validate authToken
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }
        //validate gameId
        if(gameID == null || !Database.getInstance().doesGameExist(gameID)) {
            return new Result(Strings.GAME_NOT_FOUND);
        }

        //remove the game from the database
        Database.getInstance().removeGame(gameID);
        //games changed, update the clients
        FirebaseService.getInstance().notifyLobbyClients();
        return new Result();
    }


    public RefreshLobbyResult refreshLobby(String authToken){
        List<LobbyGameMetadata> open = null; //open but not started
        List<LobbyGameMetadata> joined = null; //joined games
        List<LobbyGameMetadata> owned = null; //owned games

        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new RefreshLobbyResult(Strings.AUTH_NOT_FOUND);
        }

        open = Database.getInstance().getOpenGames(authToken);
        joined = Database.getInstance().getJoinedGames(authToken);
        owned = Database.getInstance().getOwnedGames(authToken);

        return new RefreshLobbyResult(open, joined, owned);
    }
}
