package com.derds.services;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.xml.bind.DatatypeConverter;

public class PasswordService {
    private static PasswordService instance;

    private PasswordService() {}

    public static PasswordService getInstance() {
        if(instance == null) {
            instance = new PasswordService();
        }
        return instance;
    }

    /**
     *
     * @param originalPassword the password entered by the user
     * @pre the originalPassword is not null
     * @post the salted & hashed password in the format salt:hash
     * @throws NoSuchAlgorithmException thrown if the system can't find the PBKDF2 algorithm
     * @throws InvalidKeySpecException thrown if the keyspec we created was invalid
     */
    public String getSaltedHashedPassword(char[] originalPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
        //generate new random salt for this password
        byte[] randomSalt = generateNewSalt();

        //compute a new hash using the given password & salt
        byte[] hash = getHashForPasswordAndSalt(originalPassword, randomSalt);
        originalPassword = new char[256]; //overwrite original password in memory

        //convert hash & salt array to hexadecimal strings
        String saltString = DatatypeConverter.printHexBinary(randomSalt);
        String hashString = DatatypeConverter.printHexBinary(hash);

        //format = passwordSalt:passwordHash
        return saltString + ":" + hashString;
    }

    /**
     * Determines if the given password matches the one stored in the database.  Computes a new hash using the given password &
     * the original salt stored in the database and compares it to the stored hash.
     * @param password the password entered by the user
     * @param storedPassword the salted hashed password stored in the database
     * @pre password & storedPassword aren't null; stored password is in the correct format of salt:hash
     * @post whether the entered password was correct or not
     * @throws NoSuchAlgorithmException thrown if the system can't find the PBKDF2 algorithm
     * @throws InvalidKeySpecException thrown if the keyspec we created was invalid
     */
    public boolean validatePassword(char[] password, String storedPassword) throws NoSuchAlgorithmException, InvalidKeySpecException {
        //format = passwordSalt:passwordHash
        String[] storedSections = storedPassword.split(":"); //split in half
        //get the random salt that was stored in the database & convert back to a byte array
        byte[] passwordSalt = DatatypeConverter.parseHexBinary(storedSections[0]);
        //get the salted hash that was stored in the database & convert back to a byte array
        byte[] passwordHash = DatatypeConverter.parseHexBinary(storedSections[1]);

        //compute a new hash using the entered password & the salt from the database
        byte[] testHash = getHashForPasswordAndSalt(password, passwordSalt);

        //compare the hash we just generated to the one stored in the database
        int diff = passwordHash.length ^ testHash.length;
        for(int i = 0; i < passwordHash.length && i < testHash.length; i++) {
            diff |= passwordHash[i] ^ testHash[i];
        }

        password = new char[256]; //overwrite the original password in memory

        return (diff == 0);
    }

    /**
     * Generates a random 32-bit salt
     * @pre none
     * @post a char array of 32 random characters
     */
    private byte[] generateNewSalt() {
        SecureRandom random = new SecureRandom();
        byte[] salt = new byte[32];
        random.nextBytes(salt); //get random 32 bytes
        return salt;
    }

    /**
     * Generates a hash using the PBKDF2 algorithm for SHA-512 + random salt
     * @param password the original password to hash & salt
     * @param salt the random cryptographic salt
     * @pre password & salt are not null; salt is randomly generated
     * @post the salted hash for the given password
     * @throws NoSuchAlgorithmException thrown if the system can't find the PBKDF2 algorithm
     * @throws InvalidKeySpecException thrown if the keyspec we created was invalid
     */
    private byte[] getHashForPasswordAndSalt(char[] password, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException{
        int iterations = 10;
        int hashSizeBytes = 512;

        PBEKeySpec keySpec = new PBEKeySpec(password, salt, iterations, hashSizeBytes);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");
        byte[] saltedHash = keyFactory.generateSecret(keySpec).getEncoded();
        return saltedHash;
    }
}
