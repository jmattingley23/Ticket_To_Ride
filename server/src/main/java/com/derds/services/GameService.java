package com.derds.services;

import com.derds.Database;
import com.derds.Strings;
import com.derds.command.game.ChooseDestCommand;
import com.derds.command.game.ClaimRouteCommand;
import com.derds.command.game.DrawDestCommand;
import com.derds.command.game.EndGameCommand;
import com.derds.command.game.GetChatHistoryCommand;
import com.derds.command.game.NextTurnCommand;
import com.derds.command.game.PickFaceupCardCommand;
import com.derds.command.game.RequestDrawTrainCommand;
import com.derds.command.game.SyncGameCommand;
import com.derds.command.game.UpdateRiverCommand;
import com.derds.gamecommand.AbstractGameCommand;
import com.derds.gamecommand.AbstractSyncGameCommand;
import com.derds.models.AuthModel;
import com.derds.models.DestinationCard;
import com.derds.models.Game;
import com.derds.models.Message;
import com.derds.models.Player;
import com.derds.models.PlayerStats;
import com.derds.models.Route;
import com.derds.models.TrainCard;
import com.derds.result.Result;
import com.derds.turnstate.BeginGameState;
import com.derds.turnstate.ChooseDestinationCardsState;
import com.derds.turnstate.DrawTrainCardState;
import com.derds.turnstate.ITurnStateExecutable;
import com.derds.turnstate.StartTurnState;
import com.derds.turnstate.TurnState;
import com.derds.turnstate.EndTurnState;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by drc95 on 10/30/17.
 */

public class GameService implements ITurnStateExecutable{
    private static GameService instance;
    private Gson gson;
    private static Map<TurnState.StateName, TurnState> turnStates = new HashMap<>();
    public static int MINIMUM_PIECES = 3;

    static {
        turnStates = new TreeMap<>();
        turnStates.put(EndTurnState.key, EndTurnState.instance);
        turnStates.put(BeginGameState.key, BeginGameState.instance);
        turnStates.put(StartTurnState.key, StartTurnState.instance);
        turnStates.put(ChooseDestinationCardsState.key, ChooseDestinationCardsState.instance);
        turnStates.put(DrawTrainCardState.key, DrawTrainCardState.instance);
    }

    private GameService() {
        gson = new Gson();;

        BeginGameState.instance.setTurnStateExecutable(this);
        ChooseDestinationCardsState.instance.setTurnStateExecutable(this);
        DrawTrainCardState.instance.setTurnStateExecutable(this);
        StartTurnState.instance.setTurnStateExecutable(this);
        EndTurnState.instance.setTurnStateExecutable(this);
    }

    public static GameService getInstance() {
        if(instance == null) {
            instance = new GameService();
        }
        return instance;
    }

    private void addToGame(Game game, AbstractGameCommand command) {
        command.setCommandSequenceId(game.getCommandQueueSize());
        game.addCommand(command);
    }

    private void sendViaFirebase(Game game, AbstractGameCommand command) {
        for(Player player : game.getPlayers()) {
            FirebaseService.getInstance().sendFirebaseCommand(Database.getInstance().getAuthToken(player.getMyUser().getUserId()),
                    command.getClass().getSimpleName(), command);
        }
    }


    public Result pickFaceupCard(String authToken, String playerId, TrainCard cardDrawn) {
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }

        Game game = Database.getInstance().getGameForPlayer(playerId);

        game.drawCardFromRiver(cardDrawn);

        game.getPlayerForPlayerId(playerId).getHand().addTrainCard(cardDrawn);

        PickFaceupCardCommand pickFaceupCommand = new PickFaceupCardCommand(playerId, game.getGameId(), game.getPlayerForPlayerId(playerId).getTurnState(), cardDrawn);
        pickFaceupCommand.setNewRiver(game.getDeckManager().getRiver());
        pickFaceupCommand.setTrainDeckSize(game.getDeckManager().getTrainDeckCount());

        game.updateNumberTrainCards(playerId, 1);
        game.updateNumberTrainCards(playerId, game.getDeckManager().getTrainDeckCount());

        addToGame(game, pickFaceupCommand);

        sendViaFirebase(game, pickFaceupCommand);

        return new Result(null);
    }

    public Result requestDrawDestination(String authToken, String playerId) {
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }

        Player player = Database.getInstance().getGameForPlayer(playerId).getPlayerForPlayerId(playerId);

        // TODO put all this stuff in the TurnStates
        if(turnStates.get(player.getTurnState()).drawDestinationCards(player)) {
            return new Result(null);
        } else return new Result("A problem occurred while drawing destination cards, please make sure you are allowed to do it");



    }

    public Result chooseDestination(String authToken, String playerId, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned) {
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }

        Game game = Database.getInstance().getGameForPlayer(playerId);

        if(game.getPlayerTurnState(playerId).claimDestinationCards(game.getPlayerForPlayerId(playerId),cardsChosen, cardsReturned)) {
            return new Result(null);
        } else {
            return new Result("A problem occurred while choosing destination cards, make sure your choice is valid");
        }

    }


    public Result getChatHistory(String authToken, String playerId, int lastCommandId,  List<Message> messageHistory, int skip) {
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }

        Game game = Database.getInstance().getGameForPlayer(playerId);

        List<Message> actualChatHistory = game.getChatMessages();

        GetChatHistoryCommand getChatHistoryCommand = new GetChatHistoryCommand(playerId, game.getGameId(), lastCommandId, actualChatHistory, skip);

        addToGame(game, getChatHistoryCommand);

        sendViaFirebase(game, getChatHistoryCommand);
        return new Result(null);
    }

    public Result drawTrainCardFromDeckAttempt(String authToken, String playerId){
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }

        Game game = Database.getInstance().getGameForPlayer(playerId);

        if(game.getDeckManager().getTrainDeckCount() == 0){
            game.getDeckManager().shuffle();
        }

        if(game.getDeckManager().getTrainDeckCount() < 1){
            return new Result(Strings.NO_MORE_TRAIN_CARDS);
        }

        Player player = Database.getInstance().getGameForPlayer(playerId).getPlayerForPlayerId(playerId);

        if(!turnStates.get(player.getTurnState()).drawTrainCardFromDeck(player)){
            return new Result("Draw train attempt failed");
        }

        return new Result(null);
    }

    public boolean shouldEndGame(Game game){
        if(game.isLastRound() && game.getGameMetadata().getCurrentTurn() == game.getEndingTurn() && game.isPastEndingPlayer()){
            return true;
        }
        return false;
    }

    @Override
    public void drawTrainCardFromDeck(String authToken, String playerId, boolean canDrawMoreCards) {

        Game game = Database.getInstance().getGameForPlayer(playerId);

        TrainCard topCard = game.drawTrainCard();
        game.getPlayerForPlayerId(playerId).getHand().addTrainCard(topCard);

        RequestDrawTrainCommand requestDrawTrain = new RequestDrawTrainCommand(authToken, playerId, game.getGameId(), game.getPlayerForPlayerId(playerId).getTurnState());
        requestDrawTrain.setDrawnCard(topCard);
        requestDrawTrain.setTrainDeckSize(game.getDeckManager().getTrainDeckCount());
        requestDrawTrain.setPlayerTrainHandSize(game.getPlayerForPlayerId(playerId).getHand().getTrainCardCount());

        game.updateNumberTrainCards(playerId, 1);

        addToGame(game, requestDrawTrain);
        sendViaFirebase(game, requestDrawTrain);

        if(shouldEndGame(game)){
            sendEndGame(game);
        }

        if(!canDrawMoreCards) {
            sendNextTurn(game);
        }

    }

    public Result pickFaceupCardAttempt(String authToken, String playerId, TrainCard cardDrawn){
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }
        //TODO:Make sure deck and river isn't empty
        Game game = Database.getInstance().getGameForPlayer(playerId);
        Player player = game.getPlayerForPlayerId(playerId);

        if(!turnStates.get(player.getTurnState()).selectTrainCard(player,cardDrawn)){
            return new Result("Can't select Rainbow card");
        }

        return new Result(null);
    }

    @Override
    public void drawFaceUpCard(String authToken, String playerId, TrainCard card, boolean canDrawMoreCards) {
        Game game = Database.getInstance().getGameForPlayer(playerId);

        game.drawCardFromRiver(card);

        game.getPlayerForPlayerId(playerId).getHand().addTrainCard(card);

        game.getGameMetadata().setRiver(game.getDeckManager().getRiver());

        PickFaceupCardCommand pickFaceupCommand = new PickFaceupCardCommand(playerId, game.getGameId(), game.getPlayerForPlayerId(playerId).getTurnState(), card);
        pickFaceupCommand.setNewRiver(game.getDeckManager().getRiver());
        pickFaceupCommand.setTrainDeckSize(game.getDeckManager().getTrainDeckCount());
        pickFaceupCommand.setPlayerTrainHandSize(game.getPlayerForPlayerId(playerId).getHand().getTrainCardCount());


        game.updateNumberTrainCards(playerId, 1);

        addToGame(game, pickFaceupCommand);

        sendViaFirebase(game, pickFaceupCommand);

        if(shouldEndGame(game)){
            sendEndGame(game);
        }

        if(!canDrawMoreCards) {
            sendNextTurn(game);
            //else Check if there are no more cards available to draw
        }else if(game.getDeckManager().getTrainDeckCount() < 1
                && game.getDeckManager().getRiver().getShowingCards().size() < 1){
            sendNextTurn(game);
        }
    }

    public void sendNextTurn(Game game) {
        int curTurn = game.getGameMetadata().nextTurn();

        for(Player player : game.getPlayers()) {
            if (player.getMyTurn() == curTurn) {
                if(player.getHand().getDestCardCount() > 0) {
                    player.setTurnState(TurnState.StateName.START_TURN);
                } else {
                    player.setTurnState(TurnState.StateName.BEGIN_GAME);
                }
            } else {
                player.setTurnState(TurnState.StateName.END);
            }
        }

        NextTurnCommand command = new NextTurnCommand(curTurn, game.getGameId());
        command.setCommandSequenceId(game.getCommandQueueSize());
        addToGame(game, command);
        sendViaFirebase(game,command);
    }

    // Implementation of ITurnStateExecutable


    @Override
    public void sendMessage(String message) {

    }

    public Result claimRouteAttempt(String authToken, String playerId, Route route, List<TrainCard> cardsUsed, int trainPiecesUsed) {
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }

        Game game = Database.getInstance().getGameForPlayer(playerId);
        Player player = game.getPlayerForPlayerId(playerId);

        if(!game.getGameMetadata().isPlayersTurn(playerId)) {
            return new Result(Strings.NOT_PLAYER_TURN);
        }
        try {
            if (!route.canClaim(cardsUsed)) {
                return new Result(Strings.INVALID_CARDS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(game.getPlayerForPlayerId(playerId).getMyGame().getNumPiecesLeft(playerId) < trainPiecesUsed){
            return new Result(Strings.NOT_ENOUGH_PIECES);
        }
        // TODO logic for checking if cards are correct

        if(!turnStates.get(player.getTurnState()).claimRoute(player,route,cardsUsed)){
            return new Result("Claim Route failed");
        }

        return new Result(null);
    }

    @Override
    public void claimRoute(String authToken, String playerId, Route route, List<TrainCard> cardsUsed, int trainPiecesUsed) {

        Game game = Database.getInstance().getGameForPlayer(playerId);
        //Move cards from player hand to discard pile
        Player player = game.getPlayerForPlayerId(playerId);
        player.removeCardsFromHand(cardsUsed);
        player.addClaimedRoute(route);
        game.addToDiscard(cardsUsed);

        //Claim route in map

        game.getGameMap().claimRoute(route,playerId,game.getPlayerColorForPlayerId(playerId));
        game.updateNumberTrainCards(playerId,-cardsUsed.size());
        game.updateNumberTrainPieces(playerId,trainPiecesUsed);
        game.updatePlayerPoints(playerId,route.getPoints());

        if(!game.isLastRound() && player.getMyGame().getNumPiecesLeft(playerId) < MINIMUM_PIECES){
            game.setEndingTurn(player.getMyTurn());
            game.setLastRound(true);
        }


        ClaimRouteCommand claimRouteCommand = new ClaimRouteCommand(route,cardsUsed,trainPiecesUsed,playerId, game.getGameId(), player.getTurnState());
        claimRouteCommand.setTrainDeckSize(game.getDeckManager().getTrainDeckCount());
        claimRouteCommand.setNumTrainPieces(game.getPlayerForPlayerId(playerId).getMyGame().getNumPiecesLeft(playerId));

        //Just for when the river wasn't full but its been filled
        game.getGameMetadata().setRiver(game.getDeckManager().getRiver());
        UpdateRiverCommand updateRiverCommand = new UpdateRiverCommand(playerId, game.getGameId(), game.getDeckManager().getRiver());


        if(shouldEndGame(game)){
            sendEndGame(game);
        }

        addToGame(game, claimRouteCommand);
        addToGame(game, updateRiverCommand);

        sendViaFirebase(game, claimRouteCommand);
        sendViaFirebase(game, updateRiverCommand);

        sendNextTurn(game);

        if(game.isLastRound() && !game.isPastEndingPlayer()){
            game.setPastEndingPlayer(true);
        }
    }

    @Override
    public void claimDestinationCards(String authToken, String playerId, List<DestinationCard> cardsChosen, List<DestinationCard> cardsReturned) {
        Game game = Database.getInstance().getGameForPlayer(playerId);

        game.getPlayerForPlayerId(playerId).setTurnState(TurnState.StateName.END);
        ChooseDestCommand chooseDestCommand = new ChooseDestCommand(playerId, game.getGameId(), game.getPlayerForPlayerId(playerId).getTurnState(), cardsChosen,cardsReturned);

        addToGame(game, chooseDestCommand);

        sendViaFirebase(game, chooseDestCommand);

        if(shouldEndGame(game)){
            sendEndGame(game);
        }

        sendNextTurn(game);
    }

    @Override
    public void failedClaimDestinationCards(String playerId, List<DestinationCard> pickedCards, List<DestinationCard> unpickedCards) {
        Game game = Database.getInstance().getGameForPlayer(playerId);
    }

    @Override
    public void drawDestinationCardsRequest() {
        // TODO use this

    }

    @Override
    public void drawDestinationCardsResponse(String playerId, List<DestinationCard> cards) {
        Game game = Database.getInstance().getGameForPlayer(playerId);

        DrawDestCommand drawDestCommand = new DrawDestCommand(cards, true, playerId, game.getGameId(), game.getPlayerForPlayerId(playerId).getTurnState());

        addToGame(game, drawDestCommand);

        sendViaFirebase(game, drawDestCommand);
    }

    public void sendEndGame(Game game) {
        List<PlayerStats> statsList = new ArrayList<>();
        int longestLength = 0;
        PlayerStats bestRoute = null;

        for(Player player: game.getPlayers()) {
            PlayerStats playerStats = player.getPlayerStats();
            if(playerStats.getLongestRoute() > longestLength){
                longestLength = playerStats.getLongestRoute();
                bestRoute = playerStats;
            }
            playerStats.setFinalScore(playerStats.calculateFinalScore());
            statsList.add(playerStats);
        }
        bestRoute.setFinalScore(bestRoute.getFinalScore() + 10);
        bestRoute.setHadLongestRoute(true);

        EndGameCommand endGameCommand = new EndGameCommand(null, game.getGameId(), statsList);
        endGameCommand.setCommandSequenceId(game.getCommandQueueSize());
        addToGame(game, endGameCommand);
        sendViaFirebase(game, endGameCommand);
    }

    public Result sync(String authToken, String playerId) {
        // TODO why do we need auth token?
        Game game = Database.getInstance().getGameForPlayer(playerId);

        List<Route> claimedRoutes = new ArrayList<>();
        for(Route route : game.getGameMap().getRoutes()) {
            if(route.isClaimed()) {
                Route newRoute = new Route(route);
                // clear the cities for tinier json!
                newRoute.clearCities();
                claimedRoutes.add(newRoute);
            }
        }

        game.getGameMetadata().setDestinationDeckCount(game.getDeckManager().getDestinationDeckCount());
        game.getGameMetadata().setRiver(game.getDeckManager().getRiver());

        for(Player player : game.getPlayers()) {
            // TODO check that the gamemetadata is correct
            if(player.getTurnState() == TurnState.StateName.WAIT) {
                player.setTurnState(TurnState.StateName.START_TURN);
            }
            SyncGameCommand syncGameCommand = new SyncGameCommand(player.getPlayerId(), game.getGameId(), player.getTurnState(), game.getGameMetadata().getCurrentTurn(), player, claimedRoutes);
//            addToGame(game, syncGameCommand);
            FirebaseService.getInstance().sendSyncPing(Database.getInstance().getAuthToken(player.getMyUser().getUserId()));
            //sendViaFirebase(game, syncGameCommand);
        }

        return new Result(null);
    }

    public AbstractSyncGameCommand getSyncData(String playerId) {
        // TODO why do we need auth token?
        Game game = Database.getInstance().getGameForPlayer(playerId);

        List<Route> claimedRoutes = new ArrayList<>();
        for(Route route : game.getGameMap().getRoutes()) {
            if(route.isClaimed()) {
                Route newRoute = new Route(route);
                // clear the cities for tinier json!
                newRoute.clearCities();
                claimedRoutes.add(newRoute);
            }
        }

        game.getGameMetadata().setDestinationDeckCount(game.getDeckManager().getDestinationDeckCount());
        game.getGameMetadata().setTrainDeckCount(game.getDeckManager().getTrainDeckCount());
        game.getGameMetadata().setRiver(game.getDeckManager().getRiver());

        Player player = game.getPlayerForPlayerId(playerId);

        if(player.getTurnState() == TurnState.StateName.WAIT) {
            player.setTurnState(TurnState.StateName.START_TURN);
        }
        SyncGameCommand syncGameCommand = new SyncGameCommand(player.getPlayerId(), game.getGameId(), player.getTurnState(), game.getGameMetadata().getCurrentTurn(), player, claimedRoutes);
//        addToGame(game, syncGameCommand);
        syncGameCommand.setCommandSequenceId(game.getCommandQueueSize() - 1);

        return syncGameCommand;
    }
}
