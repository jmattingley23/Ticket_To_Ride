package com.derds.services;

import com.derds.SecureKeys;
import com.derds.Strings;
import com.derds.gamecommand.AbstractGameCommand;
import com.derds.models.firebase.FirebaseDataCommand;
import com.derds.models.firebase.FirebaseDataLobby;
import com.derds.models.firebase.FirebaseMessage;
import com.derds.result.Result;
import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.instrument.Instrumentation;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FirebaseService {
    private static FirebaseService instance;
    private static Logger logger;
    private Gson gson;

    private final String LOBBY_UPDATE = "LOBBY_UPDATE";
    private final String PING = "PING";
    private Map<String, String> firebaseTokens; //map of AuthTokens to FirebaseTokens

    private FirebaseService() {
        firebaseTokens = new HashMap<>();
        gson = new Gson();
    }

    static {
        logger = Logger.getLogger("TICKET_TO_RIDE_SERVER");
    }

    public static FirebaseService getInstance() {
        if(instance == null) {
            instance = new FirebaseService();
        }
        return instance;
    }

    /**
     * Adds a firebase token to the list & associates it with the given authToken
     * @pre both tokens are not null
     * @param authToken the authToken for the user the owns the firebase token
     * @param firebaseToken the firebase token for a user's device
     * @post firebase token was added to the map
     */
    public Result addFirebaseToken(String authToken, String firebaseToken) {
        //validate authToken
        if(authToken == null || authToken.trim().length() == 0) {
            return new Result(Strings.AUTH_NOT_FOUND);
        }
        //validate firebaseToken
        if(firebaseToken == null || firebaseToken.trim().length() == 0) {
            return new Result(Strings.BAD_FIREBASE_TOKEN);
        }

        logger.log(Level.INFO, "added new firebaseToken " + firebaseToken);
        firebaseTokens.put(authToken, firebaseToken);
        return new Result();
    }

    /**
     * Removes the firebase token associated with the given authtoken
     * @pre the authtoken is not null & there is a token associated with it
     * @param authToken the authtoken linked with the firebase token to remove
     * @post whether or not the token was removed
     */
    public Result removeFirebaseTokenForAuth(String authToken) {
        firebaseTokens.remove(authToken);
        return new Result(); //no error
    }

    /**
     * Pings all of the firebase tokens in the list the list of games has changed
     * @pre none
     * @post any firebase tokens in the list were notified
     */
    public void notifyLobbyClients() {
        logger.log(Level.INFO, "notifying all connected firebase clients of game list changes");
        for (Map.Entry<String, String> tokenEntry : firebaseTokens.entrySet()) {
            logger.log(Level.INFO, "notifying " + tokenEntry.getValue());
            sendMessage(tokenEntry.getKey(), gson.toJson(new FirebaseMessage(tokenEntry.getValue(), new FirebaseDataLobby("LOBBY"))));
        }
    }

    public void sendSyncPing(String authToken) {
        sendMessage(authToken, gson.toJson(new FirebaseMessage(firebaseTokens.get(authToken), new FirebaseDataLobby("SYNC"))));
    }

    void sendFirebaseCommand(String authToken, String classType, AbstractGameCommand command) {
        sendMessage(authToken, gson.toJson(new FirebaseMessage(firebaseTokens.get(authToken), new FirebaseDataCommand(classType, command))));
    }

    private void sendMessage(String authToken, String jsonData) {
        try {
            String firebaseId = firebaseTokens.get(authToken);
            System.out.println("notifying " + firebaseId);
            //build url & open connection
            URL url = new URL("https://fcm.googleapis.com/fcm/send"); //firebase API url
            HttpURLConnection httpURLConnection = (HttpURLConnection)url.openConnection();

            //set up connection
            httpURLConnection.setRequestProperty("Authorization", "key=" + SecureKeys.FCM_KEY); //firebase api key
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setDoOutput(true);
            httpURLConnection.setConnectTimeout(10000);


            //write the message object into the connection's output stream
            OutputStream outputStream = httpURLConnection.getOutputStream();
            outputStream.write(jsonData.getBytes());

            httpURLConnection.connect();

            //get response
            InputStream inputStream = httpURLConnection.getInputStream();
            Scanner scanner = new Scanner(inputStream).useDelimiter("\\A");
            String result = scanner.hasNext() ? scanner.next() : "";
        } catch (Exception e) {
            e.printStackTrace();
            logger.log(Level.SEVERE, "Error while sending a firebase cloud message");
            logger.log(Level.SEVERE, e.getMessage());
        }
    }
}
