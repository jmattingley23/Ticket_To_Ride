package com.derds.services;

import com.derds.Database;
import com.derds.Strings;
import com.derds.command.AbstractCommand;
import com.derds.gamecommand.AbstractGameCommand;
import com.derds.models.Game;
import com.derds.result.GameResult;
import com.derds.result.Result;
import com.google.gson.Gson;

import java.util.List;

import javax.xml.crypto.Data;

import static com.derds.Strings.AUTH_NOT_FOUND;

/**
 * Created by drc95 on 10/30/17.
 */

public class CommandQueueService {
    private static CommandQueueService instance;
    private Gson gson;

    private CommandQueueService() {
        gson = new Gson();
    }

    public static CommandQueueService getInstance() {
        if(instance == null) {
            instance = new CommandQueueService();
        }
        return instance;
    }

    public Result getCommandsFromQueue(String authToken, int index, String playerId, String gameID) {
        //validate authToken
        if(authToken == null || !Database.getInstance().doesUserExist(authToken)){
            return new GameResult(AUTH_NOT_FOUND);
        }
        //validate gameId
        if(gameID == null || gameID.trim().length() == 0) {
            return new GameResult(Strings.GAME_NOT_FOUND);
        }

        for(AbstractGameCommand command : Database.getInstance().getGame(gameID).getCommandsFromQueue(index, playerId)) {
            FirebaseService.getInstance().sendFirebaseCommand(authToken, command.getClass().getSimpleName(), command);
        }
        List<AbstractGameCommand> requestedCommands = Database.getInstance().getGame(gameID).getCommandsFromQueue(index, playerId);


        return new Result(null);    // success
    }
}
