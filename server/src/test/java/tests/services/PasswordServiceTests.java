package tests.services;

import com.derds.services.PasswordService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PasswordServiceTests {
    @Before
    public void setUp() {}

    @After
    public void tearDown() {}

    /**
     * Tests giving the same password to the PasswordService 2 different times and makes sure the resulting hashes are different.  Ensures the random salt is working correctly.
     * @pre none
     * @post the two password hashes for the same password should be different
     */
    @Test
    public void testSamePasswordDifferentHashes() throws NoSuchAlgorithmException, InvalidKeySpecException {
        //make 1 password
        char[] password = "myPassword123".toCharArray();
        //hash twice
        String hash1 = PasswordService.getInstance().getSaltedHashedPassword(password);
        String hash2 = PasswordService.getInstance().getSaltedHashedPassword(password);
        //ensure they're different
        assertFalse(hash1.equals(hash2));
    }

    /**
     * Tests hashing & salting a password using the PasswordService, and then validating the original password against the hash
     */
    @Test
    public void testValidateGoodPassword() throws NoSuchAlgorithmException, InvalidKeySpecException {
        //make a password
        char[] originalPassword = "myPassword123".toCharArray();
        //hash & salt
        String hashedSaltedPassword = PasswordService.getInstance().getSaltedHashedPassword(originalPassword);
        //validate
        boolean wasValid = PasswordService.getInstance().validatePassword(originalPassword, hashedSaltedPassword);
        assertTrue(wasValid);
    }

    /**
     * Tests hashing & salting a password using the the PasswordService, and then trying to validate an incorrect password to make sure it returns false
     */
    @Test
    public void testValidateBadPassword() throws NoSuchAlgorithmException, InvalidKeySpecException {
        //make a password
        char[] originalPassword = "myPassword123".toCharArray();
        String hashedSaltedPassword = PasswordService.getInstance().getSaltedHashedPassword(originalPassword);
        //make a different password
        char[] newPassword = "someOtherPassword456".toCharArray();
        //validate
        boolean wasValid = PasswordService.getInstance().validatePassword(newPassword, hashedSaltedPassword);
        assertFalse(wasValid);
    }
}
