package tests.services;

import com.derds.Database;
import com.derds.Strings;
import com.derds.result.LoginResult;
import com.derds.services.UserService;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class UserServiceTests {
    private final String testUsername = "myUsername1"; //test username
    private final String testUsernameCaps = "MYUSERNAME"; //test all-caps username
    private final String testUsernameNumeric = "123123123123"; //test all numbers username
    private final char[] testPassword = "myPassword123".toCharArray(); //test password
    private final String testFirebaseToken = "123firebase123";

    @Before
    public void setUp() {}

    @After
    public void tearDown() {
        Database.getInstance().removeUser(testUsername); //delete test data
        Database.getInstance().removeUser(testUsernameCaps);
        Database.getInstance().removeUser(testUsernameNumeric);
    }

    /**
     * Tests a regular register with good data
     */
    @Test
    public void testRegister() {
        LoginResult result = UserService.getInstance().register(testUsername, testPassword, "Test", "Tester", testFirebaseToken);
        assertTrue(result.getMessage() == null);
    }

    /**
     * Tests registering a user, and then another user with the same username
     */
    @Test
    public void testRegisterDuplicateUsername() {
        LoginResult result1 = UserService.getInstance().register(testUsername, testPassword, "Test", "Tester", testFirebaseToken);
        assertTrue(result1.getMessage() == null);
        LoginResult result2 = UserService.getInstance().register(testUsername, testPassword, "Test", "Tester", testFirebaseToken);
        assertTrue(result2.getMessage().equals(Strings.USERNAME_EXISTS));
    }

    /**
     * Tests registering a user with a null username
     */
    @Test
    public void testRegisterNullUsername() {
        LoginResult result = UserService.getInstance().register(null, testPassword, "Test", "Tester", testFirebaseToken);
        assertTrue(result.getMessage().equals(Strings.BAD_USERNAME));
    }

    /**
     * Tests registering a user with a null password
     */
    @Test
    public void testRegisterNullPassword() {
        LoginResult result = UserService.getInstance().register(testUsername, null, "Test", "Tester", testFirebaseToken);
        assertTrue(result.getMessage().equals(Strings.BAD_PASSWORD));
    }

    /**
     * Tests registering a user with a blank username
     */
    @Test
    public void testRegisterEmptyUsername() {
        LoginResult result = UserService.getInstance().register("", testPassword, "Test", "Tester", testFirebaseToken);
        assertTrue(result.getMessage().equals(Strings.BAD_USERNAME));
    }

    /**
     * Tests registering a user with a blank password
     */
    @Test
    public void testRegisterEmptyPassword() {
        LoginResult result = UserService.getInstance().register(testUsername, "".toCharArray(), "Test", "Tester", testFirebaseToken);
        assertTrue(result.getMessage().equals(Strings.BAD_PASSWORD));
    }

    /**
     * Tests registering a user with only number in their username & then logging in
     */
    @Test
    public void testRegisterLoginNumberUsername() {
        LoginResult registerResult = UserService.getInstance().register(testUsernameNumeric, testPassword, "Test", "Tester", testFirebaseToken);
        assertTrue(registerResult.getMessage() == null);
        LoginResult loginResult = UserService.getInstance().login(testUsernameNumeric, testPassword, testFirebaseToken);
        assertTrue(loginResult.getMessage() == null);
    }

    /**
     * Tests registering a user with an all caps username & and then logging in
     */
    @Test
    public void testRegisterLoginCapsUsername() {
        LoginResult registerResult = UserService.getInstance().register(testUsernameCaps, testPassword, "Test", "Tester", testFirebaseToken);
        assertTrue(registerResult.getMessage() == null);
        LoginResult loginResult = UserService.getInstance().login(testUsernameCaps, testPassword, testFirebaseToken);
        assertTrue(loginResult.getMessage() == null);
    }

    /**
     * Tests registering a user and then logging in
     */
    @Test
    public void testGoodLogin() {
        LoginResult registerResult = UserService.getInstance().register(testUsername, testPassword, "Test", "Tester", testFirebaseToken);
        assertTrue(registerResult.getMessage() == null);
        LoginResult loginResult = UserService.getInstance().login(testUsername, testPassword, testFirebaseToken);
        assertTrue(loginResult.getMessage() == null);
    }

    /**
     * Tests logging in with a username that does not exist
     */
    @Test
    public void testLoginNonexistentUsername() {
        LoginResult result = UserService.getInstance().login(testUsername, testPassword, testFirebaseToken);
        assertTrue(result.getMessage().equals(Strings.BAD_LOGIN));
    }

    /**
     * Tests registering a user and then logging in with the wrong password
     */
    @Test
    public void testLoginWrongPassword() {
        LoginResult registerResult = UserService.getInstance().register(testUsername, testPassword, "Test", "Tester", testFirebaseToken);
        assertTrue(registerResult.getMessage() == null);
        LoginResult loginResult = UserService.getInstance().login(testUsername, "someOtherPassword456".toCharArray(), testFirebaseToken);
        assertTrue(loginResult.getMessage().equals(Strings.BAD_LOGIN));
    }

    /**
     * Tests logging in with a null username
     */
    @Test
    public void testLoginNullUsername() {
        LoginResult loginResult = UserService.getInstance().login(null, testPassword, testFirebaseToken);
        assertTrue(loginResult.getMessage().equals(Strings.BAD_USERNAME));
    }

    /**
     * Tests logging in with an null password
     */
    @Test
    public void testLoginNullPassword() {
        LoginResult loginResult = UserService.getInstance().login(testUsername, null, testFirebaseToken);
        assertTrue(loginResult.getMessage().equals(Strings.BAD_PASSWORD));
    }

    /**
     * Tests logging in with an empty username
     */
    @Test
    public void testLoginEmptyUsername() {
        LoginResult loginResult = UserService.getInstance().login("", testPassword, testFirebaseToken);
        assertTrue(loginResult.getMessage().equals(Strings.BAD_USERNAME));
    }

    /**
     * Tests logging in with an empty password
     */
    @Test
    public void testLoginEmptyPassword() {
        LoginResult loginResult = UserService.getInstance().login(testUsername, new char[0], testFirebaseToken);
        assertTrue(loginResult.getMessage().equals(Strings.BAD_PASSWORD));
    }
}
