package tests.integration;

import com.derds.Database;
import com.derds.Strings;
import com.derds.cmddata.RegisterCmdData;
import com.derds.command.RegisterCommand;
import com.derds.result.LoginResult;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class RegisterIntegrationTests {
    private Gson gson;

    private String testUsername = "testUsername";
    private String testPassword = "testPassword123";
    private String testFirstname = "Luke";
    private String testLastname = "Skywalker";
    private String testFirebaseToken = "123firebase123";

    @Before
    public void setUp() {
        gson = new Gson();
    }

    /**
     * Deletes the user from the database to prevent username collisions
     */
    @After
    public void tearDown() {
        Database.getInstance().removeUser(testUsername);
    }


    /**
     * Tests a regular register with good data
     */
    @Test
    public void testGoodRegister() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, testFirstname, testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage() == null); //register had no errors
        assertTrue(result.getUsername().equals(testUsername)); //registered correct username
        assertTrue(result.getAuthToken() != null); //got new authToken
    }

    /**
     * Tests registering two people with the same username
     */
    @Test
    public void testRegisterDuplicateUsername() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, testFirstname, testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult registerResult = registerCommand.execute();

        assertTrue(registerResult.getMessage() == null); //register had no errors
        assertTrue(registerResult.getUsername().equals(testUsername)); //registered correct username
        assertTrue(registerResult.getAuthToken() != null); //got new authToken

        RegisterCmdData registerCmdData1 = new RegisterCmdData(testUsername, testPassword, testFirstname, testLastname, testFirebaseToken);
        RegisterCommand registerCommand1 = gson.fromJson(gson.toJson(registerCmdData1), RegisterCommand.class);
        LoginResult registerResult1 = registerCommand1.execute();

        assertTrue(registerResult1.getMessage().equals(Strings.USERNAME_EXISTS));
        assertTrue(registerResult1.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with a null username
     */
    @Test
    public void testRegisterNullUsername() {
        RegisterCmdData registerCmdData = new RegisterCmdData(null, testPassword, testFirstname, testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_USERNAME)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with a null password
     */
    @Test
    public void testRegisterNullPassword() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, null, testFirstname, testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_PASSWORD)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with a null firstname
     */
    @Test
    public void testRegisterNullFirstname() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, null, testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_FIRSTNAME)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with a null lastname
     */
    @Test
    public void testRegisterNullLastname() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, testFirstname, null, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_LASTNAME)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with a null firebaseToken
     */
    @Test
    public void testRegisterNullFirebaseToken() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, testFirstname, testLastname, null);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_FIREBASE_TOKEN)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with an empty username
     */
    @Test
    public void testRegisterEmptyUsername() {
        RegisterCmdData registerCmdData = new RegisterCmdData("", testPassword, testFirstname, testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_USERNAME)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with an empty string as a password
     */
    @Test
    public void testRegisterEmptyPassword() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, "", testFirstname, testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_PASSWORD)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with an empty string as a firstname
     */
    @Test
    public void testRegisterEmptyFirstname() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, "", testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_FIRSTNAME)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with an empty string as a lastname
     */
    @Test
    public void testRegisterEmptyLastname() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, testFirstname, "", testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_LASTNAME)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }

    /**
     * Tests registering a user with an empty string as a firebaseToken
     */
    @Test
    public void testRegisterEmptyFirebaseToken() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, testFirstname, testLastname, "");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_FIREBASE_TOKEN)); //correct error
        assertTrue(result.getAuthToken() == null); //bad registers shouldn't give authToken
    }
}
