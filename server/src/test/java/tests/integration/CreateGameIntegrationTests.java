package tests.integration;

import com.derds.Database;
import com.derds.Strings;
import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.RegisterCmdData;
import com.derds.command.CreateGameCommand;
import com.derds.command.RegisterCommand;
import com.derds.models.PlayerColor;
import com.derds.result.GameResult;
import com.derds.result.LoginResult;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CreateGameIntegrationTests {
    private static String authToken;
    private Gson gson;
    private List<String> gameIds;

    /**
     * Registers user to use for the tests & saves their AuthToken
     */
    @BeforeClass
    public static void init() {
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername", "testPassword", "Luke", "Skywalker", "123firebase123");
        RegisterCommand registerCommand = new Gson().fromJson(new Gson().toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage() == null);
        assertTrue(result.getAuthToken() != null);

        authToken = result.getAuthToken();
    }

    /**
     * Make a list for gameIds to collect for deletion
     */
    @Before
    public void setUp() {
        gson = new Gson();
        gameIds = new ArrayList<>();
    }

    /**
     * Delete test games
     */
    @After
    public void tearDown() {
        for(String gameId : gameIds) {
            Database.getInstance().removeGame(gameId);
        }
    }

    @AfterClass
    public static void destroy() {
        Database.getInstance().removeUser("testUsername");
    }


    /**
     * Creates creating a game with valid data
     */
    @Test
    public void createGoodGame() {
        CreateGameCmdData createGameCmdData = new CreateGameCmdData("testGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(authToken);
        GameResult result = createGameCommand.execute();
        gameIds.add(result.getGameID());

        assertTrue(result.getMessage() == null); //no errors
        assertTrue(result.getGameID() != null); //returned a gameID
    }

    /**
     * Tests creating a game with no authToken provided
     */
    @Test
    public void createGameNoAuthToken() {
        CreateGameCmdData createGameCmdData = new CreateGameCmdData("testGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        GameResult result = createGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
        assertTrue(result.getGameID() == null); //errors shouldn't give gameID
    }

    /**
     * Tests creating a game where an authToken is provided, but doesn't exist
     */
    @Test
    public void createGameInvalidAuthToken() {
        CreateGameCmdData createGameCmdData = new CreateGameCmdData("testGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken("myAuthToken");
        GameResult result = createGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
        assertTrue(result.getGameID() == null); //errors shouldn't give gameID
    }

    /**
     * Tests creating two games with the same name.  This is allowed so no errors should occur
     */
    @Test
    public void createGameSameName() {
        CreateGameCmdData createGameCmdData = new CreateGameCmdData("testGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(authToken);
        GameResult result = createGameCommand.execute();

        assertTrue(result.getMessage() == null); //no errors
        assertTrue(result.getGameID() != null); //returned gameId
        gameIds.add(result.getGameID());

        CreateGameCmdData createGameCmdData1 = new CreateGameCmdData("testGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand1 = gson.fromJson(gson.toJson(createGameCmdData1), CreateGameCommand.class);
        createGameCommand1.setAuthToken(authToken);
        GameResult result1 = createGameCommand1.execute();
        gameIds.add(result1.getGameID());

        assertTrue(result1.getMessage() == null); //no errors
        assertTrue(result1.getGameID() != null); //returned gameId
        assertFalse(result.getGameID().equals(result1.getGameID())); //different games had different gameIds
    }

    /**
     * Tests creating a game with a null authToken
     */
    @Test
    public void createGameNullAuthToken() {
        CreateGameCmdData createGameCmdData = new CreateGameCmdData("testGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(null);
        GameResult result = createGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
        assertTrue(result.getGameID() == null); //errors shouldn't give gameId
    }

    /**
     * Tests creating a game with an empty authToken
     */
    @Test
    public void createGameEmptyAuthToken() {
        CreateGameCmdData createGameCmdData = new CreateGameCmdData("testGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken("");
        GameResult result = createGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
        assertTrue(result.getGameID() == null); //errors shouldn't give gameId
    }

    /**
     * Tests creating a game with a null name
     */
    @Test
    public void createGameNullName() {
        CreateGameCmdData createGameCmdData = new CreateGameCmdData(null, PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(authToken);
        GameResult result = createGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_GAME_NAME)); //correct error message
        assertTrue(result.getGameID() == null); //errors shouldn't give gameId
    }

    /**
     * Tests creating a game with an empty string for a name
     */
    @Test
    public void createGameEmptyName() {
        CreateGameCmdData createGameCmdData = new CreateGameCmdData(null, PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(authToken);
        GameResult result = createGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_GAME_NAME)); //correct error message
        assertTrue(result.getGameID() == null); //errors shouldn't give gameId
    }

    /**
     * Tests creating a game with a null player color
     */
    @Test
    public void createGameNullColor() {
        CreateGameCmdData createGameCmdData = new CreateGameCmdData("testGame", null);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(authToken);
        GameResult result = createGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_PLAYER_COLOR)); //correct error message
        assertTrue(result.getGameID() == null); //errors shouldn't give gameId
    }
}
