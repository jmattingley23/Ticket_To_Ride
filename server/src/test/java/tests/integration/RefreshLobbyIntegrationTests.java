package tests.integration;

import com.derds.Database;
import com.derds.Strings;
import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.JoinGameCmdData;
import com.derds.cmddata.RegisterCmdData;
import com.derds.command.CreateGameCommand;
import com.derds.command.JoinGameCommand;
import com.derds.command.RefreshLobbyCommand;
import com.derds.command.RegisterCommand;
import com.derds.models.PlayerColor;
import com.derds.result.GameResult;
import com.derds.result.LoginResult;
import com.derds.result.RefreshLobbyResult;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class RefreshLobbyIntegrationTests {
    private static String authToken;
    private static List<String> gameIds;

    private Gson gson;

    /**
     * Registers user to use for the tests & saves their AuthToken
     */
    @BeforeClass
    public static void init() {
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername", "testPassword", "Luke", "Skywalker", "123firebase123");
        RegisterCommand registerCommand = new Gson().fromJson(new Gson().toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage() == null);
        assertTrue(result.getAuthToken() != null);

        authToken = result.getAuthToken();
    }

    @Before
    public void setUp() {
        gson = new Gson();
        gameIds = new ArrayList<>(); //list of gameIds to delete
    }

    @After
    public void tearDown() {
        Database.getInstance().removeUser("testUsername2");
        for(String gameId : gameIds) {
            Database.getInstance().removeGame(gameId);
        }
    }

    @AfterClass
    public static void destroy() {
        Database.getInstance().removeUser("testUsername");
    }


    /**
     * Tests refreshing the lobby when there are no games to be returned
     */
    @Test
    public void testRefreshLobbyNoGames() {
        RefreshLobbyCommand refreshLobbyCommand = new RefreshLobbyCommand();
        refreshLobbyCommand.setAuthToken(authToken);
        RefreshLobbyResult result = refreshLobbyCommand.execute();

        assertTrue(result.getMessage() == null); //no errors
        assertTrue(result.getCurrentGames().size() == 0); //no current games
        assertTrue(result.getOpenGames().size() == 0); // no open games
        assertTrue(result.getPlayerGames().size() == 0); //no player games
    }

    /**
     * Test refreshing the lobby where there is an open game and no other types
     */
    @Test
    public void testRefreshOpenGames() {
        //register another user
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername2", "testPassword", "test", "tester", "testFirebase");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult registerResult = registerCommand.execute();
        assertTrue(registerResult.getMessage() == null); //registered another user
        assertTrue(registerResult.getAuthToken() != null); //got an authToken back
        String otherAuthToken = registerResult.getAuthToken();

        //have the other user make a game for the open games list
        CreateGameCmdData createGameCmdData = new CreateGameCmdData("otherGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(otherAuthToken);
        GameResult openGameCreateResult = createGameCommand.execute();
        assertTrue(openGameCreateResult.getMessage() == null); //game created
        assertTrue(openGameCreateResult.getGameID() != null); //got gameId back
        gameIds.add(openGameCreateResult.getGameID());

        //refresh the lobby
        RefreshLobbyCommand refreshLobbyCommand = new RefreshLobbyCommand();
        refreshLobbyCommand.setAuthToken(authToken);
        RefreshLobbyResult refreshLobbyResult = refreshLobbyCommand.execute();
        assertTrue(refreshLobbyResult.getMessage() == null); //no error on refresh
        assertTrue(refreshLobbyResult.getOpenGames().size() == 1); //1 open game
        assertTrue(refreshLobbyResult.getCurrentGames().size() == 0); //not in any games
        assertTrue(refreshLobbyResult.getPlayerGames().size() == 0); //owns no games
    }

    /**
     * Tests refreshing the lobby where there the player is in a game
     */
    @Test
    public void testRefreshCurrentGames() {
        //register another user
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername2", "testPassword", "test", "tester", "testFirebase");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult registerResult = registerCommand.execute();
        assertTrue(registerResult.getMessage() == null); //registered another user
        assertTrue(registerResult.getAuthToken() != null); //got an authToken back
        String otherAuthToken = registerResult.getAuthToken();

        //have the other user make a game for the original user to join
        CreateGameCmdData createGameCmdData2 = new CreateGameCmdData("otherGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand2 = gson.fromJson(gson.toJson(createGameCmdData2), CreateGameCommand.class);
        createGameCommand2.setAuthToken(otherAuthToken);
        GameResult joinGameCreateResult = createGameCommand2.execute();
        assertTrue(joinGameCreateResult.getMessage() == null); //game created
        assertTrue(joinGameCreateResult.getGameID() != null); //got gameId back
        String joinGameId = joinGameCreateResult.getGameID();
        gameIds.add(joinGameCreateResult.getGameID());

        //have the original user to join the other user's game for the current games list
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(joinGameId, PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(authToken);
        GameResult joinResult = joinGameCommand.execute();
        assertTrue(joinResult.getMessage() == null); //joined game
        assertTrue(joinResult.getGameID() != null); //got a game id back
        assertTrue(joinResult.getGameID().equals(joinGameId)); //joined the correct game

        //refresh the lobby
        RefreshLobbyCommand refreshLobbyCommand = new RefreshLobbyCommand();
        refreshLobbyCommand.setAuthToken(authToken);
        RefreshLobbyResult refreshLobbyResult = refreshLobbyCommand.execute();
        assertTrue(refreshLobbyResult.getMessage() == null); //no error on refresh
        assertTrue(refreshLobbyResult.getOpenGames().size() == 0); //no open games
        assertTrue(refreshLobbyResult.getCurrentGames().size() == 1); //game the user joined
        assertTrue(refreshLobbyResult.getPlayerGames().size() == 0); //user has no games
    }

    /**
     * Refreshes the lobby where the player owns a game
     */
    @Test
    public void testRefreshOwnedGames() {
        //have the user create their own game for the player games list
        CreateGameCmdData createOwnGameCmdData = new CreateGameCmdData("ownGame", PlayerColor.BLUE);
        CreateGameCommand createOwnGameCommand = gson.fromJson(gson.toJson(createOwnGameCmdData), CreateGameCommand.class);
        createOwnGameCommand.setAuthToken(authToken);
        GameResult createOwnGameResult = createOwnGameCommand.execute();
        assertTrue(createOwnGameResult.getMessage() == null); //created game
        assertTrue(createOwnGameResult.getGameID() != null); //got a game id back
        gameIds.add(createOwnGameResult.getGameID());

        //refresh the lobby
        RefreshLobbyCommand refreshLobbyCommand = new RefreshLobbyCommand();
        refreshLobbyCommand.setAuthToken(authToken);
        RefreshLobbyResult refreshLobbyResult = refreshLobbyCommand.execute();
        assertTrue(refreshLobbyResult.getMessage() == null); //no error on refresh
        assertTrue(refreshLobbyResult.getOpenGames().size() == 0); //no open games
        assertTrue(refreshLobbyResult.getCurrentGames().size() == 1); //their own game they were automatically placed in
        assertTrue(refreshLobbyResult.getPlayerGames().size() == 1); //game the user created

    }

    /**
     * Tests refreshing the lobby where there is at least 1 type of game to be found
     */
    @Test
    public void testRefreshAllTypesOfGames() {
        //register another user
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername2", "testPassword", "test", "tester", "testFirebase");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult registerResult = registerCommand.execute();
        assertTrue(registerResult.getMessage() == null); //registered another user
        assertTrue(registerResult.getAuthToken() != null); //got an authToken back
        String otherAuthToken = registerResult.getAuthToken();

        //have the other user make a game for the open games list
        CreateGameCmdData createGameCmdData = new CreateGameCmdData("otherGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(otherAuthToken);
        GameResult openGameCreateResult = createGameCommand.execute();
        assertTrue(openGameCreateResult.getMessage() == null); //game created
        assertTrue(openGameCreateResult.getGameID() != null); //got gameId back
        gameIds.add(openGameCreateResult.getGameID());

        //have the other user make another game for the original user to join
        CreateGameCmdData createGameCmdData2 = new CreateGameCmdData("otherOtherGame", PlayerColor.RED);
        CreateGameCommand createGameCommand2 = gson.fromJson(gson.toJson(createGameCmdData2), CreateGameCommand.class);
        createGameCommand2.setAuthToken(otherAuthToken);
        GameResult joinGameCreateResult = createGameCommand2.execute();
        assertTrue(joinGameCreateResult.getMessage() == null); //game created
        assertTrue(joinGameCreateResult.getGameID() != null); //got gameId back
        String joinGameId = joinGameCreateResult.getGameID();
        gameIds.add(joinGameCreateResult.getGameID());

        //have the original user to join the other user's second game for the current games list
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(joinGameId, PlayerColor.GREEN);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(authToken);
        GameResult joinResult = joinGameCommand.execute();
        assertTrue(joinResult.getMessage() == null); //joined game
        assertTrue(joinResult.getGameID() != null); //got a game id back
        assertTrue(joinResult.getGameID().equals(joinGameId)); //joined the correct game

        //have the original user create their own game for the player games list
        CreateGameCmdData createOwnGameCmdData = new CreateGameCmdData("ownGame", PlayerColor.GREEN);
        CreateGameCommand createOwnGameCommand = gson.fromJson(gson.toJson(createOwnGameCmdData), CreateGameCommand.class);
        createOwnGameCommand.setAuthToken(authToken);
        GameResult createOwnGameResult = createOwnGameCommand.execute();
        assertTrue(createOwnGameResult.getMessage() == null); //created game
        assertTrue(createOwnGameResult.getGameID() != null); //got a game id back
        gameIds.add(createOwnGameResult.getGameID());

        //refresh the lobby
        RefreshLobbyCommand refreshLobbyCommand = new RefreshLobbyCommand();
        refreshLobbyCommand.setAuthToken(authToken);
        RefreshLobbyResult refreshLobbyResult = refreshLobbyCommand.execute();
        assertTrue(refreshLobbyResult.getMessage() == null); //no error on refresh
        assertTrue(refreshLobbyResult.getOpenGames().size() == 1); //other user has 1 game the original user isn't in
        assertTrue(refreshLobbyResult.getCurrentGames().size() == 2); //game the user joined + their own game
        assertTrue(refreshLobbyResult.getPlayerGames().size() == 1); //game the user created
    }

    /**
     * Tests refreshing the lobby with an invalid authToken
     */
    @Test
    public void testRefreshLobbyInvalidAuthToken() {
        RefreshLobbyCommand refreshLobbyCommand = new RefreshLobbyCommand();
        refreshLobbyCommand.setAuthToken("fakeAuth");
        RefreshLobbyResult result = refreshLobbyCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //no errors
        assertTrue(result.getCurrentGames() == null); //no current games
        assertTrue(result.getOpenGames() == null); // no open games
        assertTrue(result.getPlayerGames() == null); //no player games
    }

    /**
     * Tests refreshing the lobby with a null authToken
     */
    @Test
    public void testRefreshLobbyNullAuthToken() {
        RefreshLobbyCommand refreshLobbyCommand = new RefreshLobbyCommand();
        refreshLobbyCommand.setAuthToken(null);
        RefreshLobbyResult result = refreshLobbyCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //no errors
        assertTrue(result.getCurrentGames() == null); //no current games
        assertTrue(result.getOpenGames() == null); // no open games
        assertTrue(result.getPlayerGames() == null); //no player games
    }

    /**
     * Tests refreshing the lobby with an empty string as an authToken
     */
    @Test
    public void testRefreshLobbyEmptyAuthToken() {
        RefreshLobbyCommand refreshLobbyCommand = new RefreshLobbyCommand();
        refreshLobbyCommand.setAuthToken("");
        RefreshLobbyResult result = refreshLobbyCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //no errors
        assertTrue(result.getCurrentGames() == null); //no current games
        assertTrue(result.getOpenGames() == null); // no open games
        assertTrue(result.getPlayerGames() == null); //no player games
    }
}
