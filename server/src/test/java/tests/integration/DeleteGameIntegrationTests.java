package tests.integration;

import com.derds.Database;
import com.derds.Strings;
import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.DeleteGameCmdData;
import com.derds.cmddata.RegisterCmdData;
import com.derds.command.CreateGameCommand;
import com.derds.command.DeleteGameCommand;
import com.derds.command.RegisterCommand;
import com.derds.models.Player;
import com.derds.models.PlayerColor;
import com.derds.result.GameResult;
import com.derds.result.LoginResult;
import com.derds.result.Result;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class DeleteGameIntegrationTests {

    private static String authToken;
    private static String gameId;
    private static List<String> gameIds;

    private Gson gson;

    /**
     * Registers user to use for the tests & saves their AuthToken
     */
    @BeforeClass
    public static void init() {
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername", "testPassword", "Luke", "Skywalker", "123firebase123");
        RegisterCommand registerCommand = new Gson().fromJson(new Gson().toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage() == null);
        assertTrue(result.getAuthToken() != null);

        authToken = result.getAuthToken();
    }

    /**
     * Creates a game & saves the gameId; creates a list for the test gameIds to collect for deletion
     */
    @Before
    public void setUp() {
        gson = new Gson();
        gameIds = new ArrayList<>();

        CreateGameCmdData createGameCmdData = new CreateGameCmdData("myGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(authToken);
        GameResult gameResult = createGameCommand.execute();

        assertTrue(gameResult.getMessage() == null);
        assertTrue(gameResult.getGameID() != null);

        gameId = gameResult.getGameID();
        gameIds.add(gameId);
    }

    /**
     * Deletes the test games
     */
    @After
    public void tearDown() {
        Database.getInstance().removeUser("testUsername2");
        for(String gameId : gameIds) {
            Database.getInstance().removeGame(gameId);
        }
    }

    @AfterClass
    public static void destroy() {
        Database.getInstance().removeUser("testUsername");
    }

    /**
     * Tests deleting a valid game
     */
    @Test
    public void testDeleteGame() {
        DeleteGameCmdData deleteGameCmdData = new DeleteGameCmdData(gameId);
        DeleteGameCommand deleteGameCommand = gson.fromJson(gson.toJson(deleteGameCmdData), DeleteGameCommand.class);
        deleteGameCommand.setAuthToken(authToken);
        Result result = deleteGameCommand.execute();

        assertTrue(result.getMessage() == null); //no error
    }

    /**
     * Tests deleting a game that doesn't exist
     */
    @Test
    public void testDeleteInvalidGame() {
        DeleteGameCmdData deleteGameCmdData = new DeleteGameCmdData("fakeGame");
        DeleteGameCommand deleteGameCommand = gson.fromJson(gson.toJson(deleteGameCmdData), DeleteGameCommand.class);
        deleteGameCommand.setAuthToken(authToken);
        Result result = deleteGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.GAME_NOT_FOUND)); //correct error message
    }

    /**
     * Tests deleting a game where the gameId null
     */
    @Test
    public void testDeleteGameNullGameId() {
        DeleteGameCmdData deleteGameCmdData = new DeleteGameCmdData(null);
        DeleteGameCommand deleteGameCommand = gson.fromJson(gson.toJson(deleteGameCmdData), DeleteGameCommand.class);
        deleteGameCommand.setAuthToken(authToken);
        Result result = deleteGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.GAME_NOT_FOUND)); //correct error message
    }

    /**
     * Tests deleting a game where the gameId is an empty string
     */
    @Test
    public void testDeleteGameEmptyGameId() {
        DeleteGameCmdData deleteGameCmdData = new DeleteGameCmdData("");
        DeleteGameCommand deleteGameCommand = gson.fromJson(gson.toJson(deleteGameCmdData), DeleteGameCommand.class);
        deleteGameCommand.setAuthToken(authToken);
        Result result = deleteGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.GAME_NOT_FOUND)); //correct error message
    }

    /**
     * Tests deleting a game where the authToken is null
     */
    @Test
    public void testDeleteGameNullAuthToken() {
        DeleteGameCmdData deleteGameCmdData = new DeleteGameCmdData(gameId);
        DeleteGameCommand deleteGameCommand = gson.fromJson(gson.toJson(deleteGameCmdData), DeleteGameCommand.class);
        deleteGameCommand.setAuthToken(null);
        Result result = deleteGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
    }

    /**
     * Tests deleting a game where the authToken is an empty string
     */
    @Test
    public void testDeleteGameEmptyAuthToken() {
        DeleteGameCmdData deleteGameCmdData = new DeleteGameCmdData(gameId);
        DeleteGameCommand deleteGameCommand = gson.fromJson(gson.toJson(deleteGameCmdData), DeleteGameCommand.class);
        deleteGameCommand.setAuthToken("");
        Result result = deleteGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
    }
}
