package tests.integration;

import com.derds.Database;
import com.derds.Strings;
import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.JoinGameCmdData;
import com.derds.cmddata.RegisterCmdData;
import com.derds.command.CreateGameCommand;
import com.derds.command.JoinGameCommand;
import com.derds.command.RegisterCommand;
import com.derds.models.PlayerColor;
import com.derds.result.GameResult;
import com.derds.result.LoginResult;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class JoinGameIntegrationTests {
    private Gson gson;
    private static String otherAuthToken;
    private static String otherGameId;
    private List<String> gameIds;

    /**
     * Registers user to use for the tests & saves their AuthToken
     */
    @BeforeClass
    public static void init() {
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername", "testPassword", "Han", "Solo", "123firebase123");
        RegisterCommand registerCommand = new Gson().fromJson(new Gson().toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage() == null); //no error
        assertTrue(result.getAuthToken() != null); //returned valid authToken

        otherAuthToken = result.getAuthToken();
    }

    /**
     * Creates a user & list of gameIds to delete
     */
    @Before
    public void setUp() {
        gson = new Gson();
        gameIds = new ArrayList<>();

        CreateGameCmdData createGameCmdData = new CreateGameCmdData("testGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(otherAuthToken);
        GameResult gameResult = createGameCommand.execute();

        assertTrue(gameResult.getMessage() == null); //no error
        assertTrue(gameResult.getGameID() != null); //return valid gameId

        otherGameId = gameResult.getGameID();
        gameIds.add(gameResult.getGameID());
    }

    /**
     * Delete test games
     */
    @After
    public void tearDown() {
        Database.getInstance().removeUser("testUsername2");
        for(String gameId : gameIds) {
            Database.getInstance().removeGame(gameId);
        }
    }

    @AfterClass
    public static void destroy() {
        Database.getInstance().removeUser("testUsername");
        Database.getInstance().removeUser("testUsername3");
        Database.getInstance().removeUser("testUsername4");
        Database.getInstance().removeUser("testUsername5");
        Database.getInstance().removeUser("testUsername6");
    }


    /**
     * Tests joining a someone else's valid game
     */
    @Test
    public void testJoinGame() {
        //register a new user
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername2", "testPassword", "Luke", "Skywalker", "123firebase123");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult loginResult = registerCommand.execute();

        assertTrue(loginResult.getMessage() == null); //no error
        assertTrue(loginResult.getAuthToken() != null); //returned valid authToken

        //join a game using the new authToken
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(otherGameId, PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(loginResult.getAuthToken());
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage() == null); //no error
        assertTrue(result.getGameID() != null); //returned valid gameId
        assertTrue(result.getGameID().equals(otherGameId)); //joined the correct game
    }

    /**
     * Tests joining a game that the player made; should return an error since the user
     * automatically joins on game creation
     */
    @Test
    public void testJoinOwnGame() {
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(otherGameId, PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(otherAuthToken);
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.ALREADY_IN_GAME)); //correct error message
        assertTrue(result.getGameID() == null); //no gameId on error
    }

    /**
     * Tries joining the same game twice in a row
     */
    @Test
    public void testJoinGameTwice() {
        //register a new user
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername2", "testPassword", "Luke", "Skywalker", "123firebase123");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult loginResult = registerCommand.execute();

        assertTrue(loginResult.getMessage() == null); //no error
        assertTrue(loginResult.getAuthToken() != null); //returned valid authToken

        //join a game using the new authToken
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(otherGameId, PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(loginResult.getAuthToken());
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage() == null); //no error joining first time
        assertTrue(result.getGameID() != null); //returned valid gameId

        //join the game again with that same new authToken
        JoinGameCmdData joinGameCmdData1 = new JoinGameCmdData(otherGameId, PlayerColor.RED);
        JoinGameCommand joinGameCommand1 = gson.fromJson(gson.toJson(joinGameCmdData1), JoinGameCommand.class);
        joinGameCommand1.setAuthToken(loginResult.getAuthToken());
        GameResult result1 = joinGameCommand1.execute();

        assertTrue(result1.getMessage().equals(Strings.ALREADY_IN_GAME)); //correct error message
        assertTrue(result1.getGameID() == null); //no gameId on error
    }

    /**
     * Tests joining a game that doesn't exist
     */
    @Test
    public void testJoinInvalidGame() {
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData("fakeGame", PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(otherAuthToken);
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.GAME_NOT_FOUND)); //correct error message
        assertTrue(result.getGameID() == null); //no gameId on error
    }

    /**
     * Tests joining a game with an authToken that doesn't exist
     */
    @Test
    public void testJoinGameInvalidAuthToken() {
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(otherGameId, PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken("fakeAuthToken");
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
        assertTrue(result.getGameID() == null); //no gameId on error
    }

    /**
     * Tests joining a game with a null gameId
     */
    @Test
    public void testJoinGameNullGameId() {
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(null, PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(otherAuthToken);
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.GAME_NOT_FOUND));
        assertTrue(result.getGameID() == null); //no gameId on error
    }

    /**
     * Tests joining a game with an empty string for a gameId
     */
    @Test
    public void testJoingGameEmptyGameId() {
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData("", PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(otherAuthToken);
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.GAME_NOT_FOUND));
        assertTrue(result.getGameID() == null); //no gameId on error
    }

    /**
     * Tests joining a game with a null authToken
     */
    @Test
    public void testJoinGameNullAuthToken() {
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(otherGameId, PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(null);
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND));
        assertTrue(result.getGameID() == null); //no gameId on error
    }

    /**
     * Tests joining a game with an empty string for an authToken
     */
    @Test
    public void testJoinGameEmptyAuthToken() {
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(otherGameId, PlayerColor.RED);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken("");
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND));
        assertTrue(result.getGameID() == null); //no gameId on error
    }

    /**
     * Tests joining a game with a null player color
     */
    @Test
    public void testJoinGameNullPlayerColor() {
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(otherGameId, null);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(otherAuthToken);
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_PLAYER_COLOR)); //correct error message
        assertTrue(result.getGameID() == null); //no gameId on error
    }

    /**
     * Tests joining a game with a player color that is already in use
     */
    @Test
    public void testJoinGameTakenPlayerColor() {
        //register a new user
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername2", "testPassword", "Luke", "Skywalker", "123firebase123");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult loginResult = registerCommand.execute();

        assertTrue(loginResult.getMessage() == null); //no error
        assertTrue(loginResult.getAuthToken() != null); //returned valid authToken

        //join a game using the new authToken
        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(otherGameId, PlayerColor.BLUE);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(loginResult.getAuthToken());
        GameResult result = joinGameCommand.execute();

        assertTrue(result.getMessage().equals(Strings.PLAYER_COLOR_TAKEN)); //correct error message
        assertTrue(result.getGameID() == null); //no gameId on error
    }

    /**
     * Tests joining a game that is full
     */
    @Test
    public void testJoinFullGame() {
        //register & join 4 more players to an existing game
        //putting these on one line to save space; ignore the long ugly lines

        //1st player created the game
        //2nd player
        LoginResult loginResult1 = gson.fromJson(gson.toJson(new RegisterCmdData("testUsername2", "testPassword", "test", "tester", "testFirebase")), RegisterCommand.class).execute();
        assertTrue(loginResult1.getMessage() == null);
        JoinGameCommand joinGameCommand1 = gson.fromJson(gson.toJson(new JoinGameCmdData(otherGameId, PlayerColor.RED)), JoinGameCommand.class);
        joinGameCommand1.setAuthToken(loginResult1.getAuthToken());
        GameResult gameResult1 = joinGameCommand1.execute();
        assertTrue(gameResult1.getMessage() == null);
        //3rd player
        LoginResult loginResult2 = gson.fromJson(gson.toJson(new RegisterCmdData("testUsername3", "testPassword", "test", "tester", "testFirebase")), RegisterCommand.class).execute();
        assertTrue(loginResult2.getMessage() == null);
        JoinGameCommand joinGameCommand2 = gson.fromJson(gson.toJson(new JoinGameCmdData(otherGameId, PlayerColor.GREEN)), JoinGameCommand.class);
        joinGameCommand2.setAuthToken(loginResult2.getAuthToken());
        GameResult gameResult2 = joinGameCommand2.execute();
        assertTrue(gameResult2.getMessage() == null);
        //4th player
        LoginResult loginResult3 = gson.fromJson(gson.toJson(new RegisterCmdData("testUsername4", "testPassword", "test", "tester", "testFirebase")), RegisterCommand.class).execute();
        assertTrue(loginResult3.getMessage() == null);
        JoinGameCommand joinGameCommand3 = gson.fromJson(gson.toJson(new JoinGameCmdData(otherGameId, PlayerColor.CYAN)), JoinGameCommand.class);
        joinGameCommand3.setAuthToken(loginResult3.getAuthToken());
        GameResult gameResult3 = joinGameCommand3.execute();
        assertTrue(gameResult3.getMessage() == null);
        //5th player
        LoginResult loginResult4 = gson.fromJson(gson.toJson(new RegisterCmdData("testUsername5", "testPassword", "test", "tester", "testFirebase")), RegisterCommand.class).execute();
        assertTrue(loginResult4.getMessage() == null);
        JoinGameCommand joinGameCommand4 = gson.fromJson(gson.toJson(new JoinGameCmdData(otherGameId, PlayerColor.YELLOW)), JoinGameCommand.class);
        joinGameCommand4.setAuthToken(loginResult4.getAuthToken());
        GameResult gameResult4 = joinGameCommand4.execute();
        assertTrue(gameResult4.getMessage() == null);

        ///////////////////////////////////
        //game has 5 players now
        //register a 6th user & try to join
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername6", "testPassword", "test", "tester", "testFirebase");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult registerResult = registerCommand.execute();

        assertTrue(registerResult.getMessage() == null); //no error to register
        assertTrue(registerResult.getAuthToken() != null); //received authToken
        assertTrue(registerResult.getUsername().equals("testUsername6")); //correct username

        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(otherGameId, PlayerColor.BLUE);
        JoinGameCommand joinGameCommand6 = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand6.setAuthToken(registerResult.getAuthToken()); //put the 6th player's token in
        GameResult fullGameJoinResult = joinGameCommand6.execute();

        assertTrue(fullGameJoinResult.getMessage().equals(Strings.TOO_MANY_PLAYERS)); //correct error message
        assertTrue(fullGameJoinResult.getGameID() == null); //no gameId on error
    }
}
