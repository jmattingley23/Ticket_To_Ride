package tests.integration;

import com.derds.Database;
import com.derds.Strings;
import com.derds.cmddata.RegisterCmdData;
import com.derds.cmddata.UpdateFirebaseTokenCmdData;
import com.derds.command.RegisterCommand;
import com.derds.command.UpdateFirebaseTokenCommand;
import com.derds.result.LoginResult;
import com.derds.result.Result;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class UpdateFirebaseTokenIntegrationTests {
    private static String authToken;

    private Gson gson;

    /**
     * Registers user to use for the tests & saves their AuthToken
     */
    @BeforeClass
    public static void init() {
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername", "testPassword", "Luke", "Skywalker", "123firebase123");
        RegisterCommand registerCommand = new Gson().fromJson(new Gson().toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage() == null);
        assertTrue(result.getAuthToken() != null);

        authToken = result.getAuthToken();
    }

    @Before
    public void setUp() {
        gson = new Gson();
    }

    @After
    public void tearDown() {}

    @AfterClass
    public static void destroy() {
        Database.getInstance().removeUser("testUsername");
    }


    /**
     * Tests updating a valid firebaseToken for a valid authToken
     */
    @Test
    public void testUpdateFirebaseToken() {
        UpdateFirebaseTokenCmdData updateFirebaseTokenCmdData = new UpdateFirebaseTokenCmdData("testFirebaseToken");
        UpdateFirebaseTokenCommand updateFirebaseTokenCommand = gson.fromJson(gson.toJson(updateFirebaseTokenCmdData), UpdateFirebaseTokenCommand.class);
        updateFirebaseTokenCommand.setAuthToken(authToken);
        Result result = updateFirebaseTokenCommand.execute();

        assertTrue(result.getMessage() == null); //no error
    }

    /**
     * Tests updating a null firebaseToken
     */
    @Test
    public void testUpdateFirebaseTokenNullToken() {
        UpdateFirebaseTokenCmdData updateFirebaseTokenCmdData = new UpdateFirebaseTokenCmdData(null);
        UpdateFirebaseTokenCommand updateFirebaseTokenCommand = gson.fromJson(gson.toJson(updateFirebaseTokenCmdData), UpdateFirebaseTokenCommand.class);
        updateFirebaseTokenCommand.setAuthToken(authToken);
        Result result = updateFirebaseTokenCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_FIREBASE_TOKEN)); //correct error message
    }

    /**
     * Tests updating an empty string firebaseToken
     */
    @Test
    public void testUpdateFirebaseTokenEmptyToken() {
        UpdateFirebaseTokenCmdData updateFirebaseTokenCmdData = new UpdateFirebaseTokenCmdData("");
        UpdateFirebaseTokenCommand updateFirebaseTokenCommand = gson.fromJson(gson.toJson(updateFirebaseTokenCmdData), UpdateFirebaseTokenCommand.class);
        updateFirebaseTokenCommand.setAuthToken(authToken);
        Result result = updateFirebaseTokenCommand.execute();

        assertTrue(result.getMessage().equals(Strings.BAD_FIREBASE_TOKEN)); //correct error message
    }

    /*
     * Tests updating a firebaseToken to a null authToken
     */
    @Test
    public void testUpdateFirebaseTokenNullAuthToken() {
        UpdateFirebaseTokenCmdData updateFirebaseTokenCmdData = new UpdateFirebaseTokenCmdData("testFirebase");
        UpdateFirebaseTokenCommand updateFirebaseTokenCommand = gson.fromJson(gson.toJson(updateFirebaseTokenCmdData), UpdateFirebaseTokenCommand.class);
        updateFirebaseTokenCommand.setAuthToken(null);
        Result result = updateFirebaseTokenCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
    }

    /*
     * Tests updating a firebaseToken to an empty string authToken
     */
    @Test
    public void testUpdateFirebaseTokenEmptyAuthToken() {
        UpdateFirebaseTokenCmdData updateFirebaseTokenCmdData = new UpdateFirebaseTokenCmdData("testFirebase");
        UpdateFirebaseTokenCommand updateFirebaseTokenCommand = gson.fromJson(gson.toJson(updateFirebaseTokenCmdData), UpdateFirebaseTokenCommand.class);
        updateFirebaseTokenCommand.setAuthToken("");
        Result result = updateFirebaseTokenCommand.execute();

        assertTrue(result.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
    }
}
