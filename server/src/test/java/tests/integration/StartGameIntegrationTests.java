package tests.integration;

import com.derds.Database;
import com.derds.Strings;
import com.derds.cmddata.CreateGameCmdData;
import com.derds.cmddata.JoinGameCmdData;
import com.derds.cmddata.RegisterCmdData;
import com.derds.cmddata.StartGameCmdData;
import com.derds.command.CreateGameCommand;
import com.derds.command.JoinGameCommand;
import com.derds.command.RegisterCommand;
import com.derds.command.StartGameCommand;
import com.derds.models.PlayerColor;
import com.derds.result.GameResult;
import com.derds.result.LoginResult;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class StartGameIntegrationTests {

    private static String authToken;
    private static String gameId;
    private List<String> gameIds;

    private Gson gson;

    /**
     * Registers user to use for the tests & saves their AuthToken
     */
    @BeforeClass
    public static void init() {
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername", "testPassword", "Luke", "Skywalker", "123firebase123");
        RegisterCommand registerCommand = new Gson().fromJson(new Gson().toJson(registerCmdData), RegisterCommand.class);
        LoginResult result = registerCommand.execute();

        assertTrue(result.getMessage() == null);
        assertTrue(result.getAuthToken() != null);

        authToken = result.getAuthToken();
    }

    /**
     * Creates a game & saves the gameId; makes list to collect gameIds for deletion
     */
    @Before
    public void setUp() {
        gson = new Gson();
        gameIds = new ArrayList<>();

        CreateGameCmdData createGameCmdData = new CreateGameCmdData("myGame", PlayerColor.BLUE);
        CreateGameCommand createGameCommand = gson.fromJson(gson.toJson(createGameCmdData), CreateGameCommand.class);
        createGameCommand.setAuthToken(authToken);
        GameResult gameResult = createGameCommand.execute();

        assertTrue(gameResult.getMessage() == null);
        assertTrue(gameResult.getGameID() != null);

        gameId = gameResult.getGameID();
        gameIds.add(gameResult.getGameID());
    }

    /**
     * Delete test games
     */
    @After
    public void tearDown() {
        Database.getInstance().removeUser("testUsername2");
        for(String gameId : gameIds) {
            Database.getInstance().removeGame(gameId);
        }
    }

    /**
     * Delete test user
     */
    @AfterClass
    public static void destroy() {
        Database.getInstance().removeUser("testUsername");
    }


    /**
     * Tests starting a game that the player owns with good data
     */
    @Test
    public void testStartGame() {
        //register another user & put them in the game
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername2", "testPassword", "test", "tester", "testFirebase");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult registerResult = registerCommand.execute();
        assertTrue(registerResult.getMessage() == null); //registered with no errors
        assertTrue(registerResult.getAuthToken() != null); //got a valid authToken

        JoinGameCmdData joinGameCmdData = new JoinGameCmdData(gameId, PlayerColor.GREEN);
        JoinGameCommand joinGameCommand = gson.fromJson(gson.toJson(joinGameCmdData), JoinGameCommand.class);
        joinGameCommand.setAuthToken(registerResult.getAuthToken());
        GameResult joinResult = joinGameCommand.execute();
        assertTrue(joinResult.getMessage() == null); //joined with no errors
        assertTrue(joinResult.getGameID() != null); //got a valid gameId back

        StartGameCmdData startGameCmdData = new StartGameCmdData(gameId);
        StartGameCommand startGameCommand = gson.fromJson(gson.toJson(startGameCmdData), StartGameCommand.class);
        startGameCommand.setAuthToken(authToken); //authToken for game owner
        GameResult startResult = startGameCommand.execute();

        assertTrue(startResult.getMessage() == null); //started game with no errors
        assertTrue(startResult.getGameID() != null); //got a valid gameId back
    }

    /**
     * Tests starting a valid game that the user owns with less than 2 players
     */
    @Test
    public void testStartGameNotEnoughPlayers() {
        StartGameCmdData startGameCmdData = new StartGameCmdData(gameId);
        StartGameCommand startGameCommand = gson.fromJson(gson.toJson(startGameCmdData), StartGameCommand.class);
        startGameCommand.setAuthToken(authToken);
        GameResult startResult = startGameCommand.execute();

        assertTrue(startResult.getMessage().equals(Strings.NOT_ENOUGH_PLAYERS)); //correct error message
        assertTrue(startResult.getGameID() == null); //no gameId on error
    }

    /**
     * Tests trying to start a game that exists but that the user doesn't own; user also has a valid authToken
     */
    @Test
    public void testStartNotOwnedGame() {
        //register another user
        RegisterCmdData registerCmdData = new RegisterCmdData("testUsername2", "testPassword", "test", "tester", "testFirebase");
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult registerResult = registerCommand.execute();
        assertTrue(registerResult.getMessage() == null); //registered with no errors
        assertTrue(registerResult.getAuthToken() != null); //got a valid authToken

        StartGameCmdData startGameCmdData = new StartGameCmdData(gameId);
        StartGameCommand startGameCommand = gson.fromJson(gson.toJson(startGameCmdData), StartGameCommand.class);
        startGameCommand.setAuthToken(registerResult.getAuthToken()); //authToken for game owner
        GameResult startResult = startGameCommand.execute();

        assertTrue(startResult.getMessage().equals(Strings.GAME_NOT_OWNED)); //correct error message
        assertTrue(startResult.getGameID() == null); //no gameId on error
    }

    /**
     * Tests trying to start a game that doesn't exist
     */
    @Test
    public void testStartGameInvalidGameId() {
        StartGameCmdData startGameCmdData = new StartGameCmdData("fakeGameId");
        StartGameCommand startGameCommand = gson.fromJson(gson.toJson(startGameCmdData), StartGameCommand.class);
        startGameCommand.setAuthToken(authToken);
        GameResult startResult = startGameCommand.execute();

        assertTrue(startResult.getMessage().equals(Strings.GAME_NOT_FOUND)); //correct error message
        assertTrue(startResult.getGameID() == null); //no gameId on error
    }

    /**
     * Tests trying to start a game with a null gameId
     */
    @Test
    public void testStartGameNullGameId() {
        StartGameCmdData startGameCmdData = new StartGameCmdData(null);
        StartGameCommand startGameCommand = gson.fromJson(gson.toJson(startGameCmdData), StartGameCommand.class);
        startGameCommand.setAuthToken(authToken);
        GameResult startResult = startGameCommand.execute();

        assertTrue(startResult.getMessage().equals(Strings.GAME_NOT_FOUND)); //correct error message
        assertTrue(startResult.getGameID() == null); //no gameId on error
    }

    /**
     * Tests trying to start a game with an empty string for a gameId
     */
    @Test
    public void testStartGameEmptyGameId() {
        StartGameCmdData startGameCmdData = new StartGameCmdData("");
        StartGameCommand startGameCommand = gson.fromJson(gson.toJson(startGameCmdData), StartGameCommand.class);
        startGameCommand.setAuthToken(authToken);
        GameResult startResult = startGameCommand.execute();

        assertTrue(startResult.getMessage().equals(Strings.GAME_NOT_FOUND)); //correct error message
        assertTrue(startResult.getGameID() == null); //no gameId on error
    }

    /**
     * Tests trying to start a game with a null authToken
     */
    @Test
    public void testStartGameNullAuthToken() {
        StartGameCmdData startGameCmdData = new StartGameCmdData(gameId);
        StartGameCommand startGameCommand = gson.fromJson(gson.toJson(startGameCmdData), StartGameCommand.class);
        startGameCommand.setAuthToken(null);
        GameResult startResult = startGameCommand.execute();

        assertTrue(startResult.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
        assertTrue(startResult.getGameID() == null); //no gameId on error
    }

    /**
     * Tests trying to start a game with an empty for an authToken
     */
    @Test
    public void testStartGameEmptyAuthToken() {
        StartGameCmdData startGameCmdData = new StartGameCmdData(gameId);
        StartGameCommand startGameCommand = gson.fromJson(gson.toJson(startGameCmdData), StartGameCommand.class);
        startGameCommand.setAuthToken("");
        GameResult startResult = startGameCommand.execute();

        assertTrue(startResult.getMessage().equals(Strings.AUTH_NOT_FOUND)); //correct error message
        assertTrue(startResult.getGameID() == null); //no gameId on error
    }
}
