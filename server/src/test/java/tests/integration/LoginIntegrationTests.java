package tests.integration;

import com.derds.Database;
import com.derds.Strings;
import com.derds.cmddata.LoginCmdData;
import com.derds.cmddata.RegisterCmdData;
import com.derds.command.LoginCommand;
import com.derds.command.RegisterCommand;
import com.derds.result.LoginResult;
import com.google.gson.Gson;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LoginIntegrationTests {
    private Gson gson;

    private String testUsername = "testUsername";
    private String testPassword = "testPassword123";
    private String testFirstname = "Luke";
    private String testLastname = "Skywalker";
    private String testFirebaseToken = "123firebase123";

    @Before
    public void setUp() {
        gson = new Gson();
    }

    /**
     * Deletes the test user from the database to prevent username collisions
     */
    @After
    public void tearDown() {
        Database.getInstance().removeUser(testUsername);
    }


    /**
     * Tests a regular login with good data
     */
    @Test
    public void testGoodLogin() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, testFirstname, testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult registerResult = registerCommand.execute();

        assertTrue(registerResult.getMessage() == null); //register had no errors
        assertTrue(registerResult.getUsername().equals(testUsername)); //registered correct username
        assertTrue(registerResult.getAuthToken() != null); //got new authToken

        LoginCmdData loginCmdData = new LoginCmdData(testUsername, testPassword, testFirebaseToken);
        LoginCommand loginCommand = gson.fromJson(gson.toJson(loginCmdData), LoginCommand.class);
        LoginResult loginResult = loginCommand.execute();

        assertTrue(loginResult.getMessage() == null); //login had no errors
        assertTrue(loginResult.getUsername().equals(testUsername)); //logged in correct username
        assertTrue(loginResult.getAuthToken() != null); //got authToken back
        assertFalse(loginResult.getAuthToken().equals(registerResult.getAuthToken())); //got different authToken back
    }

    /**
     * Tests logging in for a username that doesn't exist
     */
    @Test
    public void testNonexistentLogin() {
        LoginCmdData loginCmdData = new LoginCmdData(testUsername, testPassword, testFirebaseToken);
        LoginCommand loginCommand = gson.fromJson(gson.toJson(loginCmdData), LoginCommand.class);
        LoginResult loginResult = loginCommand.execute();

        assertTrue(loginResult.getMessage().equals(Strings.BAD_LOGIN)); //correct error message
        assertTrue(loginResult.getAuthToken() == null); //bad logins shouldn't give authToken
    }

    /**
     * Tries logging in for a username that exists but with the wrong password
     */
    @Test
    public void testLoginWrongPassword() {
        RegisterCmdData registerCmdData = new RegisterCmdData(testUsername, testPassword, testFirstname, testLastname, testFirebaseToken);
        RegisterCommand registerCommand = gson.fromJson(gson.toJson(registerCmdData), RegisterCommand.class);
        LoginResult registerResult = registerCommand.execute();

        assertTrue(registerResult.getMessage() == null); //register had no errors
        assertTrue(registerResult.getUsername().equals(testUsername)); //registered correct username
        assertTrue(registerResult.getAuthToken() != null); //got new authToken

        LoginCmdData loginCmdData = new LoginCmdData(testUsername, "otherPassword456", testFirebaseToken);
        LoginCommand loginCommand = gson.fromJson(gson.toJson(loginCmdData), LoginCommand.class);
        LoginResult loginResult = loginCommand.execute();

        assertTrue(loginResult.getMessage().equals(Strings.BAD_LOGIN));
        assertTrue(loginResult.getAuthToken() == null); //bad logins shouldn't give authToken
    }

    /**
     * Tries logging in with a null username
     */
    @Test
    public void testLoginNullUsername() {
        LoginCmdData loginCmdData = new LoginCmdData(null, testPassword, testFirebaseToken);
        LoginCommand loginCommand = gson.fromJson(gson.toJson(loginCmdData), LoginCommand.class);
        LoginResult loginResult = loginCommand.execute();

        assertTrue(loginResult.getMessage().equals(Strings.BAD_USERNAME)); //correct error
        assertTrue(loginResult.getAuthToken() == null); //bad logins shouldn't give an authToken
    }

    /**
     * Tries logging in with a null password
     */
    @Test
    public void testLoginNullPassword() {
        LoginCmdData loginCmdData = new LoginCmdData(testUsername, null, testFirebaseToken);
        LoginCommand loginCommand = gson.fromJson(gson.toJson(loginCmdData), LoginCommand.class);
        LoginResult loginResult = loginCommand.execute();

        assertTrue(loginResult.getMessage().equals(Strings.BAD_PASSWORD)); //correct error
        assertTrue(loginResult.getAuthToken() == null); //bad logins shouldn't give an authToken
    }

    /**
     * Tries logging in with a null firebaseToken
     */
    @Test
    public void testLoginNullFirebaseToken() {
        LoginCmdData loginCmdData = new LoginCmdData(testUsername, testPassword, null);
        LoginCommand loginCommand = gson.fromJson(gson.toJson(loginCmdData), LoginCommand.class);
        LoginResult loginResult = loginCommand.execute();

        assertTrue(loginResult.getMessage().equals(Strings.BAD_FIREBASE_TOKEN)); //correct error
        assertTrue(loginResult.getAuthToken() == null); //bad logins shouldn't give an authToken
    }

    /**
     * Tries logging in with an empty string as a username
     */
    @Test
    public void testLoginEmptyUsername() {
        LoginCmdData loginCmdData = new LoginCmdData("", testPassword, testFirebaseToken);
        LoginCommand loginCommand = gson.fromJson(gson.toJson(loginCmdData), LoginCommand.class);
        LoginResult loginResult = loginCommand.execute();

        assertTrue(loginResult.getMessage().equals(Strings.BAD_USERNAME)); //correct error
        assertTrue(loginResult.getAuthToken() == null); //bad logins shouldn't give an authToken
    }

    /**
     * Tries logging in with an empty string as a password
     */
    @Test
    public void testLoginEmptyPassword() {
        LoginCmdData loginCmdData = new LoginCmdData(testUsername, "", testFirebaseToken);
        LoginCommand loginCommand = gson.fromJson(gson.toJson(loginCmdData), LoginCommand.class);
        LoginResult loginResult = loginCommand.execute();

        assertTrue(loginResult.getMessage().equals(Strings.BAD_PASSWORD)); //correct error
        assertTrue(loginResult.getAuthToken() == null); //bad logins shouldn't give an authToken
    }

    /**
     * Tries logging in with an empty string as a firebaseToken
     */
    @Test
    public void testLoginEmptyFirebaseToken() {
        LoginCmdData loginCmdData = new LoginCmdData(testUsername, testPassword, "");
        LoginCommand loginCommand = gson.fromJson(gson.toJson(loginCmdData), LoginCommand.class);
        LoginResult loginResult = loginCommand.execute();

        assertTrue(loginResult.getMessage().equals(Strings.BAD_FIREBASE_TOKEN)); //correct error
        assertTrue(loginResult.getAuthToken() == null); //bad logins shouldn't give an authToken
    }
}
